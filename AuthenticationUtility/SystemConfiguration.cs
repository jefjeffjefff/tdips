﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public static class SystemConfiguration
    {
        //public static string Environment = "PRODUCTION";
        //public static string Uri = "https://tdips.shakeys.solutions"; 
        public static string DynamicsUri = "https://spavi-d365.operations.dynamics.com/";
        public static string ActiveDirectoryResource = "https://spavi-d365.operations.dynamics.com";


        //public static string Environment = "TESTING";
        //public static string Uri = "https://190.100.5.5:8085";
        //public static string DynamicsUri = "https://bmi-sm-uat.sandbox.operations.dynamics.com/";
        //public static string ActiveDirectoryResource = "https://bmi-sm-uat.sandbox.operations.dynamics.com";


        public static string Environment = "LOCAL";
        public static string Uri = "http://localhost:30662";
        //public static string DynamicsUri = "https://bmi-sm-uat.sandbox.operations.dynamics.com/";
        //public static string ActiveDirectoryResource = "https://bmi-sm-uat.sandbox.operations.dynamics.com";
    }
}
