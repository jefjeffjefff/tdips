﻿using System;
using TDIPS.Core.Models;

namespace AuthenticationUtility
{
    public partial class ClientConfiguration
    {
        public static ClientConfiguration Default { get { return ClientConfiguration.OneBox; } }

        public static ClientConfiguration OneBox = new ClientConfiguration()
        {
            UriString = SystemConfiguration.DynamicsUri,
            //UriString = "https://bmi-sm-uat.sandbox.operations.dynamics.com/",
            //UriString = "https://bmi-sm-trainingaos.sandbox.ax.dynamics.com/",
            //UriString = "https://bmi-sm-workshop.sandbox.operations.dynamics.com/",
            //UriString = "https://spavi-d365.operations.dynamics.com/",
            //UriString = "https://spav-sm-devtest1703b90ff9e74648devaos.cloudax.dynamics.com/",
            UserName = "bmims-ext@shakeys.biz",
            Password = "$h@k3y$d365@dm!n@",
            //UserName = "test.erp@shakeys.biz",
            //Password = "SP@v12017",
            ActiveDirectoryResource = SystemConfiguration.ActiveDirectoryResource,
            //ActiveDirectoryResource = "https://bmi-sm-uat.sandbox.operations.dynamics.com",
            //ActiveDirectoryResource = "https://bmi-sm-trainingaos.sandbox.ax.dynamics.com",
            //ActiveDirectoryResource = "https://bmi-sm-workshop.sandbox.operations.dynamics.com",
            //ActiveDirectoryResource = "https://spavi-d365.operations.dynamics.com",
            //ActiveDirectoryResource = "https://spav-sm-devtest1703b90ff9e74648devaos.cloudax.dynamics.com",
            ActiveDirectoryTenant = "https://login.windows.net/shakeys.biz/",
            ActiveDirectoryClientAppId = "3634ef5e-22d2-407f-badd-2b45a1be48a5",
            //ActiveDirectoryClientAppSecret = "zlcHqnRF8FU2A7+ix1W2W1Y73BnRO2U7/OgeZZXEdS8=",
            ActiveDirectoryClientAppSecret = "e2CJs7UtcfBbdZRBs1LS27XfK5va9AxZhbt4hJiaIaI=",
            TLSVersion = "",

        };

        public string TLSVersion { get; set; }
        public string UriString { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ActiveDirectoryResource { get; set; }
        public String ActiveDirectoryTenant { get; set; }
        public String ActiveDirectoryClientAppId { get; set; }
        public string ActiveDirectoryClientAppSecret { get; set; }
    }
}
