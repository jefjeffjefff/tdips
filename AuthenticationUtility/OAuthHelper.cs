﻿using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AuthenticationUtility
{
	public class OAuthHelper
	{
		/// <summary>
		/// The header to use for OAuth authentication.
		/// </summary>
		public const string OAuthHeader = "Authorization";

		/// <summary>
		/// Retrieves an authentication header from the service.
		/// </summary>
		/// <returns>The authentication header for the Web API call.</returns>
		/// 

		public static async Task<AuthenticationResult> GetAuthentication()
		{
			var tenantId = "xxx.onmicrosoft.com";
			var userName = "xxx@xxxx.onmicrosoft.com";
			var password = "xxxxxx";
			var clientId = "xxxxxxx";

			using (HttpClient client = new HttpClient())
			{
				var tokenEndpoint = $"https://login.windows.net/{tenantId}/oauth2/token";
				var accept = "application/json";

				client.DefaultRequestHeaders.Add("Accept", accept);
				string postBody = $"resource=https://graph.windows.net/&client_id={clientId}&grant_type=password&username={userName}&password={password}";
				using (HttpResponseMessage response = await client.PostAsync(tokenEndpoint, new StringContent(postBody, Encoding.UTF8, "application/x-www-form-urlencoded")))
				{
					if (response.IsSuccessStatusCode)
					{
						var jsonresult = JObject.Parse(await response.Content.ReadAsStringAsync());
						var token = (string)jsonresult["access_token"];
					}
				}
			}

			return null;
		}

		public static string GetAuthenticationHeader(bool useWebAppAuthentication = false)
		{
			string aadTenant = ClientConfiguration.Default.ActiveDirectoryTenant;
			string aadClientAppId = ClientConfiguration.Default.ActiveDirectoryClientAppId;
			string aadClientAppSecret = ClientConfiguration.Default.ActiveDirectoryClientAppSecret;
			string aadResource = ClientConfiguration.Default.ActiveDirectoryResource;

			AuthenticationContext authenticationContext = new AuthenticationContext(aadTenant, false);
			AuthenticationResult authenticationResult;

			if (useWebAppAuthentication)
			{
				if (string.IsNullOrEmpty(aadClientAppSecret))
				{
					Console.WriteLine("Please fill AAD application secret in ClientConfiguration if you choose authentication by the application.");
					throw new Exception("Failed OAuth by empty application secret.");
				}

				try
				{
					// OAuth through application by application id and application secret.
					var credential = new ClientCredential(aadClientAppId, aadClientAppSecret);
					authenticationResult = authenticationContext.AcquireTokenAsync(aadResource, credential).Result;
				}
				catch (Exception ex)
				{
					Console.WriteLine(string.Format("Failed to authenticate with AAD by application with exception {0} and the stack trace {1}", ex.ToString(), ex.StackTrace));
					throw new Exception("Failed to authenticate with AAD by application.");
				}
			}
			else
			{
				// OAuth through username and password.
				string username = ClientConfiguration.Default.UserName;
				string password = ClientConfiguration.Default.Password;

				if (string.IsNullOrEmpty(password))
				{
					Console.WriteLine("Please fill user password in ClientConfiguration if you choose authentication by the credential.");
					throw new Exception("Failed OAuth by empty password.");
				}

				try
				{
					// Get token object
					//var userCredential = new UserPasswordCredential(username, password); ;
					authenticationResult = null;
				}
				catch (Exception ex)
				{
					Console.WriteLine(string.Format("Failed to authenticate with AAD by the credential with exception {0} and the stack trace {1}", ex.ToString(), ex.StackTrace));
					throw new Exception("Failed to authenticate with AAD by the credential.");
				}
			}

			// Create and get JWT token
			return authenticationResult.CreateAuthorizationHeader();
		}
	}
}
