﻿$(document).ready(function () {
    $("#txtPassword").focus();
    $("#txtEmail").focus();
    

    if (localStorage.checkbox && localStorage.checkbox != '') {
        $('#cbxRemember').attr('checked', 'checked');
        if ($("#txtValidation").html() == undefined) {
            $('#txtEmail').val(localStorage.username);
            $('#txtPassword').val(localStorage.password);
        }

        if ($("#txtEmail").val() != "") {
            $("#txtPassword").focus();
        }

    } else {
        $('#rememberChkBox').removeAttr('checked');
        $('#txtUsername').val('');
        $('#txtPassword').val('');
    }
})

$("#frmLogin").submit(function () {
    if ($('#cbxRemember').is(":checked")) {
        localStorage.username = $('#txtEmail').val();
        localStorage.password = $('#txtPassword').val();
        localStorage.checkbox = $('#cbxRemember').val();
    }
});
