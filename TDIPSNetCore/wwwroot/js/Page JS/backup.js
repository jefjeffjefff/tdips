﻿var filepath = "";
var restoreOption = ''

$(document).ready(function () {
    var backupJson = [];
    $("#dtBackup").setCustomSearchInput();
    $.ajax({
        url: "/System/GetBackupFiles",
        success: function (data) {
            $.each(data, function (i, item) {

                backupJson.push({
                    backupDate: item
                })
            })

            var dtBackup = $('#dtBackup').DataTable({
                data: backupJson,
                "order": [[0, "desc"]],
                "columns": [
                    {
                        "data": null,
                        "title": "File Name/Date",
                        render: function (data, type, row) {
                            var details = data.backupDate.split("\\")[2].split(".")[0];
                            return details;
                        }
                    },
                    {
                        "data": null,
                        "title": "Action",
                        render: function (data, type, row) {
                            var details = "<a href='/System/DownloadBackup?filename=" + data.backupDate.split("\\")[2] + "'><button class='btn btn-sm btn-success'>Download</button></a>" +
                                "<button onclick=\"Restore('" + data.backupDate + "','SERVER')\" class='btn btn-primary btn-sm'>Restore</button";
                            return details;
                        }
                    },
                ],
                "initComplete": function () {
                    $("#dtBackup").setCustomSearch({
                        onchange: "true",
                        exemption: [1]
                    });
                }
            });
        }
    })

    $("#btnAcceptRestore").click(function () {
        if (restoreOption == 'SERVER') {
            $.ajax({
                url: "/Account/ValidateLoggedInAccount?password=" + $("#txtPassword").val(),
                success: function (data) {
                    if (data == "INVALID") {
                        swal({
                            title: "Invalid Password",
                            icon: "error",
                            text: "Please put the correct password."
                        })
                    } else {
                        HoldOn.open({
                            theme: "sk-circle",
                            message: "System is restoring now. It might take some time, please wait."
                        })

                        $.ajax({
                            url: "/System/Restore?filename=" + filepath.split(" ")[1],
                            success: function (data) {
                                if (data == "SUCCESS") {
                                    swal({
                                        title: "System Restoration Complete",
                                        icon: "success",
                                        text: "System is restored successfully. Please refresh all of your tabs now."
                                    }).then(function () {
                                        document.location.reload();
                                    });
                                } else {
                                    swal({
                                        title: "Restoration Failed",
                                        icon: "error",
                                        text: "There is an error processing your request."
                                    })

                                    console.log(data);
                                }
                            }
                        })
                    }
                }
            })
        } else if (restoreOption == 'UPLOAD') {
             
                $.ajax({
                    url: "/Account/ValidateLoggedInAccount?password=" + $("#txtPassword").val(),
                    success: function (data) {
                        if (data == "INVALID") {
                            swal({
                                title: "Invalid Password",
                                icon: "error",
                                text: "Please put the correct password."
                            })
                        } else {
                            HoldOn.open({
                                theme: "sk-circle",
                                message: "System is restoring now. It might take some time, please wait."
                            })
                            var file = new FormData($("#frmUpload")[0]);

                            $.ajax({
                                url: "/System/RestoreFromUpload",
                                type: 'POST',
                                data: file,
                                contentType: false,
                                processData: false,
                                success: function (data) {
                                    HoldOn.close();
                                    if (data == "SUCCESS") {
                                        swal({
                                            title: "System Restoration Complete",
                                            icon: "success",
                                            text: "System is restored successfully. Please refresh all of your tabs now."
                                        }).then(function () {
                                            document.location.reload();
                                        });
                                    } else {
                                        swal({
                                            title: "Restoration Failed",
                                            icon: "error",
                                            text: "There is an error processing your request."
                                        })

                                        console.log(data);
                                    }
                                },
                                error: function () {
                                    HoldOn.close();
                                    swal({
                                        title: "There is an error processing your request to the server.",
                                        icon: "error",
                                    });
                                }
                            });
                            
                        }
                    }
                })
        }
    })

    $("#btnUpload").click(function () {
        if ($("#frmUpload")[0].checkValidity() == false) {
            $("#frmUpload")[0].reportValidity()
        } else {
            swal({
                title: "System Restore Confirmation",
                text: "You are about to restore your system database from your upload. This process will completely overwrite your current data and all changes made since you last database backup will be lost." +
                    " Are you sure that you want to restore the database?",
                icon: "warning",
                buttons: true,
            })
                .then((ok) => {
                    restoreOption = "UPLOAD";

                    if (ok) {
                        $("#modalDialog").modal();
                    }
                })
        }
    })
})

function Restore(path, restoreOption) {
    swal({
        title: "System Restore Confirmation",
        text: "You are about to restore your system database from " + path.split(" ")[1].split(".")[0] +
            ". This process will completely overwrite your current data and all changes made since you last database backup will be lost." +
            " Are you sure that you want to restore the database?",
        icon: "warning",
        buttons: true,
    })
        .then((ok) => {
            filepath = path

            if (ok) {
                $("#modalDialog").modal();
            }
        })
}