﻿var hasError = false;
var onlinePayments = [];
var test;

$(document).ready(function () {
    LoadTable();
    
    $("#dtrSearch").daterangepicker()

    $("#btnSearch").click(function () {
        LoadTable();
    })

    $("#btnEOD").click(function () {
        LoadEOD()
    })

    $("#btnUploadExcel").click(function () {
        if ($("#frmUploadExcel")[0].checkValidity() == false) {
            if (isIE || isEdge) {
                swal({
                    title: "Please select an excel file first before importing.",
                    icon: "error"
                })
            } else if (isChrome) {
                $("#frmUploadExcel")[0].reportValidity()
            }
        } else {
            HoldOn.open({
                theme: "sk-circle",
                message: "Uploading an excel file.."
            })

            var excelFile = new FormData($("#frmUploadExcel")[0]);

            $.ajax({
                url: "/OnlinePayment/ImportOtherOnlinePayment",
                type: 'POST',
                data: excelFile,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data == "NULL") {
                        HoldOn.close();

                        swal({
                            title: "Please select an excel file first before importing.",
                            icon: "error"
                        })
                    } else if (data == "INVALID FILE") {
                        HoldOn.close();

                        swal({
                            title: "Invalid excel file.",
                            icon: "error"
                        })
                    } else if (data == "EMPTY") {
                        HoldOn.close();

                        swal({
                            title: "Excel file seems empty or in different format.",
                            icon: "error"
                        })
                    } else {
                        if (data.length == 0) {
                            HoldOn.close();

                            swal({
                                title: "Excel file seems empty or in different format.",
                                icon: "error"
                            })
                        } else {
                            $("#txtTransactionType").attr("disabled", false).val("")
                            $("#txtEntity").attr("disabled", false).val("")
                            $("#txtDescription").attr("disabled", false).val("")

                            onlinePayments = [];
                            hasError = false;

                            let rows = "";

                            $.each(data, function (i, item) {
                                let payment = item.OtherOnlinePayment || {};
                                let transDate = parseJsonDate(payment.transDate);
                                let accountHtml = "";
                                let account2Html = "";

                                accountHtml = `<div class='account-details' style='margin-top:10px;font-size:13px;'>
                                    <div><text>Stores:</text> ${payment.stores}</div>
                                    <div><text>Dept:</text> ${payment.department}</div>
                                    <div><text>Bus Unit:</text> ${payment.businessUnit}</div>
                                    <div><text>Purpose:</text> ${payment.purpose}</div>
                                    <div><text>Sales Segment:</text> ${payment.salesSegment}</div>
                                    <div><text>Worker:</text> ${payment.worker}</div>
                                    <div><text>Cost Center:</text> ${payment.costCenter}</div>
                                    </div>`

                                account2Html = `<div class='account-details' style='margin-top:10px;font-size:13px;'>
                                    <div><text>Stores:</text> ${payment.oStores}</div>
                                    <div><text>Dept:</text> ${payment.oDepartment}</div>
                                    <div><text>Bus Unit:</text> ${payment.oBusinessUnit}</div>
                                    <div><text>Purpose:</text> ${payment.purpose}</div>
                                    <div><text>Sales Segment:</text> ${payment.oSalesSegment}</div>
                                    <div><text>Worker:</text> ${payment.oWorker}</div>
                                    <div><text>Cost Center:</text> ${payment.oCostCenter}</div>
                                    </div>`

                                //if (payment.accountType.toUpperCase() == "LEDGER") {
                                    
                                //}

                                //if (payment.offsetAccountType.toUpperCase() == "LEDGER") {
                                    
                                //}

                                if (transDate != "") {
                                    transDate = formatDate(transDate, "MM-dd-yyyy");
                                    payment.transDate = transDate;
                                }

                                if (item.Status == "ERROR") {
                                    hasError = true;
                                }

                                onlinePayments.push(payment);

                                rows += `<tr>
                                        <td>${item.LineNumber}</td>
                                        <td>${transDate}</td>
                                        <td>${payment.description || ""}</td>
                                        <td>${((payment.accountType || "").toUpperCase())}</td>
                                        <td>${((payment.account || "Not Set") + (payment.account ? accountHtml : ""))}</td>
                                        <td>${parseMoney(payment.debit)}</td>
                                        <td>${parseMoney(payment.credit)}</td>
                                        <td>${((payment.offsetAccountType || "").toUpperCase())}</td>
                                        <td>${((payment.offsetAccount || "Not Set") + (payment.offsetAccount ? account2Html : ""))}</td>
                                        <td>
                                        <div style='text-transform:capitalize'><text style='display:block;margin-bottom:10px;' class='status'>
                                        ${(item.Status == "SUCCESS" ? ("<span class='label label-success'>" + item.Status.toLowerCase() +
                                        "</span>") : ("<span class='label label-danger'>" + item.Status.toLowerCase() + "</span>"))}
                                        </text> </div>
                                        <div>${item.Message.length > 0 ? (item.Message.join(" ")) : ""}</div>
                                        </td>                    
                                        </tr>`
                            })

                            $("#dtViewOP").DataTable().destroy();
                            $("#dtViewOP").setCustomSearchInput();
                            $("#dtViewOP tbody").html(rows);
                            $("#btnVerify").css("display", "unset");

                            $("#dtViewOP").DataTable({
                                initComplete: function () {
                                    $("#dtViewOP td:nth-child(1), #dtViewOP th:nth-child(1)," +
                                        "#dtViewOP td:nth-child(10), #dtViewOP th:nth-child(10)")
                                        .css("display", "table-cell");

                                    $("#dtViewOP").setCustomSearch({
                                    });
                                    HoldOn.close();
                                    $("#mdlViewOP").modal();
                                }
                            });
                        }
                    }
                },
                error: function () {
                    HoldOn.close();
                    swal({
                        title: "There is an error processing your request to the server.",
                        icon: "error",
                    });
                }
            });
        }
    })

    $("#btnVerify").click(function () {
        let entity = $("#txtEntity").val();
        let transactionType = $("#txtTransactionType").val();
        let description = $("#txtDescription").val();

        if (hasError == true) {
            swal({
                icon: "error",
                title: "Oops",
                text: "Please fix the error/s first before submitting the online payments."
            })
        } else if (onlinePayments.length == 0) {
            swal({
                icon: "error",
                title: "Oops",
                text: "Please upload the online payments excel first."
            })
        } else if (entity == null || transactionType == null || description == "") {
            swal({
                icon: "error",
                title: "Oops",
                text: "Please set the transaction type, entity and description first."
            })
        } else {
            swal({
                title: "Are you sure?",
                text: "Are you sure that you want to submit the records now?",
                icon: "warning",
                buttons: true
            })
                .then((yes) => {
                    if (yes) {
                        HoldOn.open({
                            theme: "sk-circle",
                            message: "Submitting your request."
                        })

                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            contentType: "application/json",
                            data: JSON.stringify({
                                batch: {
                                    entity: entity,
                                    description: description,
                                    transactionType: transactionType
                                },
                                paymentArr: onlinePayments
                            }),
                            url: "/OnlinePayment/PostOtherOnlinePayment",
                            success: function (data) {
                                HoldOn.close();
                                if (data.Status == "SUCCESS") {
                                    swal({
                                        title: "Success",
                                        text: "Your entry is successfully posted to D365 ("+ data.Message +")",
                                        icon: "success"
                                    }).then(function () {
                                        document.location.reload();
                                    });
                                } else {
                                    swal({
                                        title: "Oops",
                                        icon: "error",
                                        text: data.Message
                                    })
                                }
                            }
                        })
                    }
                });
        }
    })
    
    $("#btnVerifyEOD").click(function () {
        if ($(this).attr("validated") == "false") {
            swal({
                title: "Confirmation",
                text: "Are you sure that you want to verify and send report now? This can't be undone once confirmed.",
                icon: "warning",
                buttons: true
            })
                .then((ok) => {
                    if (ok) {
                        SendValidation();
                    }
                })
        } else {
            swal({
                title: "Validation Resending Confirmation",
                text: "Validation email is already sent as of " + $(this).attr("time") + ". Do you still want to continue and resend " +
                    "the report now?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((ok) => {
                    if (ok) {
                        SendValidation();
                    }
                })
        }
    })
})


function SendValidation() {
    HoldOn.open({
        theme: "sk-circle",
        message: "Processing your request.."
    })

    $.ajax({
        url: "/OnlinePayment/SendOtherOnlinePaymentEODMail",
        success: function (data) {
            if (data == "SUCCESS") {
                swal({
                    title: "Sending Successful.",
                    text: "Report is sent successfully.",
                    icon: "success"
                }).then(function () {
                    document.location.reload();
                });
            } else {
                swal({
                    title: "Sending Failed",
                    text: "There is an error processing your request.",
                    icon: "error"
                })
            }
        }
    })
}

function ViewOnlinePayments(batch) {
    HoldOn.open({
        theme: "sk-circle"
    })

    $("#txtTransactionType").attr("disabled", "true").val(batch.transactionType);
    $("#txtEntity").attr("disabled", "true").val(batch.entity);
    $("#txtDescription").attr("disabled", "true").val(batch.description);

    $("#dtViewOP").DataTable().destroy();

    $.ajax({
        url: "/OnlinePayment/GetOtherOnlinePayments?batchId=" + batch.batchId,
        success: function (data) {
            let rows = "";

            $.each(data, function (i, item) {
                let payment = item;
                let transDate = parseJsonDate(payment.transDate);
                let accountHtml = "";
                let account2Html = "";

                accountHtml = `<div class='account-details' style='margin-top:10px;font-size:13px;'>
                                    <div><text>Stores:</text> ${payment.stores}</div>
                                    <div><text>Dept:</text> ${payment.department}</div>
                                    <div><text>Bus Unit:</text> ${payment.businessUnit}</div>
                                    <div><text>Purpose:</text> ${payment.purpose}</div>
                                    <div><text>Sales Segment:</text> ${payment.salesSegment}</div>
                                    <div><text>Worker:</text> ${payment.worker}</div>
                                    <div><text>Cost Center:</text> ${payment.costCenter}</div>
                                    </div>`

                account2Html = `<div class='account-details' style='margin-top:10px;font-size:13px;'>
                                    <div><text>Stores:</text> ${payment.oStores}</div>
                                    <div><text>Dept:</text> ${payment.oDepartment}</div>
                                    <div><text>Bus Unit:</text> ${payment.oBusinessUnit}</div>
                                    <div><text>Purpose:</text> ${payment.purpose}</div>
                                    <div><text>Sales Segment:</text> ${payment.oSalesSegment}</div>
                                    <div><text>Worker:</text> ${payment.oWorker}</div>
                                    <div><text>Cost Center:</text> ${payment.oCostCenter}</div>
                                    </div>`

                if (transDate != "") {
                    transDate = formatDate(transDate, "MM-dd-yyyy");
                    payment.transDate = transDate;
                }

                if (item.Status == "ERROR") {
                    hasError = true;
                }

                rows += `<tr>
                                        <td></td>
                                        <td>${transDate}</td>
                                        <td>${payment.description}</td>
                                        <td>${((payment.accountType || "").toUpperCase())}</td>
                                        <td>${(payment.account + accountHtml)}</td>
                                        <td>${parseMoney(payment.debit)}</td>
                                        <td>${parseMoney(payment.credit)}</td>
                                        <td>${((payment.offsetAccountType || "").toUpperCase())}</td>
                                        <td>${(payment.offsetAccount + account2Html)}</td>
                                        <td>
                                        </td>                    
                                        </tr>`
            })

            $("#dtViewOP").DataTable().destroy();
            $("#dtViewOP").setCustomSearchInput();
            $("#dtViewOP tbody").html(rows);
            $("#btnVerify").css("display", "none");

            $("#dtViewOP").DataTable({
                initComplete: function () {
                    $("#dtViewOP td:nth-child(1), #dtViewOP th:nth-child(1)," +
                        "#dtViewOP td:nth-child(10), #dtViewOP th:nth-child(10)")
                        .css("display", "none");

                    $("#dtViewOP").setCustomSearch({
                    });
                    HoldOn.close();
                    $("#mdlViewOP").modal();
                }
            });
        }
    })
}

function LoadEOD() {
    $.ajax({
        url: "/OnlinePayment/IsEODValidationSent",
        success: function (data) {
            if (data.length > 0) {
                let time = formatDate(parseJsonDate(data[0].logDate), "h:mm tt").toUpperCase();

                $("#btnVerifyEOD").addClass("btn-danger").removeClass("btn-primary").html("Already Sent a Validation by " + data[0].user +
                    " (" + time + ")").attr("validated", "true").attr("time", time)
            } else {
                $("#btnVerifyEOD").addClass("btn-primary").removeClass("btn-success").html("Verify and Send Report").attr("validated", "false")
            }
        }
    })

    $.ajax({
        url: "/OnlinePayment/GetOtherOnlinePayments?batchId=0&accountType=BANK",
        success: function (data) {
            if (data.length == 0) {
                $("#EODContainer").html(`<div id='no-data-msg'><h3>There is no other online payment released today.</h3></div>`);
                $("#mdlEOD").modal();
                return false;
            }

            let entries = data;

            let transactionTypes = _.uniq(_.map(entries, 'transactionType'))
            
            let totalSummations = [];

            $.each(transactionTypes, function (i, transactionType) {
                let accountEntries = _.map(entries, function (o) {
                    if (o.transactionType == transactionType &&
                        (o.accountType || "").toUpperCase() == "BANK" &&
                        o.account) return o;
                });

                let offsetAccountEntries = _.map(entries, function (o) {
                    if (o.transactionType == transactionType &&
                        (o.offsetAccountType || "").toUpperCase() == "BANK" &&
                        o.offsetAccount) return o;
                });
                
                accountEntries = _.without(accountEntries, undefined)
                offsetAccountEntries = _.without(offsetAccountEntries, undefined)
                
                let accountSummation =
                    _(accountEntries)
                        .groupBy('account')
                        .map((objs, key) => ({
                            'bank': key,
                            totalDebit: (_.sumBy(objs, 'debit') || 0),
                            totalCredit: (_.sumBy(objs, 'credit') || 0),
                            type: "non-offset"
                        }))
                        .value();
                
                let offsetAccountSummation = 
                    _(offsetAccountEntries)
                        .groupBy('offsetAccount')
                        .map((objs, key) => ({
                            'bank': key,
                            totalDebit: (_.sumBy(objs, 'credit') || 0),
                            totalCredit: (_.sumBy(objs, 'debit') || 0),
                            type: "offset"
                        }))
                        .value();

                let summationsArr = accountSummation.concat(offsetAccountSummation)
                
                let summations = 
                    _(summationsArr)
                        .groupBy('bank')
                        .map((objs, key) => ({
                            'bank': key,
                            totalDebit: (_.sumBy(objs, 'totalDebit') || 0),
                            totalCredit: (_.sumBy(objs, 'totalCredit') || 0),
                        }))
                        .value();

                totalSummations.push({
                    transactionType: transactionType,
                    summations: summations,
                    accounts: accountSummation,
                    offsetAccounts: offsetAccountSummation
                })
            });

            let banks = [];

            let html = "";

            $.each(totalSummations, function (i, totalSummation) {
                html += "<div class='eod-item'><h4>" + totalSummation.transactionType + "</h4>"
                html += `<table class="table jambo_table bulk_action dataTable no-footer">
                        <thead><tr>
                            <th style="width:40%">Bank</th>
                            <th style="width:20%">Total Debit</th>
                            <th style="width:20%">Total Credit</th>
                            <th style="width:20%">Sum</th>
                        </tr></thead><tbody>`

                $.each(totalSummation.summations, function (i2, summation) {
                    html += `<tr>
                                <td>${summation.bank}</td>
                                <td>${parseMoney(summation.totalDebit, true)}</td>
                                <td>${parseMoney(summation.totalCredit, true)}</td>
                                <td>${parseMoney((summation.totalDebit - summation.totalCredit), true)}</td>
                              </tr>
                            `

                    if (banks.indexOf(summation.bank) == -1) {
                        banks.push(summation.bank);
                    }
                })

                html += `</tbody></table></div>`
            });

            let totalTable = ""
            
            totalTable += `<div class='eod-total'><h4>Total:</h4>
                    <table class="table jambo_table bulk_action dataTable no-footer"><thead><tr>
                        <th>Bank</th>
                        <th>${(transactionTypes.join("</th><th>"))}</th>
                        <th>Sum</th>
                    </tr></thead><tbody>`

            let overAllTotal = 0.0;
            
            $.each(banks, function (i, bank) {
                totalSumByBank = 0.0;

                totalTable += `<tr><td>${bank}</td>`

                $.each(transactionTypes, function (i2, transactionType) {
                    let transSums = _.chain(totalSummations).filter(function(x) {
                        return (x.transactionType == transactionType)
                    })
                        .first().value();

                    let transBankSum = _.chain(transSums.summations).filter(function (x) {
                        return (x.bank == bank)
                    })
                        .first().value();

                    let sum = transBankSum ? (transBankSum.totalDebit - transBankSum.totalCredit) : 0.00;

                    totalTable += "<td>" + parseMoney(sum, true) + "</td>"

                    totalSumByBank += sum;
                });

                overAllTotal += totalSumByBank;

                totalTable += "<td>"+ parseMoney(totalSumByBank) +"</td></tr>"
            });

            totalTable += `<tr style='font-weight:600;background-color:#d8c8c74d;'><td class='text-right' colspan='${(transactionTypes.length + 1)}'>
                        <text class='mr-2'>Total:</text>
                    </td><td>${parseMoney(overAllTotal)}</td></tr>`

            totalTable += "</tbody></table></div>"

            html = totalTable + html;
            
            $("#EODContainer").html(html);
            $("#mdlEOD").modal();
        }
    })
}

function LoadTable() {
    HoldOn.open({
        theme: "sk-circle"
    })

    let daterange = ($("#dtrSearch").val()).split(" - ");
    let dateFrom = daterange[0]
    let dateTo = daterange[1]

    $("#dtBatches").DataTable().destroy();

    $("#dtBatches").DataTable({
        ajax: {
            dataType: "json",
            dataSrc: "",
            url: "/OnlinePayment/GetOnlinePaymentBatches?dateFrom="+ dateFrom +"&dateTo=" + dateTo,
        },
        columns: [
            {
                data: "generalJournalNum",
                title: "General Journal #"
            },
            {
                data: "entity",
                title: "Entity"
            },
            {
                data: "transactionType",
                title: "Transaction Type"
            },
            {
                data: null,
                title: "Upload Date",
                render: function (data) {
                    let date = parseJsonDate(data.uploadDate);

                    if (date == "") return "";

                    return formatDate(date, "MM-dd-yyyy")
                }
            },
            {
                title: "Description/Note",
                data: "description"
            },
            {
                title: "Action",
                data: null,
                render: function (data) {
                    var details = `<div onclick='ViewOnlinePayments(${ignoreSingleQuote(data)})' class='fa-hover'>
                                    <a href='#' class='btnEdit'>
                                    <i class='fa fa-edit'></i> View
                                    </a>
                                    </div>`;
                    return details;
                }
            }
        ],
        initComplete: function () {
            HoldOn.close();
        }
    })
}