﻿var currentDate;
var dtPayment;
var voucherIdArr;
var selectedCount = 0;
var dtVoucher;

$(document).ready(function () {
    $("#txtCollectorType").change(function () {
        if ($(this).val() == "AUTHORIZED") {
            $("#txtThirdPartyCompany").closest("div").css("display", "none");
        } else if ($(this).val() == "THIRD PARTY") {
            $("#txtThirdPartyCompany").closest("div").css("display", "block");
        }
    })

    $("#dtPayment").setCustomSearchInput();
    dtPayment = $('#dtPayment').DataTable({
        initComplete: function () {
            $("#dtPayment").setCustomSearch({
                exemption: [0]
            });
        },
        ajax: {
            url: "/SEAN/GetForReleasingVoucherList",
            datatype: 'json',
            dataSrc: '',

        },
        "columns": [
            {
                "data": null,
                "title": "<th class='text-center' style='width:20px;'><input type='checkbox' id='cbxSelectAll' /></th>",
                render: function (data, type, row) {
                    var details = "";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Entity",
                render: function (data, type, row) {
                    var details = "<text class='dataAreaId " + data.entity + "'>" + data.entity + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Vendor Code",
                render: function (data, type, row) {
                    var details = "<text class='vendorCode'>" + data.vendorCode + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Company",
                render: function (data, type, row) {
                    var details = "<text class='companyName'>" + data.companyName + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Bank",
                render: function (data, type, row) {
                    var details = "<text class='bankName'>" + data.bank + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Voucher",
                render: function (data, type, row) {
                    var details = "<text class='voucherId'>" + data.voucherId + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Amount",
                render: function (data, type, row) {
                    var details = "<span>&#8369;</span><text class='amount'>" + parseMoney(data.amount) + "</text>"
                    return details;
                }
            },
            {
                "data": null,
                "title": "Check",
                render: function (data, type, row) {
                    var details = "<text class='checkNumber'>" + (data.checkNum ? data.checkNum : "") + "</text>"
                    return details;
                }
            },
            {
                "data": null,
                "title": "Date",
                render: function (data, type, row) {
                    var details = parseJsonDate(data.checkDate);
                    if (details == "") return "";
                    return "<text class='checkDate'>" + $.date(details) + "</text>"
                }
            },
        ],
        "createdRow": function (row, data, dataIndex) {
            $(row).attr("id", data.voucherId);

            if ($(row).find('.dataAreaId').html() == "SPAV") {
                $(row).addClass("spavi_row");
            } else if ($(row).find('.dataAreaId').html() == "SPCI") {
                $(row).addClass("spci_row");
            } else if ($(row).find('.dataAreaId').html() == "WBHI") {
                $(row).addClass("wbhi_row");
            } else if ($(row).find('.dataAreaId').html() == "NAF") {
                $(row).addClass("naf_row");
            } else if ($(row).find('.dataAreaId').html() == "SIL") {
                $(row).addClass("sil_row");
            } else if ($(row).find('.dataAreaId').html() == "DBE") {
                $(row).addClass("dbe_row");
            }
        },
        columnDefs: [{
            orderable: false,
            className: 'select-checkbox',
            targets: [0]
        },

        ],
        select: {
            style: 'multi',
            selector: 'td:first-child'
        },
        order: [
            [1, 'asc']
        ]
    })

    dtPayment.on("click", "#cbxSelectAll", function () {
        if ($('#cbxSelectAll').prop("checked") == false) {
            dtPayment.rows().deselect();
            $("th.select-checkbox").removeClass("selected");
        } else {
            dtPayment.rows({ search: 'applied' }).select();
            $("th.select-checkbox").addClass("selected");
        }
    })

    dtPayment.on('select', function (e, dt, type, indexes) {
        selectedCount = $("#dtPayment").DataTable().$("tr.selected").length;
        $("#btnRelease").html("Release Selected Vouchers ( "+ selectedCount +" )")
    }).on('deselect', function (e, dt, type, indexes) {
        selectedCount = $("#dtPayment").DataTable().$("tr.selected").length;
        $("#btnRelease").html("Release Selected Vouchers" + (selectedCount > 0 ? " ( "+ selectedCount +" )" : ""))
    });

    $("#btnRelease").click(function () {
        LoadVouchers();
    })

    $.contextMenu({
        selector: '#dtPayment',
        items: {
            "MFR": {
                name: "Release Selected Vouchers",
                icon: "fa-long-arrow-up",
                callback: function (key, opt) {
                    LoadVouchers();
                }
            }
        }
    });

    $("#btnReleaseSelected").click(function () {
        HoldOn.open({
            theme: "sk-circle",
            message: "Releasing Vouchers..."
        })

        let parameter;
        let hasError = false;
        let voucherArray = [];

        dtVoucher.$("tr").each(function (index) {
            let receiptNumber = $(this).find(".txtReceiptNumber").val();
            let receiptType = $(this).find(".txtReceiptType").val();
            let voucherId = $(this).find(".voucherId").html();
            let checkType = $(this).find(".txtCheckType").val();

            if (receiptType != "DRY") {
                voucherArray.push({
                    VoucherId: voucherId,
                    ReceiptNo: receiptNumber,
                    ReceiptType: receiptType,
                    CheckType: checkType
                })
            }
        });

        if (voucherArray.length == 0) {
            HoldOn.close();
            swal({
                title: "No voucher for release is submitted.",
                text: "Please fill at least one receipt number for voucher.",
                icon: "error"
            })
        } else {
            if ($("#txtCollectorName").val() == "" || ($("#txtCollectorType").val() == "THIRD PARTY") && $("#txtThirdPartyCompany").val() == "") {
                swal({
                    title: "Please complete the collector's details before proceeding",
                    icon: "error"
                })
                HoldOn.close();
                return false;
            } else {
                parameter = JSON.stringify({
                    r: voucherArray,
                    queueId: 0,
                    isInQueue: false,
                    c: {
                        CollectorName: $("#txtCollectorName").val(),
                        CollectorType: $("#txtCollectorType").val(),
                        ThirdPartyCompany: $("#txtThirdPartyCompany").val(),
                        QueueType: "SPECIAL",
                        status: "DONE"
                    }
                })
            }


            $.ajax({
                type: "Post",
                url: "/ECREA/ReleaseVoucher",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: parameter,
                success: function (data) {
                    HoldOn.close();

                    swal({
                        title: "Vouchers are released successfully!",
                        icon: "success"
                    }).then(function () {
                        document.location.reload();
                    });
                },
                error: function () {
                    swal({
                        title: "There is an error occured releasing the voucher.",
                        icon: "error"
                    });
                }
            })
        }
    })
})

function LoadVouchers() {
    HoldOn.open({
        theme:"sk-circle"
    })
    let voucherModelArr = [];
    var table = $("#dtPayment").DataTable();
    $("#dtVoucher").DataTable().destroy();

    table.$("tr.selected").each(function () {
        voucherModelArr.push({
            voucherId: $(this).find(".voucherId").html(),
            vendorCode: $(this).find(".vendorCode").html(),
            companyName: $(this).find(".companyName").html(),
            entity: $(this).find(".dataAreaId").html(),
            bank: $(this).find(".bankName").html(),
            amount: removeCommas($(this).find(".amount").html()),
            chequeNumber: $(this).find(".checkNumber").html(),
            chequeDate: $(this).find(".checkDate").html(),
        })
    });

    if (voucherModelArr.length == 0) {
        swal({
            icon: "error",
            text: "Please select at least one voucher first.",
            title: "No Voucher is Selected"
        });
        HoldOn.close();
        return false;
    }
    
    dtVoucher = $("#dtVoucher").DataTable({
        data: voucherModelArr,
        destroy: true,
        "columns": [
            {
                "data": null,
                "title": "Voucher",
                render: function (data, type, row) {
                    var details = "<text class='voucherId'>" + data.voucherId + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Entity",
                render: function (data, type, row) {
                    var details = "<text class='dataAreaId " + data.entity + "'>" + data.entity + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Bank",
                render: function (data, type, row) {
                    var details = "<text class='bankName'>" + data.bank + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Amount",
                render: function (data, type, row) {
                    var details = "<span>&#8369;</span><text class='amount'>" + parseMoney(data.amount) + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Check",
                render: function (data, type, row) {
                    var details = "<text class='checkNumber'>" + (data.chequeNumber ? data.chequeNumber : "") + "</text>"
                    return details;
                }
            },
            {
                "data": null,
                "title": "Date",
                render: function (data, type, row) {
                    var details = parseJsonDate(data.chequeDate);
                    if (details == "") return "";
                    return "<text class='checkDate'>" + $.date(details) + "</text>"
                }
            },
            {
                "data": null,
                "title": "Check Type",
                render: function (data, type, row) {
                    var details = "<select class='form-control txtCheckType'>" +
                        "<option value='STANDARD'>Standard</option>" +
                        "<option value='MANAGER'>Manager's Check</option>" +
                        "</select>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Receipt Number",
                render: function (data, type, row) {
                    var details = "<input class='form-control txtReceiptNumber' type='text' placeholder='Receipt Number' />";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Receipt Type",
                render: function (data, type, row) {
                    var details = "<select class='form-control txtReceiptType'>" +
                        "<option value='OR'>Official Receipt</option>" +
                        "<option value='AR'>Acknowledgement Receipt</option>" +
                        "<option value='PR'>Provisionary Receipt</option>" +
                        "<option value='CR'>Collection Receipt</option>" +
                        "<option value='DRY'>Don't Release Yet</option>" +
                        "</select>";
                    return details;
                }
            },
        ],
        "createdRow": function (row, data, dataIndex) {
            if ($(row).find('.dataAreaId').html() == "SPAV") {
                $(row).addClass("spavi_row");
            } else if ($(row).find('.dataAreaId').html() == "SPCI") {
                $(row).addClass("spci_row");
            } else if ($(row).find('.dataAreaId').html() == "WBHI") {
                $(row).addClass("wbhi_row");
            } else if ($(row).find('.dataAreaId').html() == "NAF") {
                $(row).addClass("naf_row");
            } else if ($(row).find('.dataAreaId').html() == "SIL") {
                $(row).addClass("sil_row");
            } else if ($(row).find('.dataAreaId').html() == "DBE") {
                $(row).addClass("dbe_row");
            }
        },
    });

    HoldOn.close();

    $("#modalSelectedVouchers").modal();
}