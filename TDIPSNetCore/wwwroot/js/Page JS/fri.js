﻿var dtVoucher;

$(document).ready(function () {
    $("#dtVoucher").setCustomSearchInput();

    dtVoucher = $('#dtVoucher').DataTable({
        ajax: {
            url: $('#dtVoucher').attr("url"),
            datatype: 'json',
            dataSrc: '',

        },
        "columns": [
            {
                "data": null,
                "title": "<th class='text-center'><input type='checkbox' id='cbxSelectAll' /></th>",
                render: function (data, type, row) {
                    var details = "";
                    return details;
                }
            },
            { data: "companyName", title: "Company Name" },
            { data: "vendorCode", title: "Vendor Code" },
            {
                "data": null,
                "title": "Entity",
                render: function (data, type, row) {
                    var details = "<text class='dataAreaId " + data.entity + "'>" + data.entity + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Bank",
                render: function (data, type, row) {
                    var details = "<text class='bankName'>" + data.bank + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Voucher Id",
                render: function (data, type, row) {
                    var details = "<text class='voucherId'>"+ data.voucherId +"</text>"
                    return details;
                }
            },
            { data: "checkNum", title: "Check" },
            {
                "data": null,
                "title": "Amount",
                render: function (data, type, row) {
                    var details = "<span>&#8369;</span><text class='amount'>" + parseMoney(data.amount) + "</text>"
                    return details;
                }
            },
            {
                "data": null,
                "title": "Releasing Date",
                render: function (data, type, row) {
                    var details = parseJsonDate(data.releasingDate);
                    if (details == "") return "";
                    return $.date(details);
                }
            },
            { data: "SEANCode", title: "SEAN Code" }
        ],
        columnDefs: [{
            className: 'text-center',
            targets: [8]
        },
        {
            orderable: false,
            className: 'select-checkbox',
            targets: [0]
        }
        ],
        select: {
            style: 'multi',
            selector: 'td:first-child'
        },
        "createdRow": function (row, data, dataIndex) {
            if (data.status == "FOR RELEASING") {
                $(row).addClass("forReleasing");
            } else if (data.status = "ONHOLD") {
                $(row).addClass("onHold");
            }


            if ($(row).find('.dataAreaId').html() == "SPAV") {
                $(row).addClass("spavi_row");
            } else if ($(row).find('.dataAreaId').html() == "SPCI") {
                $(row).addClass("spci_row");
            } else if ($(row).find('.dataAreaId').html() == "WBHI") {
                $(row).addClass("wbhi_row");
            } else if ($(row).find('.dataAreaId').html() == "NAF") {
                $(row).addClass("naf_row");
            } else if ($(row).find('.dataAreaId').html() == "SIL") {
                $(row).addClass("sil_row");
            } else if ($(row).find('.dataAreaId').html() == "DBE") {
                $(row).addClass("dbe_row");
            }
        },
        order: [
            [1, 'asc']
        ],
        "initComplete": function (settings, json) {
            $("#dtVoucher").setCustomSearch({
                exemption: [0]
            });

            totalAmount = 0.0;
            var totalAmount = 0.0;
            var itemAmountArr = [];
            var bankArray = [];
            var entityArray = [];

            dtVoucher.$("tr.forReleasing").each(function (index) {
                var amount = removeCommas(($(this).find(".amount")).html())
                let entity = $(this).find(".dataAreaId").html();
                let bank = $(this).find(".bankName").html();

                itemAmountArr.push({
                    entity: entity,
                    bankName: bank,
                    amount: amount
                })

                if (bankArray.indexOf(bank) == -1) {
                    bankArray.push(bank)
                }

                if (entityArray.indexOf(entity) == -1) {
                    entityArray.push(entity);
                }

                totalAmount += parseFloat(amount);
            });

            $.each(entityArray, function (i, item) {
                let entity = item;

                let itemArray = $.grep(itemAmountArr, function (n, i) {
                    return n.entity == entity;
                })

                let total = 0;
                let subitems = [];

                $.each(itemArray, function (i, item) {
                    let index = GetIndexInJsonByKeyVal(subitems, "bankName", item.bankName);

                    if (index >= 0) {
                        subitems[index].amount = parseFloat(subitems[index].amount) + parseFloat(item.amount);
                    } else {
                        subitems.push({
                            bankName: item.bankName,
                            amount: item.amount
                        })
                    }

                    total += parseFloat(item.amount);
                })

                $(".by_entity .container_item").append('<div class="summary_item ' + entity + '">' +
                    '<div class="item_header">' +
                    '<label>' + entity + ' (₱' + parseMoney(total) + ')</label>' +
                    '</div>' +
                    '<div class="container_sub_item">' +
                    '</div>' +
                    '</div>')


                $.each(subitems, function (i, item) {
                    $(".summary_item." + entity + " .container_sub_item").append('<div class="sub-item">' +
                        '<label>' + item.bankName + ' - ₱' + parseMoney(item.amount) +
                        '</label></div>')
                })
            })

            $.each(bankArray, function (i, item) {
                let bank = item;

                let itemArray = $.grep(itemAmountArr, function (n, i) {
                    return n.bankName == bank;
                })

                let total = 0;
                let subitems = [];

                $.each(itemArray, function (i, item) {
                    let index = GetIndexInJsonByKeyVal(subitems, "entity", item.entity);

                    if (index >= 0) {
                        subitems[index].amount = parseFloat(subitems[index].amount) + parseFloat(item.amount);
                    } else {
                        subitems.push({
                            entity: item.entity,
                            amount: item.amount
                        })
                    }

                    total += parseFloat(item.amount);
                })


                $(".by_bank .container_item").append('<div class="summary_item ' + bank + '">' +
                    '<div class="item_header">' +
                    '<label>' + bank + ' (₱' + parseMoney(total) + ')</label>' +
                    '</div>' +
                    '<div class="container_sub_item">' +
                    '</div>' +
                    '</div>')


                $.each(subitems, function (i, item) {
                    $(".summary_item." + bank + " .container_sub_item").append('<div class="sub-item">' +
                        '<label>' + item.entity + ' - ₱' + parseMoney(item.amount) +
                        '</label></div>')
                })
            })

            $("#btnRequest").html("<i class='fa fa-external-link'></i> Request <u>₱" + parseMoney(totalAmount) + "</u> Worth of Vouchers");
            $("#lblTotal").html("Total : ₱" + parseMoney(totalAmount))
        }
    })

    dtVoucher.on("click", "#cbxSelectAll", function () {
        if ($('#cbxSelectAll').prop("checked") == false) {
            dtVoucher.rows().deselect();
            $("th.select-checkbox").removeClass("selected");
        } else {
            dtVoucher.rows({ search: 'applied' }).select();
            $("th.select-checkbox").addClass("selected");
        }
    })

    $.contextMenu({
        selector: '#dtVoucher',
        items: {
            "RFD": {
                name: "Mark Selected as Uncollected",
                icon: "fa-external-link",
                callback: function (key, opt) {
                    MarkUncollected();
                }
            }
        }
    });
})

function MarkUncollected() {
    let voucherIdArr = [];
    var table = $("#dtVoucher").DataTable();

    table.$("tr.selected").each(function () {
        voucherIdArr.push($(this).find(".voucherId").text());
    });
    
    if (voucherIdArr.length == 0) {
        swal({
            title: "No Voucher is Selected for Request",
            icon: "error",
            text: "Please select at least one row before maring."
        })
    } else {
        swal({
            title: "Are you sure??",
            text: "Once marked as uncollected, this can't be undone!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((ok) => {
            HoldOn.open({
                theme: "sk-circle"
            })

            $.ajax({
                dataType: "json",
                type: "POST",
                contentType: "application/json",
                url: "/SEAN/ForceUncollectedVouchers",
                data: JSON.stringify({
                    voucherIds: voucherIdArr
                }),
                success: function () {
                    HoldOn.close();

                    swal({
                        title: "Success!",
                        icon: "success",
                        text: "Vouchers are successfully marked as uncollected."
                    }).then(function () {
                        document.location.reload();
                    });
                }
            })
        });
    }
}

function GetIndexInJsonByKeyVal(json, key, value) {
    let index = -1;

    $.each(json, function (i, item) {
        if (item[key] == value) {

            index = i;
        }
    });

    return index;
}