﻿var vendorID = 0;
var getCollectorListURL = "";
var getBankListURL = "";
var dtVendor;
var arrVendor = [];


function EditVendor(vendor) {
    HoldOn.open({
        theme: "sk-circle"
    });

    $('.entity-multiple').val("");
    $('.entity-multiple').trigger("change");

    vendorID = vendor.vendorID;
    $("#modalVendor .title").html("Update the Vendor Details");
        
    $("#txtVendorCode").val(vendor.vendorCode);
    $("#txtCompanyName").val(vendor.companyName);
    $("#txtTinNumber").val(vendor.tinNumber);

    $("#txtType").val(vendor.type);

    let segment = vendor.segment;
    
    if (segment && segment.toUpperCase() != 'NULL' && segment.length > 0 &&
        $("#txtSegment options[value='" + segment + "']").length == 0) {
        
        $("#txtSegment").append(`<option value='${segment}'>${segment}</option>`)
    }

    $("#txtSegment").val(segment);
    
    $("#txtSubsegment").val(vendor.subSegment);

    $("#txtCompanyEmail").val(vendor.companyEmail);
    $("#txtContactNumber").val(vendor.contactNumber);
    $("#txtCompanyAddress").val(vendor.companyAddress);

    let entities = vendor.dataAreaId.split(",");

    $('.entity-multiple').val(entities);
    $('.entity-multiple').trigger('change');

    $.ajax({
        url: getCollectorListURL + "?vendorID=" + vendorID,
        type: 'GET',
        async: false,
        success: function (data) {
            $.each(data, function (index, value) {
                var count = 1;

                if (index > 0 && index < data.length) {
                    count = $(".divCollector").length + 1

                    var dom = $(".divCollector1").html();

                    //limit collectors to 3

                    dom = dom.replace("1. Collector Name", "<a href='#' class='removeDiv'><i class='fa fa-trash'></i></a> " + count + ". Collector Name");
                    dom = dom.replace(/"txtCollectorName"/gi, "txtCollectorName" + count);
                    dom = dom.replace(/"txtPosition"/gi, "txtPosition" + count);
                    dom = dom.replace(/"txtEmailAddress"/gi, "txtEmailAddress" + count);
                    dom = dom.replace(/"txtContactNumber"/gi, "txtContactNumber" + count);
                    dom = dom.replace(/"txtCollectorContactNumber"/gi, "txtCollectorContactNumber" + count);
                    $("#divCollector").append("<div class='addedDOM'><br><div class='divCollector divCollector" + count + "'>" +
                        dom + "</div></div>");

                    $(".removeDiv").click(function () {
                        $(this).closest(".addedDOM").remove();
                    })
                }

                $(".divCollector" + count + " .txtCollectorName").val(value.collectorName);
                $(".divCollector" + count + " .txtPosition").val(value.position);
                $(".divCollector" + count + " .txtEmailAddress").val(value.email);
                $(".divCollector" + count + " .txtCollectorContactNumber").val(value.contactNumber);
            });
        },
        error: function () {
            HoldOn.close();
            swal({
                title: "There is an error processing your request. Please try again later.",
                icon: "error"
            })
        },
        timeout: 1000
    })

    $.ajax({
        url: getBankListURL + "?vendorID=" + vendorID,
        type: 'GET',
        async: false,
        success: function (data) {
            $.each(data, function (index, value) {
                var count = 1;

                if (index > 0) {
                    count = $(".divBank").length + 1;

                    var dom = $(".divBank1").html();

                    dom = dom.replace("1. Bank Company", "<a href='# class='removeDiv'><i class='fa fa-trash' style='color:red !important;'></i></a> " + count + ". Bank Company");
                    dom = dom.replace(/"txtBankCompany"/gi, "txtBankCompany" + count);
                    dom = dom.replace(/"txtBankAccount"/gi, "txtBankAccount" + count);
                    dom = dom.replace(/"txtBankBranch"/gi, "txtBankBranch" + count);
                    dom = dom.replace(/"txtAccountName"/gi, "txtAccountName" + count);

                    $("#divBank").append("<div class='addedDOM'><br><div class='divBank divBank" + count + "'>" +
                        dom + "</div></div>");

                    $(".removeDiv").click(function () {
                        $(this).closest(".addedDOM").remove();
                    })
                }

                $(".divBank" + count + " .txtBankCompany").val(value.bankCompany);
                $(".divBank" + count + " .txtBankAccount").val(value.bankAccount);
                $(".divBank" + count + " .txtBankBranch").val(value.bankBranch);
                $(".divBank" + count + " .txtAccountName").val(value.accountName);
            });
        },
        error: function () {
            HoldOn.close();
            swal({
                title: "There is an error processing your request. Please try again later.",
                icon: "error"
            })
        },
        timeout: 1000
    })

    HoldOn.close();

    $("#modalVendor").modal();
}

$(document).ready(function () {
    $('.entity-multiple').select2();

    if (isEdge || isIE) {
        let width = $("input[type=file]").width();

        $("input[type=file]").css("display", "none");

        $("input[type=file]").after("<input class='form-control file-upload'" +
            "style='height:31px;width:" + width + "px;' value='Choose a file' readonly></input>");

        $(".file-upload").click(function () {
            $("input[type=file]").trigger("click");
        })

        $("input[type=file]").change(function () {
            let file = $("input[type=file]").val().split("\\");

            if (file.length > 0) {
                $(".file-upload").val(file[file.length - 1]);
            } else {
                $(".file-upload").val($("input[type=file]").val());
            }
        })
    }

    $("#dtVendor").setCustomSearchInput();

    dtVendor = $('#dtVendor').DataTable({
        "initComplete": function (settings, json) {
            $("#dtVendor").setCustomSearch({
                exemption: [0, 7],
                onchange: "false"
            });
            $("th.select-checkbox").removeClass();
        },
        //"drawCallback": function (settings) {
        //    $(".btnEdit").off('click').click(function () {
        //        HoldOn.open({
        //            theme: "sk-circle"
        //        });

        //        $('.entity-multiple').val("");
        //        $('.entity-multiple').trigger("change");

        //        vendorID = $(this).attr("vendorID");
        //        $("#modalVendor .title").html("Update the Vendor Details");
                
        //        $("#txtVendorCode").val($(this).closest("tr").find(".vendorCode").html());
        //        $("#txtCompanyName").val($(this).closest("tr").find(".companyName").html());
        //        $("#txtTinNumber").val($(this).attr("tinNumber"));

        //        let segment = $(this).attr("segment");


        //        if (segment && segment.toUpperCase() != 'NULL' && segment.length > 0 &&
        //            $("#txtSegment options[value='"+ segment +"']").length == 0)
        //        {
        //            $("#txtSegment").append(`<option value='${segment}'>${segment}</option>`)
        //        }

        //        $("#txtSegment").val(segment);

                
        //        $("#txtSubsegment").val($(this).attr("subsegment"));
                
        //        $("#txtCompanyEmail").val($(this).closest("tr").find(".companyEmail").html());
        //        $("#txtContactNumber").val($(this).closest("tr").find(".contactNumber").html());
        //        $("#txtCompanyAddress").val($(this).closest("tr").find(".companyAddress").html());

        //        let entities = $(this).closest("tr").find(".dataAreaId").html().split(",");

        //        $('.entity-multiple').val(entities);
        //        $('.entity-multiple').trigger('change');

        //        $.ajax({
        //            url: getCollectorListURL + "?vendorID=" + vendorID,
        //            type: 'GET',
        //            async: false,
        //            success: function (data) {
        //                $.each(data, function (index, value) {
        //                    var count = 1;

        //                    if (index > 0 && index < data.length) {
        //                        count = $(".divCollector").length + 1

        //                        var dom = $(".divCollector1").html();

        //                        //limit collectors to 3

        //                        dom = dom.replace("1. Collector Name", "<a href='#' class='removeDiv'><i class='fa fa-trash'></i></a> " + count + ". Collector Name");
        //                        dom = dom.replace(/"txtCollectorName"/gi, "txtCollectorName" + count);
        //                        dom = dom.replace(/"txtPosition"/gi, "txtPosition" + count);
        //                        dom = dom.replace(/"txtEmailAddress"/gi, "txtEmailAddress" + count);
        //                        dom = dom.replace(/"txtContactNumber"/gi, "txtContactNumber" + count);
        //                        dom = dom.replace(/"txtCollectorContactNumber"/gi, "txtCollectorContactNumber" + count);
        //                        $("#divCollector").append("<div class='addedDOM'><br><div class='divCollector divCollector" + count + "'>" +
        //                            dom + "</div></div>");

        //                        $(".removeDiv").click(function () {
        //                            $(this).closest(".addedDOM").remove();
        //                        })
        //                    }

        //                    $(".divCollector" + count + " .txtCollectorName").val(value.collectorName);
        //                    $(".divCollector" + count + " .txtPosition").val(value.position);
        //                    $(".divCollector" + count + " .txtEmailAddress").val(value.email);
        //                    $(".divCollector" + count + " .txtCollectorContactNumber").val(value.contactNumber);
        //                });
        //            },
        //            error: function () {
        //                HoldOn.close();
        //                swal({
        //                    title: "There is an error processing your request. Please try again later.",
        //                    icon: "error"
        //                })
        //            },
        //            timeout: 1000
        //        })
                
        //        $.ajax({
        //            url: getBankListURL + "?vendorID=" + vendorID,
        //            type: 'GET',
        //            async: false,
        //            success: function (data) {
        //                $.each(data, function (index, value) {
        //                    var count = 1;

        //                    if (index > 0) {
        //                        count = $(".divBank").length + 1;

        //                        var dom = $(".divBank1").html();

        //                        dom = dom.replace("1. Bank Company", "<a href='# class='removeDiv'><i class='fa fa-trash' style='color:red !important;'></i></a> " + count + ". Bank Company");
        //                        dom = dom.replace(/"txtBankCompany"/gi, "txtBankCompany" + count);
        //                        dom = dom.replace(/"txtBankAccount"/gi, "txtBankAccount" + count);
        //                        dom = dom.replace(/"txtBankBranch"/gi, "txtBankBranch" + count);
        //                        dom = dom.replace(/"txtAccountName"/gi, "txtAccountName" + count);

        //                        $("#divBank").append("<div class='addedDOM'><br><div class='divBank divBank" + count + "'>" +
        //                            dom + "</div></div>");

        //                        $(".removeDiv").click(function () {
        //                            $(this).closest(".addedDOM").remove();
        //                        })
        //                    }

        //                    $(".divBank" + count + " .txtBankCompany").val(value.bankCompany);
        //                    $(".divBank" + count + " .txtBankAccount").val(value.bankAccount);
        //                    $(".divBank" + count + " .txtBankBranch").val(value.bankBranch);
        //                    $(".divBank" + count + " .txtAccountName").val(value.accountName);
        //                });
        //            },
        //            error: function () {
        //                HoldOn.close();
        //                swal({
        //                    title: "There is an error processing your request. Please try again later.",
        //                    icon: "error"
        //                })
        //            },
        //            timeout: 1000
        //        })

        //        HoldOn.close();

        //        $("#modalVendor").modal();
        //    })
        //},
        ajax: {
            url: $('#dtVendor').attr("url"),
            datatype: 'json',
            dataSrc: 'dataList',
            type: "POST",
            data: function (d) {
                d.v = {
                    dataAreaId: $("#1").val(),
                    vendorCode: $("#2").val(),
                    companyName: $("#3").val(),
                    companyAddress: $("#4").val(),
                    companyEmail: $("#5").val(),
                    contactNumber: $("#6").val(),
                }
            },
            beforeSend: function () {
                $('#dtVendor > tbody').html(
                    '<tr class="odd customized_loading">' +
                    '<td valign="top" colspan="' + $("#dtVendor thead th").length +'" class="dataTables_empty">Loading&hellip;</td>' +
                    '</tr>'
                );
            }
        },
        "columns": [
        {
            "data": null,
            "title": "",
            render: function (data, type, row) {
                var details = "";
                return details;
            }
        },
        {
            "data": "dataAreaId",
            "title": "Entity",
            render: function (data, type, row) {
                var details = "<text class='dataAreaId " + data + "'>" + data + "</text>";
                return details;
            }
        },
        {
            "data": "vendorCode",
            "title": "Vendor Code",
            render: function (data, type, row) {
                var details = "<text class='details vendorCode' vendorCode='"+ data +"' vendorId='"+ row.vendorId +"'>" + data + "</text>";
                return details;
            }
        },
        {
            "data": "companyName",
            "title": "Company Name",
            render: function (data, type, row) {
                var details = "<text class='companyName'>" + data + "</text>";
                return details;
            }
        },
        {
            "data": "companyAddress",
            "title": "Company Address.",
            render: function (data, type, row) {
                var details = "<text class='companyAddress'>" + data + "</text>";
                return details;
            }
        },
        {
            "data": "companyEmail",
            "title": "Company Email",
            render: function (data, type, row) {
                var details = "<text class='companyEmail'>" + data + "</text>";
                return details;
            }
        },
        {
            "data": "contactNumber",
            "title": "Contact No.",
            render: function (data, type, row) {
                var details = "<text class='contactNumber'>" + data + "</text>";
                return details;
            }
        },
        {
            "data": null,
            "title": "Actions",
            render: function (data, type, row) {
                var details = "<div class='fa-hover'><a onclick='EditVendor("+ ParamParse(data) +")' href='#' tinNumber='" + data.tinNumber + "' vendorID='" + data.vendorID +
                    "' segment='"+ data.segment +"' subsegment='"+ data.subSegment +"' class='btnEdit'><i class='fa fa-edit'></i> Edit</a></div>";

                if (data.vendorCode) {
                    if ((data.vendorCode).indexOf(",") > -1) {
                        details += "<div class='fa-hover'><a href='#' onclick=\"CancelMerge('" + data.vendorCode + "', '" + data.companyName.replace(/\'/gi, '') + "')\"><i class='fa fa-arrows-h'></i> Cancel Merge</a></div>";
                    }
                }
                
                 
                return details;
            }
        }
        ],
        columnDefs: [{
            orderable: false,
            className: 'select-checkbox',
            targets: [0]
        },
        {
            className: 'row_action',
            targets: [7],
            orderable: false
        }
        ],
        select: {
            style: 'multi',
            selector: 'td:first-child'
        },
        order: [
            [1, 'asc']
        ],
        "createdRow": function (row, data, dataIndex) {
            if ($(row).find('.dataAreaId').html() == "SPAV") {
                $(row).addClass("spavi_row");
            } else if ($(row).find('.dataAreaId').html() == "SPCI") {
                $(row).addClass("spci_row");
            } else if ($(row).find('.dataAreaId').html() == "WBHI") {
                $(row).addClass("wbhi_row");
            } else if ($(row).find('.dataAreaId').html() == "NAF") {
                $(row).addClass("naf_row");
            } else if ($(row).find('.dataAreaId').html() == "SIL") {
                $(row).addClass("sil_row");
            } else if ($(row).find('.dataAreaId').html() == "DBE") {
                $(row).addClass("dbe_row");
            }
        },
        filter: false,
        language: {
            "lengthMenu": "_MENU_",
            "processing": ""
        },
        lengthMenu: [
            [5, 10, 15, 20, 100],
            [5, 10, 15, 20, 100] // change per page values here
        ],
        orderCellsTop: true,
        pageLength: 10,
        processing: false,
        responsive: true,
        serverSide: true,
        stateSave: false
    })

    dtVendor.on('select', function (e, dt, type, indexes) {
        if (type == "row") {
            var data = dt.rows(indexes).data();
            if (IsValueExistingOnJson(arrVendor, "vendorId", data[0].vendorID) == "false") {
                arrVendor.push({
                    vendorId: data[0].vendorID,
                    vendorCode: data[0].vendorCode,
                    dataAreaId: data[0].dataAreaId
                })
            }
        }
    }).on('deselect', function (e, dt, type, indexes) {
        if (type = "row") {
            var data = dt.rows(indexes).data();
            arrVendor = RemoveElementFromJson(arrVendor, "vendorId", data[0].vendorID)
        }
    });

    $(".btnIntegrate").click(function () {
        swal({
            title: "Are you sure that you want to continue? It may take some time.",
            text: "Once proceeded, kindly wait for the process to end and don't close the window.",
            icon: "warning",
            buttons: true,
        })
            .then((ok) => {
                if (ok) {
                    HoldOn.open({
                        theme: "sk-circle",
                        message: "Integrating with Dynamics 365"
                    });

                    $.ajax({
                        url: $(this).attr("url"),
                        success: function (data) {
                            HoldOn.close();

                            if (data.length > 0)
                            {
                                swal({
                                    title: "Success",
                                    text: data.length + " vendor records are successfully added to the system. ("+ data.join(",") +")",
                                    icon: "success"
                                }).then(function () {
                                    document.location.reload();
                                });
                            } else {
                                swal({
                                    title: "Success",
                                    text: "The vendor records are already updated with D365",
                                    icon: "success"
                                })
                            }
                        }
                    })
                }
            });
    })

    $.contextMenu({
        selector: '#dtVendor',
        items: {
            "RFD": {
                name: "Merge Selected Records",
                icon: "fa-external-link",
                callback: function (key, opt) {
                    Merge();
                }
            }
        }
    });

    $("#btnUploadExcel").click(function () {
        if ($("#frmUploadExcel")[0].checkValidity() == false) {
            $("#frmUploadExcel")[0].reportValidity()
        } else {
            HoldOn.open({
                theme: "sk-circle",
                message: "Uploading an excel file.."
            })

            var excelFile = new FormData($("#frmUploadExcel")[0]);

            $.ajax({
                url: "/SEAN/ImportVendorExcel",
                type: 'POST',
                data: excelFile,
                contentType: false,
                processData: false,
                success: function (data) {
                    HoldOn.close();
                    if (data == "SUCCESS") {
                        swal({
                            title: "Excel file successfully imported!",
                            icon: "success"
                        }).then(function () {
                            document.location.reload();
                        });
                    } else if (data == "NULL") {
                        swal({
                            title: "Please select an excel file first before importing.",
                            icon: "error"
                        })
                    } else if (data == "INVALID FILE") {
                        swal({
                            title: "Invalid excel file.",
                            icon: "error"
                        })
                    } else if (data == "EMPTY") {
                        swal({
                            title: "Excel file seems empty or in different format.",
                            icon: "error"
                        })
                    } else if (data == "ERROR") {
                        swal({
                            title: "There is an error processing your request to the server.",
                            icon: "error",
                        });
                    } else {
                        swal({
                            title: "Duplicated Vendor Codes Detected",
                            icon: "warning",
                            text: "Some of the vendor codes are already existing in the database. (" + data + ") " +
                                "Other records are still added successfully."
                        }).then(function () {
                            document.location.reload();
                        });
                    }
                },
                error: function () {
                    HoldOn.close();
                    swal({
                        title: "There is an error processing your request to the server.",
                        icon: "error",
                    });
                }
            });
        }
    })

    $("#btnUploadExcel2").click(function () {
        if ($("#frmUploadExcel2")[0].checkValidity() == false) {
            $("#frmUploadExcel2")[0].reportValidity()
        } else {
            HoldOn.open({
                theme: "sk-circle",
                message: "Uploading an excel file.."
            })

            var excelFile = new FormData($("#frmUploadExcel2")[0]);

            $.ajax({
                url: "/SEAN/UploadVendorBankDetails",
                type: 'POST',
                data: excelFile,
                contentType: false,
                processData: false,
                success: function (data) {
                    HoldOn.close();
                    if (data == "NULL") {
                        swal({
                            title: "Please select an excel file first before importing.",
                            icon: "error"
                        })
                    } else if (data == "INVALID FILE") {
                        swal({
                            title: "Invalid excel file.",
                            icon: "error"
                        })
                    } else if (data.TotalRows == 0) {
                        swal({
                            title: "Oops ",
                            text: "Excel file seems empty or in different format.",
                            icon: "error"
                        })
                    } else {
                        if (data.NotFound.length > 0 || data.Incomplete.length > 0) {
                            var message = data.RowsAdded + " banks are added and " + data.RowsUpdated +
                            " banks are updated out of " + data.TotalRows + " records. ";

                            if (data.NotFound.length > 0) {
                                message += data.NotFound.join(",") + " vendor codes are not found in the database.";
                            }

                            if (data.Incomplete.length) {
                                message += "There are blank bank account fields at rows " + data.Incomplete.join(",") + ".";
                            }
                            
                            swal({
                                title: "Hmmmm",
                                text:  message,
                                icon: "warning"
                            }).then(function () {
                                location.reload();
                            })
                        } else {
                            swal({
                                title: "Success",
                                text: data.RowsAdded + " banks are added and " + data.RowsUpdated + " bank are updated out of " + data.TotalRows + " records",
                                icon: "success"
                            }).then(function () {
                                location.reload();
                            })
                        }
                    }
                },
                error: function () {
                    HoldOn.close();
                    swal({
                        title: "There is an error processing your request to the server.",
                        icon: "error",
                    });
                }
            });
        }
    })
})

$("#btnAddCollector").click(function () {
    var count = $(".divCollector").length + 1;

    var dom = $(".divCollector1").html();


    dom = dom.replace("1. Collector Name", "<a href='#' class='removeDiv'><i class='fa fa-trash'></i></a> " + count + ". Collector Name");
    dom = dom.replace(/"txtCollectorName"/gi, "txtCollectorName" + count);
    dom = dom.replace(/"txtPosition"/gi, "txtPosition" + count);
    dom = dom.replace(/"txtEmailAddress"/gi, "txtEmailAddress" + count);
    dom = dom.replace(/"txtContactNumber"/gi, "txtContactNumber" + count);
    dom = dom.replace(/"txtCollectorContactNumber"/gi, "txtCollectorContactNumber" + count);
    $("#divCollector").append("<div class='addedDOM'><br><div class='divCollector'>" +
        dom + "</div></div>");

    $(".removeDiv").click(function () {
        $(this).closest(".addedDOM").remove();
    })
    
})

$("#btnAddBank").click(function () {
    var dom = $(".divBank1").html();
    var count = $(".divBank").length + 1;

    dom = dom.replace("1. Bank Company", "<a href='#' class='removeDiv'><i class='fa fa-trash'></i></a> " + count + ". Bank Company");
    dom = dom.replace(/"txtBankCompany"/gi, "txtBankCompany" + count);
    dom = dom.replace(/"txtBankAccount"/gi, "txtBankAccount" + count);
    dom = dom.replace(/"txtBankBranch"/gi, "txtBankBranch" + count);
    dom = dom.replace(/"txtAccountName"/gi, "txtAccountName" + count);
    $("#divBank").append("<div class='addedDOM'><br><div class='divBank'>" +
        dom + "</div></div>");

    $(".removeDiv").click(function () {
        $(this).closest(".addedDOM").remove();
    })
})

$("#modalVendor").on("hidden.bs.modal", function () {
    $('.form-group.bad').each(function (i, obj) {
        $(this).removeClass("bad")
    });

    $('#frmVendor .alert').each(function (i, obj) {
        $(this).remove();
    });

    $('#frmVendor .addedDOM').each(function (i, obj) {
        $(this).remove();
    });

    $("#frmVendor")[0].reset();
});

$("#frmVendor .btnSubmit").click(function () {
    HoldOn.open({
        theme: "sk-circle",
        message: "Saving data ..."
    })

    var modelVendor = {
        vendorID: vendorID,
        vendorCode: $("#txtVendorCode").val(),
        companyName: $("#txtCompanyName").val(),
        tinNumber: $("#txtTinNumber").val(),
        companyEmail: $("#txtCompanyEmail").val(),
        contactNumber: $("#txtContactNumber").val(),
        companyAddress: $("#txtCompanyAddress").val(),
        segment: $("#txtSegment").val(),
        subSegment: $("#txtSubsegment").val(),
        dataAreaId: ($('.entity-multiple').select2("val") || []).join(","),
        type: $("#txtType").val()
    };

    var modelArrBank = [];

    var modelArrCollector = [];

    $('.divBank').each(function (i, obj) {
        modelArrBank.push({
            bankCompany: $(this).find(".txtBankCompany").val(),
            bankAccount: $(this).find(".txtBankAccount").val(),
            bankBranch: $(this).find(".txtBankBranch").val(),
            accountName: $(this).find(".txtAccountName").val()
        })
    });

    $('.divCollector').each(function (i, obj) {
        modelArrCollector.push({
            collectorName: $(this).find(".txtCollectorName").val(),
            position: $(this).find(".txtPosition").val(),
            email: $(this).find(".txtEmailAddress").val(),
            contactNumber: $(this).find(".txtCollectorContactNumber").val()
        })
    });

    var model = JSON.stringify({
        vendor: modelVendor,
        collector: modelArrCollector,
        bank: modelArrBank
    });
    
    $.ajax({
        url: $("#frmVendor").attr("url"),
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: model,
        success: function (data) {
            HoldOn.close();

            if (data == true) {
                swal({
                    title: vendorID == 0 ? "New vendor is successfully added!" : "Vendor details successfully updated!",
                    icon: "success"
                }).then(function () {
                    document.location.reload();
                });
            } else {
                swal({
                    title: "Incomplete/Invalid Request",
                    text: "Please make sure that required inputs are filled.",
                    icon: "error"
                });
            }
        },
        error: function () {
            swal({
                title: "There is an error occured adding a new vendor.",
                icon: "error"
            });
        }
    })
})

function AddVendor() {
    $('.entity-multiple').val("");
    $('.entity-multiple').trigger("change");

    vendorID = 0;
    $("#modalVendor .title").html("Add a New Vendor");
    $("#modalVendor").modal();
}

function Merge() {
    swal({
        title: "Are you sure that you want to merge the selected records?",
        icon: "warning",
        buttons: true,
    })
    .then((ok) => {
        if (ok) {
            let table = $("#dtVendor").DataTable();
            
            if (arrVendor.length < 2) {
                swal({
                    title: "Please select at least two records to merge",
                    icon: "error"
                })
            } else {
                HoldOn.open({
                    theme: "sk-circle",
                    message: "Merging Records.."
                });


                let mergedVendorCode = "";
                let vendorId = "";
                let vendorIds = "";
                let mergedEntity = "";

                $.each(arrVendor, (function (i, item) {
                    vendorId = arrVendor[0].vendorId;
                    vendorIds += "&vendorIds=" + item.vendorId;
                    mergedVendorCode += item.vendorId + "_" + item.vendorCode + ",";


                    if (mergedEntity.indexOf(item.dataAreaId) == -1) {
                        mergedEntity += item.dataAreaId + ",";
                    }
                }))
                
                mergedVendorCode = mergedVendorCode.substring(0, mergedVendorCode.length - 1);
                mergedEntity = mergedEntity.substring(0, mergedEntity.length - 1);

                $.ajax({
                    url: $("#dtVendor").attr("mergeurl") + "?refVendorId=" + vendorId + vendorIds + "&mergedVendorCode=" + mergedVendorCode + "&mergedEntity=" + mergedEntity,
                    success: function (data) {
                        HoldOn.close();
                        if (data == "SUCCESS") {
                            swal({
                                title: "Selected vendors are merged successfully!",
                                icon: "success"
                            }).then(function () {
                                document.location.reload();
                            });
                        } else {
                            swal({
                                title: "There is an error processing your request.",
                                icon: "error"
                            })
                        }
                    }
                })
            }
        }
    });
}

function CancelMerge(vendorCode, companyName) {
    swal({
        title: "Are you sure that you want to extract the selected record?",
        icon: "warning",
        buttons: true,
    })
        .then((ok) => {
            if (ok) {
                HoldOn.open({
                    theme: "sk-circle",
                    message: "Extracting Record.."
                })

                $.ajax({
                    url: $("#dtVendor").attr("cancelmergeurl") + "?vendorCode=" + vendorCode + "&companyName=" + companyName,
                    success: function (data) {
                        HoldOn.close();
                        if (data == "SUCCESS") {
                            swal({
                                title: "Selected record is extracted successfully!",
                                icon: "success"
                            }).then(function () {
                                document.location.reload();
                            });
                        } else {
                            swal({
                                title: "There is an error processing your request.",
                                icon: "error"
                            })
                        }
                    }
                })
            }
        });
}

function IsValueExistingOnJson(json, key, value) {
    let isExisting = "false";

    $.each(json, function (i, item) {
        if (item[key] == value) {
            isExisting = "true";
        }
    });

    return isExisting;
}

function RemoveElementFromJson(json, key, value)
{
    let result = [];

    $.each(json, function (i, item) {
        if (item[key] != value) {
            result.push(item);
        }
    });

    return result;
}