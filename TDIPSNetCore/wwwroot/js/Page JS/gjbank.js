﻿$(document).ready(function () {
    $("#dtBank").setCustomSearchInput();
    $("#dtBank").DataTable({
        ajax: {
            dataType: "json",
            dataSrc: "",
            url: "/GeneralJournal/GetBankList"
        },
        columns: [
            { title: "Acc No", data: "AccountNo" },
            { title: "Acc Name", data: "AccountName" },
            { title: "Acc Type", data: "AccountType" },
            { title: "Store", data: "Store" },
            { title: "Department", data: "Department" },
            { title: "Business Unit", data: "BusinessUnit" }
        ],
        initComplete: function () {
            $("#dtBank").setCustomSearch({
                onchange: "true"
            });
        }
    })

    $("#btnUploadExcel").click(function () {
        if ($("#frmUploadExcel")[0].checkValidity() == false) {
            $("#frmUploadExcel")[0].reportValidity()
        } else {
            HoldOn.open({
                theme: "sk-circle",
                message: "Uploading an excel file.."
            })

            var excelFile = new FormData($("#frmUploadExcel")[0]);

            $.ajax({
                url: "/GeneralJournal/ImportBanksExcel",
                type: 'POST',
                data: excelFile,
                contentType: false,
                processData: false,
                success: function (data) {
                    HoldOn.close();
                    let msg = ""

                    if (data.RowsAdded > 0) {
                        msg += data.RowsAdded + " record/s are added. "
                    }

                    if (data.Overwritten > 0) {
                        msg += data.Overwritten + " record/s are overwritten."
                    }

                    if (msg == "") {
                        swal({
                            title: "Oops.",
                            icon: "error",
                            text: "Your excel file seems empty or in different format. No record is added"
                        });
                    } else {
                        swal({
                            title: "Success",
                            icon: "success",
                            text: msg
                        }).then(function () {
                            document.location.reload();
                        });
                    }
                },
                error: function () {
                    HoldOn.close();
                    swal({
                        title: "There is an error processing your request to the server.",
                        icon: "error",
                    })
                }
            });
        }
    })
})