﻿var bar;
var isFirstCall = true;
var dateFrom;
var dateTo;

function LoadTable() {
    $("#dtMailLogs").DataTable().destroy();
    $("#dtMailLogs").setCustomSearchInput();
    var dtLogs = $('#dtMailLogs').DataTable({
        destroy: true,
        "aaSorting": [],
        ajax: {
            url: $('#dtMailLogs').attr("url") + "?dateFrom=" + dateFrom + "&dateTo=" + dateTo,
            datatype: 'json',
            dataSrc: '',

        },
        "columns": [
            { data: "receiverName", title: "Recipient Name" },
            { data: "description", title: "Description" },
            { data: "type", title: "Type" },
            { data: "contact", title: "Contact" },
            {
                "data": null,
                "title": "Status",
                render: function (data, type, row) {
                    let statusClass = "";
                    let status = "";
                    
                    if (data.status == "SUCCESS") {
                        statusClass = "success";
                        status = "Success";
                    } else if (data.status == "SENDING") {
                        statusClass = "primary";
                        status = "Sending";
                    } else {
                        statusClass = "danger";
                        status = data.status;
                    }

                    var details = "<h5 style='margin-top:0px !important; margin-bottom:0px !important;'><span class='label label-" + statusClass +
                        " status' style='text-transform:capitalize;'>" + (status).toLowerCase() + "</span></h5>";
                    return details;
                }
            },
            
            {
                "data": null,
                "title": "Sent",
                render: function (data, type, row) {
                    var date = new Date(data.dateTimeSent);

                    if (date.getFullYear() == 2000) {
                        return "Not sent yet.";
                    } else {
                        return formatDate(date, "d MMM yyyy dddd h:mmtt");
                    }
                }
            },
            {
                "data": null,
                "title": "Batch Status",
                render: function (data, type, row) {
                    let statusClass = "";
                    let status = "";

                    if (data.batchStatus == "COMPLETED") {
                        statusClass = "success";
                        status = "Completed";
                    } else if (data.batchStatus == "PROCESSING") {
                        statusClass = "primary";
                        status = "Processing";
                    }

                    var details = "<h5 style='margin-top:0px !important; margin-bottom:0px !important;'><span class='label label-" + statusClass +
                        " status' style='text-transform:capitalize;'>" + (status).toLowerCase() + "</span></h5>";
                    return details;
                }
            },
        ],
        "initComplete": function () {
            $("#dtMailLogs").setCustomSearch({

                onchange: "true"
            });
        }
    });
}

function TrackMailStatus() {
    $.ajax({
        type: "GET",
        url: $("#dtMailLogs").attr("urlstatus"),
        success: function (data) {
            if (data.Success == 0 && data.Failed == 0 && data.Sending == 0) {
                if (isFirstCall == false) {
                    bar.animate(1);
                    $("#statusMsg").html("All mails are processed now.");
                    
                } else {
                    bar.animate(0);
                    $("#statusMsg").html("There are no pending mails now.");
                }
            } else {
                isFirstCall = false;
                $("#statusMsg").html("Sending mails ...");
                var progressPercentage = ((data.Success + data.Failed) / (data.Success + data.Failed + data.Sending));
                bar.animate(progressPercentage);
            }
        },
        complete: function (data) {
            setTimeout(TrackMailStatus, 1000);
        }
    })
}

$(document).ready(function () {
    $("#dtrSearch").daterangepicker(null, function (a, b, c) { console.log(a.toISOString(), b.toISOString(), c) })

    var daterange = ($("#dtrSearch").val()).split(" - ");
    dateFrom = daterange[0];
    dateTo = daterange[1];

    $("#btnSearch").click(function () {
        var daterange = ($("#dtrSearch").val()).split(" - ");
        dateFrom = daterange[0];
        dateTo = daterange[1];
        var range = getMonthsBetween(
            new Date(dateFrom),
            new Date(dateTo)
        )

        if (range >= 1) {
            swal({
                title: "Date range for searching is limited up to one month only.",
                icon: "error"
            })

            return false;
        } else {
            LoadTable();
        }
    })

    LoadTable();

    bar = new ProgressBar.Line(containerprogress, {
        strokeWidth: 1,
        easing: 'easeInOut',
        duration: 1400,
        color: '#5CB85C',
        trailColor: '#eee',
        trailWidth: 1,
        svgStyle: { width: '100%', height: '100%', "margin":"5 0 0 0" },
        text: {
            style: {
                color: '#999',
                position: 'relative',
                right: '0',
                top: '0px',
                padding: "0 0 10 0",
                margin: "0 0 10 0",
                transform: null,
                "text-align": "right"
            },
            autoStyleContainer: false
        },
        from: { color: '#FFEA82' },
        to: { color: '#ED6A5A' },
        step: (state, bar) => {
            bar.setText(Math.round(bar.value() * 100) + ' %');
        }
    });

    TrackMailStatus();
})