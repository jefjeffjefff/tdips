﻿$(document).ready(function () {
    $("#dtInvoice").DataTable({
        ajax: {
            url: "/Invoice/GetInvoiceCorrectionList",
            dataType: "json",
            dataSrc: ''
        },
        columns: [
            { data: "OldInvoiceNo", title: "Old Invoice No" },
            { data: "NewInvoiceNo", title: "New Invoice No" },
            { data: "ModifiedBy", title: "Modified By" },
            {
                "data": null,
                "title": "Actions",
                render: function (data, type, row)
                {
                    var details = "<div class='fa-hover'><a onclick='DeleteInvoice(" + data.Id + 
                        ")' href='#'><i class='fa fa-trash'></i> Delete</a></div>";
                    
                    return details;
                }
            }
        ]
    })

    $("#btnUploadExcel").click(function () {
        if ($("#frmUploadExcel")[0].checkValidity() == false) {
            if (isIE || isEdge) {
                swal({
                    title: "Please select an excel file first before importing.",
                    icon: "error"
                })
            } else if (isChrome) {
                $("#frmUploadExcel")[0].reportValidity()
            }
        } else {
            HoldOn.open({
                theme: "sk-circle",
                message: "Uploading an excel file.."
            })

            var excelFile = new FormData($("#frmUploadExcel")[0]);

            $.ajax({
                url: "/Invoice/UploadInvoiceCorrectionExcel",
                type: 'POST',
                data: excelFile,
                contentType: false,
                processData: false,
                success: function (data) {
                    HoldOn.close();

                    if (data == "NULL") {
                        swal({
                            title: "Please select an excel file first before importing.",
                            icon: "error"
                        })
                    } else if (data == "INVALID FILE") {
                        swal({
                            title: "Invalid excel file.",
                            icon: "error"
                        })
                    } else if (data == "EMPTY") {
                        swal({
                            title: "Excel file seems empty or in different format.",
                            icon: "error"
                        })
                    } else {
                        if (data.RowsAdded == 0 && data.RowsUpdated == 0) {
                            swal({
                                title: "Oops ..",
                                icon: "error",
                                text: "Excel file seems empty or in different format."
                            })
                        } else {
                            swal({
                                title: "Success",
                                icon: "success",
                                text: "Excel file is uploaded successfully. " + data.RowsAdded + 
                                " invoices are added, " + data.RowsUpdated + " invoices are updated."
                            }).then(function () {
                                window.location.reload();
                            })
                        }
                    }
                },
                error: function () {
                    HoldOn.close();
                    swal({
                        title: "There is an error processing your request to the server.",
                        icon: "error",
                    });
                }
            });
        }
    })
})

function DeleteInvoice(invoiceId) {
    swal({
        title: "Are you sure that you want to delete the invoice?",
        text: "Once deleted, this can't be undone!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((yes) => {
            if (yes) {
                $.ajax({
                    type: "POST",
                    url: "/Invoice/DeleteInvoiceCorrection?invoiceId=" + invoiceId,
                    success: function (data) {
                        if (data == "SUCCESS") {
                            swal({
                                title: "Success",
                                text: "Invoice is deleted successfully.",
                                icon: "success"
                            }).then(function () {
                                document.location.reload();
                            });
                        }
                    }
                })
            }
        });
}