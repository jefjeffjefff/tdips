﻿var id = 0;
var employee_No;

$("#dtEmployee").setCustomSearchInput();

$(document).ready(function () {
    if (isEdge || isIE) {
        let width = $("input[type=file]").width();

        $("input[type=file]").css("display", "none");


        $("input[type=file]").after("<input class='form-control file-upload'" +
            "style='height:31px;width:" + width + "px;' value='Choose a file' readonly></input>");

        $(".file-upload").click(function () {
            $("input[type=file]").trigger("click");
        })

        $("input[type=file]").change(function () {
            let file = $("input[type=file]").val().split("\\");

            if (file.length > 0) {
                $(".file-upload").val(file[file.length - 1]);
            } else {
                $(".file-upload").val($("input[type=file]").val());
            }
        })
    }

    dtEmployee = $('#dtEmployee').DataTable({
        ajax: {
            url: $('#dtEmployee').attr("url"),
            datatype: 'json',
            dataSrc: '',

        },
        initComplete: function () {
            $("#dtEmployee").setCustomSearch({
                exemption: [4]
            });
        },

        "columns": [
            {
                "data": null,
                "title": "Employee Number",
                render: function (data, type, row) {
                    var details = data.employee_no
                    return details;
                }
            },
            {
                "data": null,
                "title": "Employee Name",
                render: function (data, type, row) {
                    var details = "<text class='details vendorCode'>" + data.employeeName + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Email",
                render: function (data, type, row) {
                    var details = "<text class='details vendorCode'>" + data.email + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Account #",
                render: function (data, type, row) {
                    var details = "<text class='details vendorCode'>" + data.accountNo + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Actions",
                render: function (data, type, row) {
                    var details = "<div class='fa-hover'><a href='#' onclick=\"EditEmployee('" + data.id + "','" + data.employee_no +
                        "','" + data.employeeName + "','" + data.email + "','" + data.accountNo + "')\" class='btnEdit'><i class='fa fa-edit'></i> Edit</a></div>";

                    details += "<div class='fa-hover'><a href='#' onclick=\"DeleteEmployee(" + data.id + ")\"><i class='fa fa-trash'></i> Delete</a></div>";
                    return details;
                }
            }
        ],
    })
    
    $(".btnSubmit").click(function (e) {
        let employee_no = $("#txtEmployeeNumber").val();
        let employeeName = $("#txtEmployeeName").val();
        let email = $("#txtEmail").val();
        let accountNo = $("#txtAccountNo").val();
        let isEmployeeNumChanged = !(employee_no == employee_No);

        console.log(employee_no);
        console.log(employee_No);

        if (employee_no == "" || employeeName == "" || email == "" || accountNo == "") {
            swal({
                title: "Please complete required inputs before saving.",
                icon: "error"
            });
        } else if (validateEmail($("#txtEmail").val()) == false) {
            swal({
                title: "Please provide a valid email address.",
                icon: "error"
            });
        } else {
            HoldOn.open({
                theme: "sk-circle",
                text: "Save the record..."
            })

            $.ajax({
                type: "Post",
                url: $("#frmEmployee").attr("url"),
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify({
                    id: id,
                    employee_no: employee_no,
                    employeeName: employeeName,
                    email: email,
                    accountNo: accountNo,
                    isEmployeeNumChanged: isEmployeeNumChanged
                }),
                success: function (data) {
                    HoldOn.close();
                    if (data == "SUCCESS") {
                        swal({
                            title: id == 0 ? "New employee is successfully added!" : "Employee details successfully updated!",
                            icon: "success"
                        }).then(function () {
                            document.location.reload();
                        });
                    } else if (data == "EMPLOYEE ALREADY EXISTS") {
                        swal({
                            title: "Oops",
                            text: "Employee number already exists.",
                            icon: "error"
                        });
                    } else {
                        swal({
                            title: "Incomplete/Invalid Request",
                            text: "Please make sure that required inputs are filled.",
                            icon: "error"
                        });
                    }
                },
                error: function () {
                    swal({
                        title: "There is an error occured adding a new vendor.",
                        icon: "error"
                    });
                }
            })
        }
    })

    $("#btnUploadExcel").click(function () {
        if ($("#frmUploadExcel")[0].checkValidity() == false) {
            $("#frmUploadExcel")[0].reportValidity()
        } else {
            HoldOn.open({
                theme: "sk-circle",
                message: "Uploading an excel file.."
            })

            var excelFile = new FormData($("#frmUploadExcel")[0]);

            $.ajax({
                url: "/SEAN/ImportEmployeeExcel",
                type: 'POST',
                data: excelFile,
                contentType: false,
                processData: false,
                success: function (data) {
                    HoldOn.close();
                     if (data == "NULL") {
                        swal({
                            title: "Please select an excel file first before importing.",
                            icon: "error"
                        })
                    } else if (data == "INVALID FILE") {
                        swal({
                            title: "Invalid excel file.",
                            icon: "error"
                        })
                    } else if (data == "EMPTY") {
                        swal({
                            title: "Excel file seems empty or in different format.",
                            icon: "error"
                        })
                     } else if (data.Status == "SUCCESS") {
                         swal({
                             title: "Upload Success",
                             text: `${data.Added} new employees are added. ${data.Updated} employee records are updated.`,
                             icon: "success"
                         }).then(function () {
                             document.location.reload();
                         });
                     }
                },
                error: function () {
                    HoldOn.close();
                    swal({
                        title: "There is an error processing your request to the server.",
                        icon: "error",
                    });
                }
            });
        }
    })
})

function AddEmployee() {
    id = 0;
    $("#modalEmployee .title").html("Add a New Employee");
    $("#modalEmployee").modal();
}

function DeleteEmployee(employeeId) {
    swal({
        title: "Are you sure that you want to delete the employee record?",
        text: "Once deleted, this can't be undone!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((confirmed) => {
        if (confirmed) {
            $.ajax({
                url: "/SEAN/DeleteEmployee?employeeId=" + employeeId,
                success: function (data) {
                    if (data == "SUCCESS") {
                        swal({
                            title: "Success",
                            text: "The employee record is deleted successfully.",
                            icon: "success"
                        }).then(function () {
                            document.location.reload();
                        });
                    } else {
                        swal({
                            title: "Failed. Please retry.",
                            icon: "error"
                        })
                    }
                }
            })
        }
    });
}

function EditEmployee(eid,employee_no,employeeName,email,accountNo) {
    id = eid;
    employee_No = employee_no;
    
    $("#modalEmployee .title").html("Edit Employee Record");
    $("#txtEmployeeNumber").val(employee_no);
    $("#txtEmployeeName").val(employeeName);
    $("#txtEmail").val(email);
    $("#txtAccountNo").val(accountNo);
    $("#modalEmployee").modal();
}