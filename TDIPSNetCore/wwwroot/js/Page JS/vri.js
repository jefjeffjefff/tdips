﻿//http://mvcworld.blogspot.com/2013/06/convert-json-date-to-javascript-date.html
var batchId = 0;
var totalAmount = 0.0;
var acceptVerificationRequestURL = "";
var declineVerificationRequestURL = "";
var isTreasuryManagerOnLeaveURL = "";
var acceptApprovalRequestURL = "";


$(document).ready(function () {
    var dtRequest = $('#dtRequest').DataTable({
        "initComplete": function (settings, json) {
            $(".btnView").click(function () {
                batchId = $(this).attr("batchId");
                $("#dtVoucher").DataTable().destroy();
                $("#modalViewRequest").modal();
                $("#dtVoucher").setCustomSearchInput();
                var dtVoucher = $('#dtVoucher').DataTable({
                    destroy: true,
                    "initComplete": function (settings, json) {
                        $("#dtVoucher").setCustomSearch({
                        });
                        totalAmount = 0.0;
                        var totalAmount = 0.0;
                        var itemAmountArr = [];
                        var bankArray = [];
                        var entityArray = [];

                        $(".container_item").html("")

                        dtVoucher.$("tr").each(function (index) {
                            var amount = removeCommas(($(this).find(".amount")).html())
                            let entity = $(this).find(".dataAreaId").html();
                            let bank = $(this).find(".bankName").html();

                            itemAmountArr.push({
                                entity: entity,
                                bankName: bank,
                                amount: amount
                            })

                            if (bankArray.indexOf(bank) == -1) {
                                bankArray.push(bank)
                            }

                            if (entityArray.indexOf(entity) == -1) {
                                entityArray.push(entity);
                            }

                            totalAmount += parseFloat(amount);
                        });

                        $.each(entityArray, function (i, item) {
                            let entity = item;

                            let itemArray = $.grep(itemAmountArr, function (n, i) {
                                return n.entity == entity;
                            })

                            let total = 0;
                            let subitems = [];

                            $.each(itemArray, function (i, item) {
                                let index = GetIndexInJsonByKeyVal(subitems, "bankName", item.bankName);

                                if (index >= 0) {
                                    subitems[index].amount = parseFloat(subitems[index].amount) + parseFloat(item.amount);
                                } else {
                                    subitems.push({
                                        bankName: item.bankName,
                                        amount: item.amount
                                    })
                                }

                                total += parseFloat(item.amount);
                            })

                            $(".by_entity .container_item").append('<div class="summary_item ' + entity + '">' +
                                                        '<div class="item_header">' +
                                                            '<label>' + entity + ' (₱' + parseMoney(total) + ')</label>' +
                                                        '</div>' +
                                                        '<div class="container_sub_item">' +
                                                        '</div>' +
                                                    '</div>')


                            $.each(subitems, function (i, item) {
                                $(".summary_item." + entity + " .container_sub_item").append('<div class="sub-item">' +
                                                            '<label>' + item.bankName + ' - ₱' + parseMoney(item.amount) +
                                                            '</label></div>')
                            })
                        })

                        $.each(bankArray, function (i, item) {
                            let bank = item;

                            let itemArray = $.grep(itemAmountArr, function (n, i) {
                                return n.bankName == bank;
                            })

                            let total = 0;
                            let subitems = [];

                            $.each(itemArray, function (i, item) {
                                let index = GetIndexInJsonByKeyVal(subitems, "entity", item.entity);

                                if (index >= 0) {
                                    subitems[index].amount = parseFloat(subitems[index].amount) + parseFloat(item.amount);
                                } else {
                                    subitems.push({
                                        entity: item.entity,
                                        amount: item.amount
                                    })
                                }

                                total += parseFloat(item.amount);
                            })


                            $(".by_bank .container_item").append('<div class="summary_item ' + bank + '">' +
                                                        '<div class="item_header">' +
                                                            '<label>' + bank + ' (₱' + parseMoney(total) + ')</label>' +
                                                        '</div>' +
                                                        '<div class="container_sub_item">' +
                                                        '</div>' +
                                                    '</div>')


                            $.each(subitems, function (i, item) {
                                $(".summary_item." + bank + " .container_sub_item").append('<div class="sub-item">' +
                                                            '<label>' + item.entity + ' - ₱' + parseMoney(item.amount) +
                                                            '</label></div>')
                            })
                        })

                        $("#btnRequest").html("<i class='fa fa-external-link'></i> Request <u>₱" + parseMoney(totalAmount) + "</u> Worth of Vouchers");
                        $("#lblTotal").html("Total : ₱" + parseMoney(totalAmount))
                    },
                    ajax: {
                        url: $('#dtVoucher').attr("url") + "?batchId=" + batchId,
                        datatype: 'json',
                        dataSrc: '',

                    },
                    "columns": [
                    { data: "vendorCode", title: "Vendor Code" },
                    { data: "companyName", title: "Company Name" },
                    {
                        "data": null,
                        "title": "Entity",
                        render: function (data, type, row) {
                            var details = "<text class='dataAreaId " + data.entity + "'>" + data.entity + "</text>";
                            return details;
                        }
                    },
                    {
                        "data": null,
                        "title": "Bank",
                        render: function (data, type, row) {
                            var details = "<text class='bankName'>" + data.bank + "</text>";
                            return details;
                        }
                    },
                    {
                        "data": null,
                        "title": "Amount",
                        render: function (data, type, row) {
                            totalAmount += data.amount;
                            var details = "<span style='line-height:0px !important;'>&#8369;</span><text class='amount'>" + parseMoney(data.amount) + "</text>";
                            return details;
                        }
                    },
                    {
                        "data": null,
                        "title": "Releasing Date",
                        render: function (data, type, row) {
                            if (data.releasingDate == null) {
                                details == ""
                            } else {
                                var date = parseJsonDate(data.releasingDate);   
                                details = date.format("mmm dd yyyy");
                            }

                            return details;
                        }
                    },
                    ],
                    "createdRow": function (row, data, dataIndex) {
                        if ($(row).find('.dataAreaId').html() == "SPAV") {
                            $(row).addClass("spavi_row");
                        } else if ($(row).find('.dataAreaId').html() == "SPCI") {
                            $(row).addClass("spci_row");
                        } else if ($(row).find('.dataAreaId').html() == "WBHI") {
                            $(row).addClass("wbhi_row");
                        } else if ($(row).find('.dataAreaId').html() == "NAF") {
                            $(row).addClass("naf_row");
                        } else if ($(row).find('.dataAreaId').html() == "SIL") {
                            $(row).addClass("sil_row");
                        } else if ($(row).find('.dataAreaId').html() == "DBE") {
                            $(row).addClass("dbe_row");
                        }
                    },
                })

            })
        },
        ajax: {
            url: $('#dtRequest').attr("url") + "?status=FOR VERIFICATION",
            datatype: 'json',
            dataSrc: '',

        },
        "columns": [
        {
            "data": null,
            "title": "Date and Time of Request",
            render: function (data, type, row) {
                if (data.logDate == null) {
                    details == ""
                } else {
                    var date = parseJsonDate(data.logDate);

                    details = date.format("mmm dd yyyy") + "  " + date.format("shortTime");
                }

                return details;
            }
        },
        { data: "userId", title: "Requested By" },
        { data: "note", title: "Note" },
        {
            "data": null,
            "title": "Action",
            render: function (data, type, row) {
                var details = "<div class='fa-hover'><a href='#' batchId='" + data.batchId + "' class='btnView'><i class='fa fa-folder-open-o'></i> View</a></div>";
                return details;
            }
        },
        ]
    })

    $("#btnAccept").click(function () {
        $("#modalDialog .dialog-message").html("Are you sure that you want to <text class='text-success'><u>VERIFY</u></text> the requested records?")
        $("#btnAcceptDeclineRequest").attr("function", "ACCEPT");
        $("#btnAcceptDeclineRequest").html("Proceed and Accept Request");
        $("#btnAcceptDeclineRequest").removeClass("btn-danger").addClass("btn-primary");
        $("#modalViewRequest").modal('toggle');
        $("#modalDialog").modal();
    })

    $("#btnDecline").click(function () {
        $("#modalDialog .dialog-message").html("Are you sure that you want to <text class='text-danger'><u>DECLINE</u></text> the requested records?")
        $("#btnAcceptDeclineRequest").attr("function", "DECLINE")
        $("#btnAcceptDeclineRequest").html("Proceed and Decline Request");
        $("#btnAcceptDeclineRequest").removeClass("btn-primary").addClass("btn-danger");
        $("#modalViewRequest").modal('toggle');
        $("#modalDialog").modal();
    })

    $("#btnClose").click(function () {
        $("#modalDialog").modal('toggle');
        $("#modalViewRequest").modal();
    })

    $("#btnAcceptDeclineRequest").click(function () {
        HoldOn.open({
            theme: "sk-circle",
            message: "Processing your request..."
        });

        let url = '';
        let msg = ''

        if ($(this).attr("function").toUpperCase() == "ACCEPT") {
            $.ajax({
                type: "GET",
                async: false,
                url: isTreasuryManagerOnLeaveURL,
                success: function (data) {
                    if (data == "FALSE") {
                        url = acceptVerificationRequestURL + "?requestBatchId=" + batchId + "&note=" + $("#txtNotes").val();
                        msg = 'Accepted';
                        

                        SendRequest(url, "Verification Request Successfully Accepted!")
                    } else {
                        HoldOn.close();

                        swal({
                            title: "Treasury Manager is on Leave",
                            text: "Selected vouchers will que directly to the releasing page since the treasury manager is on leave. Do you still want to continue?",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        })
                        .then((willAccept) => {
                            if (willAccept) {
                                HoldOn.open({
                                    theme: "sk-circle",
                                    message: "Processing your request..."
                                });

                                url = acceptApprovalRequestURL + "?requestBatchId=" + batchId + "&note=" + $("#txtNotes").val();
                                msg = 'Accepted';
                                SendRequest(url, "Verification/Approval Request Successfully Accepted!")
                            }
                        });
                    }
                }
            })

            
        } else {
            //Require user to put a note why he/she is declining the request.

            if ($("#txtNotes").val() == "") {
                HoldOn.close();

                swal({
                    title: "Note is Required",
                    text: "A note is required when you are declining the request.",
                    icon: "error"
                })

                return false;
            }

            url = declineVerificationRequestURL + "?declinedBatchId=" + batchId + "&note=" + $("#txtNotes").val();
            SendRequest(url, "Verification Request Successfully Declined!")
        }
    })
})

function SendRequest(url, msg) {
    $.ajax({
        url: url,
        type: 'GET',
        success: function (data) {
            HoldOn.close()
            if (data == "SUCCESS") {
                swal({
                    title: msg,
                    icon: "success"
                }).then(function () {
                    document.location.reload();
                });
            } else {
                swal({
                    title: "There is an error processing your request.",
                    icon: "error"
                })
            }
        },
        error: function () {
            HoldOn.close();
            swal({
                title: "There is an error processing your request. Please try again later.",
                icon: "error"
            })
        },
    })
}


function GetTotal(dtVoucher) {

}

function GetIndexInJsonByKeyVal(json, key, value) {
    let index = -1;

    $.each(json, function (i, item) {
        if (item[key] == value) {

            index = i;
        }
    });

    return index;
}

