﻿var voucherId = "";

$(document).ready(function () {
    $("#dtPayment").setCustomSearchInput();
    dtPayment = $('#dtPayment').DataTable({
        initComplete: function () {
            $("#dtPayment").setCustomSearch({
                exemption: [8],
                onchange: "false"
            });
        },
        ajax: {
            url: $('#dtPayment').attr("url"),  //"?status=ACCEPTED&status=REQUESTED FOR HOLD&status=REQUESTED FOR UNHOLD",
            datatype: 'json',
            dataSrc: 'dataList',
            data: function (d) {
                d.vendorcode = $("#1").val(),
                    d.companyName = $("#2").val(),
                    d.bank = $("#3").val(),
                    d.entity = $("#0").val(),
                    d.voucherId = $("#4").val(),
                    d.amount = removeCommas($("#5").val()),
                    d.payment = $("#6").val(),
                    d.date = $("#8").val(),
                    d.status = "REQUESTED FOR HOLD REQUESTED FOR UNHOLD"
            },
            beforeSend: function () {
                $('#dtPayment > tbody').html(
                    '<tr class="odd customized_loading">' +
                    '<td valign="top" colspan="' + $("#dtPayment thead th").length + '" class="dataTables_empty">Loading&hellip;</td>' +
                    '</tr>'
                );
            }
        },
        "columns": [
            {
                "data": "dataAreaId",
                "title": "Entity",
                render: function (data, type, row) {
                    var details = "<text class='dataAreaId " + data + "'>" + data + "</text>";
                    return details;
                }
            },
            { data: "vendorCode", title: "Vendor Code" },
            {
                "data": "voucherId",
                "title": "Voucher ID",
                render: function (data, type, row) {
                    var details = "<text class='voucherId'>" + data + "</text>";
                    return details;
                }
            },
            {
                "data": "amount",
                "title": "Amount",
                render: function (data, type, row) {
                    var details = "<span>&#8369;</span><text class='amount'>" + parseMoney(data) + "</text>"
                    return details;
                }
            },
            {
                "data": null,
                "title": "Request Date",
                render: function (data, type, row) {
                    var details = parseJsonDate(data.requestDetails.logdate);
                    if (details == "") return "";
                    return $.date(details);
                }
            },
            {
                "data": null,
                "title": "Requested By",
                render: function (data, type, row) {
                    var details = data.requestDetails.userId;
                    return details;
                }
            },
            {
                "data": null,
                "title": "Note",
                render: function (data, type, row) {
                    var details = data.requestDetails.note;
                    return details;
                }
            },
            {
                "data": 'status',
                "title": "Status",
                render: function (data, type, row) {
                    return StatusLabel(data);
                }
            },
            {
                "data": null,
                "title": "Action",
                render: function (data, type, row) {
                    var details = "";
                    if (data.status == "REQUESTED FOR HOLD") {
                        details = "<div class='fa-hover'><a href='#' onclick=\"AcceptRequest('" + data.voucherId + "')\"><i class='fa fa-check'></i> Accept</a></div>" +
                            "<div style='margin-top:3px;' class='fa-hover'><a href='#' onclick=\"DeclineHold('"+ data.voucherId +"')\"><i class='fa fa-remove'></i> Decline</a></div>";
                    } else if (data.status == "REQUESTED FOR UNHOLD") {
                        details = "<div class='fa-hover'><a href='#' onclick=\"Unhold('" + data.voucherId + "')\"><i class='fa fa-check-circle'></i> Unhold</a></div>" +
                            "<div style='margin-top:3px;' class='fa-hover'><a href='#' onclick=\"DeclineUnhold('" + data.voucherId +"')\"><i class='fa fa-remove'></i> Decline</a></div>";
                    }
                    return details;
                }
            },
        ],
        "columnDefs": [{
            className: 'row_action',
            targets: [8]
        },
            {
                "targets": [4,5,6],
                "orderable": false
            }
        ],
        "createdRow": function (row, data, dataIndex) {
            if ($(row).find('.dataAreaId').html() == "SPAV") {
                $(row).addClass("spavi_row");
            } else if ($(row).find('.dataAreaId').html() == "SPCI") {
                $(row).addClass("spci_row");
            } else if ($(row).find('.dataAreaId').html() == "WBHI") {
                $(row).addClass("wbhi_row");
            } else if ($(row).find('.dataAreaId').html() == "NAF") {
                $(row).addClass("naf_row");
            } else if ($(row).find('.dataAreaId').html() == "SIL") {
                $(row).addClass("sil_row");
            } else if ($(row).find('.dataAreaId').html() == "DBE") {
                $(row).addClass("dbe_row");
            }
        },
        filter: false,
        language: {
            "lengthMenu": "_MENU_",
            "processing": ""
        },
        lengthMenu: [
            [5, 10, 15, 20, 100],
            [5, 10, 15, 20, 100] // change per page values here
        ],
        orderCellsTop: true,
        pageLength: 10,
        processing: false,
        responsive: true,
        serverSide: true,
        stateSave: false
    })
})

function AcceptRequest(voucherId) {
    swal({
        title: "Are you sure that you want to accept the hold request?",
        icon: "warning",
        buttons: true,
    })
    .then((ok) => {
        if (ok) {
            $.ajax({
                url: $("#dtPayment").attr("accepturl") + "?voucherId=" + voucherId,
                success: function (data) {
                    if (data == "SUCCESS") {
                        swal({
                            title: "Voucher is put on hold successfully.",
                            icon: "success"
                        }).then(function () {
                            document.location.reload();
                        });
                    } else {
                        swal({
                            title: "There is an error processing your request.",
                            icon: "error"
                        })
                    }
                }
            })
        }
    })
}

function DeclineHold(voucherId) {
    swal({
        title: "Are you sure that you want to decline the request?",
        icon: "warning",
        buttons: true,
    })
        .then((ok) => {
            if (ok) {
                $.ajax({
                    url: $("#dtPayment").attr("declineholdurl") + "?voucherId=" + voucherId,
                    success: function (data) {
                        if (data == "SUCCESS") {
                            swal({
                                title: "Hold request is declined successfully.",
                                icon: "success"
                            }).then(function () {
                                document.location.reload();
                            });
                        } else {
                            swal({
                                title: "There is an error processing your request.",
                                icon: "error"
                            })
                        }
                    }
                })
            }
        })
}

function DeclineUnhold(voucherId) {
    swal({
        title: "Are you sure that you want to decline the request?",
        icon: "warning",
        buttons: true,
    })
        .then((ok) => {
            if (ok) {
                $.ajax({
                    url: $("#dtPayment").attr("declineunholdurl") + "?voucherId=" + voucherId,
                    success: function (data) {
                        if (data == "SUCCESS") {
                            swal({
                                title: "Unhold request is declined successfully.",
                                icon: "success"
                            }).then(function () {
                                document.location.reload();
                            });
                        } else {
                            swal({
                                title: "There is an error processing your request.",
                                icon: "error"
                            })
                        }
                    }
                })
            }
        })
}

function Unhold(voucherId) {
    $.ajax({
        url: $("#dtPayment").attr("prevstatusurl") + "?voucherId=" + voucherId,
        success: function (status) {
            swal({
                title: "Are you sure that you want to unhold the voucher? The voucher status will be back to " + status.toLowerCase() + " once unhold.",
                icon: "warning",
                buttons: true,
            })
            .then((ok) => {
                if (ok) {
                    $.ajax({
                        url: $("#dtPayment").attr("unholdurl") + "?voucherId=" + voucherId,
                        success: function (data) {
                            if (data == "SUCCESS") {
                                swal({
                                    title: "Voucher is unhold successfully.",
                                    icon: "success"
                                }).then(function () {
                                    document.location.reload();
                                });
                            } else {
                                swal({
                                    title: "There is an error processing your request.",
                                    icon: "error"
                                })
                            }
                        }
                    })
                }
            })
        }
    })
}
