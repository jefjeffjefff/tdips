﻿var totalAmount = 0.0;
var dtQueue;
var isInQueue = true;
var queueId = 0;
var queueArr;
var currentdata


$(document).ready(function () {
    dtQueue = $('#dtQueue').DataTable({
        "order": [[1, "asc"], [0, "desc"]],
        ajax: {
            url: $('#dtQueue').attr("url") + "?status=QUEUEING",
            datatype: 'json',
            dataSrc: '',
        },
        "columns": [
            { data: "queueNumber", title: "Queue Number" },
            { data: "queueType", title: "Queue Type" },
            {
                "data": null,
                "title": "Company Name",
                render: function (data, type, row) {
                    var details = "<text class='companyName'>" + data.companyName + "</text>"
                    return details;
                }
            },
            {
                "data": null,
                "title": "Collector Name",
                render: function (data, type, row) {
                    var details = "<text class='collectorName'>" + data.collectorName + "</text>"
                    return details;
                }
            },
            { data: "collectorType", title: "Collector Type" },
            { data: "thirdPartyCompany", title: "Third Party Company" },
            {
                "data": null,
                "title": "Action",
                render: function (data, type, row) {
                    var details = "<div class='fa-hover'><a href='#' onclick='LoadCompanyVouchers(" + ignoreSingleQuote(data) + ")' vendorCode='" + data.vendorCode + "' class='btnRelease'><i class='fa fa-level-up'></i> Release</a></div>" +
                        "<div style='margin-top:3px;' class='fa-hover'><a href='#' onclick='Dequeue(" + data.queueId + ")' vendorCode='" + data.vendorCode + "' class='btnRelease'><i class='fa fa-remove'></i> Dequeue</a></div>";
                    return details;
                }
            },
        ],
        "drawCallback": function (data) {
            queueArr = data.json;
            //do whatever  
        },
        "initComplete": function (data) {
            queueArr = data.json;
        },
        "columnDefs": [{
            className: 'row_action',
            targets: [6]
        }
        ],
        "createdRow": function (row, data, dataIndex) {
            if (data.queueType == "PRIORITY") {
                $(row).addClass("priority_row");
            } else if (data.queueType == "REGULAR") {
                $(row).addClass("regular_row");
            }
        },
    })


    dtDequeue = $('#dtDequeue').DataTable({
        ajax: {
            url: $('#dtQueue').attr("url") + "?status=DEQUEUED",
            datatype: 'json',
            dataSrc: '',
        },
        "columns": [
            { data: "queueNumber", title: "Queue Number" },
            { data: "queueType", title: "Queue Type" },
            { data: "companyName", title: "Company Name" },
            { data: "collectorName", title: "Collector Name" },
            { data: "collectorType", title: "Collector Type" },
            { data: "thirdPartyCompany", title: "Third Party Company" },
            {
                "data": null,
                "title": "Action",
                render: function (data, type, row) {
                    var details = "<div class='fa-hover'><a href='#' onclick='LoadCompanyVouchers(" + ignoreSingleQuote(data) + ")' vendorCode='" + data.vendorCode + "' class='btnRelease'><i class='fa fa-level-up'></i> Release</a></div>" +
                        "<div style='margin-top:3px;' class='fa-hover'><a href='#' onclick='Queue(" + data.queueId + ")' vendorCode='" + data.vendorCode + "' class='btnRelease'><i class='fa fa-history'></i> Queue</a></div>";
                    return details;
                }
            },
        ],
        "columnDefs": [{
            className: 'row_action',
            targets: [6]
        }
        ]
    })

    $(".btnCall").click(function () {
        var queueType = $(this).attr("queueType");
        var btntext = "";

        if (queueType == "REGULAR") {
            btntext = "Call Regular Number";
        } else {
            btntext = "Call Priority Number";
        }

        var firstQueue = GetFirstQueue(queueType);

        if (firstQueue == null) {
            swal({
                title: "There is no current queue for " + queueType.toLowerCase() + " number",
                icon: "error"
            })
        } else {
            $(".btnCall[queueType='" + queueType + "']").html('<i class="fa fa-spinner fa-spin"></i> ' + btntext);

            let queueNumber = firstQueue.queueNumber;
            let companyName = firstQueue.companyName;
            let collectorName = firstQueue.collectorName;

            let text = "Calling out our business partner," + queueType + " number " + queueNumber + ". " + companyName + ". Please proceed to the window. ";
            text = text.replace("&", "and");
            $.ajax({
                type: "POST",
                url: $("#container_audio").attr("url") + "?text=" + text + "&companyName=" + companyName +
                    "&collectorName=" + collectorName + "&queueNumber=" + queueNumber + "&queueType=" + queueType + "&repeat=0",
                success: function () {
                    $(".btnCall[queueType='" + queueType + "']").html(btntext);
                }
            })
        };
    })

    setInterval(function () {
        dtQueue.ajax.reload(null, false);
    }, 3000);
})


function GetFirstQueue(queueType) {
    let queue = null;

    $.each(queueArr, function (i, item) {
        if (item.queueType == queueType) {
            queue = item;
            return false;
        }
    })

    return queue;
}

function Dequeue(queueId) {
    swal({
        title: "Are you sure that you want to dequeue the vendor??",
        icon: "warning",
        buttons: true,
    })
        .then((dequeue) => {
            if (dequeue) {
                $.ajax({
                    url: $("#dtQueue").attr("dequeueurl") + "?queueId=" + queueId,
                    success: function (data) {
                        swal({
                            title: "Vendor is dequeued",
                            icon: "success"
                        }).then(function () {
                            document.location.reload();
                        });
                    }
                })
            }
        })
}

function Queue(queueId) {
    swal({
        title: "Are you sure that you want to put the vendor back in queue?",
        icon: "warning",
        buttons: true,
    })
        .then((dequeue) => {
            if (dequeue) {
                $.ajax({
                    url: $("#dtQueue").attr("queueurl") + "?queueId=" + queueId,
                    success: function (data) {
                        swal({
                            title: "Vendor is in queue again.",
                            icon: "success"
                        }).then(function () {
                            document.location.reload();
                        });
                    }
                })
            }
        })
}

function LoadCompanyVouchers(json) {
    currentdata = json;
    queueId = json.queueId;
    isInQueue = true;

    if (isInQueue) {
        $(".container_collector_details").css("display", "none");
    } else {
        $(".container_collector_details").css("display", "block");
    }

    $("#dtVoucher").DataTable().destroy();

    $("#myModalLabel strong").html(json.companyName);

    $("#modalViewVouchers").modal();

    var dtVoucher = $('#dtVoucher').DataTable({
        "destroy": true,
        "initComplete": function (settings, json) {
            $("#btnRelease").off("click").click(function () {
                HoldOn.open({
                    theme: "sk-circle",
                    message: "Releasing Vouchers ..."
                })

                let parameter;
                let hasError = false;
                let voucherArray = [];
                
                dtVoucher.$("tr").each(function (index) {
                    let receiptNumber = $(this).find(".txtReceiptNumber").val();
                    let receiptType = $(this).find(".txtReceiptType").val();
                    let voucherId = $(this).find(".voucherId").html();
                    let checkType = $(this).find(".txtCheckType").val();
                    
                    if (receiptType != "DRY") {
                        voucherArray.push({
                            VoucherId: voucherId,
                            ReceiptNo: receiptNumber,
                            ReceiptType: receiptType,
                            CheckType: checkType
                        })
                    }
                });

                if (voucherArray.length == 0) {
                    HoldOn.close();

                    swal({
                        title: "No voucher for release is submitted.",
                        text: "Please fill at least one receipt details for voucher.",
                        icon: "error"
                    })
                } else {
                    parameter = JSON.stringify({
                        r: voucherArray,
                        queueId: queueId,
                        isInQueue: true,
                        c: {
                            CollectorName: currentdata.collectorName,
                            CollectorType: currentdata.collectorType,
                            ThirdPartyCompany: currentdata.thirdPartyCompany,
                        }
                    })

                    $.ajax({
                        type: "Post",
                        url: $("#modalViewVouchers").attr("url"),
                        type: "POST",
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        data: parameter,
                        success: function (data) {
                            HoldOn.close();

                            swal({
                                title: "Vouchers are released successfully!",
                                icon: "success"
                            }).then(function () {
                                document.location.reload();
                            });
                        },
                        error: function () {
                            swal({
                                title: "There is an error occured releasing the voucher.",
                                icon: "error"
                            });
                        }
                    })
                }
            })
        },
        ajax: {
            url: $('#dtVoucher').attr("url") + "?vendorId=" + json.vendorCode,
            datatype: 'json',
            dataSrc: '',

        },
        "columns": [
            {
                "data": null,
                "title": "Voucher",
                render: function (data, type, row) {
                    var details = "<text class='voucherId'>" + data.voucherId + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Entity",
                render: function (data, type, row) {
                    var details = "<text class='dataAreaId " + data.entity + "'>" + data.entity + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Bank",
                render: function (data, type, row) {
                    var details = "<text class='bankName'>" + data.bank + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Amount",
                render: function (data, type, row) {
                    totalAmount += data.amount;
                    var details = "<span>&#8369;</span><text class='amount'>" + parseMoney(data.amount) + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Check",
                render: function (data, type, row) {
                    var details = "<text class='checkNumber'>" + (data.checkNum ? data.checkNum : "") + "</text>"
                    return details;
                }
            },
            {
                "data": null,
                "title": "Date",
                render: function (data, type, row) {
                    var details = parseJsonDate(data.checkDate);
                    if (details == "") return "";
                    return "<text class='checkDate'>" + $.date(details) + "</text>"
                }
            },
            {
                "data": null,
                "title": "Check Type",
                render: function (data, type, row) {
                    var details = "<select class='form-control txtCheckType'>" +
                        "<option value='STANDARD'>Standard</option>" +
                        "<option value='MANAGER'>Manager's Check</option>" +
                        "</select>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Receipt Number",
                render: function (data, type, row) {
                    var details = "<input class='form-control txtReceiptNumber' style='width:130px;' type='text' placeholder='Receipt No' />";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Receipt Type",
                render: function (data, type, row) {
                    var details = "<select class='form-control txtReceiptType'>" +
                        "<option value='OR'>Official Receipt</option>" +
                        "<option value='AR'>Acknowledgement Receipt</option>" +
                        "<option value='PR'>Provisionary Receipt</option>" +
                        "<option value='CR'>Collection Receipt</option>" +
                        "<option value='DRY'>Don't Release Yet</option>" +
                        "</select>";
                    return details;
                }
            },
        ],
        "createdRow": function (row, data, dataIndex) {
            if ($(row).find('.dataAreaId').html() == "SPAV") {
                $(row).addClass("blue_row");
            } else if ($(row).find('.dataAreaId').html() == "SPCI") {
                $(row).addClass("yellow");
            } else if ($(row).find('.dataAreaId').html() == "WBHI") {
                $(row).addClass("green_row");
            } else if ($(row).find('.dataAreaId').html() == "NAF") {
                $(row).addClass("orange_row");
            } else if ($(row).find('.dataAreaId').html() == "SIL") {
                $(row).addClass("red_row");
            } else if ($(row).find('.dataAreaId').html() == "PINK") {
                $(row).addClass("pink_row");
            } else if ($(row).find('.dataAreaId').html() == "DBE") {
                $(row).addClass("dbe_row");
            }
        },
    })
}