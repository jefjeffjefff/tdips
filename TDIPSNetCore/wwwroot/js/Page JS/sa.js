﻿var dtVendor;
var pond;

$(document).ready(function () {
    // Get a reference to the file input element
    const inputElement = document.querySelector('input[type="file"]');

    // Create the FilePond instance
    pond = FilePond.create(inputElement, {
        allowMultiple: true,
        allowReorder: true,
        chunkUploads: true
    });
    
    $('#summernote').summernote({
        placeholder: 'Write something here.',
        tabsize: 2,
        height: 400,
        callbacks: {
            onChange: function () {
                $("#btnSaveDraft").removeClass("btn-success")
                    .addClass("btn-danger")
                    .html("<span class='fa fa-warning'></span> Save to Draft")
                    .css("display", "unset");

            }
        },
        hint: [
            {
                words: ['ReceiverName', 'CompanyName'],
                match: /\B@(\w*)$/,
                search: function (keyword, callback) {
                    callback($.grep(this.words, function (item) {
                        return item.indexOf(keyword) === 0;
                    }));
                },
                content: function (item) {
                    return '@(' + item + ")";
                }
            }
        ]
    });

    $("#txtSubject").keyup(function () {
        $("#btnSaveDraft").removeClass("btn-success")
            .addClass("btn-danger")
            .html("<span class='fa fa-warning'></span> Save to Draft")
            .css("display", "unset");
    })


    $("#dtVendor").setCustomSearchInput();
    dtVendor = $('#dtVendor').DataTable({
        "lengthChange": false,
        "initComplete": function (settings, json) {
            $("#dtVendor").setCustomSearch({
                exemption: [0]
            });
        },
        ajax: {
            url: "/Mail/GetVendorToEmailList",
            datatype: 'json',
            dataSrc: '',
        },
        "columns": [
            {
                "data": null,
                "title": "<th class='text-center'><input type='checkbox' id='cbxSelectAll' /></th>",
                render: function (data, type, row) {
                    var details = "";
                    return details;
                }
            },
            {
                "data": "dataAreaId",
                "title": "Entity",
                render: function (data, type, row) {
                    var details = "<text class='dataAreaId " + data + "'>" + data + "</text>";
                    return details;
                }
            },
            {
                "data": "vendorCode",
                "title": "Vendor Code",
                render: function (data, type, row) {
                    var details = "<text class='details vendorCode' vendorCode='" + data + "' vendorId='" + row.vendorId + "'>" + data + "</text>";
                    return details;
                }
            },
            {
                "data": "companyName",
                "title": "Company Name",
                render: function (data, type, row) {
                    var details = "<text class='companyName'>" + data + "</text>";
                    return details;
                }
            },
        ],
        columnDefs: [{
            orderable: false,
            className: 'select-checkbox',
            targets: [0]
        },
        ],
        select: {
            style: 'multi',
            selector: 'td:first-child'
        },
        order: [
            [1, 'asc']
        ],
        "createdRow": function (row, data, dataIndex) {
            $(row).attr("vendorId", data.vendorID);

            if ($(row).find('.dataAreaId').html() == "SPAV") {
                $(row).addClass("spavi_row");
            } else if ($(row).find('.dataAreaId').html() == "SPCI") {
                $(row).addClass("spci_row");
            } else if ($(row).find('.dataAreaId').html() == "WBHI") {
                $(row).addClass("wbhi_row");
            } else if ($(row).find('.dataAreaId').html() == "NAF") {
                $(row).addClass("naf_row");
            } else if ($(row).find('.dataAreaId').html() == "SIL") {
                $(row).addClass("sil_row");
            } else if ($(row).find('.dataAreaId').html() == "DBE") {
                $(row).addClass("dbe_row");
            }
        }
    })

    dtVendor.on("click", "#cbxSelectAll", function () {
        if ($('#cbxSelectAll').prop("checked") == false) {
            dtVendor.rows().deselect();
        } else {
            dtVendor.rows({ search: 'applied' }).select();
        }
    })
})

function DeleteDraft(draftId, that) {
    swal({
        title: "Are you sure?",
        text: "Are you sure that you want to delete the selected draft?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                type: "POST",
                url: "/Mail/DeleteDraft?draftId=" + draftId ,
                success: function (data) {
                    if (data == "SUCCESS") {
                        swal({
                            title: "Success",
                            text: "Draft is deleted successfully.",
                            icon: "success"
                        }).then(function () {
                            let tr = $(that).closest("tr");
                            $("#dtDrafts").DataTable().row(tr).remove().draw();
                        });
                    }
                }
            })
        }
    });
}

function SendingOptions() {
    let subject = $("#txtSubject").val();
    let msgbody = $('#summernote').summernote('code');

    $("#modalSend").modal();

    if ($.trim(msgbody) == "" || subject == "") {
        swal({
            icon: "error",
            title: "Oops",
            text: "Please fill up the subject and message before sending."
        })
    } else {
        $("#modalSend").modal();
    }
    
}

function LoadDrafts() {
    $("#dtDrafts").DataTable().destroy();

    $("#dtDrafts").setCustomSearchInput();

    $("#dtDrafts").DataTable({
        destory: true,
        "lengthChange": false,
        ajax: {
            url: "/Mail/GetDrafts",
            dataType: "json",
            dataSrc: ""
        },
        columns: [
            {
                title: "Subject",
                data: "Subject"
            },
            {
                title: "Date Created",
                data: null,
                render: function (data) {
                    let date = parseJsonDate(data.DateCreated)
                    return formatDate(date, "d MMM yyyy hh:mmtt");
                }
            },
            {
                title: "Action",
                data: null,
                render: function (data) {
                    return `<button onclick='LoadDraft(${data.Id})' class='btn btn-success btn-sm'>
                            <span class='fa fa-mail-forward'></span> Select</button>
                            <button onclick='DeleteDraft(${data.Id}, this)' class='btn btn-danger btn-sm'>
                            <span class='fa fa-trash'></span> Delete</button>`
                }
            }
        ],
        initComplete: function () {
            $("#dtDrafts").setCustomSearch();

            $("#modalDrafts").modal();
        }
    })
}

function LoadDraft(id) {
    HoldOn.open({
        theme: "sk-circle"
    })

    $.ajax({
        url: "/Mail/LoadDraft?draftId=" + id,
        success: function (data) {
            $('#summernote').summernote('code', data.Body);
            $("#txtSubject").val(data.Subject);
            $("#btnSaveDraft").attr("draftId", id)
                .removeClass("btn-danger").addClass("btn-success")
                .html("<span class='fa fa-check'></span> Saved to Draft");
            
            $("#modalDrafts").modal("toggle");
            HoldOn.close();
        }
    })
}

function SaveDraft(that) {
    that = that || $("#btnSaveDraft")

    let subject = $("#txtSubject").val();
    let msgbody = $('#summernote').summernote('code');
    
    if ($.trim(msgbody) == "" && subject == "") {
        swal({
            title: "Oops",
            icon: "error",
            text: "Please insert something first before saving to draft!"
        })
    } else {
        $.ajax({
            url: "/Mail/SaveComposedMailToDraft",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
                draft: {
                    Body: msgbody,
                    Subject: subject,
                    Id: parseInt($(that).attr("draftId"))
                }
            } 
            ),
            success: function (data) {
                $(that).attr("draftId", data);
                $(that).removeClass("btn-danger").addClass("btn-success");
                $(that).html("<span class='fa fa-check'></span> Saved to Draft");
            }
        })
    }
}

function SendMail() {
    let table = $("#dtVendor").DataTable();
    let vendorIdArr = [];

    table.$("tr.selected").each(function () {
        vendorIdArr.push(parseInt($(this).attr("vendorId")));
    });

    if (vendorIdArr.length == 0) {
        swal({
            icon: "error",
            title: "Oops",
            text: "Please select at least one recipient before sending!"
        })
    } else {
        HoldOn.open({
            text: "Sending.",
            theme: "sk-circle"
        })
        
        let sentFiles = pond.getFiles();

        if (sentFiles.length > 0) {
            pond.setOptions({
                server: {
                    process: {
                        url: '/Mail/SaveAttachments',
                        method: 'POST',
                    }
                },
                onprocessfiles: function (error, file) {
                    if (error) {
                        swal({
                            title: "Oops",
                            icon: "error",
                            text: "There is an error processing your files."
                        })
                        return;
                    }

                    Send(vendorIdArr);
                }
            });

            pond.processFiles();
        } else {
            Send(vendorIdArr);
        }
    }
}

function Send(vendorIdArr) {
    let sentFiles = pond.getFiles();
    let filesArr = [];

    $.each(sentFiles, function (i, file) {
        filesArr.push(file.filename);
    })

    let subject = $("#txtSubject").val();
    let msgbody = $('#summernote').summernote('code');

    let message = {
        Body: msgbody,
        Subject: subject,
        Attachment: filesArr
    }

    
    let isIncludeCollectors = $("#isIncludeCollectors").is(":checked");
    let saveToDraft = $("#isSaveToDraft").is(":checked")

    if (saveToDraft) {
        SaveDraft();
    }

    $.ajax({
        url: "/Mail/SendMessage",
        dataType: "json",
        contentType: "application/json",
        type: "POST",
        data: JSON.stringify({
            message: message,
            _includeCollectors: isIncludeCollectors,
            vendorId: vendorIdArr
        }),
        success: function (data) {
            HoldOn.close();

            if (data == "SUCCESS") {
                swal({
                    text: "Messages are being sent now.",
                    icon: "success",
                    title: "Success"
                }).then(function () {
                    window.location.reload();
                })
            }
        }
    })
}