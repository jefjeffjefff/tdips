﻿$(document).ready(function () {
    $("#dtFile").DataTable({
        ajax: {
            url: "/GeneralJournal/GetGJFileList",
            dataSrc: "",
            dataType: "json"
        },
        columns: [
            {
                title: "Date/Time",
                data: null,
                render: function (data) {
                    let date = new Date(parseInt(data.CreationTime.replace('/Date(', '')))
                    return formatDate(date, "MM/dd/yyyy hh:mm");
                }
            },
            {
                title: "Action",
                data: null,
                render: function (data) {
                    let filefullname = data.FullName.split("\\");
                    let filepath = "/Spreadsheets/General Journal/" + filefullname[filefullname.length - 1];
                    return "<button onclick=\"window.location.href='" + filepath + "'\" class='btn btn-sm btn-primary'>Download</button>"
                }
            },
        ]
    })


    $("#btnUploadExcel").click(function () {
        if ($("#frmUploadExcel")[0].checkValidity() == false) {
            $("#frmUploadExcel")[0].reportValidity()
        } else {
            HoldOn.open({
                theme: "sk-circle",
                message: "Uploading an excel file.."
            })

            var excelFile = new FormData($("#frmUploadExcel")[0]);

            $.ajax({
                url: "/GeneralJournal/UploadTransactionExcel",
                type: 'POST',
                data: excelFile,
                contentType: false,
                processData: false,
                success: function (data) {
                    HoldOn.close();

                    if (data == "ERROR") {
                        swal({
                            title: "Ooops:",
                            text: "There is an error processing your request to the server. Please make sure that you are uploading the right excel format",
                            icon: "error",
                        })
                    } else {
                        let msg = "";
                        let icon = "success";
                        let title = "Success";

                        if (data.TotalRecord == 0 & data.RowsAdded == 0) {
                            swal({
                                title: "Oooops",
                                icon: "error",
                                text: "Your file seems empty or in different format"
                            })
                        } else {
                            msg = data.RowsAdded + " out of " + data.TotalRecord + " transactions are successfully inserted to the file. "

                            if (data.TotalRecord > data.RowsAdded) {
                                msg = "Only " + msg + " Reason: There might be mismatched account numbers - " + data.MissingBanks.join(", ") +
                                    ". Please check the Description column in your file."
                                icon = "warning";
                                title = "Hmmmm"
                            }

                            swal({
                                title: title,
                                text: msg,
                                icon: icon,
                            }).then(function () {
                                window.location.href = "/Spreadsheets/General Journal/" + data.Filename
                            })
                        }
                    }
                },
                error: function () {
                    HoldOn.close();
                    swal({
                        title: "Ooops:",
                        text: "There is an error processing your request to the server. Please make sure that you are uploading the right excel format",
                        icon: "error",
                    })
                }
            });
        }
    })
})