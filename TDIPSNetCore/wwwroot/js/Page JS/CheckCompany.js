﻿var vendorID = 0;
var getCollectorListURL = "";
var getBankListURL = "";
var dtVendor;
var vc = '';


$(document).ready(function () {
    $("#txtCollectorType").change(function () {
        if ($(this).val() == "AUTHORIZED") {
            $("#txtThirdPartyCompany").closest("div").css("display", "none");
        } else if ($(this).val() == "THIRD PARTY") {
            $("#txtThirdPartyCompany").closest("div").css("display", "block");
        }
    })


    $("#dtVendor").setCustomSearchInput();
    dtVendor = $('#dtVendor').DataTable({
        "initComplete": function (settings, json) {
            $("#dtVendor").setCustomSearch({
                exemption: [6],
                onchange: "false"
            });
        },
        ajax: {
            url: $('#dtVendor').attr("url"),
            datatype: 'json',
            dataSrc: 'dataList',
            type: "POST",
            data: function (d) {
                d.v = {
                    dataAreaId: $("#0").val(),
                    vendorCode: $("#1").val(),
                    companyName: $("#2").val(),
                    companyAddress: $("#3").val(),
                    companyEmail: $("#4").val(),
                    contactNumber: $("#5").val(),
                }
            },
            beforeSend: function () {
                $('#dtVendor > tbody').html(
                    '<tr class="odd customized_loading">' +
                    '<td valign="top" colspan="' + $("#dtVendor thead th").length + '" class="dataTables_empty">Loading&hellip;</td>' +
                    '</tr>'
                );
            }
        },
        "columns": [
            {
                "data": "dataAreaId",
                "title": "Entity",
                render: function (data, type, row) {
                    var details = "<text class='dataAreaId " + data + "'>" + data + "</text>";
                    return details;
                }
            },
            {
                "data": "vendorCode",
                "title": "Vendor Code",
                render: function (data, type, row) {
                    var details = "<text class='details vendorCode' vendorCode='" + data + "' vendorId='" + row.vendorId + "'>" + data + "</text>";
                    return details;
                }
            },
            {
                "data": "companyName",
                "title": "Company Name",
                render: function (data, type, row) {
                    var details = "<text class='companyName'>" + data + "</text>";
                    return details;
                }
            },
            {
                "data": "companyAddress",
                "title": "Company Address.",
                render: function (data, type, row) {
                    var details = "<text class='companyAddress'>" + data + "</text>";
                    return details;
                }
            },
            {
                "data": "companyEmail",
                "title": "Company Email",
                render: function (data, type, row) {
                    var details = "<text class='companyEmail'>" + data + "</text>";
                    return details;
                }
            },
            {
                "data": "contactNumber",
                "title": "Contact No.",
                render: function (data, type, row) {
                    var details = "<text class='contactNumber'>" + data + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Actions",
                render: function (data, type, row) {
                    var details = "<div class='fa-hover'><a href='#' onclick='LoadCompanyVouchers(\"" + data.vendorCode + "\", \"" + data.companyName.replace(/'/g, "") + "\")'><i class='fa fa-edit'></i> Release</a></div>";

                    return details;
                }
            }
        ],
        columnDefs: [
        {
            className: 'row_action',
            targets: [6]
        }
        ],
        "createdRow": function (row, data, dataIndex) {
            if ($(row).find('.dataAreaId').html() == "SPAV") {
                $(row).addClass("spavi_row");
            } else if ($(row).find('.dataAreaId').html() == "SPCI") {
                $(row).addClass("spci_row");
            } else if ($(row).find('.dataAreaId').html() == "WBHI") {
                $(row).addClass("wbhi_row");
            } else if ($(row).find('.dataAreaId').html() == "NAF") {
                $(row).addClass("naf_row");
            } else if ($(row).find('.dataAreaId').html() == "SIL") {
                $(row).addClass("sil_row");
            } else if ($(row).find('.dataAreaId').html() == "DBE") {
                $(row).addClass("dbe_row");
            }
        },
        filter: false,
        language: {
            "lengthMenu": "_MENU_",
            "processing": ""
        },
        lengthMenu: [
            [5, 10, 15, 20, 100],
            [5, 10, 15, 20, 100] // change per page values here
        ],
        orderCellsTop: true,
        pageLength: 10,
        processing: false,
        responsive: true,
        serverSide: true,
        stateSave: false
    })
    
})

function LoadCompanyVouchers(vendorCode, companyName) {
    var totalAmount = 0;
    vc = vendorCode;
    
    $(".container_collector_details").css("display", "block");

    $("#myModalLabel strong").html(companyName);

    $("#dtVoucher").DataTable().destroy();

    var dtVoucher = $('#dtVoucher').DataTable({
        "destroy" : true,
        "initComplete": function (settings, json) {
            if (json.length == 0) {
                swal({
                    title: "This company has no voucher for release",
                    icon: "error"
                })
            } else {
                $("#modalViewVouchers").modal();
            }

            $("#btnRelease").off("click").click(function () {
                HoldOn.open({
                    theme: "sk-circle",
                    message: "Releasing Vouchers..."
                })

                let parameter;
                let hasError = false;
                let voucherArray = [];

                dtVoucher.$("tr").each(function (index) {
                    let receiptNumber = $(this).find(".txtReceiptNumber").val();
                    let receiptType = $(this).find(".txtReceiptType").val();
                    let voucherId = $(this).find(".voucherId").html();
                    let checkType = $(this).find(".txtCheckType").val();

                    if (receiptType != "DRY") {
                        voucherArray.push({
                            VoucherId: voucherId,
                            ReceiptNo: receiptNumber,
                            ReceiptType: receiptType,
                            CheckType: checkType
                        })
                    }
                });

                if (voucherArray.length == 0) {
                    HoldOn.close();
                    swal({
                        title: "No voucher for release is submitted.",
                        text: "Please fill at least one receipt number for voucher.",
                        icon: "error"
                    })
                } else {
                    if ($("#txtCollectorName").val() == "" || ($("#txtCollectorType").val() == "THIRD PARTY") && $("#txtThirdPartyCompany").val() == "") {
                        swal({
                            title: "Please complete the collector's details before proceeding",
                            icon: "error"
                        })
                        HoldOn.close();
                        return false;
                    } else {
                        parameter = JSON.stringify({
                            r: voucherArray,
                            queueId: 0,
                            isInQueue: false,
                            c: {
                                CollectorName: $("#txtCollectorName").val(),
                                CollectorType: $("#txtCollectorType").val(),
                                ThirdPartyCompany: $("#txtThirdPartyCompany").val(),
                                QueueType: "SPECIAL",
                                status: "DONE"
                            },
                            vendorCode: vc
                        })
                    }


                    $.ajax({
                        type: "Post",
                        url: $("#modalViewVouchers").attr("url"),
                        type: "POST",
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        data: parameter,
                        success: function (data) {
                            HoldOn.close();

                            swal({
                                title: "Vouchers are released successfully!",
                                icon: "success"
                            }).then(function () {
                                document.location.reload();
                            });
                        },
                        error: function () {
                            swal({
                                title: "There is an error occured releasing the voucher.",
                                icon: "error"
                            });
                        }
                    })
                }
            })
        },
        ajax: {
            url: $('#dtVoucher').attr("url") + "?vendorId=" + vendorCode,
            datatype: 'json',
            dataSrc: '',
        },
        "columns": [
            {
                "data": null,
                "title": "Voucher",
                render: function (data, type, row) {
                    var details = "<text class='voucherId'>" + data.voucherId + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Entity",
                render: function (data, type, row) {
                    var details = "<text class='dataAreaId " + data.entity + "'>" + data.entity + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Bank",
                render: function (data, type, row) {
                    var details = "<text class='bankName'>" + data.bank + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Amount",
                render: function (data, type, row) {
                    totalAmount += data.amount;
                    var details = "<span>&#8369;</span><text class='amount'>" + parseMoney(data.amount) + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Check",
                render: function (data, type, row) {
                    var details = "<text class='checkNumber'>" + (data.checkNum ? data.checkNum : "") + "</text>"
                    return details;
                }
            },
            {
                "data": null,
                "title": "Date",
                render: function (data, type, row) {
                    var details = parseJsonDate(data.checkDate);
                    if (details == "") return "";
                    return "<text class='checkDate'>" + $.date(details) + "</text>"
                }
            },
            {
                "data": null,
                "title": "Check Type",
                render: function (data, type, row) {
                    var details = "<select class='form-control txtCheckType'>" +
                        "<option value='STANDARD'>Standard</option>" +
                        "<option value='MANAGER'>Manager's Check</option>" +
                        "</select>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Receipt Number",
                render: function (data, type, row) {
                    var details = "<input class='form-control txtReceiptNumber' type='text' placeholder='Receipt Number' />";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Receipt Type",
                render: function (data, type, row) {
                    var details = "<select class='form-control txtReceiptType'>" +
                        "<option value='OR'>Official Receipt</option>" +
                        "<option value='AR'>Acknowledgement Receipt</option>" +
                        "<option value='PR'>Provisionary Receipt</option>" +
                        "<option value='CR'>Collection Receipt</option>" +
                        "<option value='DRY'>Don't Release Yet</option>" +
                        "</select>";
                    return details;
                }
            },
        ],
        "createdRow": function (row, data, dataIndex) {
            if ($(row).find('.dataAreaId').html() == "SPAV") {
                $(row).addClass("blue_row");
            } else if ($(row).find('.dataAreaId').html() == "SPCI") {
                $(row).addClass("yellow");
            } else if ($(row).find('.dataAreaId').html() == "WBHI") {
                $(row).addClass("green_row");
            } else if ($(row).find('.dataAreaId').html() == "NAF") {
                $(row).addClass("orange_row");
            } else if ($(row).find('.dataAreaId').html() == "SIL") {
                $(row).addClass("red_row");
            } else if ($(row).find('.dataAreaId').html() == "PINK") {
                $(row).addClass("pink_row");
            } else if ($(row).find('.dataAreaId').html() == "DBE") {
                $(row).addClass("dbe_row");
            }
        },
    })
}

