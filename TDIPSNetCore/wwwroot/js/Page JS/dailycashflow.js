﻿$(document).ready(function () {
    $("#dtCF").DataTable({
        ajax: {
            url: "/Cashflow/GetDailyCashFlowsList",
            dataSrc: '',
            dataType: "json"
        },
        columns: [
            {
                title: "Upload Date",
                data: null,
                render: function (data) {
                    console.log(data.DateOfUpload);
                    var details = formatDate(parseJsonDate(data.DateOfUpload), "d MMM yyyy dddd hh:mmtt");
                    return details;
                }
            },
            {
                title: "Uploaded by",
                data: "UploadedBy"
            },
            {
                title: "Date Range",
                data: "DailyCashDateRange"
            }
        ]
    })


    $("#btnUploadExcel").click(function () {
        if ($("#frmUploadExcel")[0].checkValidity() == false) {
            if (isIE || isEdge) {
                swal({
                    title: "Please select an excel file first before importing.",
                    icon: "error"
                })
            } else if (isChrome) {
                $("#frmUploadExcel")[0].reportValidity()
            }
        } else {
            HoldOn.open({
                theme: "sk-circle",
                message: "Uploading an excel file.."
            })

            var excelFile = new FormData($("#frmUploadExcel")[0]);

            $.ajax({
                url: "/Cashflow/UploadDailyCashflow",
                type: 'POST',
                data: excelFile,
                contentType: false,
                processData: false,
                success: function (data) {
                    HoldOn.close();

                    if (data == "NULL") {
                        swal({
                            title: "Please select an excel file first before importing.",
                            icon: "error"
                        })
                    } else if (data == "INVALID FILE") {
                        swal({
                            title: "Invalid excel file.",
                            icon: "error"
                        })
                    } else if (data == "EMPTY") {
                        swal({
                            title: "Excel file seems empty or in different format.",
                            icon: "error"
                        })
                    } else if (data == "SUCCESS") {
                        swal({
                            title: "Success",
                            icon: "success",
                            text: "You have successfully added entries for daily cashflow."
                        }).then(function () {
                            document.location.reload();
                        });
                    }
                },
                error: function () {
                    HoldOn.close();
                    swal({
                        title: "There is an error processing your request to the server.",
                        icon: "error",
                    });
                }
            });
        }
    })
})