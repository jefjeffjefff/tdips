﻿var excelUploadResult = {};
var dtOP;

$(document).ready(function () {
    if (isEdge || isIE)
    {
        $("input[type=file]").css("display", "none");

        $(".file_upload_container").append("<input class='form-control file-upload'" +
            "style='height:31px;width:280px;' value='Choose a file' readonly></input>");

        $(".file-upload").click(function () {
            $("input[type=file]").trigger("click");
        })

        $("#file_upload").change(function () {
            let file = $("#file_upload").val().split("\\");

            if (file.length > 0) {
                $(".file-upload").val(file[file.length - 1]);
            } else {
                $(".file-upload").val($("#file_upload").val());
            }
        })
    }

    $.ajax({
        url: "/SEAN/GetEmployeeWithOnlinePaymentsList",
        success: function (data) {
            LoadOPTable(data, $("#dtOP"))
        }
    })

    $("#btnVerify").click(function () {
        if (excelUploadResult.OnlinePayments.length > 0) {
            swal({
                title: "Are you sure that you want to verify the loaded online payments now?",
                text: "Once verified, this can't be undone!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        SaveOnlinePayment();
                    }
                });
        } else {
            swal({
                title: "Ooops",
                icon: "error",
                text: "Make sure that you have online payments loaded from excel before verifying"
            })
        }
    })
   
    $("#dtrSearch").daterangepicker()
    
    $("#btnUploadExcel").click(function () {
        if ($("#frmUploadExcel")[0].checkValidity() == false) {
            if (isIE || isEdge) {
                swal({
                    title: "Please select an excel file first before importing.",
                    icon: "error"
                })
            } else if (isChrome) {
                $("#frmUploadExcel")[0].reportValidity()
            }
        } else {
            HoldOn.open({
                theme: "sk-circle",
                message: "Uploading an excel file.."
            })

            var excelFile = new FormData($("#frmUploadExcel")[0]);

            $.ajax({
                url: $("#frmUploadExcel").attr("url"),
                type: 'POST',
                data: excelFile,
                contentType: false,
                processData: false,
                success: function (data) {
                    HoldOn.close();

                    if (data == "NULL") {
                        swal({
                            title: "Please select an excel file first before importing.",
                            icon: "error"
                        })
                    } else if (data == "INVALID FILE") {
                        swal({
                            title: "Invalid excel file.",
                            icon: "error"
                        })
                    } else if (data == "EMPTY") {
                        swal({
                            title: "Excel file seems empty or in different format.",
                            icon: "error"
                        })
                    } else {
                        let msg = "";
                        if (data.EmployeeNotFound.length > 0) {
                            msg += "Employees " + data.EmployeeNotFound.join(", ") + " are not found in the database. "
                        }

                        if (data.Incomplete.length > 0) {
                            msg += "You have incomplete inputs for row " + data.Incomplete.join(", ") + ". Please check your excel file. "
                        }

                        if (data.BlankEmail.length > 0) {
                            msg += "You have no emails for employees " + data.BlankEmail.join(", ") + ". "
                        }

                        if (msg == "") {
                            msg = data.RowsAdded + " online payments are loaded!"
                        } else {
                            msg += "Only " + data.RowsAdded + " out of " + data.TotalRows + " online payments are loaded."
                        }

                        var options = [
                            {
                                className: 'd-none',
                                targets: [5]
                            }
                        ]

                        LoadOPTable(data.OnlinePayments, $("#dtViewOP"), options, true);
                        $("#msg").html(msg);
                        $("#mdlViewOP").modal();

                        excelUploadResult = data;
                    }
                },
                error: function () {
                    HoldOn.close();
                    swal({
                        title: "There is an error processing your request to the server.",
                        icon: "error",
                    });
                }
            });
        }
    })

    $("#btnSearch").click(function () {
        let daterange = ($("#dtrSearch").val()).split(" - ");
        let dateFrom = daterange[0]
        let dateTo = daterange[1]

        $("#dtOP").DataTable().destroy();

        $.ajax({
            url: "/SEAN/GetEmployeeWithOnlinePaymentsList?from=" + dateFrom + "&to=" + dateTo,
            success: function (data) {
                LoadOPTable(data, $("#dtOP"))
            }
        })
    })

    $("#btnExport").click(function () {
        HoldOn.open({
            text: "Exporting ..",
            theme: "sk-circle"
        })

        let daterange = ($("#dtrSearch").val()).split(" - ");
        let dateFrom = daterange[0]
        let dateTo = daterange[1]

        $.ajax({
            url: "/SEAN/ExportOnlinePayments?from=" + dateFrom + "&to=" + dateTo,
            success: function (data) {
                HoldOn.close();
                window.location.href = "/Report/DownloadExcel?fileName=" + data;
            }
        })
    })
})

function LoadOPTable(onlinepayments, table, columnDefs, willSummarize) {
    willSummarize = willSummarize || false;

    $(table).setCustomSearchInput();
    
    dtOP = $(table).DataTable({
        destroy: true,
        initComplete: function () {
            $(table).setCustomSearch();

            $(".container_item").html("")
        },
        data: onlinepayments,
        "columns": [
            {
                "data": null,
                "title": "Employee #",
                render: function (data, type, row) {
                    var details = data.Employee.employee_no
                    return details;
                }
            },
            {
                "data": null,
                "title": "Employee Name",
                render: function (data, type, row) {
                    var details = "<text>" + data.Employee.employeeName + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Email",
                render: function (data, type, row) {
                    var details = "<text>" + data.Employee.email + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Account #",
                render: function (data, type, row) {
                    var details = "<text>" + data.employeeAccNum + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Payment Date",
                render: function (data, type, row) {
                    if (data.transDate == "") return "";

                    var time = new Date(data.transDate);
                    return formatDate(time, "MM-dd-yyyy");
                }
            },
            {
                "data": null,
                "title": "Log Date",
                render: function (data, type, row) {
                    if (data.strLogDate == "") return "";

                    var time = new Date(data.strLogDate);
                    return formatDate(time, "MM-dd-yyyy");
                }
            },
            {
                "data": null,
                "title": "Entity",
                render: function (data, type, row) {
                    var details = "<text class='dataAreaId " + data.entity + "'>" + data.entity + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Bank",
                render: function (data, type, row) {
                    var details = "<text class='bankName'>" + data.bank + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Amount",
                render: function (data, type, row) {
                    var details = "<span>&#8369;</span><text class='amount' amount='" + data.amount + "'>" + parseMoney(data.amount) + "</text>";
                    return details;
                }
            },
        ],
        "createdRow": function (row, data, dataIndex) {
            if ($(row).find('.dataAreaId').html() == "SPAV") {
                $(row).addClass("spavi_row");
            } else if ($(row).find('.dataAreaId').html() == "SPCI") {
                $(row).addClass("spci_row");
            } else if ($(row).find('.dataAreaId').html() == "WBHI") {
                $(row).addClass("wbhi_row");
            } else if ($(row).find('.dataAreaId').html() == "NAF") {
                $(row).addClass("naf_row");
            } else if ($(row).find('.dataAreaId').html() == "SIL") {
                $(row).addClass("sil_row");
            } else if ($(row).find('.dataAreaId').html() == "DBE") {
                $(row).addClass("dbe_row");
            }
        },
        columnDefs: columnDefs
    })

    if (willSummarize) {
        totalAmount = 0.0;
        var totalAmount = 0.0;
        var itemAmountArr = [];
        var bankArray = [];
        var entityArray = [];

        dtOP.$("tr").each(function (index) {
            var amount = removeCommas(($(this).find(".amount")).html())
            let entity = $(this).find(".dataAreaId").html();
            let bank = $(this).find(".bankName").html();

            itemAmountArr.push({
                entity: entity,
                bankName: bank,
                amount: amount
            })

            if (bankArray.indexOf(bank) == -1) {
                bankArray.push(bank)
            }

            if (entityArray.indexOf(entity) == -1) {
                entityArray.push(entity);
            }

            totalAmount += parseFloat(amount);
        });

        $.each(entityArray, function (i, item) {
            let entity = item;

            let itemArray = $.grep(itemAmountArr, function (n, i) {
                return n.entity == entity;
            })

            let total = 0;
            let subitems = [];

            $.each(itemArray, function (i, item) {
                let index = GetIndexInJsonByKeyVal(subitems, "bankName", item.bankName);

                if (index >= 0) {
                    subitems[index].amount = parseFloat(subitems[index].amount) + parseFloat(item.amount);
                } else {
                    subitems.push({
                        bankName: item.bankName,
                        amount: item.amount
                    })
                }

                total += parseFloat(item.amount);
            })

            $(".by_entity .container_item").append('<div class="summary_item ' + entity + '">' +
                '<div class="item_header">' +
                '<label>' + entity + ' (₱' + parseMoney(total) + ')</label>' +
                '</div>' +
                '<div class="container_sub_item">' +
                '</div>' +
                '</div>')


            $.each(subitems, function (i, item) {
                $(".summary_item." + entity + " .container_sub_item").append('<div class="sub-item">' +
                    '<label>' + item.bankName + ' - ₱' + parseMoney(item.amount) +
                    '</label></div>')
            })
        })

        $.each(bankArray, function (i, item) {
            let bank = item;

            let itemArray = $.grep(itemAmountArr, function (n, i) {
                return n.bankName == bank;
            })

            let total = 0;
            let subitems = [];

            $.each(itemArray, function (i, item) {
                let index = GetIndexInJsonByKeyVal(subitems, "entity", item.entity);

                if (index >= 0) {
                    subitems[index].amount = parseFloat(subitems[index].amount) + parseFloat(item.amount);
                } else {
                    subitems.push({
                        entity: item.entity,
                        amount: item.amount
                    })
                }

                total += parseFloat(item.amount);
            })


            $(".by_bank .container_item").append('<div class="summary_item ' + bank + '">' +
                '<div class="item_header">' +
                '<label>' + bank + ' (₱' + parseMoney(total) + ')</label>' +
                '</div>' +
                '<div class="container_sub_item">' +
                '</div>' +
                '</div>')


            $.each(subitems, function (i, item) {
                $(".summary_item." + bank + " .container_sub_item").append('<div class="sub-item">' +
                    '<label>' + item.entity + ' - ₱' + parseMoney(item.amount) +
                    '</label></div>')
            })
        })

        $("#btnRequest").html("<i class='fa fa-external-link'></i> Request <u>₱" + parseMoney(totalAmount) + "</u> Worth of Vouchers");
        $("#lblTotal").html("Total : ₱" + parseMoney(totalAmount))
    }
}

function SaveOnlinePayment() {
    if (excelUploadResult.OnlinePayments.length > 0) {
        HoldOn.open({
            message: "Saving ...",
            theme: "sk-circle"
        })

        Object.keys(excelUploadResult.OnlinePayments).forEach(function (key) {
            excelUploadResult.OnlinePayments[key].paymentDate = excelUploadResult.OnlinePayments[key].transDate;
        });

        $.ajax({
            url: "/SEAN/SaveOnlinePayments",
            dataType: "json",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({
                onlinePayments: excelUploadResult.OnlinePayments,
                isWithEmployee: excelUploadResult.IsWithEmployee
            }),
            success: function (data) {
                HoldOn.close()

                swal({
                    icon: "success",
                    title: "Success",
                    text: "Online payments are successfully saved."
                }).then(function () {
                    document.location.reload();
                })
            }
        })
    }
}

function GetIndexInJsonByKeyVal(json, key, value) {
    let index = -1;

    $.each(json, function (i, item) {
        if (item[key] == value) {

            index = i;
        }
    });

    return index;
}