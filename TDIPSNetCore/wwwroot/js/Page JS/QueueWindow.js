﻿var videoList;
var currentVideoIndex = 0;
var isAudioOnPlay = false;
var fetchQueueTimeOut;
var counter = 0;

$.ajax({
    type: "GET",
    url: $("video").attr("url"),
    async: false,
    success: function (data) {
        videoList = data;
    }
})

//$.connection.hub.start()
//    .done(function () {})
//    .fail(function (e) { console.log(e) })

var connection = new signalR.HubConnectionBuilder().withUrl("/companyCallHub").build();

function fetchQueue() {
    $.ajax({
        type: "GET",
        url: $("body").attr("url") + "?status=QUEUEING",
        success: function (data) {
            currentData = data;

            if (isAudioOnPlay == true) return false;

            if (data == "") {
                $(".left_area").html("<label style='font-size:calc(2vw + 2vh + 2vmin); margin-right:13px; background:rgba(187,27,36, 0.6);'>No number is in line now.</label>")
            } else {
                $.each(data, function (i, item) {
                    if (i == 0) {
                        $(".left_area").html("<div class='title_number'>" +
                            "<div class='title_number_type regular'>" +
                            "<label>Regular Number</label>" + 
                            "</div>" +
                            "<div class='title_number_type priority'>" +
                            "<label>Priority Number</label>" +
                            "</div>" +
                            "</div> " +
                            "<div class='title_now_serving'>" +
                            "<label>Now Serving</label>" +
                            "</div> " +
                            "<div class='number_now_serving container_numbers container_numbers_0'>" +
                            "<div class='regular number container_number'>" +
                            "<div class= 'container_number_label' >" +
                            "<label class='regularNum numberLabel'></label>" +
                            "</div>" +
                            "<div class='companyName'>" +
                            "<label></label>" +
                            "</div></div>" +
                            "<div class='priority number container_number'>" +
                            "<div class='container_number_label'>" +
                            "<label class='priorityNum numberLabel'></label>" +
                            "</div>" + 
                            "<div class= 'companyName' >" +
                            "<label></label>" +
                            "</div ></div ></div > " +
                            "<div class='title_next_number'>" +
                            "<label>Next Numbers</label>" +
                            "</div>" +
                            "<div class='container_next_numbers'><div class='vertical_line'></div>" +
                            "<div class='number_next_serving container_numbers container_numbers_1'>" +
                            "<div class='regular number container_number'>" +
                            "<div class='container_number_label'>" +
                            "<label class='regularNum numberLabel'></label>" +
                            "</div>" +
                            "<div class= 'companyName' >" +
                            "<label></label>" +
                            "</div >" +
                            "</div>" +
                            "<div class='priority number container_number'>"+
                            "<div class='container_number_label'>" +
                            "<label class='priorityNum numberLabel'></label>" +
                            "</div>" +
                            "<div class= 'companyName' >" +
                            "<label></label>" +
                            "</div >" +
                            "</div></div>" +
                            "<div class='number_next_serving container_numbers container_numbers_2'>" +
                            "<div class='regular number container_number'>" +
                            "<div class='container_number_label'>" +
                            "<label class='regularNum numberLabel'></label>" +
                            "</div>" +
                            "<div class= 'companyName' >" +
                            "<label></label>" +
                            "</div >" +
                            "</div>" +
                            "<div class='priority number container_number'>" +
                            "<div class='container_number_label'>" +
                            "<label class='priorityNum numberLabel'></label>" +
                            "</div>" +
                            "<div class= 'companyName' >" +
                            "<label></label>" +
                            "</div >" +
                            "</div></div>" +
                            "<div class='number_next_serving container_numbers container_numbers_3'>" +
                            "<div class='regular number container_number'><div class='container_number_label'>" +
                            "<label class='regularNum numberLabel'></label>" +
                            "</div>" +
                            "<div class= 'companyName' >" +
                            "<label></label>" +
                            "</div>" +
                            "</div>" +
                            "<div class='priority number container_number'>" +
                            "<div class='container_number_label'><label class='priorityNum numberLabel'></label></div>" +
                            "<div class= 'companyName' >" +
                            "<label></label>" +
                            "</div>" +
                            "</div></div></div>")
                    }

                    let index = $(".container_numbers ." + ((item.queueType).toLowerCase()) + ".number.filled").length;


                    $(".container_numbers_" + index + " ." + ((item.queueType).toLowerCase()) + ".number .numberLabel").html(item.queueNumber);

                    let companyName = item.companyName.substring(0,20);

                    $(".container_numbers_" + index + " ." + ((item.queueType).toLowerCase()) + ".number").find(".companyName").html(companyName);

                    $(".container_numbers_" + index + " ." + ((item.queueType).toLowerCase()) + ".number").addClass("filled");
                })

                $(".container_number").not(".filled").each(function () {
                    $(this).html("-");
                })
                
            }
        },
        complete: function (data) {
             fetchQueueTimeOut = setTimeout(fetchQueue, 5000);
        }
    })
}

function CallNumber() {
        $.ajax({
            type: "GET",
            url: $("#container_audio").attr("getcallurl"),
            success: function (data) {
                if (data && isAudioOnPlay == false) {
                    isAudioOnPlay = true;
                    clearTimeout(fetchQueueTimeOut);
                    $("video").each(function () {
                        $(this).get(0).muted = true;
                    });

                    var callid = data.id;

                    $("#container_audio").html("<audio id='callaudio' controls src='" + $("#container_audio").attr("source") + "TTS" + callid + ".wav" + "'></audio>")

                    $("#beep_shakeys").get(0).play();

                    $('#beep_shakeys').off('onended').on('ended', function () {
                        try {
                            var media = document.getElementById("callaudio");
                            const playPromise = media.play();
                            if (playPromise !== null) {
                                playPromise.catch(() => { media.play(); })
                            }

                            //$("#container_audio audio").get(0).play();

                            $(".container_numbers_0 .regular.number .companyName").css({
                                'animation': '',
                                'animation-iteration-count': ''
                            });

                            $(".container_numbers_0 .priority.number .companyName").css({
                                'animation': '',
                                'animation-iteration-count': ''
                            });

                            $(".container_numbers_0 ." + data.queueType.toLowerCase() + ".number .companyName").css({
                                'animation': 'text-blink .8s',
                                'animation-iteration-count': 'infinite'
                            });
                        }
                        catch (e) {
                            fetchQueueTimeOut = setTimeout(fetchQueue, 5000);

                            $("video").each(function () {
                                $(this).get(0).muted = false;
                            });
                            
                            $.ajax({
                                url: $("#container_audio").attr("deleteurl") + "?callid=" + callid,
                                type: "POST",
                                complete: function (data) {
                                    isAudioOnPlay = false;
                                }
                            })
                        }
                        
                        $('#container_audio audio').off('onended').on('ended', function () {
                            fetchQueueTimeOut = setTimeout(fetchQueue, 5000);
                            
                            $("video").each(function () {
                                $(this).get(0).muted = false;
                            });

                            $(".container_numbers_0 ." + data.queueType.toLowerCase() + ".number .companyName").css({
                                'animation': '',
                                'animation-iteration-count': ''
                            });

                            $.ajax({
                                url: $("#container_audio").attr("deleteurl") + "?callid=" + callid,
                                type: "POST",
                                complete: function (data) {
                                    isAudioOnPlay = false;
                                }
                            })
                        });
                    })
                }
            },
            complete: function (data) {
                setTimeout(CallNumber, 1000);
            }
        })
}

function date_time() {
    let date = new Date;
    let year = date.getFullYear();
    let month = date.getMonth();
    let months = new Array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC');
    let d = date.getDate();
    let day = date.getDay();
    let days = new Array('SUN', 'MON', 'TUES', 'WED', 'THURS', 'FRI', 'SAT');
    let h = date.getHours();
    let AmPm = ""

    if (h > 12) {
        h -= 12;
        AmPm = "PM";
    } else {
        AmPm = "AM";
    }

    if (h < 10) {
        h = "0" + h;
    }

    m = date.getMinutes();
    if (m < 10) {
        m = "0" + m;
    }
    s = date.getSeconds();
    if (s < 10) {
        s = "0" + s;
    }

    $(".container_datetime.date label").html(months[month] + " " + d + " " + year + " | " + days[day]);
    $(".container_datetime.time label").html(h + ':' + m + ':' + s + " " + AmPm);

    setTimeout(date_time, 1000);
    return true;
}

function Call() {
    connection.on("CompanyCall", function (user, call) {
        console.log(call);

        var callid = call.split("_")[0];
        var queueType = call.split("_")[1];

        if (isAudioOnPlay == false) {
            isAudioOnPlay = true;
            clearTimeout(fetchQueueTimeOut);
            $("video").each(function () {
                $(this).get(0).muted = true;
            });

            $("#container_audio").html("<audio id='callaudio' controls src='" + $("#container_audio").attr("source") + "TTS" + callid + ".wav" + "'></audio>")

            $("#beep_shakeys").get(0).play();

            $('#beep_shakeys').off('onended').on('ended', function () {
                try {
                    var media = document.getElementById("callaudio");
                    const playPromise = media.play();
                    if (playPromise !== null) {
                        playPromise.catch(() => { media.play(); })
                    }

                    //$("#container_audio audio").get(0).play();

                    $(".container_numbers_0 .regular.number .companyName").css({
                        'animation': '',
                        'animation-iteration-count': ''
                    });

                    $(".container_numbers_0 .priority.number .companyName").css({
                        'animation': '',
                        'animation-iteration-count': ''
                    });

                    $(".container_numbers_0 ." + queueType.toLowerCase() + ".number .companyName").css({
                        'animation': 'text-blink .8s',
                        'animation-iteration-count': 'infinite'
                    });
                }
                catch (e) {
                    fetchQueueTimeOut = setTimeout(fetchQueue, 5000);

                    $("video").each(function () {
                        $(this).get(0).muted = false;
                    });

                    isAudioOnPlay = false;
                }

                $('#container_audio audio').off('onended').on('ended', function () {
                    fetchQueueTimeOut = setTimeout(fetchQueue, 5000);

                    $("video").each(function () {
                        $(this).get(0).muted = false;
                    });

                    $(".container_numbers_0 ." + queueType.toLowerCase() + ".number .companyName").css({
                        'animation': '',
                        'animation-iteration-count': ''
                    });

                    isAudioOnPlay = false;
                });
            })
        }
    }
    )
}

$(document).ready(function () {
    $.ajax({
        type: "GET",
        url: $("#txtAnnouncement").attr("url"),
        success: function (data) {
            $("#txtAnnouncement").html(data);
            new Marquee('txtAnnouncement', {
                direction: 'rtl',
                speed: .6,
                bgcolor: '#010101',
                textcolor: '#fff'
            });
        }
    })
    
    setTimeout(fetchQueue, 3000);

    connection.start().then(function () {
        console.log("signal r connected");
    }).catch(function (err) {
        return console.error(err.toString());
    });

    Call();
    date_time();
    PlayVideo();
})

function PlayVideo() {
    var videoPlayer = $("#video0");

    if (videoList.length > 0) {
        videoPlayer.attr("src", $("#video0").attr("source") + videoList[currentVideoIndex].videoId + "." + videoList[currentVideoIndex].fileExtension);
        videoPlayer.get(0).play();
        currentVideoIndex = currentVideoIndex + 1;
    }
}

function PlayNext() {
    if (currentVideoIndex > (videoList.length - 1)) {
        currentVideoIndex = 0
    }
    counter++;
    if ($("#video" + currentVideoIndex).length == 0) {
        $(".container_video").append("<div style='position:absolute;top:0;width:100%;z-index:'" + counter + ";><video onended='PlayNext()' "+ (isAudioOnPlay ? "muted" : "") + " id='video" + currentVideoIndex + "' class='video_player' src='" + $("#video0").attr("source") + videoList[currentVideoIndex].videoId + "." + videoList[currentVideoIndex].fileExtension + "' width ='100%' height ='100%' autoplay autobuffer controls ></video></div>");
        $("#video" + currentVideoIndex).get(0).play();
    } else {
        $("#video" + currentVideoIndex).get(0).play();
        $("#video" + currentVideoIndex).closest("div").css("z-index", counter);
        
    }
    
    $(".video_player").css("visibility", "hidden");
    $("#video" + currentVideoIndex).css("visibility", "visible");
    currentVideoIndex++;
}
