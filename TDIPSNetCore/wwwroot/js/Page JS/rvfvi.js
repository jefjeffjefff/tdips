﻿var requestVoucherForVerificationUrl = "";
var currentDate;
var dtPayment;
var voucherIdArr;
var dtVoucher;

//https://datatables.net/extensions/select/examples/

function MakeRequest() {

    let voucherModelArr = [];
    var table = $("#dtPayment").DataTable();
    let isEmptyCheckDetailsThere = false;

    table.$("tr.selected").each(function () {
        let status = ($(this).find(".status").text()).toUpperCase();
        let voucherId = $(this).find(".voucherId").text();
        if (status == "UNRELEASED" || status == "UNCOLLECTED" || status == "REQUESTED FOR HOLD") {
            if ($(this).find(".checkNumber").html() == "" || $(this).find(".checkDate").html() == "") {
                isEmptyCheckDetailsThere = true;
            }

            voucherModelArr.push({
                voucherId: voucherId,
                status: status,
                voucherId: $(this).find(".voucherId").html(),
                vendorCode: $(this).find(".vendorCode").html(),
                companyName: $(this).find(".companyName").html(),
                entity: $(this).find(".dataAreaId").html(),
                bank: $(this).find(".bankName").html(),
                amount: removeCommas($(this).find(".amount").html()),
                checkNum: $(this).find(".checkNumber").html(),
                checkDate: $(this).find(".checkDate").html(),
            })
        }
    });

    if (voucherModelArr.length == 0) {
        swal({
            title: "No Row is Selected for Verification Request",
            icon: "error",
            text: "Please select at least one record with status 'Unreleased' or 'Uncollected' before requesting."
        })
    } else {
        if (isEmptyCheckDetailsThere == true) {
            swal({
                title: "Confirmation",
                text: "Some of the selected records have empty data for check details. Do you still want to continue?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((ok) => {
                    if (ok) {
                        $("#modalRequest").modal();
                    }
                });
        } else {
            $("#modalRequest").modal();
        }
    }
}

function LoadVouchers() {
    HoldOn.open({
        theme: "sk-circle"
    })
    let voucherModelArr = [];
    var table = $("#dtPayment").DataTable();
    $("#dtVoucher").DataTable().destroy();

    table.$("tr.selected").each(function () {
        voucherModelArr.push({
            voucherId: $(this).find(".voucherId").html(),
            vendorCode: $(this).find(".vendorCode").html(),
            companyName: $(this).find(".companyName").html(),
            entity: $(this).find(".dataAreaId").html(),
            bank: $(this).find(".bankName").html(),
            amount: removeCommas($(this).find(".amount").html()),
            chequeNumber: $(this).find(".checkNumber").html(),
            chequeDate: $(this).find(".checkDate").html(),
        })
    });
    
    if (voucherModelArr.length == 0) {
        swal({
            icon: "error",
            text: "Please select at least one voucher first.",
            title: "No Voucher is Selected"
        });
        HoldOn.close();
        return false;
    }

    dtVoucher = $("#dtVoucher").DataTable({
        data: voucherModelArr,
        destroy: true,
        "columns": [
            {
                "data": null,
                "title": "Voucher",
                render: function (data, type, row) {
                    var details = "<text companyName='" + data.companyName +
                        "' vendorCode='"+ data.vendorCode +"' class='voucherId'>" + data.voucherId + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Entity",
                render: function (data, type, row) {
                    var details = "<text class='dataAreaId " + data.entity + "'>" + data.entity + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Bank",
                render: function (data, type, row) {
                    var details = "<text class='bankName'>" + data.bank + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Amount",
                render: function (data, type, row) {
                    var details = "<span>&#8369;</span><text class='amount' amount='"+ data.amount +"'>" + parseMoney(data.amount) + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Check",
                render: function (data, type, row) {
                    var details = "<text class='checkNumber'>" + (data.chequeNumber ? data.chequeNumber : "") + "</text>"
                    return details;
                }
            },
            {
                "data": null,
                "title": "Date",
                render: function (data, type, row) {
                    var details = new Date(data.chequeDate);
                    if (details == "") return "";
                    return "<text class='checkDate'>" + $.date(details) + "</text>"
                }
            },
            {
                "data": null,
                "title": "Check Type",
                render: function (data, type, row) {
                    var details = "<select class='form-control txtCheckType'>" +
                        "<option value='STANDARD'>Standard</option>" +
                        "<option value='MANAGER'>Manager's Check</option>" +
                        "</select>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Receipt Number",
                render: function (data, type, row) {
                    var details = "<input class='form-control txtReceiptNumber' type='text' placeholder='Receipt Number' />";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Receipt Type",
                render: function (data, type, row) {
                    var details = "<select class='form-control txtReceiptType'>" +
                        "<option value='OR'>Official Receipt</option>" +
                        "<option value='AR'>Acknowledgement Receipt</option>" +
                        "<option value='PR'>Provisionary Receipt</option>" +
                        "<option value='CR'>Collection Receipt</option>" +
                        "</select>";
                    return details;
                }
            },
        ],
        "createdRow": function (row, data, dataIndex) {
            if ($(row).find('.dataAreaId').html() == "SPAV") {
                $(row).addClass("spavi_row");
            } else if ($(row).find('.dataAreaId').html() == "SPCI") {
                $(row).addClass("spci_row");
            } else if ($(row).find('.dataAreaId').html() == "WBHI") {
                $(row).addClass("wbhi_row");
            } else if ($(row).find('.dataAreaId').html() == "NAF") {
                $(row).addClass("naf_row");
            } else if ($(row).find('.dataAreaId').html() == "SIL") {
                $(row).addClass("sil_row");
            } else if ($(row).find('.dataAreaId').html() == "DBE") {
                $(row).addClass("dbe_row");
            }
        },
    });

    HoldOn.close();

    $("#modalRelease").modal();
}

function MarkVoucherAsReleased() {
    voucherIdArr = [];
    
    var table = $("#dtPayment").DataTable();
    table.$("tr.selected").each(function () {
        let voucherId = $(this).find(".voucherId").text();
        voucherIdArr.push(voucherId);

    });

    if (voucherIdArr.length == 0) {
        swal({
            title: "No Row is Selected to Mark as Released",
            icon: "error",
            text: "Please select at least one record with status 'Unreleased' or 'Uncollected' before marking."
        })
    } else {
        swal({
            title: "Confirmation",
            text: "Are you sure thay you want to mark vouchers as Released? Once marked as released, this can't be undone.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((ok) => {
                if (ok) {
                    LoadVouchers();
                }
            });
    }
}

$(document).ready(function () {
    if (isEdge || isIE)
    {
        let width = $("input[type=file]").width();
            
        $("input[type=file]").css("display", "none");


        $("input[type=file]").after("<input class='form-control file-upload'" +
            "style='height:31px;width:"+ width +"px;' value='Choose a file' readonly></input>");

        $(".file-upload").click(function () {
            $("input[type=file]").trigger("click");
        })

        $("input[type=file]").change(function () {
            let file = $("input[type=file]").val().split("\\");

            if (file.length > 0) {
                $(".file-upload").val(file[file.length - 1]);
            } else {
                $(".file-upload").val($("input[type=file]").val());
            }
        })
    }

    $(".btnRelease").click(function () {
        if (!$("#dtReleaseDate").val()) {
            swal({
                icon: "error",
                title: "Please set the release date first before proceeding"
            })

            return false;
        } else {
            HoldOn.open({
                theme: "sk-circle",
                message: "Processing your Request ..."
            })

            let voucherModelArr = [];

            var table = $("#dtPayment").DataTable();
            table.$("tr.selected").each(function () {
                let voucherId = $(this).find(".voucherId").text();

                voucherModelArr.push({
                    voucherId: voucherId,
                    voucherId: $(this).find(".voucherId").html(),
                    vendorCode: $(this).find(".vendorCode").html(),
                    companyName: $(this).find(".companyName").html(),
                    entity: $(this).find(".dataAreaId").html(),
                    bank: $(this).find(".bankName").html(),
                    amount: removeCommas($(this).find(".amount").html()),
                    checkNum: $(this).find(".checkNumber").html(),
                    checkDate: $(this).find(".checkDate").html(),
                })
            });
            

            $.ajax({
                type: "POST",
                url: $("#dtPayment").attr("markurl"),
                data: JSON.stringify({
                    voucherIdArr: voucherIdArr,
                    releaseDate: $("#dtReleaseDate").val(),
                    voucher: voucherModelArr
                }),
                dataType: 'json',
                traditional: true,
                contentType: "application/json",
                success: function (data) {
                    HoldOn.close();

                    swal({
                        title: "Voucher/s are marked as released!",
                        icon: "success"
                    }).then(function () {
                        document.location.reload();
                    });
                }
            })
        }
    })

    $("#dtPayment").setCustomSearchInput();
    dtPayment = $('#dtPayment').DataTable({
        initComplete: function () {
            $("#dtPayment").setCustomSearch({
                exemption: [0]
            });
        },
        ajax: {
            url: $('#dtPayment').attr("url"),
            datatype: 'json',
            dataSrc: '',

        },
        "columns": [
            {
                "data": null,
                "title": "<th class='text-center'><input type='checkbox' id='cbxSelectAll' /></th>",
                render: function (data, type, row) {
                    var details = "";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Entity",
                render: function (data, type, row) {
                    var details = "<text class='dataAreaId " + data.dataAreaId + "'>" + data.dataAreaId + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Vendor Code",
                render: function (data, type, row) {
                    var details = "<text class='vendorCode'>" + data.vendorCode + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Company",
                render: function (data, type, row) {
                    var details = "<text class='companyName'>" + data.companyName + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Bank",
                render: function (data, type, row) {
                    var details = "<text class='bankName'>" + data.bankName + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Voucher",
                render: function (data, type, row) {
                    var details = "<text class='voucherId'>" + data.voucherId + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Amount",
                render: function (data, type, row) {
                    var details = "<span>&#8369;</span><text class='amount'>" + parseMoney(data.amount) + "</text>"
                    return details;
                }
            },
            { data: "methodOfPayment", title: "MOP" },
            {
                "data": null,
                "title": "Check",
                render: function (data, type, row) {
                    var details = "<text class='checkNumber'>" + (data.chequeNumber ? data.chequeNumber : "") + "</text>"
                    return details;
                }
            },
            {
                "data": null,
                "title": "Date",
                render: function (data, type, row) {
                    var details = parseJsonDate(data.chequeDate);
                    if (details == "") return "";
                    return "<text class='checkDate'>" + $.date(details) + "</text>"
                }
            },
            {
                "data": null,
                "title": "Status",
                render: function (data, type, row) {
                    return StatusLabel(data.status);
                }
            }
        ],
        "createdRow": function (row, data, dataIndex) {
            $(row).attr("id", data.voucherId);

            if ($(row).find('.dataAreaId').html() == "SPAV") {
                $(row).addClass("spavi_row");
            } else if ($(row).find('.dataAreaId').html() == "SPCI") {
                $(row).addClass("spci_row");
            } else if ($(row).find('.dataAreaId').html() == "WBHI") {
                $(row).addClass("wbhi_row");
            } else if ($(row).find('.dataAreaId').html() == "NAF") {
                $(row).addClass("naf_row");
            } else if ($(row).find('.dataAreaId').html() == "SIL") {
                $(row).addClass("sil_row");
            } else if ($(row).find('.dataAreaId').html() == "DBE") {
                $(row).addClass("dbe_row");
            }
        },
        columnDefs: [{
            orderable: false,
            className: 'select-checkbox',
            targets: [0]
        }, {
            className: 'text-right',
            targets: [5]
        }

        ],
        select: {
            style: 'multi',
            selector: 'td:first-child'
        },
        order: [
            [1, 'asc']
        ]
    })

    dtPayment.on("click", "#cbxSelectAll", function () {
        if ($('#cbxSelectAll').prop("checked") == false) {
            dtPayment.rows().deselect();
            $("th.select-checkbox").removeClass("selected");
        } else {
            dtPayment.rows({ search: 'applied' }).select();
            $("th.select-checkbox").addClass("selected");
        }
    })

    dtPayment.on('select', function (e, dt, type, indexes) {
        GetTotal(dtPayment, type);
    }).on('deselect', function (e, dt, type, indexes) {
        GetTotal(dtPayment, type);
    });


    $("#dtrIntegrate").daterangepicker(null, function (a, b, c) { console.log(a.toISOString(), b.toISOString(), c) })

    $.contextMenu({
        selector: '#dtPayment',
        items: {
            "RFD": {
                name: "Request Selected Items for Verification",
                icon: "fa-external-link",
                callback: function (key, opt) {
                    MakeRequest();
                }
            },
            "MFR": {
                name: "Mark Vouchers as Released",
                icon: "fa-long-arrow-up",
                callback: function (key, opt) {
                    MarkVoucherAsReleased();
                }
            }
        }
    });

    $("#btnRequest").click(function () {
        MakeRequest();
    })

    $("#btnRequest2").click(function () {
        MakeRequest();
    })

    $("#btnSubmitRequest").click(function () {
        let voucherModelArr = [];
        var table = $("#dtPayment").DataTable();
        table.$("tr.selected").each(function () {
            let status = ($(this).find(".status").text()).toUpperCase();
            let voucherId = $(this).find(".voucherId").text();
            if (status == "UNRELEASED" || status == "UNCOLLECTED" || status == "REQUESTED FOR HOLD") {
                voucherModelArr.push({
                    voucherId: voucherId,
                    status: status,
                    voucherId: $(this).find(".voucherId").html(),
                    vendorCode: $(this).find(".vendorCode").html(),
                    companyName: $(this).find(".companyName").html(),
                    entity: $(this).find(".dataAreaId").html(),
                    bank: $(this).find(".bankName").html(),
                    amount: removeCommas($(this).find(".amount").html()),
                    checkNum: $(this).find(".checkNumber").html(),
                    checkDate: $(this).find(".checkDate").html(),
                })
            }
        });

        if (voucherModelArr.length == 0) {
            swal({
                title: "No Row is Selected for Verification Request",
                icon: "error",
                text: "Please select at least one record with status 'Unreleased' or 'Uncollected' before requesting."
            })
        } else {
            rvfai.requestVoucherForVerification(voucherModelArr);
        }
    })

    $("#btnViewSelected").click(function () {
        HoldOn.open({
            theme: "sk-circle"
        })

        setTimeout(function () {
            var table = $("#dtPayment").DataTable();

            $("#dtSelectedPayment").DataTable().destroy();
            $("#dtSelectedPayment tbody").html("");

            $("#dtSelectedPayment").setCustomSearchInput();

            table.$("tr.selected").each(function () {
                $("#dtSelectedPayment tbody").append("<tr>" + $(this).closest("tr").html() + "</tr>");
            });

            $("#dtSelectedPayment").find("td.select-checkbox").each(function () {
                $(this).remove();
            });

            $("#dtSelectedPayment").DataTable({
                destroy: true,
                initComplete: function () {
                    $("#dtSelectedPayment").setCustomSearch();
                },
                "createdRow": function (row, data, dataIndex) {
                    if ($(row).find('.dataAreaId').html() == "SPAV") {
                        $(row).addClass("spavi_row");
                    } else if ($(row).find('.dataAreaId').html() == "SPCI") {
                        $(row).addClass("spci_row");
                    } else if ($(row).find('.dataAreaId').html() == "WBHI") {
                        $(row).addClass("wbhi_row");
                    } else if ($(row).find('.dataAreaId').html() == "NAF") {
                        $(row).addClass("naf_row");
                    } else if ($(row).find('.dataAreaId').html() == "SIL") {
                        $(row).addClass("sil_row");
                    } else if ($(row).find('.dataAreaId').html() == "DBE") {
                        $(row).addClass("dbe_row");
                    }
                },
            });

            HoldOn.close();

            $("#modalViewSelected").modal();
        }, 300);

        
    })

    $("#btnExport").click(function () {
        if ($("#dtSelectedPayment").find(".dataTables_empty").length) {
            swal({
                icon: "error",
                title: "No Voucher is Selected",
                text: "Please select at least one voucher before exporting."
            })
        } else {
            let table = $("#dtSelectedPayment").DataTable();
            let voucherArr = [];

            table.$("tr").each(function () {
                voucherArr.push({
                    Entity: $(this).find(".dataAreaId").html(),
                    VendorCode: $(this).find(".vendorCode").html(),
                    CompanyName: $(this).find(".companyName").html(),
                    BankName: $(this).find(".bankName").html(),
                    VoucherNumber: $(this).find(".voucherId").html(),
                    Amount: removeCommas($(this).find(".amount").html()),
                    CheckNumber: $(this).find(".checkNumber").html(),
                    CheckDate: $(this).find(".checkDate").html()
                })
            });

            $.ajax({
                url: "/SEAN/ExportVoucherToExcel",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify({
                    voucherArr: voucherArr
                }),
                success: function (data) {
                    if (data == "ERROR") {
                        swal({
                            title: "There is en error processing your request",
                            icon: "error"
                        })
                    } else {
                        window.location.href = "/Report/DownloadExcel?fileName=" + data;
                    }
                }
            });
        }
    })

    $("#btnUploadExcel").click(function () {
        if ($("#frmUploadExcel")[0].checkValidity() == false) {
            $("#frmUploadExcel")[0].reportValidity()
        } else {
            HoldOn.open({
                theme: "sk-circle",
                message: "Uploading an excel file.."
            })

            var excelFile = new FormData($("#frmUploadExcel")[0]);

            $.ajax({
                url: "/SEAN/ImportVoucherNoExcel",
                type: 'POST',
                data: excelFile,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data == "NULL") {
                        swal({
                            title: "Please select an excel file first before importing.",
                            icon: "error"
                        })
                        HoldOn.close();
                    } else if (data == "INVALID FILE") {
                        swal({
                            title: "Invalid excel file.",
                            icon: "error"
                        })
                        HoldOn.close();
                    } else if (data == "EMPTY") {
                        swal({
                            title: "Excel file seems empty or in different format.",
                            icon: "error"
                        })
                        HoldOn.close();
                    } else {
                        dtPayment.rows().deselect();

                        let notFoundArr = [];

                        for (var i = 0; i < data.length; i++) {
                            if (dtPayment.rows(data[i]).count() == 0) {
                                notFoundArr.push(data[i]);
                            };
                        }

                        $("#frmUploadExcel")[0].reset()

                        dtPayment.rows(data).select();

                        if (notFoundArr.length > 0) {
                            swal({
                                title: "Not All Vouchers from Excel is Selected",
                                icon: "error",
                                text: "The following voucher/s (" + RemoveHashtagFromArray(notFoundArr).join(",") + ") " +
                                    "can't be found in the table because of the ff possible reasons: 1) You have entered a wrong id. " +
                                    "2) The voucher is not existing in the system yet. Please sync with D365 or import excel first. " +
                                    "3) The voucher status is not unreleased or uncollected."
                            })
                        }

                        HoldOn.close();
                    }
                },
                error: function () {
                    HoldOn.close();
                    swal({
                        title: "There is an error processing your request to the server.",
                        icon: "error",
                    });
                }
            });
        }
    })

    $("#dtRelease").daterangepicker({ singleDatePicker: !0, singleClasses: "picker_2" });
    $("#txtCollectorType").change(function () {
        if ($(this).val() == "AUTHORIZED") {
            $("#txtThirdPartyCompany").closest("div").css("display", "none");
        } else if ($(this).val() == "THIRD PARTY") {
            $("#txtThirdPartyCompany").closest("div").css("display", "block");
        }
    })

    $("#btnReleaseSelected").click(function () {
        HoldOn.open({
            theme: "sk-circle",
            message: "Releasing Vouchers..."
        })

        let parameter;
        let hasError = false;
        let voucherArray = [];

        dtVoucher.$("tr").each(function (index) {
            let dataAreaId = $(this).find(".dataAreaId").html();
            let bankName = $(this).find(".bankName").html();
            let amount = $(this).find(".amount").attr("amount");
            let checkNum = $(this).find(".checkNumber").html();
            let checkDate = $(this).find(".checkDate").html();
            let companyName = $(this).find(".voucherId").attr("companyName");
            let vendorCode = $(this).find(".voucherId").attr("vendorCode");
            let receiptNumber = $(this).find(".txtReceiptNumber").val();
            let receiptType = $(this).find(".txtReceiptType").val();
            let voucherId = $(this).find(".voucherId").html();
            let checkType = $(this).find(".txtCheckType").val();

            if (receiptType != "DRY") {
                voucherArray.push({
                    voucherId: voucherId,
                    vendorCode: vendorCode,
                    companyName: companyName,
                    entity: dataAreaId,
                    bank: bankName,
                    amount: amount,
                    checkNum: checkNum,
                    checkDate: checkDate,
                    ReceiptNo: receiptNumber,
                    ReceiptType: receiptType,
                    CheckType: checkType
                })
            }
        });

        if (voucherArray.length == 0) {
            HoldOn.close();
            swal({
                title: "No voucher for release is submitted.",
                text: "Please fill at least one receipt number for voucher.",
                icon: "error"
            })
        } else {
            parameter = JSON.stringify({
                voucher: voucherArray,
                voucherIdArr: voucherIdArr,
                releaseDate: $("#dtRelease").val(),
                c: {
                    CollectorName: $("#txtCollectorName").val(),
                    CollectorType: $("#txtCollectorType").val(),
                    ThirdPartyCompany: $("#txtThirdPartyCompany").val()
                }
            })
            
            $.ajax({
                type: "Post",
                url: "/SEAN/MarkVouchersAsReleased",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: parameter,
                success: function (data) {
                    HoldOn.close();
                    swal({
                        title: "Vouchers are released successfully!",
                        icon: "success"
                    }).then(function () {
                        document.location.reload();
                    });
                },
                error: function () {
                    swal({
                        title: "There is an error occured releasing the voucher.",
                        icon: "error"
                    });
                }
            })
        }
    })
})

var rvfai = {
    requestVoucherForVerification: function (voucherModelArr) {
        if (new Date($(".txtReleasingDate").val()) < currentDate) {
            swal({
                title: "Invalid Date",
                text: "Please select the date later than today.",
                icon: "error"
            })

            return false;
        }

        HoldOn.open({
            theme: "sk-circle",
            message: "Sending " + voucherModelArr.length + " data record/s for verification..."
        });

        let date = ($(".txtReleasingDate").val()).split("/");

        var model = JSON.stringify({
            voucher: voucherModelArr,
            releasingDate: date[1] + "/" + (date[0]) + "/" + date[2],
            note: $("#txtNotes").val()
        });

        $.ajax({
            url: requestVoucherForVerificationUrl,
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: model,
            success: function (data) {
                HoldOn.close()
                if (data == "SUCCESS") {
                    swal({
                        title: "Verification Request Successfully Sent!",
                        icon: "success"
                    }).then(function () {
                        document.location.reload();
                    });
                } else {
                    swal({
                        title: "There is an error processing your request.",
                        icon: "error"
                    })
                }
            }
        });
    }
}


function GetTotal(dtPayment, type) {
    $(".container_item").html("");

    if (type === 'row') {
        var totalAmount = 0.0;
        var itemAmountArr = [];
        var bankArray = [];
        var entityArray = [];

        dtPayment.$("tr.selected").each(function () {
            var amount = removeCommas($(this).find(".amount").text())
            totalAmount += parseFloat(amount);

            let entity = $(this).find(".dataAreaId").html();
            let bank = $(this).find(".bankName").html();

            itemAmountArr.push({
                entity: entity,
                bankName: bank,
                amount: amount
            })

            if (bankArray.indexOf(bank) == -1) {
                bankArray.push(bank)
            }

            if (entityArray.indexOf(entity) == -1) {
                entityArray.push(entity);
            }
        });

        $.each(entityArray, function (i, item) {
            let entity = item;

            let itemArray = $.grep(itemAmountArr, function (n, i) {
                return n.entity == entity;
            })

            let total = 0;
            let subitems = [];

            $.each(itemArray, function (i, item) {
                let index = GetIndexInJsonByKeyVal(subitems, "bankName", item.bankName);

                if (index >= 0) {
                    subitems[index].amount = parseFloat(subitems[index].amount) + parseFloat(item.amount);
                } else {
                    subitems.push({
                        bankName: item.bankName,
                        amount: item.amount
                    })
                }

                total += parseFloat(item.amount);
            })

            $(".by_entity .container_item").append('<div class="summary_item ' + entity + '">' +
                '<div class="item_header">' +
                '<label>' + entity + ' (₱' + parseMoney(total) + ')</label>' +
                '</div>' +
                '<div class="container_sub_item">' +
                '</div>' +
                '</div>')


            $.each(subitems, function (i, item) {
                $(".summary_item." + entity + " .container_sub_item").append('<div class="sub-item">' +
                    '<label>' + item.bankName + ' - ₱' + parseMoney(item.amount) +
                    '</label></div>')
            })
        })

        $.each(bankArray, function (i, item) {
            let bank = item;

            let itemArray = $.grep(itemAmountArr, function (n, i) {
                return n.bankName == bank;
            })

            let total = 0;
            let subitems = [];

            $.each(itemArray, function (i, item) {
                let index = GetIndexInJsonByKeyVal(subitems, "entity", item.entity);

                if (index >= 0) {
                    subitems[index].amount = parseFloat(subitems[index].amount) + parseFloat(item.amount);
                } else {
                    subitems.push({
                        entity: item.entity,
                        amount: item.amount
                    })
                }

                total += parseFloat(item.amount);
            })


            $(".by_bank .container_item").append('<div class="summary_item ' + bank + '">' +
                '<div class="item_header">' +
                '<label>' + bank + ' (₱' + parseMoney(total) + ')</label>' +
                '</div>' +
                '<div class="container_sub_item">' +
                '</div>' +
                '</div>')


            $.each(subitems, function (i, item) {
                $(".summary_item." + bank + " .container_sub_item").append('<div class="sub-item">' +
                    '<label>' + item.entity + ' - ₱' + parseMoney(item.amount) +
                    '</label></div>')
            })
        })

        //$("#btnRequest").html("<i class='fa fa-external-link'></i> Request <u>₱" + parseMoney(totalAmount) + "</u> Worth of Vouchers");
        $("#btnRequest2").html("<i class='fa fa-external-link'></i> Request <u>₱" + parseMoney(totalAmount) + "</u> Worth of Vouchers");
        $("#lblTotal").html("Total : ₱" + parseMoney(totalAmount))
        $("#lblTotal").css("display", "inline-block");
    }
}

function AddData(json, key, value) {
    json[key] += json[key]
}

function RemoveHashtagFromArray(arr) {
    resultArr = [];

    for (let i = 0; i < arr.length; i++) {
        if (arr[i].indexOf("#") > -1) {
            resultArr.push(arr[i].substring(1))
        } else {
            resultArr.push(arr[i]);
        }
    }

    return resultArr;
}


function CheckValueInJson(json, key, value) {
    let isExisting = false;

    $.each(json, function (i, item) {
        if (item[key] == value) {
            isExisting = true;
        }
    });

    return isExisting;
}

function GetIndexInJsonByKeyVal(json, key, value) {
    let index = -1;

    $.each(json, function (i, item) {
        if (item[key] == value) {
            index = i;
        }
    });

    return index;
}