﻿var currentDate = new Date();

const _MS_PER_DAY = 1000 * 60 * 60 * 24;

// a and b are javascript Date objects
function dateDiffInDays(a, b) {
    // Discard the time and time-zone information.
    const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

Date.prototype.addDays = function (days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

function LoadTable() {
    currentDate = new Date($("#single_cal2").val());
    $("#txtAsOfDate").html("(as of " + formatDate(currentDate, "MMMM dd, yyyy") + ")");

    $("#dtPayment").DataTable().destroy();

    dtPayment = $('#dtPayment').DataTable({
        destroy: true,
        initComplete: function () {
            $("#dtPayment").setCustomSearch({
            });
            HoldOn.close();
        },
        ajax: {
            url: $('#dtPayment').attr("url") + "&statusDate=" + formatDate(currentDate, "MMMM dd, yyyy") +
            "&filterCheckDate=true",
            datatype: 'json',
            dataSrc: '',
        },
        "columns": [
            {
                "data": null,
                "title": "Entity",
                render: function (data, type, row) {
                    var details = "<text class='entity dataAreaId'>" + data.dataAreaId + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Voucher",
                render: function (data, type, row) {
                    var details = "<text class='entity dataAreaId'>" + data.voucherId + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Check",
                render: function (data, type, row) {
                    var details = "<text class='checkNumber'>" + (data.chequeNumber ? data.chequeNumber : "") + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Payee",
                render: function (data, type, row) {
                    var details = "<text class='payee'>" + data.companyName + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Date Received",
                render: function (data, type, row) {
                    let checkDate = parseJsonDate(data.chequeDate);
                    if (checkDate == "") return "Not Set";
                    var details = "<text class='dateReceived'>" + formatDate(checkDate, "MM/dd/yyyy") + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "No of Days",
                render: function (data, type, row) {
                    let checkDate = parseJsonDate(data.chequeDate);
                    if (checkDate == "") return "Not Set";

                    var details = "<text class='noOfDays'>" + (dateDiffInDays(checkDate, currentDate) - 1) + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Term",
                render: function (data, type, row) {
                    return "30";
                }
            },
            {
                "data": null,
                "title": "Due Date",
                render: function (data, type, row) {
                    let checkDate = parseJsonDate(data.chequeDate);
                    if (checkDate == "") return "Not Set";

                    var details = "<text class='dueDate'>" + formatDate((checkDate.addDays(30)), "MM/dd/yyyy") + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Over Due",
                render: function (data, type, row) {
                    let checkDate = parseJsonDate(data.chequeDate);
                    if (checkDate == "") return "Not Set";
                    let dueDate = checkDate.addDays(30);
                    var details = "<text class='overDue'>" + (dateDiffInDays(dueDate, currentDate) - 1) + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Check Date",
                render: function (data, type, row) {
                    let checkDate = parseJsonDate(data.chequeDate);
                    if (checkDate == "") return "Not Set";

                    var details = "<text class='checkDate'>" + formatDate((checkDate.addDays(30)), "MM/dd/yyyy") + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Amount",
                render: function (data, type, row) {
                    var details = "<text class='amount1'>" + parseMoney(data.amount) + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Current",
                render: function (data, type, row) {
                    var details = "<text class='current'>0.00</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "1-30",
                render: function (data, type, row) {
                    var details = "<text class='term1'>0.00</text>"
                    return details;
                }
            },
            {
                "data": null,
                "title": "31-60",
                render: function (data, type, row) {
                    var details = "<text class='term2'>0.00</text>"
                    return details;
                }
            },
            {
                "data": null,
                "title": "61-90",
                render: function (data, type, row) {
                    var details = "<text class='term3'>0.00</text>"
                    return details;
                }
            },
            {
                "data": null,
                "title": "91-120",
                render: function (data, type, row) {
                    var details = "<text class='term4'>0.00</text>"
                    return details;
                }
            },
            {
                "data": null,
                "title": "Over 120",
                render: function (data, type, row) {
                    var details = "<text class='term5'>0.00</text>"
                    return details;
                }
            },
            {
                "data": null,
                "title": "Amount",
                render: function (data, type, row) {
                    var details = "<text class='amount2'>" + parseMoney(data.amount) + "</text>";
                    return details;
                }
            },
        ],
        "createdRow": function (row, data, dataIndex) {
            if ($(row).find('.overDue').html() < 1) {
                $(row).find(".current").html(parseMoney(data.amount));
            } else if ($(row).find('.overDue').html() < 31) {
                $(row).find(".term1").html(parseMoney(data.amount));
            } else if ($(row).find('.overDue').html() < 61) {
                $(row).find(".term2").html(parseMoney(data.amount));
            } else if ($(row).find('.overDue').html() < 91) {
                $(row).find(".term3").html(parseMoney(data.amount));
            } else if ($(row).find('.overDue').html() < 121) {
                $(row).find(".term4").html(parseMoney(data.amount));
            } else if ($(row).find('.overDue').html() > 120) {
                $(row).find(".term5").html(parseMoney(data.amount));
            }

            if ($(row).find('.dataAreaId').html() == "SPAV") {
                $(row).addClass("spavi_row");
            } else if ($(row).find('.dataAreaId').html() == "SPCI") {
                $(row).addClass("spci_row");
            } else if ($(row).find('.dataAreaId').html() == "WBHI") {
                $(row).addClass("wbhi_row");
            } else if ($(row).find('.dataAreaId').html() == "NAF") {
                $(row).addClass("naf_row");
            } else if ($(row).find('.dataAreaId').html() == "SIL") {
                $(row).addClass("sil_row");
            } else if ($(row).find('.dataAreaId').html() == "DBE") {
                $(row).addClass("dbe_row");
            }
        },
        order: [
            [2, 'asc']
        ]
    })
}

$(document).ready(function () {
    $("#dtPayment").setCustomSearchInput();
    LoadTable();
    

    $("#btnExport").click(function () {
        HoldOn.open({
            theme: "sk-circle",
            message: "Exporting the Records into Excel ..."
        });

        //var table = $("#dtPayment").DataTable();
        //var rows = [];

        //table.$("tr").each(function () {
        //    rows.push({
        //        Entity: $(this).find(".entity").html(),
        //        CheckNumber: $(this).find(".checkNumber").html(),
        //        Payee: $(this).find(".payee").html(),
        //        DateReceived: $(this).find(".dateReceived").html(),
        //        NoOfDays: $(this).find(".noOfDays").html(),
        //        DueDate: $(this).find(".dueDate").html(),
        //        Amount: removeCommas($(this).find(".amount1").html()),
        //        OverDue: $(this).find(".overDue").html(),
        //        CheckDate: $(this).find(".checkDate").html(),
        //        Current: removeCommas($(this).find(".current").html()),
        //        Term1: removeCommas($(this).find(".term1").html()),
        //        Term2: removeCommas($(this).find(".term2").html()),
        //        Term3: removeCommas($(this).find(".term3").html()),
        //        Term4: removeCommas($(this).find(".term4").html()),
        //        Term5: removeCommas($(this).find(".term5").html()),
        //        Amount2: removeCommas($(this).find(".amount1").html()),
        //        Term: 30
        //    })
        //});
        

        //var model = JSON.stringify({
        //    asofdate: formatDate(currentDate, "MM/dd/yyyy"),
        //    a: rows
        //})


        $.ajax({
            url: $(this).attr("url") + "?asOfDate=" + formatDate(currentDate, "yyyy-MM-dd"),
            type: "POST",
            //contentType: 'application/json; charset=utf-8',
            //dataType: 'json',
            //traditional: true,
            //data: model,
            success: function (data) {
                HoldOn.close()

                if (data == "ERROR") {
                    swal({
                        title: "There is en error processing your request",
                        icon: "error"
                    })
                } else {
                    window.location.href = $("#btnExport").attr("downloadurl") + "?fileName=" + data
                }
            }
        });
    })

    $("#btnSet").click(function () {
        HoldOn.open({
            theme: "sk-circle"
        })
        setTimeout(function () {
            LoadTable();
        }, 300)
    })
})

