﻿$(document).ready(function () {
    var dtUser = $('#dtUser').DataTable({
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('offersDataTables', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('offersDataTables'));
        },
        ajax: {
            url: $('#dtUser').attr("url"),
            datatype: 'json',
            dataSrc: '',

        },
        "columns": [
        { data: "username", title: "Username" },
        {
            "data": null,
            "title": "Roles",
            render: function (data, type, row) {
                var details = data.roles;
                return details;
            }
        },
        {
            "data": null,
            "title": "Status",
            render: function (data, type, row) {
                var details = (data.status == true ? "<span class='label label-danger'>On Leave</span>" : "<span class='label label-success'>Active</span>");
                return details;
            }
        }
        ]
    })

    $("#btnAdd").click(function () {
        $("#modalUser").modal();
        $("#txtUsername").val("");
        $("#txtPassword").val("");
    })


    $("#btnAddUser").click(function () {
        let username = $("#txtUsername").val();
        let role = $("#txtUsertype").val();
        let password = $("#txtPassword").val();
        let password2 = $("#txtPassword2").val();
        let email = $("#txtEmail").val();

        $("#frmUser")[0].reportValidity();

        

        if (username == "" || role == "" || password == "" || password2 == "") {
            swal({
                title: "Please fill up all inputs before adding the user.",
                icon: "error"
            })
        } else if (password != password2) {
            swal({
                title: "Passwords don't match.",
                icon: "error"
            })
        } else if (validateEmail(email) == false) {
            swal({
                title: "Please provide an email address.",
                icon: "error"
            })
        } else {
            var model = JSON.stringify({
                UserName: username,
                Password: password,
                Email: email,
                Role: role
            });

            $.ajax({
                type: "Post",
                url: $("#frmUser").attr("url"),
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: model,
                success: function (data) {
                    if (data == "SUCCESS") {
                        swal({
                            title: "New user is successfully added!",
                            icon: "success"
                        }).then(function () {
                            document.location.reload();
                        });
                    } else {
                        swal({
                            title: "Registration Error",
                            text: String(data),
                            icon: "error"
                        });
                    }
                },
                error: function () {
                    swal({
                        title: "There is an error occured adding a new vendor.",
                        icon: "error"
                    });
                }
            })
        }
    })
})