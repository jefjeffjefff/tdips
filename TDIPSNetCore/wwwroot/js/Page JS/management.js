﻿$(document).ready(function () {
    $.ajax({
        type: "GET",
        url: $("#txtAnnouncement").attr("url"),
        success: function (data) {
            $("#txtAnnouncement").val(data);
        }
    })


    $('#cbxActivateRating').on('ifChanged', function (e) {
        var isActivate = $(this).prop("checked")

        $.ajax({
            url: "/ECREA/SetKioskRatingActivation?isActivate=" + isActivate,
        })
    });

    $("#txtAnnouncement").focusout(function () {
        $.ajax({
            type: "GET",
            url: $(this).attr("saveUrl") + "?text=" + $(this).val()
        })
        $(this).css("background-color", "#F1F1F1");
    });

    $("#txtAnnouncement").focus(function () {
        $(this).css("background-color", "inherit");
    });


    $("#btnUploadVideo").click(function () {
        if ($("input[type=file]").val() == "") {
            swal({
                title: "Please select a video file first before uploading.",
                icon: "error"
            })
        } else {
            HoldOn.open({
                theme: "sk-circle",
                message: "Uploading a video file .."
            });

            var videoFile = new FormData($("#frmUploadVideo")[0]);

            $.ajax({
                url: $("#frmUploadVideo").attr("url"),
                type: 'POST',
                data: videoFile,
                contentType: false,
                processData: false,
                success: function (data) {
                    HoldOn.close();
                    if (data == "SUCCESS") {
                        swal({
                            title: "Video file is successfully uploaded!",
                            icon: "success"
                        }).then(function () {
                            document.location.reload();
                        });
                    } else if (data == "INVALID FORMAT") {
                        swal({
                            title: "Invalid Video Format",
                            icon: "error",
                            text: "Please choose a video file."
                        })
                    } else {
                        swal({
                            title: "Uploading failed. Please retry.",
                            icon: "error"
                        })
                    }
                },
                error: function () {
                    HoldOn.close();
                    swal({
                        title: "There is an error processing your request to the server.",
                        icon: "error",
                    });
                }
            });
        }
    })

    $.ajax({
        type: "GET",
        url: $(".video_container").attr("url"),
        success: function (data) {
            $.each(data, function (i, item) {
                $(".video_container").append('<div class="div_video">' +
                        '<video width="360" height="270" controls preload="none">' +
                         '<source src="' + $(".video_container").attr("source") + item.videoId + "." +
                         item.fileExtension + '" type="video/mp4">' +
                         'Your browser does not support the video tag.' +
                         '</video><div class="container_txt_btn"><div class="title_container"><label class="video_title">'+ item.fileName +'</label></div>' +
                         '<button videoId="' + item.videoId + '" fileExtension="' + item.fileExtension +
                         '" class="btn btn-danger btnDeleteVideo">Delete</button></div></div>')
            })

            $(".btnDeleteVideo").click(function () {
                swal({
                    title: "Are you sure that you want to delete the video?",
                    text: "Once deleted, you will not be able to recover this video!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            type: "POST",
                            url: $(".video_container").attr("deleteUrl") + "?videoId=" + $(this).attr("videoId") + "&fileExtension=" + $(this).attr("fileExtension"),
                            success: function (data) {
                                if (data == "SUCCESS") {
                                    swal({
                                        title: "Video is deleted.",
                                        icon: "success"
                                    }).then(function () {
                                        document.location.reload();
                                    });
                                } else {
                                    swal({
                                        title: "Failed. Please retry.",
                                        icon: "error"
                                    })
                                }
                            }
                        })
                    }
                });
            })
        }
    })
})