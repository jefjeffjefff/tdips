﻿var requestVoucherForApprovalUrl = "";
var importPaymentExcelUrl = "";
var importCheckExcelUrl = "";
var integratePaymentD365Url = "";
var insertCheckDetailsUrl = "";

//https://datatables.net/extensions/select/examples/

$(document).ready(function () {
    if (isEdge || isIE) {
        $("#cbxFileType").css("height", "38px")
        $("#excelFile").css("height", "38px")
    }

    $("#btnViewLog").click(function () {
        LoadD365Logs();
        $("#mdlViewLog").modal();
    })

    $("#dtPayment").setCustomSearchInput();
    
    var dtPayment = $('#dtPayment').dataTable({
        ajax: {
            url: "/SEAN/GetViewData",
            datatype: 'json',
            dataSrc: 'dataList',
            type: "POST",
            data: function (d) {
                d.vendorcode = $("#0").val(),
                    d.companyName = $("#1").val(),
                    d.bank = $("#2").val(),
                    d.entity = $("#3").val(),
                    d.voucherId = $("#4").val(),
                    d.amount = removeCommas($("#5").val()),
                    d.payment = $("#6").val(),
                    d.check = $("#7").val(),
                    d.date = $("#8").val(),
                    d.status = $("#dtPayment").attr("initstatus") || $("#9").val()
            },
            beforeSend: function () {
                $('#dtPayment > tbody').html(
                    '<tr class="odd customized_loading">' +
                    '<td valign="top" colspan="' + $("#dtPayment thead th").length + '" class="dataTables_empty">Loading&hellip;</td>' +
                    '</tr>'
                );
            }
        },
        "columns": [
            { data: "vendorCode", title: "Vendor Code" },
            { data: "companyName", title: "Company Name" },
            { data: "bankName", title: "Bank" },
            {
                "data": "dataAreaId",
                "title": "Entity",
                render: function (data, type, row) {
                    var details = "<text class='dataAreaId'>" + data + "</text>";
                    return details;
                }
            },
            {
                "data": "voucherId",
                "title": "Voucher ID",
                render: function (data, type, row) {
                    var details = "<text class='voucherId'>" + data + "</text>";
                    return details;
                }
            },
            {
                "data": "amount",
                "title": "Amount",
                render: function (data, type, row) {
                    var details = "<span>&#8369;</span>" + parseMoney(data);
                    return details;
                }
            },
            {
                "data": "methodOfPayment",
                "title": "Payment",
                render: function (data, type, row) {
                    var details = "<div style='text-transform:capitalize'>" + data.toLowerCase() + "</div>";
                    return details;
                }
            },
            { data: "chequeNumber", title: "Check" },
            {
                "data": "chequeDate",
                "title": "Date",
                render: function (data, type, row) {
                    var details = parseJsonDate(data);
                    if (details == "") return "";
                    return $.date(details);
                }
            },
            {
                "data": "status",
                "title": "Status",
                render: function (data, type, row) {
                    return StatusLabel(data);
                }
            }
        ],
        "createdRow": function (row, data, dataIndex) {
            $(row).addClass("row_data");
            $(row).attr("source", data.source);

            if ($(row).find('.dataAreaId').html() == "SPAV") {
                $(row).addClass("spavi_row");
            } else if ($(row).find('.dataAreaId').html() == "SPCI") {
                $(row).addClass("spci_row");
            } else if ($(row).find('.dataAreaId').html() == "WBHI") {
                $(row).addClass("wbhi_row");
            } else if ($(row).find('.dataAreaId').html() == "NAF") {
                $(row).addClass("naf_row");
            } else if ($(row).find('.dataAreaId').html() == "SIL") {
                $(row).addClass("sil_row");
            } else if ($(row).find('.dataAreaId').html() == "DBE") {
                $(row).addClass("dbe_row");
            }
        },
        columnDefs: [{
            className: 'text-right td_amount',
            targets: [5]
        },
            {
                className: 'td_status',
                targets: [9]
            },
            {
                className: 'td_vendorCode',
                targets: [0]
            },
            {
                className: 'td_companyName',
                targets: [1]
            },
            {
                className: 'td_bank',
                targets: [2]
            },
            {
                className: 'td_entity',
                targets: [3]
            },
            {
                className: 'td_voucherId',
                targets: [4]
            },
            {
                className: 'td_method',
                targets: [6]
            },
            {
                className: 'td_checkNumber',
                targets: [7]
            },
            {
                className: 'td_checkDate',
                targets: [8]
            },
        ],
        "initComplete": function (settings, json) {
            $("#dtPayment").setCustomSearch({
                onchange: "false",
                initValue: [{ target: 9, value: $("#dtPayment").attr("initStatus").replace(/%20/, " ") }],
                placeholder: true
            });
            
            $("#dtPayment").attr("initStatus", "")
            
        },
        "drawCallback": function () {
            $("#dtPayment .row_data td:not(:empty):not(.td_status)").off('click').click(function () {
                $("#dtStatusLogs").DataTable().destroy();
                let tr = $(this).closest("tr");

                let voucherId = $(tr).find(".voucherId").html();

                $("#dtStatusLogs").DataTable({
                    ajax: {
                        url: "/SEAN/GetVoucherStatusLogs?voucherId=" + voucherId,
                        dataSrc: '',
                        dataType: 'json'
                    },
                    destroy: true,
                    columns: [
                        {
                            "title" : "Date/Time", "data" : "logDate"
                        },
                        {
                            "title": "User", "data": "userId"
                        },
                        {
                            "title": "Status", "data": "status"
                        }
                    ],
                    columnDefs: [{
                        targets: [0, 1, 2],
                        className: "text-center"
                    }],
                    searching: false,
                    paging: false,
                    info: false
                });

                let status = $(tr).find(".td_status span").html();
                
                if (status.toUpperCase() == "RELEASED") {
                    $.ajax({
                        url: "/SEAN/GetCollectedCheckDetails?voucherId=" + voucherId,
                        success: function (data) {
                            let releasedDate = formatDate(parseJsonDate(data.releasedDate), "MM-dd-yyyy")
                            

                            $("#collected-check-details .releasedDate").html(releasedDate);

                            $("#collected-check-details .checkType").html(data.checkType == "MANAGER" ? "Manager's Check" : "Standard Check");

                            $("#collected-check-details .receiptType").html(data.receiptType);

                            $("#collected-check-details .receiptNo").html(data.receiptNum);
                        }
                    })



                    $("#collected-check-details").css("display", "flex");
                    
                } else {
                    $("#collected-check-details").css("display", "none");
                }


                $("#mdlViewVoucher .voucherId").html(voucherId);
                $("#mdlViewVoucher .amount").html($(tr).find(".td_amount").html());
                $("#mdlViewVoucher .bank").html($(tr).find(".td_bank").html());
                $("#mdlViewVoucher .entity").html($(tr).find(".td_entity").html());
                $("#mdlViewVoucher .method").html($(tr).find(".td_method").html());
                $("#mdlViewVoucher .checkDate").html($(tr).find(".td_checkDate").html());
                $("#mdlViewVoucher .checkNumber").html($(tr).find(".td_checkNumber").html());
                $("#mdlViewVoucher .vendorCode").html($(tr).find(".td_vendorCode").html());
                $("#mdlViewVoucher .companyName").html($(tr).find(".td_companyName").html());
                $("#mdlViewVoucher .source").html($(tr).attr("source"));
                $("#mdlViewVoucher .status").html(status);
                
                $("#mdlViewVoucher").modal();
            })
        },
        filter: false,
        language: {
            "lengthMenu": "_MENU_",
            "processing": ""
        },
        lengthMenu: [
            [5, 10, 15, 20, 100],
            [5, 10, 15, 20, 100] // change per page values here
        ],
        orderCellsTop: true,
        pageLength: 10,
        processing: false,
        responsive: true,
        serverSide: true,
        stateSave: false
    })

    $("#dtrIntegrate").daterangepicker(null, function (a, b, c) {})
})

$("#btnImportExcel").click(function () {
    if ($("#frmImportExcel")[0].checkValidity() == false) {
        if (isChrome) {
            $("#frmImportExcel")[0].reportValidity()
        } else {
            swal({
                icon: "error",
                title: "Empty File",
                text: "Please select a file first."
            })
        }
    } else {
        if ($("#cboExcelFileType").val() == "PAYMENT") {
            payment.importExcelPayment();
        } else if ($("#cboExcelFileType").val() == "CHECK") {
            payment.importExcelCheck();
        }
    }
})

$("#btnIntegrate").click(function () {
    var daterange = ($("#dtrIntegrate").val()).split(" - ");
    var dateFrom = daterange[0];
    var dateTo = daterange[1];
    var range = getMonthsBetween(
        new Date(dateFrom),
        new Date(dateTo)
    )

    if (range >= 1) {
        swal({
            title: "Date range for D365 synchronization is limited up to one month only.",
            icon: "error"
        })

        return false;
    }


    HoldOn.open({
        theme: "sk-circle",
        message: "Integrating with Dynamics 365"
    });

    $.ajax({
        url: integratePaymentD365Url + "?dateFrom=" + dateFrom + "&dateTo=" + dateTo,
        type: 'GET',
        success: function (data) {
            HoldOn.close();
            if (data == 0) {
                swal({
                    title: "No Integration Occured",
                    icon: "error",
                    text: "Requested data may already has been integrated into the database or there is no data result coming from D365 at all."
                })
            } else if (data == "ERROR") {
                swal({
                    title: "There is an error processing your request.",
                    icon: "error"
                })
            } else {
                swal({
                    title: data + " data records is/are successfully integrated into the database.",
                    icon: "success"
                }).then(function () {
                    document.location.reload();
                });
            }
        },
        error: function () {
            HoldOn.close();
            swal({
                title: "There is an error processing your request. Please try again later.",
                icon: "error"
            })
        },
        //timeout: 50000
    })
})

function LoadD365Logs() {
    $("#dtLogs").DataTable().destroy()

    var dtLogs = $('#dtLogs').DataTable({
        ajax: {
            url: $('#dtLogs').attr("url") + "?type=INTEGRATE PAYMENTS",
            datatype: 'json',
            dataSrc: '',

        },
        "columns": [
            {
                "data": null,
                "title": "Date/Time",
                render: function (data, type, row) {
                    if (isEdge || isIE) {
                        let datetime = data.strTime.split(":");
                        if (datetime.length > 1) {
                            return datetime[0] + ":" + datetime[1];
                        }
                    } else {
                        var time = new Date(data.strTime);
                        return formatDate(time, "d MMM yyyy dddd h:mmtt");
                    }
                }
            },
            { data: "userId", title: "User" },
            { data: "description", title: "Description" },
        ],
        columnDefs: [{
            className: 'text-center',
            targets: [0, 1, 2]
        }]
    });
}

var payment = {
    importExcelPayment: function () {
        HoldOn.open({
            theme: "sk-circle",
            message: "Importing Excel File (Payment Table)"
        });

        var excelFile = new FormData($("#frmImportExcel")[0]);
        $.ajax({
            url: importPaymentExcelUrl,
            type: 'POST',
            data: excelFile,
            contentType: false,
            processData: false,
            success: function (data) {
                HoldOn.close();
                if (data == "SUCCESS") {
                    swal({
                        title: "Excel file successfully imported!",
                        icon: "success"
                    }).then(function () {
                        document.location.reload();
                    });
                } else if (data == "NULL") {
                    swal({
                        title: "Please select an excel file first before importing.",
                        icon: "error"
                    })
                } else if (data == "INVALID FILE") {
                    swal({
                        title: "Invalid excel file.",
                        icon: "error"
                    })
                } else if (data == "EMPTY") {
                    swal({
                        title: "Excel file seems empty or in different format.",
                        icon: "error"
                    })
                } else {
                    swal({
                        title: "Duplicate Record",
                        icon: "warning",
                        text: "Voucher/s " + data + " already exist/s in the database. Other records still successfully inserted to the system."
                    }).then(function () {
                        document.location.reload();
                    });
                }
            },
            error: function () {
                HoldOn.close();
                swal({
                    title: "There is an error processing your request to the server.",
                    icon: "error",
                });
            }
        });
    },
    importExcelCheck: function () {
        HoldOn.open({
            theme: "sk-circle",
            message: "Importing Excel File (Check Table)"
        });

        var excelFile = new FormData($("#frmImportExcel")[0]);
        $.ajax({
            url: importCheckExcelUrl,
            type: 'POST',
            data: excelFile,
            contentType: false,
            processData: false,
            success: function (data) {
                HoldOn.close();
                if (data == "SUCCESS") {
                    swal({
                        title: "Excel file successfully imported!",
                        icon: "success"
                    }).then(function () {
                        document.location.reload();
                    });
                } else if (data == "NULL") {
                    swal({
                        title: "Please select an excel file first before importing.",
                        icon: "error"
                    })
                } else if (data == "INVALID FILE") {
                    swal({
                        title: "Invalid excel file.",
                        icon: "error"
                    })
                } else if (data == "EMPTY") {
                    swal({
                        title: "Excel file seems empty or in different format.",
                        icon: "error"
                    })
                } else if (data.indexOf("INCOMPLETE") != -1) {
                    swal({
                        title: "Some Checks Not Inserted",
                        icon: "warning",
                        text: "Only " + data.replace("INCOMPLETE", "") + " records are inserted. Check details for others are incomplete."
                    }).then(function () {
                        document.location.reload();
                    });
                } else {
                    swal({
                        title: "Voucher Not Found.",
                        icon: "warning",
                        text: "Voucher/s " + data + " is not found in the database. Other records still successfully inserted to the system."
                    }).then(function () {
                        document.location.reload();
                    });
                }
            },
            error: function () {
                HoldOn.close();
                swal({
                    title: "There is an error processing your request to the server.",
                    icon: "error",
                });
            }
        });
    }
}