﻿var receiptId;
var voucherID;
var logId;

function LoadDataTable() {
    $("#dtVoucher").DataTable().destroy();
    let daterange = ($("#dtrSearch").val()).split(" - ");
    let dateFrom = daterange[0]
    let dateTo = daterange[1]

    $("#dtVoucher").setCustomSearchInput();

    var dtVoucher = $('#dtVoucher').DataTable({
        ajax: {
            url: $('#dtVoucher').attr("url") + "?dateFrom=" + dateFrom + "&dateTo=" + dateTo +
                "&checkType=" + $("#txtCheckType").val(),
            datatype: 'json',
            dataSrc: '',

        },
        "columns": [
            { data: "voucherId", title: "Voucher Id" },
            { data: "vendorCode", title: "Vendor Code" },
            { data: "companyName", title: "Company Name" },
            {
                "data": null,
                "title": "Released Date",
                render: function (data, type, row) {
                    var details = parseJsonDate(data.releasedDate);
                    if (details == "") return "";
                    return $.date(details);
                }
            },
            {
                "data": null,
                "title": "Receipt No",
                render: function (data, type, row) {
                    var details = "<text class='receiptNo'>" + data.receiptNum + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Receipt Type",
                render: function (data, type, row) {
                    let rt = "";

                    if (data.receiptType == "OR") {
                        rt = "Official Receipt"
                    } else if (data.receiptType == "AR") {
                        rt = "Acknowledgement Receipt"
                    } else if (data.receiptType == "PR") {
                        rt = "Provisionary Receipt"
                    } else if (data.receiptType == "CR") {
                        rt = "Collection Receipt"
                    }

                    var details = "<text class='receiptType'>" + rt + "</text>";
                    return details;
                }
            },

            { data: "checkNum", title: "Check" },
            {
                "data": null,
                "title": "Check Date",
                render: function (data, type, row) {
                    var details = parseJsonDate(data.checkDate);

                    if (details == "") return "";
                    return $.date(details);
                }
            },
            {
                "data": null,
                "title": "Check Type",
                render: function (data, type, row) {
                    var details = "<text val='" + data.checkType + "' class='checkType'>";

                    if (data.checkType == "STANDARD") {
                        details += "Standard"
                    }
                    else if (data.checkType == "MANAGER") {
                        details += "Manager's Checks"
                    } else {
                        details += "Telegraphi"
                    }

                    details += "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Entity",
                render: function (data, type, row) {
                    var details = "<text class='dataAreaId'>" + data.entity + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Bank",
                render: function (data, type, row) {
                    var details = "<text class='bankName'>" + data.bank + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Amount",
                render: function (data, type, row) {
                    var details = "<span>&#8369;</span><text class='amount'>" + parseMoney(data.amount) + "</text>"
                    return details;
                }
            },
            {
                "data": null,
                "title": "Collector",
                render: function (data, type, row) {
                    var details = "<div class='fa-hover'><a href='#' onclick='ViewCollector(" + ignoreSingleQuote(data) + ")'><i class='fa fa-user'></i> Collector</a></div>"
                    return details;
                }
            },
            {
                "data": null,
                "title": "Action",
                render: function (data, type, row) {
                    var details = "<div class='fa-hover'><a href='#' class='btnEdit' receiptType='" + data.receiptType + "' logId='" + data.logId + "'" +
                        "'><i class='fa fa-folder-open-o'></i> Edit</a></div>" +
                        "<div style='margin-top:5px' class='fa-hover'><a href='#' onclick=\"" + ("ReverseVoucher") +
                        "('" + data.voucherId + "')\"><i class='fa fa-" + (data.status == "RELEASED" ? "exchange" : "refresh") +
                        "'></i> " + ("Reverse") + "</a></div>";
                    return details;
                }
            }
        ],
        columnDefs: [{
            className: 'row_action',
            targets: [13]
        },
        {
            className: 'row_collector',
            targets: [12]
        },

        ],
        select: {
            style: 'single'
        },
        "initComplete": function (settings, json) {
            $("#dtVoucher").setCustomSearch({
                exemption: [12, 13]
            });
            $(".container_item").html("")

            let totalAmount = 0.0;
            let itemAmountArr = [];
            let bankArray = [];
            let entityArray = [];

            dtVoucher.$("tr").each(function (index) {
                let amount = removeCommas(($(this).find(".amount")).html())
                let entity = $(this).find(".dataAreaId").html();
                let bank = $(this).find(".bankName").html();

                itemAmountArr.push({
                    entity: entity,
                    bankName: bank,
                    amount: amount
                })

                if (bankArray.indexOf(bank) == -1) {
                    bankArray.push(bank)
                }

                if (entityArray.indexOf(entity) == -1) {
                    entityArray.push(entity);
                }

                totalAmount += parseFloat(amount);
            });

            $.each(entityArray, function (i, item) {
                let entity = item;

                let itemArray = $.grep(itemAmountArr, function (n, i) {
                    return n.entity == entity;
                })

                let total = 0;
                let subitems = [];

                $.each(itemArray, function (i, item) {
                    let index = GetIndexInJsonByKeyVal(subitems, "bankName", item.bankName);

                    if (index >= 0) {
                        subitems[index].amount = parseFloat(subitems[index].amount) + parseFloat(item.amount);
                    } else {
                        subitems.push({
                            bankName: item.bankName,
                            amount: item.amount
                        })
                    }

                    total += parseFloat(item.amount);
                })

                $(".by_entity .container_item").append('<div class="summary_item ' + entity + '">' +
                    '<div class="item_header">' +
                    '<label>' + entity + ' (₱' + parseMoney(total) + ')</label>' +
                    '</div>' +
                    '<div class="container_sub_item">' +
                    '</div>' +
                    '</div>')

                $.each(subitems, function (i, item) {
                    $(".summary_item." + entity + " .container_sub_item").append('<div class="sub-item">' +
                        '<label>' + item.bankName + ' - ₱' + parseMoney(item.amount) +
                        '</label></div>')
                })
            })

            $.each(bankArray, function (i, item) {
                let bank = item;

                let itemArray = $.grep(itemAmountArr, function (n, i) {
                    return n.bankName == bank;
                })

                let total = 0;
                let subitems = [];

                $.each(itemArray, function (i, item) {
                    let index = GetIndexInJsonByKeyVal(subitems, "entity", item.entity);

                    if (index >= 0) {
                        subitems[index].amount = parseFloat(subitems[index].amount) + parseFloat(item.amount);
                    } else {
                        subitems.push({
                            entity: item.entity,
                            amount: item.amount
                        })
                    }

                    total += parseFloat(item.amount);
                })


                $(".by_bank .container_item").append('<div class="summary_item ' + bank + '">' +
                    '<div class="item_header">' +
                    '<label>' + bank + ' (₱' + parseMoney(total) + ')</label>' +
                    '</div>' +
                    '<div class="container_sub_item">' +
                    '</div>' +
                    '</div>')


                $.each(subitems, function (i, item) {
                    $(".summary_item." + bank + " .container_sub_item").append('<div class="sub-item">' +
                        '<label>' + item.entity + ' - ₱' + parseMoney(item.amount) +
                        '</label></div>')
                })
            })

            $("#btnRequest").html("<i class='fa fa-external-link'></i> Request <u>₱" + parseMoney(totalAmount) + "</u> Worth of Vouchers");
            $("#lblTotal").html("Total : ₱" + parseMoney(totalAmount))

            HoldOn.close();
        },
        "drawCallback": function () {
            UpdateDateLabel();


            $(".btnEdit").off("click").click(function () {
                $("#txtReceiptNo").val($(this).closest("tr").find(".receiptNo").html())
                $("#txtReceiptType").val($(this).attr("receiptType"))
                $("#txtCheckType").val($(this).closest("tr").find(".checkType").attr("val"))
                receiptId = $(this).attr("receiptId");
                logId = $(this).attr("logId");
                $("#modalEdit").modal();
            })
        },
        "createdRow": function (row, data, dataIndex) {
            if ($(row).find('.dataAreaId').html() == "SPAV") {
                $(row).addClass("spavi_row");
            } else if ($(row).find('.dataAreaId').html() == "SPCI") {
                $(row).addClass("spci_row");
            } else if ($(row).find('.dataAreaId').html() == "WBHI") {
                $(row).addClass("wbhi_row");
            } else if ($(row).find('.dataAreaId').html() == "NAF") {
                $(row).addClass("naf_row");
            } else if ($(row).find('.dataAreaId').html() == "SIL") {
                $(row).addClass("sil_row");
            } else if ($(row).find('.dataAreaId').html() == "DBE") {
                $(row).addClass("dbe_row");
            }
        },
    })

}

$(document).ready(function () {
    $('#dtVoucher_container').doubleScroll({
        resetOnWindowResize: true,
        onlyIfScroll: true
    });

    if (isEdge || isIE) {
        let width = $("input[type=file]").width();

        $("input[type=file]").css("display", "none");


        $("input[type=file]").after("<input class='form-control file-upload'" +
            "style='height:31px;width:" + width + "px;' value='Choose a file' readonly></input>");

        $(".file-upload").click(function () {
            $("input[type=file]").trigger("click");
        })

        $("input[type=file]").change(function () {
            let file = $("input[type=file]").val().split("\\");

            if (file.length > 0) {
                $(".file-upload").val(file[file.length - 1]);
            } else {
                $(".file-upload").val($("input[type=file]").val());
            }
        })
    }

    LoadDataTable();

    $("#btnSearch").click(function () {
        HoldOn.open({
            theme: "sk-circle",
            text: "Loading ..."
        })
        LoadDataTable();
    })

    $("#txtCheckType").change(function () {
        HoldOn.open({
            theme: "sk-circle",
            text: "Loading ..."
        })
        LoadDataTable();
    })

    $("#btnSave").click(function () {
        if ($("#txtReceiptNo").val() == "") {
            swal({
                title: "Please put the receipt number first before saving.",
                icon: "error"
            })
        } else {
            HoldOn.open({
                theme: "sk-circle",
            })

            $.ajax({
                type: "POST",
                url: $(this).attr("url") + "?logId=" + logId + "&receiptNumber=" + $("#txtReceiptNo").val() +
                    "&receiptType=" + $("#txtReceiptType").val() + "&checkType=" + $("#txtCheckType").val(),
                success: function (data) {
                    if (data == "SUCCESS") {
                        swal({
                            title: "Receipt details updated successfully.",
                            icon: "success"
                        }).then(function () {
                            document.location.reload();
                        });
                    } else {
                        swal({
                            title: "There is an error processing your request. Please try again later.",
                            icon: "error"
                        })
                    }
                },
                complete: function () {
                    HoldOn.close();
                }
            })
        }
    })

    $("#btnAcceptReverse").click(function () {
        if ($("#txtReverse").val() == "REVERSE") {
            $.ajax({
                url: $("#dtVoucher").attr("reverseurl") + "?voucherId=" + voucherID,
                success: function (data) {
                    swal({
                        title: "Voucher is reversed successfully.",
                        icon: "success"
                    }).then(function () {
                        document.location.reload();
                    });
                }
            })
        } else {
            swal({
                title: "Please type 'REVERSE' in the input box before proceeding.",
                icon: "error"
            })
        }
    })

    $("#dtrSearch").daterangepicker()

    $("#btnImport").click(function () {
        if ($("#frmImportExcel")[0].checkValidity() == false) {
            $("#frmImportExcel")[0].reportValidity()
        } else {
            HoldOn.open({
                theme: "sk-circle",
                message: "Importing Excel File (Receipt details)"
            });

            var excelFile = new FormData($("#frmImportExcel")[0]);

            $.ajax({
                url: $("#btnImport").attr("url"),
                type: 'POST',
                data: excelFile,
                contentType: false,
                processData: false,
                success: function (data) {
                    HoldOn.close();
                    if (data == "SUCCESS") {
                        swal({
                            title: "Excel file successfully imported!",
                            icon: "success"
                        }).then(function () {
                            document.location.reload();
                        });
                    } else {
                        swal({
                            title: "There is an error processing your request.",
                            icon: "error",
                            text: data
                        })
                    }
                },
                error: function () {
                    HoldOn.close();
                    swal({
                        title: "There is an error processing your request to the server.",
                        icon: "error",
                    });
                }
            });
        }
    })

    $("#btnToday").click(function () {
        HoldOn.open({
            theme: "sk-circle"
        })

        $.ajax({
            url: "/SEAN/IsEODValidationSent",
            success: function (data) {
                if (data.length > 0) {
                    let time = formatDate(parseJsonDate(data[0].logDate), "h:mm tt").toUpperCase();

                    $("#btnVerify").addClass("btn-danger").removeClass("btn-primary").html("Already Sent a Validation by " + data[0].user +
                        " (" + time + ")").attr("validated", "true").attr("time", time)
                } else {
                    $("#btnVerify").addClass("btn-primary").removeClass("btn-success").html("Verify and Send Report").attr("validated", "false")
                }
            }
        })

        $.ajax({
            url: "/SEAN/GetCollectedChecks?checkType=STANDARD",
            async: false,
            success: function (data) {
                let totalAmount = 0.0;
                let itemAmountArr = [];
                let bankArray = [];
                let entityArray = [];
                
                $("#standard_check_container .dtTodaySummaryEntity tbody").html("");

                $.each(data, function (i, item) {
                    let amount = item.amount;
                    let entity = item.entity;
                    let bank = item.bank;

                    itemAmountArr.push({
                        entity: entity,
                        bankName: bank,
                        amount: amount
                    })

                    if (bankArray.indexOf(bank) == -1) {
                        bankArray.push(bank)
                    }

                    if (entityArray.indexOf(entity) == -1) {
                        entityArray.push(entity);
                    }

                    totalAmount += parseFloat(amount);
                })

                $.each(entityArray, function (i, item) {
                    let entity = item;

                    let itemArray = $.grep(itemAmountArr, function (n, i) {
                        return n.entity == entity;
                    })

                    let total = 0;
                    let subitems = [];

                    $.each(itemArray, function (i, item) {
                        let index = GetIndexInJsonByKeyVal(subitems, "bankName", item.bankName);

                        if (index >= 0) {
                            subitems[index].amount = parseFloat(subitems[index].amount) + parseFloat(item.amount);
                        } else {
                            subitems.push({
                                bankName: item.bankName,
                                amount: item.amount
                            })
                        }

                        total += parseFloat(item.amount);
                    })

                    let trHtml = "";
                    trHtml += "<tr><td>" + entity + " - ₱" + parseMoney(total) + "</td>";

                    $.each(subitems, function (i, item) {
                        if (i == 0) {
                            trHtml += "<td>" + item.bankName + "</td><td>₱" + parseMoney(item.amount) + "</td></tr>";
                        } else {
                            trHtml += "<tr><td></td><td>" + item.bankName + "</td><td>₱" + parseMoney(item.amount) + "</td></tr>";
                        }
                    })
                    $("#standard_check_container .dtTodaySummaryEntity").append(trHtml);
                })
                
                $("#standard_check_container .dtTodaySummaryEntity").append("<tr class='tr_total'><td colspan='2' class='text-right'>Total:</td><td>₱" +
                    parseMoney(totalAmount, true) + "</td></tr>");
            }
        })
        let today = new Date();
        let dd = String(today.getDate()).padStart(2, '0');
        let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        let yyyy = today.getFullYear();
        today = mm + "/" + dd + "/" + yyyy

        $.ajax({
            url: "/SEAN/GetEmployeeWithOnlinePaymentsList?from=" + today + "&to=" + today,
            async: false,
            success: function (data) {
                let trHtml = "";
                let totalAmount = 0.0;

                $("#op_container").css("display", "block")
                $("#op_container table tbody").html("");

                $.each(data, function (i, item) {
                    totalAmount += parseFloat(item.amount);

                    trHtml += "<tr><td>" + item.Employee.employee_no + "</td><td>" + item.Employee.employeeName +
                        "</td><td>₱" + parseMoney(item.amount) + "</td></tr>";
                })

                trHtml += "<tr class='tr_total'><td colspan='2' class='text-right'>Total:</td><td>₱" +
                    parseMoney(totalAmount, true) + "</td></tr>";

                $("#op_container table").append(trHtml);
            }
        })

        $.ajax({
            url: "/SEAN/GetCollectedChecks?checkType=MANAGER",
            async: false,
            success: function (data) {

                $("#managers_check_container").css("display", "block")

                let totalAmount = 0.0;
                let itemAmountArr = [];
                let bankArray = [];
                let entityArray = [];
                
                $("#managers_check_container .dtTodaySummaryEntity tbody").html("");

                $.each(data, function (i, item) {
                    let amount = item.amount;
                    let entity = item.entity;
                    let bank = item.bank;

                    itemAmountArr.push({
                        entity: entity,
                        bankName: bank,
                        amount: amount
                    })

                    if (bankArray.indexOf(bank) == -1) {
                        bankArray.push(bank)
                    }

                    if (entityArray.indexOf(entity) == -1) {
                        entityArray.push(entity);
                    }

                    totalAmount += parseFloat(amount);
                })

                $.each(entityArray, function (i, item) {
                    let entity = item;

                    let itemArray = $.grep(itemAmountArr, function (n, i) {
                        return n.entity == entity;
                    })

                    let total = 0;
                    let subitems = [];

                    $.each(itemArray, function (i, item) {
                        let index = GetIndexInJsonByKeyVal(subitems, "bankName", item.bankName);

                        if (index >= 0) {
                            subitems[index].amount = parseFloat(subitems[index].amount) + parseFloat(item.amount);
                        } else {
                            subitems.push({
                                bankName: item.bankName,
                                amount: item.amount
                            })
                        }

                        total += parseFloat(item.amount);
                    })

                    let trHtml = "";
                    trHtml += "<tr><td>" + entity + " - ₱" + parseMoney(total) + "</td>";

                    $.each(subitems, function (i, item) {
                        if (i == 0) {
                            trHtml += "<td>" + item.bankName + "</td><td>₱" + parseMoney(item.amount) + "</td></tr>";
                        } else {
                            trHtml += "<tr><td></td><td>" + item.bankName + "</td><td>₱" + parseMoney(item.amount) + "</td></tr>";
                        }
                    })
                    $("#managers_check_container .dtTodaySummaryEntity").append(trHtml);
                })
                
                $("#managers_check_container .dtTodaySummaryEntity").append("<tr class='tr_total'><td colspan='2' class='text-right'>Total:</td><td>₱" +
                    parseMoney(totalAmount, true) + "</td></tr>");

                HoldOn.close();
                $("#modalTodaySummary").modal();
            }
        })

        $.ajax({
            url: "/OnlinePayment/GetGeneralOnlinePaymentList?isByTransdate=false",
            success: function (data) {
                $("#gop_container").html("");

                var modesArr = GetArrayFromValues(data, "modeOfPayment");

                for (var i = 0; i < modesArr.length; i++)
                {
                    var rows = FilterRowsFromJson(data, "modeOfPayment", modesArr[i])
                    
                    let totalAmount = 0.0;
                    let itemAmountArr = [];
                    let bankArray = [];
                    let entityArray = [];
                    let trs = "";
                    
                    $.each(rows, function (i, item) {
                        let amount = item.amount;
                        let entity = item.entity;
                        let bank = item.bank;

                        itemAmountArr.push({
                            entity: entity,
                            bankName: bank,
                            amount: amount
                        })

                        if (bankArray.indexOf(bank) == -1) {
                            bankArray.push(bank)
                        }

                        if (entityArray.indexOf(entity) == -1) {
                            entityArray.push(entity);
                        }

                        totalAmount += parseFloat(amount);
                    })

                    $.each(entityArray, function (i, item) {
                        let entity = item;

                        let itemArray = $.grep(itemAmountArr, function (n, i) {
                            return n.entity == entity;
                        })

                        let total = 0;
                        let subitems = [];

                        $.each(itemArray, function (i, item) {
                            let index = GetIndexInJsonByKeyVal(subitems, "bankName", item.bankName);

                            if (index >= 0) {
                                subitems[index].amount = parseFloat(subitems[index].amount) + parseFloat(item.amount);
                            } else {
                                subitems.push({
                                    bankName: item.bankName,
                                    amount: item.amount
                                })
                            }

                            total += parseFloat(item.amount);
                        })

                        let trHtml = "";
                        trHtml += "<tr><td>" + entity + " - ₱" + parseMoney(total) + "</td>";

                        $.each(subitems, function (i, item) {
                            if (i == 0) {
                                trHtml += "<td>" + item.bankName + "</td><td>₱" + parseMoney(item.amount) + "</td></tr>";
                            } else {
                                trHtml += "<tr><td></td><td>" + item.bankName + "</td><td>₱" + parseMoney(item.amount) + "</td></tr>";
                            }
                        })
                        trs += trHtml;
                    })

                    trs += 

                    $("#gop_container").append(`<div class="container" id="gop_${modesArr[i]}_container">
                    <h3 class="check_title">General Online Payments (${modesArr[i]})</h3>
                    <table class="table table-sm dtTodaySummaryOP">
                        <thead>
                            <tr>
                                <td style="width:30%;">Entity</td>
                                <td style="width:40%;">Bank</td>
                                <td>Amount</td>
                            </tr>
                        </thead>
                        <tbody>${trs}<tr class='tr_total'><td colspan='2' class='text-right'>Total:</td><td>₱
                        ${parseMoney(totalAmount, true)}</td></tr></tbody>
                    </table>
                    </div>`)
                    
                }
            }
        })
    })

    $("#btnVerify").click(function () {
        if ($(this).attr("validated") == "false") {
            swal({
                title: "Confirmation",
                text: "Are you sure that you want to verify and send report now? This can't be undone once confirmed.",
                icon: "warning",
                buttons: true
            })
                .then((ok) => {
                    if (ok) {
                        SendValidation();
                    }
                })
        } else {
            swal({
                title: "Validation Resending Confirmation",
                text: "Validation email is already sent as of " + $(this).attr("time") + ". Do you still want to continue and resend " +
                    "the report now?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((ok) => {
                if (ok) {
                    SendValidation();
                }
            })
        }
    })
})

function GetArrayFromValues(json, field) {
    var resultArr = [];

    $.each(json, function (i, item) {
        if (resultArr.indexOf(item[field]) == -1) {
            resultArr.push(item[field])
        }
    })

    return resultArr;
}

function FilterRowsFromJson(json, field, value)
{
    resultArr = [];

    $.each(json, function (i, item) {
        if (item[field] == value) {
            resultArr.push(item);
        }
    })

    return resultArr;
}

function SendValidation() {
    HoldOn.open({
        theme: "sk-circle",
        message: "Processing your request.."
    })

    $.ajax({
        url: "/Report/WriteReleasedToExcel?isSendToMail=true",
        success: function (data) {
            if (data == "SUCCESS") {
                swal({
                    title: "Success",
                    text: "Report is being sent now.",
                    icon: "success"
                }).then(function () {
                    document.location.reload();
                });
            } else {
                swal({
                    title: "Sending Failed",
                    text: "There is an error processing your request.",
                    icon: "error"
                })
            }
        }
    })
}

function ViewCollector(data) {
    if (!data) {
        swal({
            icon: "error",
            title: "No Record Found",
            text: "No collector was recorded for this released check."
        })

        return false;
    }

    $("#txtCompanyName").val(data.companyName);
    $("#txtVoucherId").val(data.voucherId);
    $("#txtCollectorName").val(data.collectorName);
    $("#txtCollectorType").val(data.collectorType);

    $("#txtThirdPartyCompany").closest("div").remove();

    if (data.collectorType == "THIRD PARTY") {
        $("#txtCollectorType").closest("div").after("<div>" +
            "<label for='txtThirdPartyCompany' > Third Party Company:</label>" +
            "<input type='text' class='form-control' value='" + data.thirdPartyCompany + "' id='txtThirdPartyCompany' readonly />" +
            "</div>");
    }

    $("#modalCollector").modal();
}

function UpdateDateLabel() {
    let daterange = ($("#dtrSearch").val()).split(" - ");
    let dateFrom = new Date(daterange[0]);
    let dateTo = new Date(daterange[1]);
    $("#lblDate").html("(" + formatDate(dateFrom, "MMM d, yyyy") + " - " + formatDate(dateTo, "MMM d, yyyy") + ")");
}

function ReverseVoucher(voucherId) {
    swal({
        title: "Are you sure that you want to reverse the collected voucher? It's status will go back to unreleased status once confirmed.",
        icon: "warning",
        buttons: true,
    })
        .then((reverse) => {
            voucherID = voucherId;

            if (reverse) {
                $("#modalDialog").modal();
            }
        })
}

function AdverseVoucher(voucherId) {
    swal({
        title: "Are you sure that you want to return the voucher status?",
        icon: "warning",
        buttons: true,
    })
        .then((reverse) => {
            if (reverse) {
                $.ajax({
                    url: $("#dtVoucher").attr("adverseurl") + "?voucherId=" + voucherId,
                    success: function (data) {
                        swal({
                            title: "Voucher status is adversed.",
                            icon: "success"
                        }).then(function () {
                            document.location.reload();
                        });
                    }
                })
            }
        })
}

function GetIndexInJsonByKeyVal(json, key, value) {
    let index = -1;

    $.each(json, function (i, item) {
        if (item[key] == value) {

            index = i;
        }
    });

    return index;
}

