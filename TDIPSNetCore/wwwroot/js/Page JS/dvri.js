﻿var batchId = 0;
var totalAmount = 0.0;
var getVoucherIDListFromBatchURL = '';
var requestVoucherForVerificationUrl = "";
var updateVoucherStatusBatch = "";
var declinedBatchIdArr = [];
var dtPayment;

function MakeRequest() {
    let voucherModelArr = [];
    var table = $("#dtPayment").DataTable();
    table.$("tr.selected").each(function () {
        let status = ($(this).find(".status").text()).toUpperCase();
        let voucherId = $(this).find(".voucherId").text();

        if (status == "UNRELEASED" || status == "UNCOLLECTED" || status == "DECLINED") {
            voucherModelArr.push({
                voucherId: voucherId,
                status: status
            })
        }
    });

    if (voucherModelArr.length == 0) {
        swal({
            title: "No Row is Selected for Verification Request",
            icon: "error",
            text: "Please select at least one record with status 'Unreleased' or 'Uncollected' before requesting."
        })
    } else {
        $("#modalRequest").modal();
    }
}

$(document).ready(function () {
    var dtRequest = $('#dtDeclinedRequest').DataTable({
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('offersDataTables', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('offersDataTables'));
        },
        "initComplete": function (settings, json) {
            $(".btnView").click(function () {
                HoldOn.open({
                    theme: "sk-circle"
                });

                batchId = $(this).attr("batchId");

                $.ajax({
                    type: "GET",
                    async: false,
                    url: getVoucherIDListFromBatchURL + "?batchId=" + batchId + "&status=UNRELEASED",
                    success: function (data) {
                        declinedBatchIdArr = data;
                    }
                })

                $("#dtPayment").setCustomSearchInput();
                dtPayment = $('#dtPayment').DataTable({
                    "destroy": true,
                    "createdRow": function (row, data, dataIndex) {
                        $(row).attr("id", data.voucherId);

                        if (declinedBatchIdArr.indexOf(data.voucherId) > -1) {
                            $(row).addClass("selected");
                        };

                        if ($(row).find('.dataAreaId').html() == "SPAV") {
                            $(row).addClass("spavi_row");
                        } else if ($(row).find('.dataAreaId').html() == "SPCI") {
                            $(row).addClass("spci_row");
                        } else if ($(row).find('.dataAreaId').html() == "WBHI") {
                            $(row).addClass("wbhi_row");
                        } else if ($(row).find('.dataAreaId').html() == "NAF") {
                            $(row).addClass("naf_row");
                        } else if ($(row).find('.dataAreaId').html() == "SIL") {
                            $(row).addClass("sil_row");
                        } else if ($(row).find('.dataAreaId').html() == "DBE") {
                            $(row).addClass("dbe_row");
                        }
                    },
                    "initComplete": function (settings, json) {
                        $("#dtPayment").setCustomSearch({
                            exemption: [0]
                        });

                        var totalAmount = 0.0;
                        GetTotal(dtPayment, 'row');
                        dtPayment.$("tr.selected").each(function () {
                            $(this).addClass("danger");
                            var amount = removeCommas($(this).find(".amount").text())
                            totalAmount += parseFloat(amount);
                        });

                        //show declined vouchers first.
                        dtPayment.order([9, 'asc']).draw();

                        $("#btnRequest").html("<i class='fa fa-external-link'></i> Re-request with <u>₱" + parseMoney(totalAmount) + "</u> Worth of Vouchers");

                        HoldOn.close();
                        $("#modalViewDecline").modal();

                    },
                    ajax: {
                        url: $('#dtPayment').attr("url"),
                        datatype: 'json',
                        dataSrc: '',

                    },
                    "columns": [
                        {
                            "data": null,
                            "title": "<th style='padding-left:22px !important; margin-right:0px !important;'><input type='checkbox' id='cbxSelectAll' /></th>",
                            render: function (data, type, row) {
                                var details = "";
                                return details;
                            }
                        },
                        {
                            "data": null,
                            "title": "Entity",
                            render: function (data, type, row) {
                                var details = "<text class='dataAreaId'>" + data.dataAreaId + "</text>";
                                return details;
                            }
                        },
                        {
                            "data": null,
                            "title": "Vendor Code",
                            render: function (data, type, row) {
                                var details = "<text class='vendorCode'>" + data.vendorCode + "</text>";
                                return details;
                            }
                        },
                        {
                            "data": null,
                            "title": "Company Name",
                            render: function (data, type, row) {
                                var details = "<text class='companyName'>" + data.companyName + "</text>";
                                return details;
                            }
                        },
                        {
                            "data": null,
                            "title": "Bank",
                            render: function (data, type, row) {
                                var details = "<text class='bankName'>" + data.bankName + "</text>";
                                return details;
                            }
                        },
                        {
                            "data": null,
                            "title": "Voucher ID",
                            render: function (data, type, row) {
                                var details = "<text voucherId='" + data.voucherId + "' class='voucherId'>" + data.voucherId + "</text>";
                                return details;
                            }
                        },
                        {
                            "data": null,
                            "title": "Amount",
                            render: function (data, type, row) {
                                var details = "<span>&#8369;</span><text class='amount'>" + parseMoney(data.amount) + "</text>"
                                return details;
                            }
                        },
                        {
                            "data": null,
                            "title": "Check Number",
                            render: function (data, type, row) {
                                var details = "<text class='checkNumber'>" + (data.chequeNumber ? data.chequeNumber : "") + "</text>"
                                return details;
                            }
                        },
                        {
                            "data": null,
                            "title": "Cheque Date",
                            render: function (data, type, row) {
                                var details = parseJsonDate(data.chequeDate);
                                if (details == "") return "";
                                return "<text class='checkDate'>" + $.date(details) + "</text>"
                            }
                        },
                        {
                            "data": null,
                            "title": "Status",
                            render: function (data, type, row) {
                                if (declinedBatchIdArr.indexOf(data.voucherId) > -1) {
                                    var details = "<h5 style='margin-top:0px !important; margin-bottom:0px !important;'><span class='label label-danger" +
                                        " status' style='text-transform:capitalize;'>Declined</span></h5>";
                                    return details;
                                } else {
                                    return StatusLabel(data.status);
                                }
                            }
                        }
                    ],
                    columnDefs: [{
                        className: 'select-checkbox',
                        targets: [0]
                    }, {
                        className: 'text-right',
                        targets: [5]
                    }

                    ],
                    select: {
                        style: 'multi',
                        selector: 'td:first-child'
                    }
                })

                dtPayment.on("click", "#cbxSelectAll", function () {
                    if ($('#cbxSelectAll').prop("checked") == false) {
                        dtPayment.rows().deselect();
                        $("th.select-checkbox").removeClass("selected");
                    } else {
                        dtPayment.rows({ search: 'applied' }).select();
                        $("th.select-checkbox").addClass("selected");
                    }
                })

                dtPayment.on('select', function (e, dt, type, indexes) {
                    if (type === 'row') {
                        var totalAmount = 0.0;
                        dtPayment.$("tr.selected").each(function () {
                            var amount = removeCommas($(this).find(".amount").text())
                            totalAmount += parseFloat(amount);
                        });
                        GetTotal(dtPayment, type);

                        $("#btnRequest").html("<i class='fa fa-external-link'></i> Re-request with <u>₱" + parseMoney(totalAmount) + "</u> Worth of Vouchers");
                    }
                }).on('deselect', function (e, dt, type, indexes) {
                    var totalAmount = 0.0;
                    dtPayment.$("tr.selected").each(function () {
                        var amount = removeCommas($(this).find(".amount").text())
                        totalAmount += parseFloat(amount);
                    });
                    GetTotal(dtPayment, type);

                    $("#btnRequest").html("<i class='fa fa-external-link'></i> Re-request <u>₱" + parseMoney(totalAmount) + "</u> Worth of Vouchers");
                });

                $.contextMenu({
                    selector: '#dtPayment',
                    items: {
                        "RFD": {
                            name: "Request Selected Items for Verification",
                            icon: "fa-external-link",
                            callback: function (key, opt) {
                                MakeRequest();
                            }
                        }
                    }
                });
            })
        },
        ajax: {
            url: $('#dtDeclinedRequest').attr("url") + "?status=UNRELEASED&batchStatus=DECLINED",
            datatype: 'json',
            dataSrc: '',

        },
        "columns": [
            {
                "data": null,
                "title": "Date",
                render: function (data, type, row) {
                    if (data.logDate == null) {
                        details == ""
                    } else {
                        var date = new Date(parseInt(data.logDate.substr(6)));
                        details = date.format("mmm dd yyyy")
                    }

                    return details;
                }
            },
            { data: "userId", title: "Declined By" },
            { data: "note", title: "Note" },
            {
                "data": null,
                "title": "Action",
                render: function (data, type, row) {
                    var details = "<div class='fa-hover'><a href='#' batchId='" + data.batchId + "' class='btnView'><i class='fa fa-folder-open-o'></i> View</a></div>";
                    return details;
                }
            },
        ]
    })

    $("#btnRequest").click(function () {
        MakeRequest();
    })

    $("#btnSubmitRequest").click(function () {
        let voucherModelArr = [];
        var table = $("#dtPayment").DataTable();
        table.draw();
        table.$("tr.selected").each(function () {
            let status = ($(this).find(".status").text()).toUpperCase();
            let voucherId = $(this).find(".voucherId").text();
            if (status == "UNRELEASED" || status == "UNCOLLECTED" || status == "REQUESTED FOR HOLD" || status == "DECLINED") {
                voucherModelArr.push({
                    voucherId: voucherId,
                    status: status,
                    voucherId: $(this).find(".voucherId").html(),
                    vendorCode: $(this).find(".vendorCode").html(),
                    companyName: $(this).find(".companyName").html(),
                    entity: $(this).find(".dataAreaId").html(),
                    bank: $(this).find(".bankName").html(),
                    amount: removeCommas($(this).find(".amount").html()),
                    checkNum: $(this).find(".checkNumber").html(),
                    checkDate: $(this).find(".checkDate").html(),
                })
            }
        });

        if (voucherModelArr.length == 0) {
            swal({
                title: "No Row is Selected for Verification Request",
                icon: "error",
                text: "Please select at least one record with status 'Unreleased' or 'Uncollected' before requesting."
            })
        } else {
            rvfai.requestVoucherForVerification(voucherModelArr);
        }
    })

    $("#btnUploadExcel").click(function () {
        if ($("#frmUploadExcel")[0].checkValidity() == false) {
            $("#frmUploadExcel")[0].reportValidity()
        } else {
            HoldOn.open({
                theme: "sk-circle",
                message: "Uploading an excel file.."
            })

            var excelFile = new FormData($("#frmUploadExcel")[0]);

            $.ajax({
                url: "/SEAN/ImportVoucherNoExcel",
                type: 'POST',
                data: excelFile,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data == "NULL") {
                        swal({
                            title: "Please select an excel file first before importing.",
                            icon: "error"
                        })
                        HoldOn.close();
                    } else if (data == "INVALID FILE") {
                        swal({
                            title: "Invalid excel file.",
                            icon: "error"
                        })
                        HoldOn.close();
                    } else if (data == "EMPTY") {
                        swal({
                            title: "Excel file seems empty or in different format.",
                            icon: "error"
                        })
                        HoldOn.close();
                    } else {
                        dtPayment.rows().deselect();

                        let notFoundArr = [];

                        for (var i = 0; i < data.length; i++) {
                            if (dtPayment.rows(data[i]).count() == 0) {
                                notFoundArr.push(data[i]);
                            };
                        }

                        $("#frmUploadExcel")[0].reset()

                        dtPayment.rows(data).select();

                        if (notFoundArr.length > 0) {
                            swal({
                                title: "Not All Vouchers from Excel is Selected",
                                icon: "error",
                                text: "The following voucher/s (" + RemoveHashtagFromArray(notFoundArr).join(",") + ") " +
                                    "can't be found in the table because of the ff possible reasons: 1) You have entered a wrong id. " +
                                    "2) The voucher is not existing in the system yet. Please sync with D365 or import excel first. " +
                                    "3) The voucher status is not unreleased or uncollected."
                            })
                        }

                        HoldOn.close();
                    }
                },
                error: function () {
                    HoldOn.close();
                    swal({
                        title: "There is an error processing your request to the server.",
                        icon: "error",
                    });
                }
            });
        }
    })
})

var rvfai = {
    requestVoucherForVerification: function (voucherModelArr) {
        HoldOn.open({
            theme: "sk-circle",
            message: "Sending " + voucherModelArr.length + " data record/s for verification..."
        });

        let date = ($(".txtReleasingDate").val()).split("/");

        var model = JSON.stringify({
            voucher: voucherModelArr,
            releasingDate: date[1] + "/" + (date[0]) + "/" + date[2],
            note: $("#txtNotes").val()
        });

        $.ajax({
            type: "Post",
            url: updateVoucherStatusBatch + "?batchId=" + batchId,
            async: false
        });

        $.ajax({
            type: "Post",
            url: requestVoucherForVerificationUrl,
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: model,
            success: function (data) {
                HoldOn.close()
                if (data == "SUCCESS") {
                    swal({
                        title: "Verification Request Successfully Sent!",
                        icon: "success"
                    }).then(function () {
                        document.location.reload();
                    });
                } else {
                    swal({
                        title: "There is an error processing your request.",
                        icon: "error"
                    })
                }
            }
        });
    }
}

function GetTotal(dtPayment, type) {
    $(".container_item").html("");

    if (type === 'row') {
        var totalAmount = 0.0;
        var itemAmountArr = [];
        var bankArray = [];
        var entityArray = [];

        dtPayment.$("tr.selected").each(function () {
            var amount = removeCommas($(this).find(".amount").text())
            totalAmount += parseFloat(amount);

            let entity = $(this).find(".dataAreaId").html();
            let bank = $(this).find(".bankName").html();

            itemAmountArr.push({
                entity: entity,
                bankName: bank,
                amount: amount
            })

            if (bankArray.indexOf(bank) == -1) {
                bankArray.push(bank)
            }

            if (entityArray.indexOf(entity) == -1) {
                entityArray.push(entity);
            }
        });

        $.each(entityArray, function (i, item) {
            let entity = item;

            let itemArray = $.grep(itemAmountArr, function (n, i) {
                return n.entity == entity;
            })

            let total = 0;
            let subitems = [];

            $.each(itemArray, function (i, item) {
                let index = GetIndexInJsonByKeyVal(subitems, "bankName", item.bankName);

                if (index >= 0) {
                    subitems[index].amount = parseFloat(subitems[index].amount) + parseFloat(item.amount);
                } else {
                    subitems.push({
                        bankName: item.bankName,
                        amount: item.amount
                    })
                }

                total += parseFloat(item.amount);
            })

            $(".by_entity .container_item").append('<div class="summary_item ' + entity + '">' +
                '<div class="item_header">' +
                '<label>' + entity + ' (₱' + parseMoney(total) + ')</label>' +
                '</div>' +
                '<div class="container_sub_item">' +
                '</div>' +
                '</div>')


            $.each(subitems, function (i, item) {
                $(".summary_item." + entity + " .container_sub_item").append('<div class="sub-item">' +
                    '<label>' + item.bankName + ' - ₱' + parseMoney(item.amount) +
                    '</label></div>')
            })
        })

        $.each(bankArray, function (i, item) {
            let bank = item;

            let itemArray = $.grep(itemAmountArr, function (n, i) {
                return n.bankName == bank;
            })

            let total = 0;
            let subitems = [];

            $.each(itemArray, function (i, item) {
                let index = GetIndexInJsonByKeyVal(subitems, "entity", item.entity);

                if (index >= 0) {
                    subitems[index].amount = parseFloat(subitems[index].amount) + parseFloat(item.amount);
                } else {
                    subitems.push({
                        entity: item.entity,
                        amount: item.amount
                    })
                }

                total += parseFloat(item.amount);
            })


            $(".by_bank .container_item").append('<div class="summary_item ' + bank + '">' +
                '<div class="item_header">' +
                '<label>' + bank + ' (₱' + parseMoney(total) + ')</label>' +
                '</div>' +
                '<div class="container_sub_item">' +
                '</div>' +
                '</div>')


            $.each(subitems, function (i, item) {
                $(".summary_item." + bank + " .container_sub_item").append('<div class="sub-item">' +
                    '<label>' + item.entity + ' - ₱' + parseMoney(item.amount) +
                    '</label></div>')
            })
        })

        $("#btnRequest").html("<i class='fa fa-external-link'></i> Request <u>₱" + parseMoney(totalAmount) + "</u> Worth of Vouchers");
        $("#lblTotal").html("Total : ₱" + parseMoney(totalAmount))
    }
}

function GetIndexInJsonByKeyVal(json, key, value) {
    let index = -1;

    $.each(json, function (i, item) {
        if (item[key] == value) {
            index = i;
        }
    });

    return index;
}

function RemoveHashtagFromArray(arr) {
    resultArr = [];

    for (let i = 0; i < arr.length; i++) {
        if (arr[i].indexOf("#") > -1) {
            resultArr.push(arr[i].substring(1))
        } else {
            resultArr.push(arr[i]);
        }
    }

    return resultArr;
}
