﻿function LoadDataTable() {
    $("#dtVoucher").DataTable().destroy();
    let daterange = ($("#dtrSearch").val()).split(" - ");
    let dateFrom = daterange[0]
    let dateTo = daterange[1]

    $("#dtVoucher").setCustomSearchInput();

    var dtVoucher = $('#dtVoucher').DataTable({
        ajax: {
            url: $('#dtVoucher').attr("url") + "?dateFrom=" + dateFrom + "&dateTo="
                + dateTo + "&checkType=" + $("#txtCheckType").val(),
            datatype: 'json',
            dataSrc: '',

        },
        "columns": [
            {
                "data": null,
                "title": "Released Date",
                render: function (data, type, row) {
                    var details = parseJsonDate(data.releasedDate);
                    if (details == "") return "";
                    return $.date(details);
                }
            },
            { data: "vendorCode", title: "Vendor Code" },
            { data: "companyName", title: "Company Name" },
            { data: "voucherId", title: "Voucher Id" },
            {
                "data": null,
                "title": "Amount",
                render: function (data, type, row) {
                    var details = "<span>&#8369;</span><text class='amount'>" + parseMoney(data.amount) + "</text>"
                    return details;
                }
            },
            {
                "data": null,
                "title": "Bank",
                render: function (data, type, row) {
                    var details = "<text class='bankName'>" + data.bank + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Entity",
                render: function (data, type, row) {
                    var details = "<text class='dataAreaId'>" + data.entity + "</text>";
                    return details;
                }
            },
            {
                "data": null,
                "title": "Check Type",
                render: function (data, type, row) {
                    var details = "<text style='text-transform:capitalize;' class='checkType'>" +
                        (data.checkType ? data.checkType : "").toLowerCase() + "</text>";
                    return details;
                }
            },
        ],
        "initComplete": function (settings, json) {
            $("#dtVoucher").setCustomSearch({
            });

            totalAmount = 0.0;
            var totalAmount = 0.0;
            var itemAmountArr = [];
            var bankArray = [];
            var entityArray = [];

            dtVoucher.$("tr").each(function (index) {
                var amount = removeCommas(($(this).find(".amount")).html())
                let entity = $(this).find(".dataAreaId").html();
                let bank = $(this).find(".bankName").html();

                itemAmountArr.push({
                    entity: entity,
                    bankName: bank,
                    amount: amount
                })

                if (bankArray.indexOf(bank) == -1) {
                    bankArray.push(bank)
                }

                if (entityArray.indexOf(entity) == -1) {
                    entityArray.push(entity);
                }

                totalAmount += parseFloat(amount);
            });

            $.each(entityArray, function (i, item) {
                let entity = item;

                let itemArray = $.grep(itemAmountArr, function (n, i) {
                    return n.entity == entity;
                })

                let total = 0;
                let subitems = [];

                $.each(itemArray, function (i, item) {
                    let index = GetIndexInJsonByKeyVal(subitems, "bankName", item.bankName);

                    if (index >= 0) {
                        subitems[index].amount = parseFloat(subitems[index].amount) + parseFloat(item.amount);
                    } else {
                        subitems.push({
                            bankName: item.bankName,
                            amount: item.amount
                        })
                    }

                    total += parseFloat(item.amount);
                })

                $(".by_entity .container_item").append('<div class="summary_item ' + entity + '">' +
                    '<div class="item_header">' +
                    '<label>' + entity + ' (₱' + parseMoney(total) + ')</label>' +
                    '</div>' +
                    '<div class="container_sub_item">' +
                    '</div>' +
                    '</div>')


                $.each(subitems, function (i, item) {
                    $(".summary_item." + entity + " .container_sub_item").append('<div class="sub-item">' +
                        '<label>' + item.bankName + ' - ₱' + parseMoney(item.amount) +
                        '</label></div>')
                })
            })

            $.each(bankArray, function (i, item) {
                let bank = item;

                let itemArray = $.grep(itemAmountArr, function (n, i) {
                    return n.bankName == bank;
                })

                let total = 0;
                let subitems = [];

                $.each(itemArray, function (i, item) {
                    let index = GetIndexInJsonByKeyVal(subitems, "entity", item.entity);

                    if (index >= 0) {
                        subitems[index].amount = parseFloat(subitems[index].amount) + parseFloat(item.amount);
                    } else {
                        subitems.push({
                            entity: item.entity,
                            amount: item.amount
                        })
                    }

                    total += parseFloat(item.amount);
                })


                $(".by_bank .container_item").append('<div class="summary_item ' + bank + '">' +
                    '<div class="item_header">' +
                    '<label>' + bank + ' (₱' + parseMoney(total) + ')</label>' +
                    '</div>' +
                    '<div class="container_sub_item">' +
                    '</div>' +
                    '</div>')


                $.each(subitems, function (i, item) {
                    $(".summary_item." + bank + " .container_sub_item").append('<div class="sub-item">' +
                        '<label>' + item.entity + ' - ₱' + parseMoney(item.amount) +
                        '</label></div>')
                })
            })

            $("#btnRequest").html("<i class='fa fa-external-link'></i> Request <u>₱" + parseMoney(totalAmount) + "</u> Worth of Vouchers");
            $("#lblTotal").html("Total : ₱" + parseMoney(totalAmount))

            HoldOn.close();
        },
        "drawCallback": function () {
            UpdateDateLabel();
        },
        "createdRow": function (row, data, dataIndex) {
            if ($(row).find('.dataAreaId').html() == "SPAV") {
                $(row).addClass("spavi_row");
            } else if ($(row).find('.dataAreaId').html() == "SPCI") {
                $(row).addClass("spci_row");
            } else if ($(row).find('.dataAreaId').html() == "WBHI") {
                $(row).addClass("wbhi_row");
            } else if ($(row).find('.dataAreaId').html() == "NAF") {
                $(row).addClass("naf_row");
            } else if ($(row).find('.dataAreaId').html() == "SIL") {
                $(row).addClass("sil_row");
            } else if ($(row).find('.dataAreaId').html() == "DBE") {
                $(row).addClass("dbe_row");
            }
        },
    })

}

$(document).ready(function () {
    LoadDataTable();

    $("#btnSearch").click(function () {
        HoldOn.open({
            theme: "sk-circle",
            text: "Loading ..."
        })
        LoadDataTable();
    })
    
    $("#dtrSearch").daterangepicker(null, function (a, b, c) { console.log(a.toISOString(), b.toISOString(), c) })
    
    $("#btnExport").click(function () {
        HoldOn.open({
            theme: "sk-circle",
            message: "Exporting the Records into Excel ..."
        });

        let daterange = ($("#dtrSearch").val()).split(" - ");
        let dateFrom = daterange[0]
        let dateTo = daterange[1]
        
        $.ajax({
            url: $(this).attr("url") + "?dateFrom=" + dateFrom + "&dateTo=" + dateTo,
            type: "POST",
            success: function (data) {
                HoldOn.close()

                if (data == "ERROR") {
                    swal({
                        title: "There is en error processing your request",
                        icon: "error"
                    })
                } else {
                    window.location.href = $("#btnExport").attr("downloadurl") + "?fileName=" + data
                }
            }
        });
    })

    $("#txtCheckType").change(function () {
        HoldOn.open({
            theme: "sk-circle",
            text: "Loading ..."
        })
        LoadDataTable();
    })
})

function UpdateDateLabel() {
    let daterange = ($("#dtrSearch").val()).split(" - ");
    let dateFrom = new Date(daterange[0]);
    let dateTo = new Date(daterange[1]);
    $("#lblDate").html("(" + formatDate(dateFrom, "MMM d, yyyy") + " - " + formatDate(dateTo, "MMM d, yyyy") + ")");
}


function GetIndexInJsonByKeyVal(json, key, value) {
    let index = -1;

    $.each(json, function (i, item) {
        if (item[key] == value) {

            index = i;
        }
    });

    return index;
}

