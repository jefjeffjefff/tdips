﻿var totalAmount = 0.0;
var hasError = false;
var onlinePayments = [];

function LoadOPTable(from, to) {
    HoldOn.open({
        theme: "sk-circle"
    })

    $("#dtOP").DataTable().destroy();

    $("#dtrSearch").daterangepicker()

    $("#dtOP").setCustomSearchInput();
    $("#dtOP").DataTable({
        ajax: {
            url: "/OnlinePayment/GetGeneralOnlinePaymentList?from=" + from + "&to=" + to,
            dataType: "json",
            dataSrc: ""
        },
        initComplete: function () {
            $("#dtOP").setCustomSearch();
            HoldOn.close();
        },
        destroy: true,
        columns: [
            {
                title: "Entity",
                data: null,
                render: function (data) {
                    return `<text>
                            ${data.companyName} (${data.vendorCode})    
                            </text>`
                }
            },
            {
                title: "Acc #",
                data: "bankAccountNumber"
            },
            {
                title: "Invoice Vouchers",
                data: "invoiceVouchers"
            },
            {
                title: "Entity",
                data: null,
                render: function (data) {
                    return `<text>
                            ${data.entity}    
                            </text>`
                }
            },
            {
                title: "Bank",
                data: null,
                render: function (data) {
                    return `<text>
                            ${data.bank}
                            </text>`
                }
            },
            {
                title: "Trans Date",
                data: null,
                render: function (data) {
                    let date = parseJsonDate(data.transDate);

                    if (date == "") return "";

                    return formatDate(date, "MM-dd-yyyy")
                }
            },
            {
                title: "Log Date",
                data: null,
                render: function (data) {
                    let date = parseJsonDate(data.logDate);
                    return formatDate(date, "MM-dd-yyyy")
                }
            },
            {
                title: "Amount",
                data: null,
                render: function (data) {
                    return parseMoney(data.amount)
                }
            },
            {
                title: "Mode",
                data: null,
                render: function (data) {
                    return data.modeOfPayment
                }
            }
        ]
    })
}


$(document).ready(function () {
    LoadOPTable();

    $("#btnSearch").click(function () {
        let daterange = ($("#dtrSearch").val()).split(" - ");
        let dateFrom = daterange[0]
        let dateTo = daterange[1]

        LoadOPTable(dateFrom, dateTo);
    })

    $("#btnUploadExcel").click(function () {
        if ($("#frmUploadExcel")[0].checkValidity() == false) {
            if (isIE || isEdge) {
                swal({
                    title: "Please select an excel file first before importing.",
                    icon: "error"
                })
            } else if (isChrome) {
                $("#frmUploadExcel")[0].reportValidity()
            }
        } else {
            HoldOn.open({
                theme: "sk-circle",
                message: "Uploading an excel file.."
            })

            var excelFile = new FormData($("#frmUploadExcel")[0]);

            $.ajax({
                url: "/OnlinePayment/ImportGeneralOnlinePayment",
                type: 'POST',
                data: excelFile,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data == "NULL") {
                        HoldOn.close();

                        swal({
                            title: "Please select an excel file first before importing.",
                            icon: "error"
                        })
                    } else if (data == "INVALID FILE") {
                        HoldOn.close();

                        swal({
                            title: "Invalid excel file.",
                            icon: "error"
                        })
                    } else if (data == "EMPTY") {
                        HoldOn.close();

                        swal({
                            title: "Excel file seems empty or in different format.",
                            icon: "error"
                        })
                    } else {
                        if (data.length == 0) {
                            HoldOn.close();

                            swal({
                                title: "Excel file seems empty or in different format.",
                                icon: "error"
                            })
                        } else {
                            onlinePayments = [];
                            hasError = false;

                            let rows = "";
                            
                            $.each(data, function (i, item) {
                                let payment = item.GeneralOnlinePayment
                                let transDate = parseJsonDate(payment.transDate);
                                payment.vendor = payment.vendor || {}

                                if (transDate != "")
                                {
                                    transDate = formatDate(transDate, "MM-dd-yyyy");
                                    payment.transDate = transDate;
                                }

                                if (item.Status == "ERROR") {
                                    hasError = true;
                                }

                                onlinePayments.push(payment);
                                
                                rows += `<tr>
                                        <td>${item.LineNumber}</td>
                                        <td>${payment.vendor.companyName}</td>
                                        <td>${payment.bankAccountNumber}</td>
                                        <td>${(payment.invoiceVouchers || "")}</td>
                                        <td><text class='dataAreaId'>${payment.entity}</text></td>
                                        <td><text class='bankName'>${payment.bank}</text></td>
                                        <td>${transDate}</td>
                                        <td><text class='amount'>${parseMoney(payment.amount)}</text></td>
                                        <td>${payment.modeOfPayment || ""}</td>
                                        <td>
                                        <div style='text-transform:capitalize'><text style='display:block;margin-bottom:10px;' class='status'>
                                        ${(item.Status == "SUCCESS" ? ("<span class='label label-success'>" + item.Status.toLowerCase() +
                                        "</span>") : ("<span class='label label-danger'>" + item.Status.toLowerCase() + "</span>")) }
                                        </text> </div>
                                        <div>${item.Message.length > 0 ? (item.Message.join(" ")) : ""}</div>
                                        </td>                    
                                        </tr>`
                            })
                            
                            $("#dtViewOP").DataTable().destroy();
                            $("#dtViewOP").setCustomSearchInput();
                            $("#dtViewOP tbody").html(rows);

                            $("#dtViewOP").DataTable({
                                "createdRow": function (row, data, dataIndex) {
                                    if ($(row).find('.status').text().toUpperCase() == "SUCCESS") {
                                        $(row).addClass("success");
                                    } else if ($(row).find('.status').html().toUpperCase() == "ERROR") {
                                        $(row).addClass("danger");
                                    }
                                },
                                initComplete: function () {
                                    $("#dtViewOP").setCustomSearch({
                                    });
                                    $(".container_item").html("")

                                    totalAmount = 0.0;
                                    var totalAmount = 0.0;
                                    var itemAmountArr = [];
                                    var bankArray = [];
                                    var entityArray = [];

                                    $("#dtViewOP").DataTable().$("tr").each(function (index) {
                                        var amount = removeCommas(($(this).find(".amount")).html())
                                        let entity = $(this).find(".dataAreaId").html();
                                        let bank = $(this).find(".bankName").html();

                                        itemAmountArr.push({
                                            entity: entity,
                                            bankName: bank,
                                            amount: amount
                                        })

                                        if (bankArray.indexOf(bank) == -1) {
                                            bankArray.push(bank)
                                        }

                                        if (entityArray.indexOf(entity) == -1) {
                                            entityArray.push(entity);
                                        }

                                        totalAmount += parseFloat(amount);
                                    });

                                    $.each(entityArray, function (i, item) {
                                        let entity = item;

                                        let itemArray = $.grep(itemAmountArr, function (n, i) {
                                            return n.entity == entity;
                                        })

                                        let total = 0;
                                        let subitems = [];

                                        $.each(itemArray, function (i, item) {
                                            let index = GetIndexInJsonByKeyVal(subitems, "bankName", item.bankName);

                                            if (index >= 0) {
                                                subitems[index].amount = parseFloat(subitems[index].amount) + parseFloat(item.amount);
                                            } else {
                                                subitems.push({
                                                    bankName: item.bankName,
                                                    amount: item.amount
                                                })
                                            }

                                            total += parseFloat(item.amount);
                                        })

                                        $(".by_entity .container_item").append('<div class="summary_item ' + entity + '">' +
                                            '<div class="item_header">' +
                                            '<label>' + entity + ' (₱' + parseMoney(total) + ')</label>' +
                                            '</div>' +
                                            '<div class="container_sub_item">' +
                                            '</div>' +
                                            '</div>')


                                        $.each(subitems, function (i, item) {
                                            $(".summary_item." + entity + " .container_sub_item").append('<div class="sub-item">' +
                                                '<label>' + item.bankName + ' - ₱' + parseMoney(item.amount) +
                                                '</label></div>')
                                        })
                                    })

                                    $.each(bankArray, function (i, item) {
                                        let bank = item;

                                        let itemArray = $.grep(itemAmountArr, function (n, i) {
                                            return n.bankName == bank;
                                        })

                                        let total = 0;
                                        let subitems = [];

                                        $.each(itemArray, function (i, item) {
                                            let index = GetIndexInJsonByKeyVal(subitems, "entity", item.entity);

                                            if (index >= 0) {
                                                subitems[index].amount = parseFloat(subitems[index].amount) + parseFloat(item.amount);
                                            } else {
                                                subitems.push({
                                                    entity: item.entity,
                                                    amount: item.amount
                                                })
                                            }

                                            total += parseFloat(item.amount);
                                        })


                                        $(".by_bank .container_item").append('<div class="summary_item ' + bank + '">' +
                                            '<div class="item_header">' +
                                            '<label>' + bank + ' (₱' + parseMoney(total) + ')</label>' +
                                            '</div>' +
                                            '<div class="container_sub_item">' +
                                            '</div>' +
                                            '</div>')


                                        $.each(subitems, function (i, item) {
                                            $(".summary_item." + bank + " .container_sub_item").append('<div class="sub-item">' +
                                                '<label>' + item.entity + ' - ₱' + parseMoney(item.amount) +
                                                '</label></div>')
                                        })
                                    })

                                    $("#btnRequest").html("<i class='fa fa-external-link'></i> Request <u>₱" + parseMoney(totalAmount) + "</u> Worth of Vouchers");
                                    $("#lblTotal").html("Total : ₱" + parseMoney(totalAmount))


                                    HoldOn.close();
                                    $("#mdlViewOP").modal();
                                }
                            })
                        }
                    }
                },
                error: function () {
                    HoldOn.close();
                    swal({
                        title: "There is an error processing your request to the server.",
                        icon: "error",
                    });
                }
            });
        }
    })

    $("#btnVerify").click(function () {
        if (hasError == true) {
            swal({
                icon: "error",
                title: "Oops",
                text: "Please fix the error/s first before submitting the online payments."
            })
        } else {
            swal({
                title: "Are you sure?",
                text: "Are you sure that you want to submit the records now?",
                icon: "warning",
                buttons: true
            })
                .then((yes) => {
                    if (yes) {
                        HoldOn.open({
                            theme: "sk-circle",
                            message: "Submitting your request."
                        })

                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            contentType: "application/json",
                            data: JSON.stringify({
                                model: onlinePayments
                            }),
                            url: "/OnlinePayment/SubmitOnlinePayment",
                            success: function (data) {
                                HoldOn.close();

                                if (data == "SUCCESS") {
                                    swal({
                                        title: "Success",
                                        text: "The mails are sending now.",
                                        icon: "success"
                                    }).then(function () {
                                        document.location.reload();
                                    });
                                } else {
                                    swal({
                                        title: "Failed. Please retry.",
                                        icon: "error"
                                    })
                                }
                            }
                        })
                    }
                });
        }
    })
})

function GetIndexInJsonByKeyVal(json, key, value) {
    let index = -1;

    $.each(json, function (i, item) {
        if (item[key] == value) {

            index = i;
        }
    });

    return index;
}