﻿var dateFrom;
var dateTo;

function LoadTable() {
    $("#dtLogs").DataTable().destroy();

    $("#dtLogs").setCustomSearchInput();
    var dtLogs = $('#dtLogs').DataTable({
        "order": [[0, "desc"]],
        destroy: true,
        ajax: {
            url: $('#dtLogs').attr("url") + "?dateFrom=" + dateFrom + "&dateTo=" + dateTo,
            datatype: 'json',
            dataSrc: '',

        },
        "columns": [
            {
                "data": null,
                "title": "Date/Time",
                render: function (data, type, row) {
                    var time = new Date(data.strTime);
                    return formatDate(time, "d MMM yyyy dddd hh:mmtt");
                }
            },
            { data: "userId", title: "User" },
            { data: "description", title: "Description" },
        ],
        initComplete: function () {
            $("#dtLogs").setCustomSearch({
            });
        },
    });
}

function Time(d) {
    var hr = d.getHours();
    var min = d.getMinutes();
    if (min < 10) {
        min = "0" + min;
    }
    var ampm = "am";
    if (hr > 12) {
        hr -= 12;
        ampm = "pm";
    }

    return (hr + ":" + min + ampm);
}

$(document).ready(function () {
    $("#dtrSearch").daterangepicker(null, function (a, b, c) { console.log(a.toISOString(), b.toISOString(), c) })

    var daterange = ($("#dtrSearch").val()).split(" - ");
    dateFrom = daterange[0];
    dateTo = daterange[1];
    
    LoadTable();

    $("#btnSearch").click(function () {
        var daterange = ($("#dtrSearch").val()).split(" - ");
        dateFrom = daterange[0];
        dateTo = daterange[1];
        var range = getMonthsBetween(
            new Date(dateFrom),
            new Date(dateTo)
        )

        if (range >= 1) {
            swal({
                title: "Date range for searching is limited up to one month only.",
                icon: "error"
            })

            return false;
        } else {
            $("#dtLogs").DataTable().destroy()
            LoadTable();
        }
    })
})