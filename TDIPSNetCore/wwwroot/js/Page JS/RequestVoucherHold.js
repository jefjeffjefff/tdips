﻿var voucherId = "";
var requestType = "";

$(document).ready(function () {
    $("#dtPayment").setCustomSearchInput();

    dtPayment = $('#dtPayment').DataTable({
        initComplete: function () {
            $("#dtPayment").setCustomSearch({
                exemption: [8],
                onchange: "false"
            });
        },
        ajax: {
            url: $('#dtPayment').attr("url"),
            datatype: 'json',
            dataSrc: 'dataList',
            "contentType": "application/json",
            data: function (d) {
                d.vendorcode = $("#1").val(),
                    d.companyName = $("#2").val(),
                    d.bank = $("#3").val(),
                    d.entity = $("#0").val(),
                    d.voucherId = $("#4").val(),
                    d.amount = removeCommas($("#5").val()),
                    d.payment = $("#6").val(),
                    d.date = $("#8").val(),
                    d.status = $("#7").val()
            },
            beforeSend: function () {
                $('#dtPayment > tbody').html(
                    '<tr class="odd customized_loading">' +
                    '<td valign="top" colspan="' + $("#dtPayment thead th").length + '" class="dataTables_empty">Loading&hellip;</td>' +
                    '</tr>'
                );
            },
        },
        "columns": [
            {
                "data": "dataAreaId",
                "title": "Entity",
                render: function (data, type, row) {
                    var details = "<text class='dataAreaId " + data + "'>" + data + "</text>";
                    return details;
                }
            },
            { data: "vendorCode", title: "Vendor Code" },
            { data: "companyName", title: "Company Name" },
            {
                "data": "bankName",
                "title": "Bank",
                render: function (data, type, row) {
                    var details = "<text class='bankName'>" + data + "</text>";
                    return details;
                }
            },
            {
                "data": "voucherId",
                "title": "Voucher ID",
                render: function (data, type, row) {
                    var details = "<text class='voucherId'>" + data + "</text>";
                    return details;
                }
            },
            {
                "data": "amount",
                "title": "Amount",
                render: function (data, type, row) {
                    var details = "<span>&#8369;</span><text class='amount'>" + parseMoney(data) + "</text>"
                    return details;
                }
            },
            { data: "methodOfPayment", title: "Payment" },
            {
                "data": "status",
                "title": "Status",
                render: function (data, type, row) {
                    if (data == "HOLD REQUEST ACCEPTED") {
                        return StatusLabel("ON HOLD");
                    } else {
                        return StatusLabel(data);
                    }
                }
            },
            {
                "data": null,
                "title": "Action",
                render: function (data, type, row) {
                    if (data.status == "HOLD REQUEST ACCEPTED" || data.status == "UNHOLD REQUEST DECLINED") {
                        var details = "<div class='fa-hover'><a href='#' onclick=\"OpenModal('" + data.voucherId + "','UNHOLD')\"><i class='fa fa-caret-square-o-up'></i> Request Unhold</a></div>"
                    } else if (data.status == "REQUESTED FOR HOLD" || data.status == "REQUESTED FOR UNHOLD") {
                        var details = "Requested"
                    } else {
                        var details = "<div class='fa-hover'><a href='#' onclick=\"OpenModal('" + data.voucherId + "','HOLD')\"><i class='fa fa-info-circle'></i> Request Hold</a></div>"
                    }
                    
                    return details;
                }
            },
        ],
        "columnDefs": [{
            className: 'row_action',
            targets: [8],
            orderable: false
        },
        ],
        "createdRow": function (row, data, dataIndex) {
            if ($(row).find('.dataAreaId').html() == "SPAV") {
                $(row).addClass("spavi_row");
            } else if ($(row).find('.dataAreaId').html() == "SPCI") {
                $(row).addClass("spci_row");
            } else if ($(row).find('.dataAreaId').html() == "WBHI") {
                $(row).addClass("wbhi_row");
            } else if ($(row).find('.dataAreaId').html() == "NAF") {
                $(row).addClass("naf_row");
            } else if ($(row).find('.dataAreaId').html() == "SIL") {
                $(row).addClass("sil_row");
            } else if ($(row).find('.dataAreaId').html() == "DBE") {
                $(row).addClass("dbe_row");
            }
        },
        filter: false,
        language: {
            "lengthMenu": "_MENU_",
            "processing": ""
        },
        lengthMenu: [
            [5, 10, 15, 20, 100],
            [5, 10, 15, 20, 100] // change per page values here
        ],
        orderCellsTop: true,
        pageLength: 10,
        processing: false,
        responsive: true,
        serverSide: true,
        stateSave: false
    })

    $("#btnRequest").click(function () {
        if (requestType == "HOLD") {
            var url = $("#modalDialog").attr("requesturl") + "?voucherId=" + voucherId + "&note=" + $("#txtNotes").val()
        } else if (requestType == "UNHOLD") {
            var url = $("#modalDialog").attr("unholdrequesturl") + "?voucherId=" + voucherId + "&note=" + $("#txtNotes").val()
        }

        $.ajax({
            url: url,
            success: function (data) {
                if (data == "SUCCESS") {
                    swal({
                        title: "Request is sent successfully.",
                        icon: "success"
                    }).then(function () {
                        document.location.reload();
                    });
                } else {
                    swal({
                        title: "There is an error processing your request.",
                        icon: "error"
                    })
                }
            }
        })
    })
})

function OpenModal(id, type) {
    voucherId = id;
    requestType = type;

    if (requestType == "HOLD") {
        $(".dialog-message").html("Are you sure that you want to hold the voucher status?")
    } else if (requestType == "UNHOLD") {
        $(".dialog-message").html("Are you sure that you want to unhold the voucher status? The voucher status might go back to previous status once the request is accepted.")
    }

    $("#txtNotes").val("");
    $("#modalDialog").modal();
}

