﻿HoldOn.open({
    theme: "sk-circle"
})

$(document).ready(function () {
    $.ajax({
        url: "/Home/VoucherStatus",
        success: function (data) {
            console.log(data);

            $("#unreleased .lblcount").html(data.Unreleased);
            $("#for_verification .lblcount").html(data.ForVerification);
            $("#for_approval .lblcount").html(data.ForApproval);
            $("#for_releasing .lblcount").html(data.ForRelease);
            $("#released .lblcount").html(data.Released);
            $("#uncollected .lblcount").html(data.Uncollected);

            HoldOn.close();
        }
    });

    TrackQueueProgress();
    
    $.ajax({
        url: "/Home/ReleasedVouchers",
        success: function (data) {
            let amountArr = [];
            let datesArr = [];
            let entityArr = ReturnUniqueDataFromJson(data, 'entity');
            let dateArr = ReturnUniqueDataFromJson(data, 'releasedDate');
            let entityAmountJson = [];
            let entityAmountArr = [];

            $.each(dateArr, function (i, date) {
                amountArr.push(GetTotalAmountByDate(data, 'releasedDate', date))
                let itemdate = new Date(date);
                datesArr.push(formatDate(itemdate, "MMM d, yyyy dddd"));

                entityAmountArr = [];

                $.each(entityArr, function (i, entity) {
                    entityAmountArr.push(GetTotalAmountByDateAndEntity(data, date, entity))
                })

                entityAmountJson.push({
                    date: date,
                    amountArr: entityAmountArr
                })
            })

            
            var ctx = document.getElementById("myChart").getContext('2d');

            var entityChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: datesArr,
                    datasets: [{
                        label: 'Total Amount',
                        data: amountArr,
                        backgroundColor: 'rgba(65, 142, 200)',
                        borderColor: 'rgba(65, 142, 200, 1)',
                        borderWidth: 1
                    }],
                },
                options: {
                    responsive: true,
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, data) {
                                var value = data.datasets[0].data[tooltipItem.index];
                                value = parseMoney(value);
                                return value;
                            }
                        } // end callbacks:
                    }, //end tooltips
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                userCallback: function (value, index, values) {
                                    value = parseMoney(value);
                                    return value;
                                }
                            }
                        }],
                        xAxes: [{
                            barPercentage: 0.2
                        }]
                    }
                }
            });

            $.each(entityAmountJson, function (i, item) {
                $(".container_released_entity_chart .content").append("<div class='chart-container' style='height:415px'>" +
                    "<canvas style='margin-right:15px;margin-left:15px;' id ='myChart" + i + "' width='400' height='395'></canvas></div>");

                let date = formatDate(new Date(item.date), "MMM d, yyyy");

                

                new Chart(document.getElementById("myChart" + i), {
                    type: 'bar',
                    
                    data: {
                        labels: entityArr,
                        datasets: [
                            {
                                label: "Total Amount",
                                backgroundColor: GetColorArrFromEntity(entityArr),
                                data: item.amountArr
                            }
                        ]
                    },
                    options: {
                        responsive: false,
                        legend: { display: false },
                        title: {
                            padding: 20,
                            fontColor: "#262626",
                            lineHeight: 2,
                            display: true,
                            fontSize: 13,
                            fontFamily: 'Poppins',
                            text: "Released Vouchers as of " + date + " (" + parseMoney(GetTotalAmountByDate(data, 'releasedDate', item.date)) + ")"
                        },
                        tooltips: {
                            callbacks: {
                                label: function (tooltipItem, data) {
                                    var value = data.datasets[0].data[tooltipItem.index];
                                    value = parseMoney(value);
                                    return value;
                                }
                            } // end callbacks:
                        }, //end tooltips
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    userCallback: function (value, index, values) {
                                        value = parseMoney(value);
                                        return value;
                                    }
                                },
                                offset:true
                            }],
                            xAxes: [{
                                ticks: {
                                }
                            }],
                            tooltips: {
                                mode: 'nearest',
                                axis: 'y',
                                intersect: false
                            }
                        }
                    }
                });
            })

            $(".container_released_entity_chart").trigger("click");
        }
    })

    $.ajax({
        url: "/Home/GetBatchMailDates",
        success: function (data) {
            let options = '';
            
            $.each(data, function (i, item) {
                let stringDate = item.date.split("-");
                if (stringDate.length > 0) {
                    if (stringDate[1].length == 1) {
                        stringDate[1] = "0" + stringDate[1];
                    }

                    if (stringDate[2].length == 1) {
                        stringDate[2] = "0" + stringDate[2];
                    }
                }
                
                let date = new Date(stringDate.join("-"));
                

                options = "<option value='" + item.date + "'>" + formatDate(date, "MMM d, yyyy dddd") + "</option>" + options;
            })

            $("#cbxDate").append(options);
            UpdateMailLogs();
        }
    })

    $.ajax({
        url: "/ECREA/GetLastECREARatings",
        success: function (data) {
            var rows = ""
            var yeses = 0;
            var nos = 0;

            $.each(data, function (i, item) {
                rows += `<tr>
                    <td>${item.CollectorName}</td>
                    <td>${item.VendorCode}</td>
                    <td>${(item.Rate == true ? "Yes" : "No")}</td>
                </tr>`;

                if (item.Rate == true) {
                    yeses++;
                } else {
                    nos++;
                }
            })

            $("#tblRatings tbody").append(rows);

            var config = {
                type: 'pie',
                data: {
                    datasets: [{
                        data: [
                            yeses,
                            nos,
                        ],
                        backgroundColor: [
                            "#06A18D",
                            "#C01C25",
                        ],
                        label: 'Dataset 1'
                    }],
                    labels: [
                        'Yes',
                        'No',
                    ]
                },
                options: {
                    responsive: true
                }
            };

            var ctx = document.getElementById('chart-area').getContext('2d');
            window.myPie = new Chart(ctx, config);
        }
    })

    $("#cbxDate").change(function () {
        UpdateMailLogs();
    })
})

function UpdateMailLogs() {
    $.ajax({
        url: "/Home/GetMailLog?date=" + $("#cbxDate").val(),
        success: function (data) {
            $(".container_mail_log").find('.content').html(
                '<div id="email_log"><canvas id="emailChart" width="400" height="250"></canvas></div>' +
                '<div id="sms_log"><canvas id="smsChart" width="400" height="250"></canvas></div>'
            )

            let logs = {
                SMS: {
                    success: 0,
                    failed: 0,
                    stopped: 0,
                    sending: 0
                },
                EMAIL: {
                    success: 0,
                    failed: 0,
                    stopped: 0,
                    sending: 0
                }
            }

            $.each(data, function (i, item) {
                if (item.status == 'SUCCESS') {
                    logs[item.type].success += 1
                } else if (item.status == 'SENDING') {
                    logs[item.type].sending += 1
                } else if (item.status == 'STOPPED') {
                    logs[item.type].stopped += 1
                } else {
                    logs[item.type].failed += 1
                }
            })

            let logschart = {
                SMS: {
                    labelArr: [],
                    colorArr: [],
                    dataArr: []
                },
                EMAIL: {
                    labelArr: [],
                    colorArr: [],
                    dataArr: []
                }
            }
            
            let colors = {
                success: 'rgba(39, 174, 96, .8)', //green
                failed: 'rgba(221, 18, 25, .8)', //red
                stopped: 'rgba(241, 196, 15, .8)', //yellow
                sending: 'rgba(41, 128, 185, .8)' //blue
            }

            $.each(['SMS', 'EMAIL'], function (i, item) {
                $.each(['success', 'failed', 'stopped', 'sending'], function (x, stats) {
                    if (logs[item][stats] > 0) {
                        logschart[item].labelArr.push(stats.capitalize())
                        logschart[item].colorArr.push(colors[stats])
                        logschart[item].dataArr.push(logs[item][stats])
                    }
                })
            })
            
            $.each(['SMS', 'EMAIL'], function (i, item) {
                var ctx = document.getElementById(item.toLowerCase() + "Chart").getContext('2d');
                var barChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: logschart[item].labelArr,
                        datasets: [{
                            label: 'Count',
                            data: logschart[item].dataArr,
                            backgroundColor: logschart[item].colorArr,
                            borderColor: 'rgba(65, 142, 200, 1)',
                            borderWidth: 1
                        }],
                    },
                    options: {
                        title: {
                            fontColor: "#262626",
                            display: true,
                            fontSize: 13,
                            text: item,
                            fontFamily: "Poppins"
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }],
                            xAxes: [{
                                barPercentage: logschart[item].labelArr.length > 1 ? 0.5 : 0.3
                            }]
                        }
                    },
                    
                });
            })
        }
    })
}

function ReturnUniqueDataFromJson(json, key) {
    var returningArr = [];

    $.each(json, function (i, item) {
        if (returningArr.indexOf(item[key]) == -1) {
            returningArr.push(item[key]);
        }
    });

    return returningArr;
}


function TrackQueueProgress() {
    $.ajax({
        url: "/Home/QueueingProgress",
        success: function (data) {
            if (data.length == 0) {
                $("#queueChart").css("display", "none");
                $("#noQueueMsg").css("display", "block");
            } else {
                $("#queueChart").remove();
                $(".container_queue_progress .content").append('<canvas style="display:none;" id="queueChart" width="400" height="150"></canvas>')

                $("#queueChart").css("display", "block");
                $("#noQueueMsg").css("display", "none");

                var options = ['Served', 'Waiting Reg', 'Waiting Prio', 'Dequeued']
                var dataArr = GetQueueStatusCounts(data);
                var currentTime = new Date();
                

                var ctx = document.getElementById("queueChart").getContext('2d');

                var queueChart = new Chart(ctx, {
                    type: 'horizontalBar',
                    data: {
                        labels: options,
                        datasets: [{
                            label: 'Count',
                            data: dataArr,
                            borderColor: 'rgba(65, 142, 200, 1)',
                            borderWidth: 1,
                            backgroundColor: ["rgba(39, 174, 96, .8)", "rgb(50,130,184,.7)", "rgb(241,196,15,.9)","rgba(221, 18, 25, .8)"]
                        }],
                    },
                    options: {
                        animation: false,
                        title: {
                            fontColor: "#262626",
                            display: true,
                            fontSize: 13,
                            text: formatDate(currentTime, "yyyy-MM-dd dddd h:mmtt"),
                            fontFamily: "Poppins"
                        },
                        scales: {
                            xAxes: [{
                                display: true,
                                ticks: {
                                    beginAtZero: true,
                                    userCallback: function (label, index, labels) {
                                        // when the floored value is the same as the value we have a whole number
                                        if (Math.floor(label) === label) {
                                            return label;
                                        }

                                    },
                                }
                            }]
                        }
                    },
                    
                });
            }
        },
        complete: function (data) {
            fetchQueueTimeOut = setTimeout(TrackQueueProgress, 10000);
        }
    })
}

function GetColorArrFromEntity(entityArr) {
    let colors = [];

    $.each(entityArr, function (i, entity) {
        if (entity == 'DBE') {
            colors.push("rgba(142, 68, 173, .8)") //violet
        } else if (entity == 'WBHI') {
            colors.push("rgba(251, 197, 49, .8)") //yellow
        } else if (entity == 'NAF') {
            colors.push("rgba(26, 188, 156, .8)") //green
        } else if (entity == 'SPCI') {
            colors.push("rgba(230, 126, 34, .8)") //orange
        } else if (entity == 'SPAV') {
            colors.push('rgba(221, 18, 25, .8)'); //red
        } else {
            colors.push('#95a5a6'); //grey
        }
    })

    return colors;
}

function GetTotalAmountByDateAndEntity(json, date, entity) {
    var total = 0.0;

    $.each(json, function (i, item) {
        if (item['releasedDate'] == date & item['entity'] == entity) {
            total += item.amount;
        }
    });
    
    return total;
}

function GetTotalAmountByDate(json, key, date) {
    var total = 0.0;

    $.each(json, function (i, item) {
        if (item[key] == date) {
            total += item.amount;
        }
    });

    return total;
}

function GetQueueStatusCounts(json) {
    let served = 0;
    let waitingReg = 0;
    let waitingPrio = 0;
    let dequeued = 0;

    $.each(json, function (i, item) {
        if (item.status == 'DONE') {
            served++;
        } else if (item.status == 'DEQUEUED') {
            dequeued++;
        } else if (item.status == 'QUEUEING' && item.queueType == 'REGULAR') {
            waitingReg++;
        } else if (item.status == 'QUEUEING' && item.queueType == 'PRIORITY') {
            waitingPrio++;
        }
    });

    return [served, waitingReg, waitingPrio, dequeued];
}