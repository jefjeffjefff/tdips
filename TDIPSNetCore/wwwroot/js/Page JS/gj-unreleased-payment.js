﻿var entries;
var hasError;

$(document).ready(function () {
    var month = parseInt(formatDate(dateToday, "MM")) - 1
    var year = parseInt(formatDate(dateToday, "yyyy"));

    var yearOptions = "";

    for (var i = year - 1; i < year + 2; i++)
    {
        yearOptions += `<option value='${i}'>${i}</option>`
    }

    $("#txtYear").html(yearOptions);
    $("#txtYear").val(year);
    $("#txtMonth").val(month);

    $("#txtYear, #txtMonth").change(function () {
        LoadUnreleasedGJEntries();
    });

    $("#dtGJ").DataTable({
        ajax: {
            dataSrc: '',
            url: "/GeneralJournal/GetPostedUnreleasedGeneralJournal"
        },
        columns: [
            {
                title: "Post Date",
                data: null,
                render: function (data) {
                    return formatDate(parseJsonDate(data.postDate), "MMMM dd, yyyy");
                }
            },
            { title: "Posted By", data: "postedBy" },
            { title: "General Journal #", data: "generalJournalNum" },
            { title: "Description", data: "description" },
            { title: "Entity", data: "entity" },
            { title: "Month", data: "month" },
            { title: "Year", data: "year"}
        ]
    })
});



function ShowGJModal() {
    $("#modalGJ").modal();
    LoadUnreleasedGJEntries();
}

function ClickPostUnreleasedGJEntries() {
    if ($("#txtDescription").val() == "") {
        swal({
            title: "Oops",
            text: "Please put some description first before posting.",
            icon: "error"
        })

        return false;
    }

    if (entries.length == 0) {
        swal({
            title: "Oops",
            text: "There is no entry to be posted.",
            icon: "error"
        })

        return false;
    }
    
    if (hasError == true) {
        swal({
            title: "Are you sure that you want to post the entries to D365?",
            text: "Not all entries would be posted as there are errors!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((confirm) => {
                if (confirm) {
                    PostUnreleasedGJEntries();
                }
            });
    } else {
        PostUnreleasedGJEntries();
    }
}


function PostUnreleasedGJEntries()
{
    HoldOn.open({
        theme: "sk-circle",
        message: "Posting to D365"
    })

    $.ajax({
        url: "/GeneralJournal/PostUnreleasedGeneralJournal",
        dataType: "json",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({
            description: $("#txtDescription").val(),
            dataAreaId: $("#txtEntity").val(),
            entries: entries,
            month: $("#txtMonth").val(),
            year: $("#txtYear").val()
        }),
        success: function (data) {
            HoldOn.close();

            HoldOn.close();
            if (data.Status == "SUCCESS") {
                swal({
                    title: "Success",
                    text: "Your entry is successfully posted to D365 (" + data.Message + ")",
                    icon: "success"
                }).then(function () {
                    document.location.reload();
                });
            } else {
                swal({
                    title: "Oops",
                    icon: "error",
                    text: data.Message
                })
            }
        }
    })
}

function LoadUnreleasedGJEntries()
{
    HoldOn.open({
        theme: "sk-circle"
    })

    $.ajax({
        url: "/GeneralJournal/GetUnreleasedGJEntries?month=" + $("#txtMonth").val() +
            "&year=" + $("#txtYear").val() + "&entity=" + $("#txtEntity").val(),
        success: function (data) {
            entries = data;
            hasError = false;

            HoldOn.close();
            $("#dtViewGJ").DataTable().destroy();
            $("#dtViewGJ").DataTable({
                destroy: true,
                data: data,
                dataSrc: '',
                columns: [
                    { title: "Voucher", data: "VoucherNum" },
                    { title: "Account Type", data: "AccountType" },
                    { title: "Account", data: "Account" },
                    { title: "Department", data: "Department" },
                    { title: "Business Unit", data: "BusinessUnit" },
                    { title: "Purpose", data: "Purpose" },
                    {
                        title: "TransDate",
                        data: null,
                        render: function (data) {
                            return formatDate(parseJsonDate(data.Transdate), "MM-dd-yyyy");
                        }
                    },
                    { title: "TXT/Document", data: "TXT" },
                    {
                        title: "Credit",
                        data: null,
                        render: function (data) {
                            return parseMoney(data.Credit);
                        }
                    },
                    { title: "Offset Acc Type", data: "OffsetAccountType" },
                    { title: "Offset Acc", data: "OffsetAccount" },
                    { title: "Posting Profile", data: "PostingProfile" },
                    {
                        title: "Due Date",
                        data: null,
                        render: function (data) {
                            return formatDate(parseJsonDate(data.DueDate), "MM-dd-yyyy");
                        }
                    },
                    {
                        title: "Status",
                        data: null,
                        render: function (data) {
                            var details = "";

                            if (data.EntryStatus == "ERROR") {
                                details += "<span class='label label-danger mb-2'>Error</span>"
                            } else {
                                details += "<span class='label label-success mb-2'>Success</span>"
                            }

                            return details + " <div style='margin-top:10px;'>" + data.EntryDescription.join(". ") + "</div>";
                        }
                    },
                ],
                "createdRow": function (row, data, index) {
                    if (data.EntryStatus == "ERROR") {
                        hasError = true;
                    }
                }
            });
        }
    })
}