﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TDIPS.Models
{
    public class BankAccount
    {
        public int Id { get; set; }
        public string accountNum { get; set; }
        public string accountId { get; set; }
        public string name { get; set; }
        public string dataAreaId { get; set; }
    }
}