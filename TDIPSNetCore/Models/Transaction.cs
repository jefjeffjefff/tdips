﻿//using LinqToExcel.Attributes;
using LinqToExcel.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Models
{
    public class Transaction
    {
        [ExcelColumn("POSTING DATE")]
        public DateTime? POSTINGDATE { get; set; }

        public string BRANCH { get; set; }

        public string DESCRIPTION { get; set; }
        
        public string DEBIT { get; set; }

        public string CREDIT { get; set; }
    }
}
