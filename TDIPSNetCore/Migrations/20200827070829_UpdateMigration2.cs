﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TDIPSNetCore.Web.Migrations
{
    public partial class UpdateMigration2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Test",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<bool>(
                name: "IsUserOnLeave",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsUserOnLeave",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<string>(
                name: "Test",
                table: "AspNetUsers",
                type: "text",
                nullable: true);
        }
    }
}
