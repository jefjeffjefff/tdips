﻿using AuthenticationUtility;
using DynamicsIntegration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using TDIPS.Core.Models;

//https://spav-sm-devtest1703b90ff9e74648devaos.cloudax.dynamics.com//api/services/SPCIAutoExDocumentServiceGroup/SPCIAutoExDocumentService/getChequeDetails

namespace TDIPS
{
	public class GenerateDynamicsData
	{
        const string url = "/api/services/SPCIAutoExpressGroupService/SPCIAutoExpressService/";
        public static string sessionUrl = "/api/services/SPCIAutoExpressGroupService/SPCIAutoExpressService/getInvoiceByVoucher";


        public string GenerateInvoiceDetails(string[] voucherIdArr)
        {
            //var apiUrl = url + "getInvoiceByVoucher";


            DataTable dtVoucher = new DataTable();
            dtVoucher.Clear();
            dtVoucher.Columns.Add("Voucher", typeof(string));

            for (int i = 0; i < voucherIdArr.Length; i++)
            {
                DataRow dr = dtVoucher.NewRow();
                dr["Voucher"] = voucherIdArr[i];
                dtVoucher.Rows.Add(dr);
            }

            //var rowsCount = dtVoucher.Rows.Count;

            //DataTable dt = new DataTable();
            //dt.Columns.Add("Voucher");
            //dt.Rows.Add("SPAV-VPMT-000010301");
            //dt.Rows.Add("SPAV-IVINV-000005940");
            //dt.Rows.Add("SPCI_VPMT-000003969");
            //dt.Rows.Add("WBHI-APV-000000028");
            
            //TDIPSServiceContract dataContract = new TDIPSServiceContract();
            //RootObject root = new RootObject();
            //root._dataContract = dataContract;

            //dataContract.dtVoucher = dt;
            //string json = JsonConvert.SerializeObject(root);

            //return Generate(apiUrl);

            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            string GetUserSessionOperationPath = string.Format("{0}{1}  ", ClientConfiguration.Default.UriString.TrimEnd('/'), sessionUrl);
            TDIPSServiceContract dataContract = new TDIPSServiceContract();

            //DataTable dt = new DataTable();
            //dt.Columns.Add("Voucher");
            //dt.Rows.Add("SPAV-VPMT-000010301");
            //dt.Rows.Add("SPAV-IVINV-000005940");
            //dt.Rows.Add("SPCI_VPMT-000003969");
            //dt.Rows.Add("WBHI-APV-000000028");


            dataContract.dtVoucher = dtVoucher;

            RootObject root = new RootObject();
            root._dataContract = dataContract;

            string json = JsonConvert.SerializeObject(root);

            var request = HttpWebRequest.Create(GetUserSessionOperationPath);

            request.Headers[OAuthHelper.OAuthHeader] = OAuthHelper.GetAuthenticationHeader(true);
            request.Method = "POST";
            request.ContentLength = 0;

            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(json);
            request.ContentLength = byteArray.Length;
            request.ContentType = @"application/json";

            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }

            using (var response = (HttpWebResponse)request.GetResponse())
            {
                using (Stream responseStream = response.GetResponseStream())
                {
                    using (StreamReader streamReader = new StreamReader(responseStream))
                    {
                        string responseString = streamReader.ReadToEnd();

                        return responseString;
                    }
                }
            }
        }

        public string PostUnreleasedGeneralJournal(string description, string dataAreaId, UnreleasedGJEntry[] entries)
        {
            var apiUrl = url + "insertGeneralJournal";
            TDIPSServiceContract dataContract = new TDIPSServiceContract();
            RootObject root = new RootObject();
            root._dataContract = dataContract;

            var dt = new DataTable();

            #region DataTable Columns
            dt.Columns.Add("ACCOUNTTYPE");
            dt.Columns.Add("ACCOUNT");
            dt.Columns.Add("STORES");
            dt.Columns.Add("DEPARTMENT");
            dt.Columns.Add("BUSINESSUNIT");
            dt.Columns.Add("PURPOSE");
            dt.Columns.Add("SALESSEGMENT");
            dt.Columns.Add("WORKER");
            dt.Columns.Add("COSTCENTER");
            dt.Columns.Add("<allocation for future segments 1>");
            dt.Columns.Add("<allocation for future segments 2>");
            dt.Columns.Add("TRANSDATE");
            dt.Columns.Add("TXT");
            dt.Columns.Add("DEBIT");
            dt.Columns.Add("CREDIT");
            dt.Columns.Add("OFFSETACCOUNTTYPE");
            dt.Columns.Add("OFFSETACCOUNT");
            dt.Columns.Add("STORES OFFSET");
            dt.Columns.Add("DEPARTMENT OFFSET");
            dt.Columns.Add("BUSINESSUNIT OFFSET");
            dt.Columns.Add("PURPOSE OFFSET");
            dt.Columns.Add("SALESSEGMENT OFFSET");
            dt.Columns.Add("WORKER OFFSET");
            dt.Columns.Add("COSTCENTER OFFSET");
            dt.Columns.Add("<allocation for future segments offset 1>");
            dt.Columns.Add("<allocation for future segments offset 2>");
            dt.Columns.Add("POSTINGPROFILE");
            dt.Columns.Add("INVOICE");
            dt.Columns.Add("DUEDATE");
            dt.Columns.Add("TAXGROUP");
            dt.Columns.Add("TAXITEMGROUP");
            dt.Columns.Add("WITHHOLDINGTAX");
            dt.Columns.Add("DOCUMENT");
            dt.Columns.Add("SALESTAXCODE");
            dt.Columns.Add("OFFSETTEXT");

            dt.Rows.Add("ACCOUNTTYPE", "ACCOUNT", "STORES", "DEPARTMENT", "BUSINESSUNIT", "PURPOSE", "SALESSEGMENT", "WORKER",
                "COSTCENTER", "<allocation for future segments>", "<allocation for future segments>", "TRANSDATE", "TXT", "DEBIT",
                "CREDIT", "OFFSETACCOUNTTYPE", "OFFSETACCOUNT", "STORES", "DEPARTMENT", "BUSINESSUNIT", "PURPOSE", "SALESSEGMENT", "WORKER",
                "COSTCENTER", "<allocation for future segments>", "<allocation for future segments>", "POSTINGPROFILE", "INVOICE", "DUEDATE",
                "TAXGROUP", "TAXITEMGROUP", "WITHHOLDINGTAX", "DOCUMENT", "SALESTAXCODE", "OFFSETTEXT");
            #endregion

            foreach (var entry in entries)
            {
                if (entry.EntryStatus == "ERROR")
                {
                    continue;
                }

                dt.Rows.Add(
                    entry.AccountType,
                    entry.Account,
                    "",
                    entry.Department,
                    entry.BusinessUnit,
                    entry.Purpose,
                    "",
                    "",
                    "",
                    "",
                    "",
                    DateTime.ParseExact(entry.strTransdate, "MM-dd-yyyy", CultureInfo.InvariantCulture),
                    entry.TXT,
                    "",
                    entry.Credit,
                    entry.OffsetAccountType,
                    entry.OffsetAccount,
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    entry.PostingProfile,
                    "",
                    DateTime.ParseExact(entry.strTransdate, "MM-dd-yyyy", CultureInfo.InvariantCulture),
                    "",
                    "",
                    "",
                    entry.Document,
                    "",
                    ""
                );
            }

            dataContract.DataAreaId = dataAreaId;
            dataContract.dtGeneralJournal = dt;
            dataContract.Description = description;
            dataContract.DocumentNumber = "GEN JRN";

            string json = JsonConvert.SerializeObject(root);

            return Integrate(apiUrl, json);
        }

        public string PostOtherOnlinePayments(string description, string dataAreaId, OtherOnlinePayment[] paymentArr)
        {
            var apiUrl = url + "insertGeneralJournal";
            TDIPSServiceContract dataContract = new TDIPSServiceContract();
            RootObject root = new RootObject();
            root._dataContract = dataContract;

            var dt = new DataTable();

            #region DataTable Columns
            dt.Columns.Add("ACCOUNTTYPE");
            dt.Columns.Add("ACCOUNT");
            dt.Columns.Add("STORES");
            dt.Columns.Add("DEPARTMENT");
            dt.Columns.Add("BUSINESSUNIT");
            dt.Columns.Add("PURPOSE");
            dt.Columns.Add("SALESSEGMENT");
            dt.Columns.Add("WORKER");
            dt.Columns.Add("COSTCENTER");
            dt.Columns.Add("<allocation for future segments 1>");
            dt.Columns.Add("<allocation for future segments 2>");
            dt.Columns.Add("TRANSDATE");
            dt.Columns.Add("TXT");
            dt.Columns.Add("DEBIT");
            dt.Columns.Add("CREDIT");
            dt.Columns.Add("OFFSETACCOUNTTYPE");
            dt.Columns.Add("OFFSETACCOUNT");
            dt.Columns.Add("STORES OFFSET");
            dt.Columns.Add("DEPARTMENT OFFSET");
            dt.Columns.Add("BUSINESSUNIT OFFSET");
            dt.Columns.Add("PURPOSE OFFSET");
            dt.Columns.Add("SALESSEGMENT OFFSET");
            dt.Columns.Add("WORKER OFFSET");
            dt.Columns.Add("COSTCENTER OFFSET");
            dt.Columns.Add("<allocation for future segments offset 1>");
            dt.Columns.Add("<allocation for future segments offset 2>");
            dt.Columns.Add("POSTINGPROFILE");
            dt.Columns.Add("INVOICE");
            dt.Columns.Add("DUEDATE");
            dt.Columns.Add("TAXGROUP");
            dt.Columns.Add("TAXITEMGROUP");
            dt.Columns.Add("WITHHOLDINGTAX");
            dt.Columns.Add("DOCUMENT");
            dt.Columns.Add("SALESTAXCODE");
            dt.Columns.Add("OFFSETTEXT");

            dt.Rows.Add("ACCOUNTTYPE", "ACCOUNT", "STORES", "DEPARTMENT", "BUSINESSUNIT", "PURPOSE", "SALESSEGMENT", "WORKER",
                "COSTCENTER", "<allocation for future segments>", "<allocation for future segments>", "TRANSDATE", "TXT", "DEBIT",
                "CREDIT", "OFFSETACCOUNTTYPE", "OFFSETACCOUNT", "STORES", "DEPARTMENT", "BUSINESSUNIT", "PURPOSE", "SALESSEGMENT", "WORKER",
                "COSTCENTER", "<allocation for future segments>", "<allocation for future segments>", "POSTINGPROFILE", "INVOICE", "DUEDATE",
                "TAXGROUP", "TAXITEMGROUP", "WITHHOLDINGTAX", "DOCUMENT", "SALESTAXCODE", "OFFSETTEXT");
            #endregion

            for (int i = 0; i < paymentArr.Length; i++)
            {
                dt.Rows.Add(
                    paymentArr[i].accountType ?? "",
                    paymentArr[i].account ?? "",
                    paymentArr[i].stores ?? "",
                    paymentArr[i].department ?? "",
                    paymentArr[i].businessUnit ?? "",
                    paymentArr[i].purpose ?? "",
                    paymentArr[i].salesSegment ?? "",
                    paymentArr[i].worker ?? "",
                    paymentArr[i].costCenter ?? "",
                    "", 
                    "",
                    paymentArr[i].transDate,
                    paymentArr[i].description ?? "",
                    paymentArr[i].debit,
                    paymentArr[i].credit,
                    paymentArr[i].offsetAccountType ?? "",
                    paymentArr[i].offsetAccount ?? "",
                    paymentArr[i].oStores ?? "",
                    paymentArr[i].oDepartment ?? "",
                    paymentArr[i].oBusinessUnit ?? "",
                    paymentArr[i].oPurpose ?? "",
                    paymentArr[i].oSalesSegment ?? "",
                    paymentArr[i].oWorker ?? "",
                    paymentArr[i].oCostCenter ?? "",
                    "", 
                    "", 
                    "", 
                    "",
                    "", 
                    "", 
                    "", 
                    "", 
                    "", 
                    "",
                    "");
            }

            dataContract.DataAreaId = dataAreaId;
            dataContract.dtGeneralJournal = dt;
            dataContract.Description = description;
            dataContract.DocumentNumber = "GEN JRN";

            string json = JsonConvert.SerializeObject(root);

            return Integrate(apiUrl, json);
        }

        public string GenerateLedgerTransactionList(DateTime? dateFrom, DateTime? dateTo, string dataAreaId, string mainAccount, string purpose)
        {
            var apiUrl = url + "getVoucherTrans";

            TDIPSServiceContract dataContract = new TDIPSServiceContract();
            RootObject root = new RootObject();

            root._dataContract = dataContract;

            dataContract.DateFrom = dateFrom ?? DateTime.Today;
            dataContract.DateTo = dateTo ?? DateTime.Today;
            dataContract.DataAreaId = dataAreaId;
            dataContract.Purpose = purpose;
            dataContract.MainAccount = mainAccount;

            string json = JsonConvert.SerializeObject(root);

            return Integrate(apiUrl, json);
        }

        public string GetGeneralJournal(string journalNumber)
        {
            var apiUrl = url + "getVoucher";

            TDIPSServiceContract dataContract = new TDIPSServiceContract();
            RootObject root = new RootObject();

            root._dataContract = dataContract;

            dataContract.JournalNumber = journalNumber;
            
            string json = JsonConvert.SerializeObject(root);

            return Integrate(apiUrl, json);
        }
        
        public string GeneratePayments(DateTime dateFrom, DateTime dateTo)
		{
			var apiUrl = url + "getPaidChequeDetails";

            TDIPSServiceContract dataContract = new TDIPSServiceContract();
            RootObject root = new RootObject();
            root._dataContract = dataContract;

            dataContract.DateFrom = dateFrom;
            dataContract.DateTo = dateTo;
            dataContract.DataAreaId = "spav";
            string json = JsonConvert.SerializeObject(root);

            return Integrate(apiUrl, json);
		}

        public string GenerateEmployeesReimbursements(DateTime dateFrom, DateTime dateTo)
        {
            var apiUrl = url + "getReimbursementEmployee";

            TDIPSServiceContract dataContract = new TDIPSServiceContract();
            RootObject root = new RootObject();
            root._dataContract = dataContract;

            dataContract.DateFrom = dateFrom;
            dataContract.DateTo = dateTo;

            string json = JsonConvert.SerializeObject(root);

            return Integrate(apiUrl, json);
        }

        public string GenerateVendors()
        {
            var apiUrl = url + "getVendorlist";

            return Integrate(apiUrl);
        }

        public string GenerateBanks()
        {
            var apiUrl = url + "getBankAccounts";

            return Integrate(apiUrl);
        }

        public string Integrate(string apiUrl, string json = "")
        {
            try
            {
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                string GetUserSessionOperationPath = string.Format("{0}{1}", ClientConfiguration.Default.UriString.TrimEnd('/'), apiUrl);

                var request = HttpWebRequest.Create(GetUserSessionOperationPath);

                request.Headers[OAuthHelper.OAuthHeader] = OAuthHelper.GetAuthenticationHeader(true);
                request.Method = "POST";
                request.ContentLength = 0;
                request.Timeout = 600000;

                System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                Byte[] byteArray = encoding.GetBytes(json);
                request.ContentLength = byteArray.Length;
                request.ContentType = @"application/json";

                using (Stream dataStream = request.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        using (StreamReader streamReader = new StreamReader(responseStream))
                        {
                            string responseString = streamReader.ReadToEnd();

                            return responseString;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
	}
}