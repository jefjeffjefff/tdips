﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TDIPS.Core.Interfaces;
using TDIPS.Infrastructure.Business_Logic;

namespace TDIPSNetCore.Web.Controllers
{
    //[CustomAuthorize("!AP", "!AUDIT")]
    [Authorize(Policy = "DenyAudit")]
    public class SystemController : Controller
    {
        ISystemLogRepository SystemLogRepository = new SystemLogRepository();
        IMailRepository MailRepository = new MailRepository();
        IBackupRepository BackupRepository = new BackupRepository();
        // GET: System

        public ActionResult Logs()
        {
            return View();
        }

        public ActionResult GetSystemLogsList(Nullable<DateTime> dateFrom = null, Nullable<DateTime> dateTo = null, string type = "")
        {
            var resultData = SystemLogRepository.List(dateFrom, dateTo, type);
            var result = new ContentResult
            {
                Content = JsonConvert.SerializeObject(resultData, Formatting.Indented),
                ContentType = "application/json"
            };

            return result;
        }


        public ActionResult GetMailLogsList(DateTime dateFrom, DateTime dateTo)
        {
            var resultData = MailRepository.MailLogs(dateFrom, dateTo);
            var result = new ContentResult
            {
                Content = JsonConvert.SerializeObject(resultData, Formatting.Indented),
                ContentType = "application/json"
            };

            return result;
        }

        public ActionResult MailLogs()
        {
            return View();
        }

        public ActionResult TrackMailStatus()
        {
            return Json(MailRepository.TrackMailStatus());
        }

        public ActionResult Backup()
        {
            return View();
        }

        public ActionResult GetBackupFiles()
        {
            string[] files = Directory.GetFiles(@"c:\Backup\", "*.bak", SearchOption.AllDirectories);
            List<string> filelist = new List<string>();

            for (int i = 0; i < files.Length; i++)
            {
                filelist.Add(files[i]);
            }

            return Json(filelist.ToList());
        }

        public ActionResult DownloadBackup(string filename)
        {
            string file = @"c:\backup\" + filename;
            return File(file, "bak", Path.GetFileName(file));
        }
    }
}
