﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ClosedXML.Excel;
using LinqToExcel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TDIPS;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.Business_Logic;

namespace TDIPSNetCore.Web.Controllers
{
    [Authorize]
    public class GeneralJournalController : Controller
    {
        // GET: GeneralJournal
        IGeneralJournalRepository GeneralJournalRepository = new GeneralJournalRepository();
        private readonly GenerateDynamicsData _integration = new GenerateDynamicsData();
        ISystemLogRepository SystemLogRepository = new SystemLogRepository();

        private readonly IWebHostEnvironment _webHostEnvironment;

        public GeneralJournalController(IWebHostEnvironment hostingEnvironment)
        {
            _webHostEnvironment = hostingEnvironment;
        }

        public ActionResult Banks()
        {
            return View();
        }

        public ActionResult DownloadExcel(string fileName)
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(_webHostEnvironment.WebRootPath + "/SpreadSheets/General Journal/" + fileName + ".xlsx");
            string fileName2 = fileName + ".xlsx";

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName2);
        }

        public void DeleteExcelFile(string fullpath)
        {
            if (System.IO.File.Exists(fullpath))
            {
                System.IO.File.Delete(fullpath);
            }
        }

        public ActionResult UploadTool()
        {
            return View();
        }

        public ActionResult GetBankList()
        {
            return Json(GeneralJournalRepository.GetBankList());
        }

        public ActionResult GetGJFileList()
        {
            DirectoryInfo directory = new DirectoryInfo(_webHostEnvironment.WebRootPath + "/Spreadsheets/General Journal");
            FileInfo[] FileList = directory.GetFiles("*", SearchOption.AllDirectories);

            return Json(FileList.Select(x => new { x.FullName, x.CreationTime }).Where(x =>
            !x.FullName.Contains("TextFile1")).ToList());
        }

        public ActionResult Transactions()
        {
            return View();
        }

        public ActionResult GetPostedUnreleasedGeneralJournal()
        {
            var result = GeneralJournalRepository.GetPostedUnreleasedGeneralJournal();

            return Json(result);
        }

        public ActionResult ImportBanksExcel(IFormFile FileUpload)
        {
            if (FileUpload == null)
            {
                return Json("NULL");
            }
            else if (!(FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || FileUpload.ContentType == "application/octet-stream"))
            {
                return Json("INVALID FILE");
            }

            var parsedFileName = Path.GetFileName(FileUpload.FileName);
            var filename = string.Format(DateTime.Now.ToString("yyyy-MM-dd HH_mm_ss_") + parsedFileName);
            string targetpath = _webHostEnvironment.WebRootPath + "/SpreadSheets/";
            FileUpload.SaveAs(targetpath + filename);
            string pathToExcelFile = targetpath + filename;
            var connectionString = "";

            if (filename.EndsWith(".xls"))
            {
                connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
            }
            else if (filename.EndsWith(".xlsx"))
            {
                connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);
            }

            var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connectionString);
            var ds = new DataSet();

            adapter.Fill(ds, "ExcelTable");

            string sheetName = "Sheet1";

            var excelFile = new ExcelQueryFactory(pathToExcelFile);
            var banks = from a in excelFile.Worksheet<GJBank>(sheetName) select a;

            ResultMsg rm = new ResultMsg();

            foreach (var bank in banks)
            {
                string result = GeneralJournalRepository.AddBank(bank);

                if (result == "OVERWRITTEN")
                {
                    rm.Overwritten++;
                }
                else
                {
                    rm.RowsAdded++;
                }
            }

            if ((System.IO.File.Exists(pathToExcelFile)))
            {
                System.IO.File.Delete(pathToExcelFile);
            }

            rm.Status = "SUCCESS";
            return Json(rm);
        }

        public ActionResult UploadTransactionExcel(IFormFile FileUpload)
        {
            try
            {
                if (FileUpload == null)
                {
                    return Json("NULL");
                }
                else if (!(FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || FileUpload.ContentType == "application/octet-stream"))
                {
                    return Json("INVALID FILE");
                }

                var parsedFileName = Path.GetFileName(FileUpload.FileName);
                var filename = string.Format(DateTime.Now.ToString("yyyy-MM-dd HH_mm_ss_") + parsedFileName);
                string targetpath = _webHostEnvironment.WebRootPath + "/SpreadSheets/";
                FileUpload.SaveAs(targetpath + filename);
                string pathToExcelFile = targetpath + filename;
                var connectionString = "";

                if (filename.EndsWith(".xls"))
                {
                    connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
                }
                else if (filename.EndsWith(".xlsx"))
                {
                    connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);
                }

                OleDbConnection oconn = new OleDbConnection(connectionString);

                oconn.Open();

                DataTable dbSchema = oconn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                if (dbSchema == null || dbSchema.Rows.Count < 1)
                {
                    throw new Exception("Error: Could not determine the name of the first worksheet.");
                }

                string sheetName = dbSchema.Rows[0]["TABLE_NAME"].ToString();

                oconn.Close();

                var excelFile = new ExcelQueryFactory(pathToExcelFile);

                var excelTable = excelFile.WorksheetNoHeader(sheetName.Replace("$", ""));

                var table = excelTable.ToList();
                var totalRows = table.Count();
                var totalRecords = 0;

                var TransactionList = new List<GJTransaction>();

                //start from 3rd row (zero-based indexing), length = allRows.Count() or computed range of rows you want
                for (int i = 16; i < totalRows; i++)
                {
                    var row = table[i];

                    if ((row[1] ?? "").ToString().Contains("/"))
                    {
                        totalRecords++;

                        var gjb = new GJBank
                        {
                            AccountName = row[3]
                        };

                        var gjt = new GJTransaction
                        {
                            Gjbank = gjb,
                            Debit = decimal.Parse(((row[7] == "" ? "0.00" : row[7]).ToString().Replace(",", ""))),
                            Credit = decimal.Parse(((row[8] == "" ? "0.00" : row[8]).ToString().Replace(",", ""))),
                            AccountNo = GenerateAccountNumber(row[6].ToString()),
                            PostingDate = DateTime.ParseExact(row[1].ToString(), "MM/dd/yyyy", null)
                        };

                        TransactionList.Add(gjt);
                    }
                }

                var GJTBList = GeneralJournalRepository.JoinGJTandGJB(TransactionList);

                var banksMatched = GJTBList.Select(x => x.AccountNo).ToList();

                var missingBanks = TransactionList.Where(x => !banksMatched.Contains(x.AccountNo))
                        .Select(x => x.AccountNo).Distinct().ToList();

                var postingDate = TransactionList.OrderByDescending(x => x.PostingDate).FirstOrDefault().PostingDate;

                if ((System.IO.File.Exists(pathToExcelFile)))
                {
                    System.IO.File.Delete(pathToExcelFile);
                }

                GJTransaction gjt2 = new GJTransaction
                {
                    Gjbank = new GJBank
                    {
                        AccountName = "BDO-MOTHER",
                        AccountType = "Bank",
                        Department = "FINANCE",
                        BusinessUnit = "CSO"
                    },
                    PostingDate = postingDate,
                    Debit = GJTBList.AsEnumerable().Sum(x => x.Credit)
                };

                GJTBList.Add(gjt2);

                filename = "General Journal Upload Tool - LV BDO MASTER FILE";
                string path = _webHostEnvironment.WebRootPath + "/Spreadsheets/General Journal/";

                //writing to excel starts here

                using (XLWorkbook wb = new XLWorkbook())
                {
                    var ws = wb.Worksheets.Add("General Journal Template");
                    ws.Cell("A1").Value = "ACCOUNTTYPE";
                    ws.Cell("B1").Value = "ACCOUNT";
                    ws.Cell("C1").Value = "STORES";
                    ws.Cell("D1").Value = "DEPARTMENT";
                    ws.Cell("E1").Value = "BUSINESSUNIT";
                    ws.Cell("F1").Value = "PURPOSE";
                    ws.Cell("G1").Value = "SALESSEGMENT";
                    ws.Cell("H1").Value = "WORKER";
                    ws.Cell("I1").Value = "COSTCENTER";
                    ws.Cell("J1").Value = "<allocation for future segments>";
                    ws.Cell("K1").Value = "<allocation for future segments>";
                    ws.Cell("L1").Value = "TRANSDATE";
                    ws.Cell("M1").Value = "TXT";
                    ws.Cell("N1").Value = "DEBIT";
                    ws.Cell("O1").Value = "CREDIT";
                    ws.Cell("P1").Value = "OFFSETACCOUNTTYPE";
                    ws.Cell("Q1").Value = "OFFSETACCOUNT";
                    ws.Cell("R1").Value = "STORES";
                    ws.Cell("S1").Value = "DEPARTMENT";
                    ws.Cell("T1").Value = "BUSINESSUNIT";
                    ws.Cell("U1").Value = "PURPOSE";
                    ws.Cell("V1").Value = "SALESSEGMENT";
                    ws.Cell("W1").Value = "WORKER";
                    ws.Cell("X1").Value = "COSTCENTER";
                    ws.Cell("Y1").Value = "<allocation for future segments>";
                    ws.Cell("Z1").Value = "<allocation for future segments>";
                    ws.Cell("AA1").Value = "POSTINGPROFILE";
                    ws.Cell("AB1").Value = "INVOICE";
                    ws.Cell("AC1").Value = "DUEDATE";
                    ws.Cell("AD1").Value = "TAXGROUP";
                    ws.Cell("AE1").Value = "TAXITEMGROUP";
                    ws.Cell("AF1").Value = "WITHHOLDINGTAX";
                    ws.Cell("AG1").Value = "DOCUMENT";
                    ws.Cell("AH1").Value = "SALESTAXCODE";
                    ws.Cell("AI1").Value = "OFFSETTEXT";

                    ws.Cell("A2").Value = GJTBList.Select(x => new
                    {
                        x.Gjbank.AccountType,
                        x.Gjbank.AccountName,
                        x.Gjbank.Store,
                        x.Gjbank.Department,
                        x.Gjbank.BusinessUnit,
                        purpose = "",
                        salessegment = "",
                        worker2 = "",
                        costcenter = "",
                        affs = "",
                        affs2 = "",
                        postingDate = (x.PostingDate ?? DateTime.Now).ToString("MM/dd/yyyy"),
                        txt = "F TRANSFER STORE TO CSO - " + x.Gjbank.Store,
                        debit = x.Debit > 0 ? x.Debit.ToString() : "",
                        credit = x.Credit > 0 ? x.Credit.ToString() : "",
                        //x.Gjbank.AccountNo
                    });

                    int nextRow = ws.LastRowUsed().RowNumber();
                    ws.Cell("M" + nextRow).Value = "BDO FUND TRANSFER TO CSO " + (postingDate ?? DateTime.Now).ToString("MMMM yyyy");
                    ws.Rows().AdjustToContents();
                    ws.Columns().AdjustToContents();

                    wb.SaveAs(path + filename + DateTime.Now.ToString("MM-dd-yyyy hh-mm") + ".xlsx");
                }

                ResultMsg rm = new ResultMsg
                {
                    Filename = filename + DateTime.Now.ToString("MM-dd-yyyy hh-mm") + ".xlsx",
                    TotalRecord = totalRecords,
                    RowsAdded = GJTBList.Count(),
                    MissingBanks = missingBanks
                };

                return Json(rm);
            }
            catch (Exception ex)
            {
                return Json("ERROR");
            }
        }

        public ActionResult GetUnreleasedGJEntries(int month, int year, string entity)
        {
            var from = new DateTime(year, month, 1);

            var to = new DateTime(year, month, DateTime.DaysInMonth(year, month));

            var result = GeneralJournalRepository.GetUnreleasedGJEntries(entity, from, to, to);

            return Json(result);
        }

        public class PostUnreleasedGeneralJournalModel
        {
            public string description { get; set; }

            public string dataAreaId { get; set; }

            public UnreleasedGJEntry[] entries { get; set; }

            public int month { get; set; }

            public int year { get; set; }
        }

        public ActionResult PostUnreleasedGeneralJournal([FromBody] PostUnreleasedGeneralJournalModel model)
        {
            var result = new PostResult();

            var postResult = _integration.PostUnreleasedGeneralJournal(model.description, model.dataAreaId, model.entries);

            postResult = Regex.Replace(postResult, @"\""", "");

            if (postResult.Contains("Success:"))
            {
                var generalJournalNumber = Regex.Replace(postResult, @"Success: ", "");


                if (string.IsNullOrWhiteSpace(generalJournalNumber))
                {
                    result.Status = "FAILED";
                    result.Message = "There is an error processing your request.";
                }
                else
                {
                    var postingDate = new DateTime(model.year, model.month, 1);

                    GeneralJournalRepository.AddGeneralJournal(model.dataAreaId, model.description, generalJournalNumber,
                        User.Identity.Name, postingDate.ToString("MMMM"), postingDate.ToString("yyyy"));

                    result.Status = "SUCCESS";
                    result.Message = generalJournalNumber;

                    SystemLogRepository.Add(new SystemLog
                    {
                        logDate = DateTime.Now,
                        userId = User.Identity.Name,
                        type = "GENERAL JOURNAL POST",
                        description = "The user posted to general journal " + generalJournalNumber + ".",
                        status = "SUCCESS"
                    });
                }
            }
            else
            {
                result.Status = "FAILED";
                result.Message = postResult;
            }

            return Json(result);
        }

        public class PostResult
        {
            public string Status { get; set; }
            public string Message { get; set; }
        }

        public ActionResult UnreleasedPayment()
        {
            return View();
        }

        public string GenerateAccountNumber(string input)
        {
            if (String.IsNullOrWhiteSpace(input)) return "-";

            var AccountNumber = GetStringBetweenStrings(input, "<", ">");

            var isNonDigitExisting = AccountNumber.Any(c => !char.IsDigit(c));

            if (isNonDigitExisting)
            {
                return AccountNumber;
            }
            else
            {
                try
                {
                    return Int64.Parse(AccountNumber).ToString("000000000000");
                }
                catch (Exception ex)
                {
                    throw;
                }

            }
        }

        public string GetStringBetweenStrings(string text, string string1, string string2)
        {
            if (text == "") return text;

            int pFrom = text.IndexOf(string1) + string1.Length;
            int pTo = text.LastIndexOf(string2);

            if (pFrom == 0 && pTo == -1)
            {
                return text;
            }

            return text.Substring(pFrom, pTo - pFrom);
        }

        private class ResultMsg
        {
            public int TotalRecord;
            public int Overwritten;
            public int RowsAdded;
            public string Status;
            public string Filename;
            public List<String> MissingBanks;
        }
    }
}
