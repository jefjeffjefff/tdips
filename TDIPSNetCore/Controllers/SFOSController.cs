﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TDIPS;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.Business_Logic;

namespace TDIPSNetCore.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SFOSController : ControllerBase
    {
        IVendorRepository VendorRepository = new VendorRepository();
        IBankRepository BankRepository = new BankRepository();
        IOnlinePaymentRepository OnlinePaymentRepository = new OnlinePaymentRepository();
        IVoucherRepository VoucherRepository = new VoucherRepository();
        private readonly GenerateDynamicsData _integration = new GenerateDynamicsData();

        [Route("api/SFOS/PostGeneralOnlinePayment")]
        [HttpPost]
        public string PostGeneralOnlinePayment([FromBody] JObject data)
        {
            GeneralOnlinePayment[] GeneralOnlinePayments = data["GeneralOnlinePayments"].ToObject<GeneralOnlinePayment[]>();

            var vendorCodeArr = GeneralOnlinePayments.Select(x => x.vendorCode).ToArray();
            var vendors = VendorRepository.GetVendorListByVendorCodes(vendorCodeArr);
            var vendorIds = vendors.Select(x => x.vendorID).ToList();
            var paymentModes = GetPaymentModeList();
            var banks = BankRepository.GetBankListByVendorIds(vendorIds);
            var index = 1;
            var hasError = false;

            var results = new List<GeneralOnlinePaymentPostResult>();

            foreach (var payment in GeneralOnlinePayments)
            {
                if (String.IsNullOrWhiteSpace(payment.vendorCode) &&
                    String.IsNullOrWhiteSpace(payment.invoiceVouchers) &&
                    String.IsNullOrWhiteSpace(payment.entity) &&
                    String.IsNullOrWhiteSpace(payment.bank) &&
                    payment.transDate == null &&
                    payment.amount == null &&
                    String.IsNullOrWhiteSpace(payment.modeOfPayment))
                {
                    continue;
                }

                var vendor = vendors.FirstOrDefault(x => x.vendorCode.Contains(payment.vendorCode));

                payment.vendor = vendor;

                var result = new GeneralOnlinePaymentPostResult
                {
                    GeneralOnlinePayment = payment,
                    LineNumber = index,
                    Message = new List<string>()
                };

                if (String.IsNullOrWhiteSpace(payment.vendorCode) ||
                    String.IsNullOrWhiteSpace(payment.invoiceVouchers) ||
                    String.IsNullOrWhiteSpace(payment.entity) ||
                    String.IsNullOrWhiteSpace(payment.bank) ||
                    payment.transDate == null ||
                    payment.amount == null ||
                    String.IsNullOrWhiteSpace(payment.modeOfPayment) ||
                    String.IsNullOrWhiteSpace(payment.bankAccountNumber))
                {
                    result.Status = "ERROR";
                    result.Message.Add("There is an incomplete fields.");
                }

                if (
                    !String.IsNullOrWhiteSpace(payment.modeOfPayment) &&
                    !String.IsNullOrWhiteSpace(payment.entity) &&
                    !(
                    paymentModes.Any(x => x.Entity == payment.entity?.ToUpper() &&
                    x.Mode.Contains(payment.modeOfPayment?.ToUpper()))
                    )
                   )
                {
                    result.Status = "ERROR";
                    result.Message.Add($"The specified mode of payment is not allowed in the " +
                        $"{payment.entity?.ToUpper()} entity.");
                }

                if (!String.IsNullOrWhiteSpace(payment.vendorCode) && vendor == null)
                {
                    result.Status = "ERROR";
                    result.Message.Add("The vendor code " + payment.vendorCode + " is not found in the system.");
                }
                else
                {
                    if (String.IsNullOrWhiteSpace(vendor.companyEmail) &&
                  String.IsNullOrWhiteSpace(vendor.contactNumber))
                    {
                        result.Status = "ERROR";
                        result.Message.Add("The vendor has no email address nor contact number.");
                    }

                    var bank = banks.FirstOrDefault(x => x.vendorID == vendor.vendorID &&
                    x.bankAccount == payment.bankAccountNumber);

                    if (!String.IsNullOrWhiteSpace(payment.bankAccountNumber) && bank == null)
                    {
                        result.Status = "ERROR";
                        result.Message.Add("The bank account number " + payment.bankAccountNumber +
                            " is not found in the vendor's bank account numbers list.");
                    }
                    else
                    {
                        result.GeneralOnlinePayment.BankDetails = bank;
                    }
                }

                if (String.IsNullOrWhiteSpace(result.Status))
                {
                    result.Status = "SUCCESS";
                }
                else
                {
                    hasError = true;
                }

                index++;
                results.Add(result);
            }

            if (hasError == true)
            {
                string json = JsonConvert.SerializeObject(new
                {
                    results
                });

                return json;
            }
            else
            {
                OnlinePaymentRepository.SaveGeneralOnlinePayment(GeneralOnlinePayments);
                return "SUCCESS";
            }
        }


        [Route("api/sfos/postpaymentjournal/{id}")]
        [HttpGet]
        public string PostPaymentJournal(string id)
        {
            id = "SPAV-JRNL-000051632";

            var test = _integration.GetGeneralJournal(id);

            var vouchers = JsonConvert.DeserializeObject<IEnumerable<D365IntegratedPayment>>(_integration.GetGeneralJournal(id));

            foreach (var voucher in vouchers)
            {
                VoucherRepository.AddVoucherStatus(voucher.voucher, "REIMBURSED", "SFOS", voucher, false);
            };

            return "SUCCESS";
        }

        public List<PaymentMode> GetPaymentModeList()
        {
            var paymentModes = new List<PaymentMode>
            {
                new PaymentMode {
                    Entity = "SPAV",
                    Mode = new string[] { "CHECK", "CREDITCARD", "FUNDTRANSF", "ONLINE", "TELEGRAPHI" }
                },
                new PaymentMode {
                    Entity = "SPCI",
                    Mode = new string[] { "AUTOCREDIT", "CHECK", "CREDITCARD", "ONLINE" }
                },
                new PaymentMode {
                    Entity = "WBHI",
                    Mode = new string[] { "CHECK", "CREDITCARD", "FUNDTRANSF", "ONLINE", "TELEGRAPHI" }
                },
                new PaymentMode {
                    Entity = "NAF",
                    Mode = new string[] { "CHECK", "CREDITCARD", "FUNDTRANSF", "ONLINE", "TELEGRAPHI" }
                },
                new PaymentMode{
                    Entity = "SIL",
                    Mode = new string[] { "CHECK", "CREDITCARD", "FUNDTRANSF", "ONLINE", "TELEGRAPHI"}
                },
                new PaymentMode{
                    Entity = "SPRF",
                    Mode = new string[] { "CHECK", "CREDITCARD", "FUNDTRANSF", "ONLINE", "TELEGRAPHI"}
                },
                new PaymentMode{
                    Entity = "SSI",
                    Mode = new string[] { "CHECK", "CREDITCARD", "FUNDTRANSF", "ONLINE", "TELEGRAPHI" }
                }
            };

            return paymentModes;
        }

        public class PaymentMode
        {
            public string[] Mode { get; set; }
            public string Entity { get; set; }
        }

        public class GeneralOnlinePaymentPostResult
        {
            public GeneralOnlinePayment GeneralOnlinePayment { get; set; }
            public string Status { get; set; }
            public List<string> Message { get; set; }
            public int LineNumber { get; set; }
        }
    }
}
