﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LinqToExcel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.Business_Logic;

namespace TDIPSNetCore.Web.Controllers
{
    [Authorize(Policy = "")]
    public class CashflowController : Controller
    {
        ICashflowRepository CashflowRepository = new CashflowRepository();

        private readonly IWebHostEnvironment _webHostEnvironment;

        public CashflowController(IWebHostEnvironment hostingEnvironment)
        {
            _webHostEnvironment = hostingEnvironment;
        }

        // GET: Cashflow
        public ActionResult DailyCashFlow()
        {
            return View();
        }

        public ActionResult ProjectionCashflow()
        {
            return View();
        }

        public ActionResult GetDailyCashflowsList()
        {
            return Json(CashflowRepository.GetDailyCashFlowsList());
        }

        public ActionResult GetProjectionCashflowsList()
        {
            return Json(CashflowRepository.GetProjectionCashFlowsList());
        }

        public ActionResult UploadDailyCashflow(IFormFile FileUpload)
        {
            try
            {
                if (FileUpload == null)
                {
                    return Json("NULL");
                }
                else if (!(FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || FileUpload.ContentType == "application/octet-stream"))
                {
                    return Json("INVALID FILE");
                }

                var parsedFileName = Path.GetFileName(FileUpload.FileName);
                var filename = string.Format(DateTime.Now.ToString("yyyy-MM-dd HH_mm_ss_") + parsedFileName);
                string targetpath = _webHostEnvironment.WebRootPath + "~/SpreadSheets/";
                FileUpload.SaveAs(targetpath + filename);
                string pathToExcelFile = targetpath + filename;
                var connectionString = "";

                if (filename.EndsWith(".xls"))
                {
                    connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
                }
                else if (filename.EndsWith(".xlsx"))
                {
                    connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);
                }

                OleDbConnection oconn = new OleDbConnection(connectionString);

                oconn.Open();

                DataTable dbSchema = oconn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                if (dbSchema == null || dbSchema.Rows.Count < 1)
                {
                    throw new Exception("Error: Could not determine the name of the first worksheet.");
                }

                //string sheetName = dbSchema.Rows[4]["TABLE_NAME"].ToString();

                var sheets = new List<string>();

                var excelSheetNames = dbSchema.Rows;

                for (int i = 0; i < excelSheetNames.Count; i++)
                {
                    sheets.Add(excelSheetNames[i]["TABLE_NAME"].ToString());
                }

                oconn.Close();

                var excelFile = new ExcelQueryFactory(pathToExcelFile);

                var bankClassifications = new List<BankClassification>()
                {
                    new BankClassification {
                        Bank = "BDO",
                        Classification = "SHAKEYS"
                    },
                    new BankClassification {
                        Bank = "UCPB",
                        Classification = "SHAKEYS"
                    },
                    new BankClassification {
                        Bank = "UCBP",
                        Classification = "SHAKEYS"
                    },
                    new BankClassification {
                        Bank = "BPI",
                        Classification = "SHAKEYS"
                    },
                    new BankClassification {
                        Bank = "UBP",
                        Classification = "SHAKEYS"
                    },
                    new BankClassification {
                        Bank = "MBTC",
                        Classification = "SHAKEYS"
                    },
                    new BankClassification {
                        Bank = "SBTC",
                        Classification = "SHAKEYS"
                    },
                    new BankClassification {
                        Bank = "CBS",
                        Classification = "SHAKEYS"
                    },
                    new BankClassification {
                        Bank = "CB",
                        Classification = "SHAKEYS"
                    },
                    new BankClassification {
                        Bank = "AUB",
                        Classification = "SHAKEYS"
                    },
                    new BankClassification {
                        Bank = "NAF(UCPB&BDO)",
                        Classification = "NAF"
                    },
                    new BankClassification {
                        Bank = "INVESTMENT",
                        Classification = "INVESTMENT"
                    },
                    new BankClassification {
                        Bank = "SEACREST",
                        Classification = "SEACREST"
                    },
                    new BankClassification {
                        Bank = "SPCI-BPI",
                        Classification = "SPCI"
                    },
                    new BankClassification {
                        Bank = "SPCI-BDO",
                        Classification = "SPCI"
                    },
                    new BankClassification {
                        Bank = "SPCI-UNIONBANK",
                        Classification = "SPCI"
                    },
                    new BankClassification {
                        Bank = "PMMF/TD(30-35)",
                        Classification = "PMMF"
                    },
                    new BankClassification {
                        Bank = "WOWBRAND",
                        Classification = "P2"
                    },
                    new BankClassification {
                        Bank = "DSRA(OLSA)",
                        Classification = "N/A"
                    },
                };

                foreach (var sheet in sheets)
                {
                    var sheetName = sheet;

                    sheetName = sheetName.Replace("$", "").Replace("'", "");

                    var excelTable = excelFile.WorksheetNoHeader(sheetName);

                    var table = excelTable.ToList();

                    var dailyCashflowDate = table[2][0].ToString();

                    var index = 0;

                    var Entries = new List<DailyCashFlowTransactionEntry>();

                    foreach (var bankCell in table[4])
                    {
                        var cell = bankCell.ToString();

                        if (!(string.IsNullOrWhiteSpace(cell) || cell.Contains("GRAND TOTAL")
                            || cell.Contains("FOR THE MONTH")))
                        {
                            Entries.Add(new DailyCashFlowTransactionEntry
                            {
                                ExcelColumnIndex = index,
                                Bank = new DailyCashFlowBank
                                {
                                    BankClassification = bankClassifications
                                    .FirstOrDefault(x => x.Bank == cell?.Replace(" ", String.Empty))?.Classification,
                                    Bank = cell
                                },
                                Transactions = new List<DailyCashFlowBankTransaction>(),
                                Totals = new List<DailyCashFlowBankTotal>()
                            });
                        }
                        index++;

                    }

                    index = 0;

                    foreach (var beginningBalanceCell in table[6])
                    {
                        var cell = beginningBalanceCell.ToString();

                        if (cell != "BEGINNING BALANCE")
                        {
                            var entry = Entries.FirstOrDefault(x => x.ExcelColumnIndex == index);

                            if (entry != null)
                            {
                                if (cell.Any(char.IsDigit))
                                {
                                    var balance = Convert.ToDecimal(cell.Replace(",", ""));
                                    entry.Bank.BeginningBalance = balance;
                                }
                            }
                        }

                        index++;
                    }

                    var transactionTypes = new string[]
                    {
                    "ADD RECEIPTS/COLLECTIONS",
                    "LESS PAYMENTS/DISBURSEMENTS FOR OPERATIONS",
                    "CASH INFLOW (OUTFLOW) FROM INVESTING ACTIVITIES",
                    "CASH INFLOW (OUTFLOW) FROM FINANCING ACTIVITIES",
                    "ADD (DEDUCT) ADJUSTMENT",
                    };

                    var dailyCashflowBankTotalTypes = new string[]
                    {
                    "TOTAL RECEIPTS/COLLECTIONS",
                    "TOTAL PAYMENTS/DISBURSEMENTS",
                    "NET CASH INFLOW (OUTFLOW) FROM OPERATIONS",
                    "CASHFLOW FROM INVESTING ACTIVITIES",
                    "CASHFLOW FROM FINANCING ACTIVITIES",
                    "NET AVAILABLE CASH BALANCE",
                    "DIFFERENCE",
                    "CASH BALANCE PER BOOK",
                    "LESS RESTRICTED BALANCE - ADB",
                    "AVAILABLE CASH ENDING BALANCE",
                    "ENDING BALANCE",
                    "CASH BALANCE PER BANK"
                    };

                    var currentTransactionType = "";

                    for (int i = 8; i < table.Count; i++)
                    {
                        if (transactionTypes.Contains(table[i][0]))
                        {
                            currentTransactionType = table[i][0];
                            continue;
                        }

                        if (currentTransactionType == "") continue;

                        var fieldName = table[i][0].ToString();

                        for (int x = 1; x < table[i].Count(); x++)
                        {
                            var entry = Entries.FirstOrDefault(e => e.ExcelColumnIndex == x);

                            var cell = table[i][x].ToString();

                            if (entry != null)
                            {
                                var isDebit = true;
                                var amountSignDesider = 1;

                                if (cell.Any(char.IsDigit))
                                {
                                    var inputType = "MANUAL INPUT";
                                    var transactionType = currentTransactionType;

                                    if (transactionType.Contains("LESS"))
                                    {
                                        isDebit = false;
                                    }

                                    if (cell.Contains("(") && cell.Contains(")"))
                                    {
                                        amountSignDesider = -1;
                                    }

                                    cell = cell.Replace("(", "");
                                    cell = cell.Replace(")", "");
                                    cell = cell.Replace(",", "");

                                    var balance = Convert.ToDecimal(cell);

                                    if (dailyCashflowBankTotalTypes.Contains(fieldName))
                                    {
                                        entry.Totals.Add(new DailyCashFlowBankTotal
                                        {
                                            Amount = balance * amountSignDesider,
                                            Type = fieldName
                                        });
                                    }
                                    else
                                    {
                                        entry.Transactions.Add(new DailyCashFlowBankTransaction
                                        {
                                            TransactionName = fieldName,
                                            TransactionType = transactionType,
                                            Debit = (isDebit == true ? balance : 0.00m) * amountSignDesider,
                                            Credit = (isDebit == false ? balance : 0.00m) * amountSignDesider,
                                            InputType = inputType
                                        });
                                    }
                                }
                            }
                        }
                    }

                    var monthAndDay = sheetName.Split(' ');

                    if (monthAndDay.Length != 2) continue;

                    if (monthAndDay[1].Length == 1)
                    {
                        monthAndDay[1] = "0" + monthAndDay[1];
                    }

                    DateTime dailyCashDate = DateTime.ParseExact(DateTime.Now.ToString("yyyy") + " " +
                                       monthAndDay[0] + " " + monthAndDay[1], "yyyy MMMM dd",
                                       System.Globalization.CultureInfo.InvariantCulture);

                    var dailyCashflow = new DailyCashFlow
                    {
                        DateOfUpload = DateTime.Now,
                        DailyCashDateRange = dailyCashflowDate,
                        DailyCashDate = dailyCashDate,
                        UploadedBy = User.Identity.Name
                    };

                    CashflowRepository.AddDailyCashFlow(dailyCashflow, Entries);
                }

                return Json("SUCCESS");
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public ActionResult UploadProjectionCashflow(IFormFile FileUpload)
        {
            try
            {
                if (FileUpload == null)
                {
                    return Json("NULL");
                }
                else if (!(FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || FileUpload.ContentType == "application/octet-stream"))
                {
                    return Json("INVALID FILE");
                }

                var parsedFileName = Path.GetFileName(FileUpload.FileName);
                var filename = string.Format(DateTime.Now.ToString("yyyy-MM-dd HH_mm_ss_") + parsedFileName);
                string targetpath = _webHostEnvironment.WebRootPath + "/SpreadSheets/";
                FileUpload.SaveAs(targetpath + filename);
                string pathToExcelFile = targetpath + filename;
                var connectionString = "";

                if (filename.EndsWith(".xls"))
                {
                    connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
                }
                else if (filename.EndsWith(".xlsx"))
                {
                    connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);
                }

                OleDbConnection oconn = new OleDbConnection(connectionString);

                oconn.Open();

                DataTable dbSchema = oconn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                if (dbSchema == null || dbSchema.Rows.Count < 1)
                {
                    throw new Exception("Error: Could not determine the name of the first worksheet.");
                }

                //string sheetName = dbSchema.Rows[4]["TABLE_NAME"].ToString();

                var sheets = new List<string>();

                var excelSheetNames = dbSchema.Rows;

                for (int i = 0; i < excelSheetNames.Count; i++)
                {
                    sheets.Add(excelSheetNames[i]["TABLE_NAME"].ToString());
                }

                oconn.Close();

                var excelFile = new ExcelQueryFactory(pathToExcelFile);

                var projectionSegments = CashflowRepository.GetProjectionCashflowSegments();

                foreach (var sheet in sheets)
                {
                    var sheetName = sheet;

                    sheetName = sheetName.Replace("$", "").Replace("'", "");

                    var excelTable = excelFile.WorksheetNoHeader(sheetName);

                    var table = excelTable.ToList();

                    var projectionCashflowYear = table[2][0].ToString().Split(' ')[1];
                    var transactions = new List<ProjectionCashflowTransaction>();

                    for (int i = 8; i < table.Count; i++)
                    {
                        var segment = projectionSegments.FirstOrDefault(x => x.Segment == table[i][0].ToString());

                        var cell = table[i][3].ToString();

                        try
                        {
                            if (segment != null && !string.IsNullOrWhiteSpace(cell))
                            {
                                transactions.Add(new ProjectionCashflowTransaction
                                {
                                    SegmentId = segment.Id,
                                    Amount = Convert.ToDecimal(cell.Replace(",", "")) * 1000000
                                });
                            }
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }

                    var projectionCashflow = new ProjectionCashflow()
                    {
                        UploadedBy = User.Identity.Name,
                        DateOfUpload = DateTime.Now,
                        ProjectionMonth = table[6][3] + " " + projectionCashflowYear,
                        ProjectionDateRange = "1 - " + table[7][3]
                    };

                    CashflowRepository.AddProjectionCashflow(projectionCashflow, transactions);
                }

                return Json("SUCCESS");
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public class BankClassification
        {
            public string Bank { get; set; }
            public string Classification { get; set; }
        }

        public class BankCell
        {
            public string Bank { get; set; }
            public int Index { get; set; }
        }
    }
}
