﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.AzureAD.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Threading.Tasks;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.Business_Logic;
using TDIPS.Models;
using ApplicationUser = TDIPSNetCore.Areas.Identity.Data.ApplicationUser;

namespace TDIPSNetCore.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;

        IUserRepository UserRepository = new UserRepository();
        ISystemLogRepository SystemLogRepository = new SystemLogRepository();

        public AccountController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        //[Route("/Identity/Account/Login")]
        [AllowAnonymous]
        public IActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [Route("Identity/Account/AccessDenied")]
        public IActionResult AccessDenied()
        {
            return RedirectToAction("AccessDenied", "Error");
        }

        [AllowAnonymous]
        public IActionResult Index() {
            return View();
        }

        [AllowAnonymous]
        public IActionResult MicrosoftLogin([FromRoute] string scheme)
        {
            // Request a redirect to the external login provider.
            scheme ??= AzureADDefaults.AuthenticationScheme;
            var redirectUrl = Url.Content("~/Account/ValidateMicrosoftAccount");
            return Challenge(
                new AuthenticationProperties { RedirectUri = redirectUrl },
                scheme);
        }

        [AllowAnonymous]
        public async Task<ActionResult> ValidateMicrosoftAccount()
        {
            var userClaims = User.Identity as System.Security.Claims.ClaimsIdentity;

            var claims = userClaims.Claims.ToArray();

            var email = "";

            for (int i = 0; i < claims.Length; i++)
            {
                if (claims[i].ToString().Contains("preferred_username"))
                {
                    email = claims[i].ToString().Split(' ')[1];
                }
            }

            var user = await userManager.FindByEmailAsync(email);

            if (user == null)
            {
                await HttpContext.SignOutAsync(AzureADDefaults.AuthenticationScheme);

                return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                await signInManager.SignInAsync(user, new AuthenticationProperties { 
                    IsPersistent = true
                });

                SystemLog sl = new SystemLog
                {
                    userId = user.UserName,
                    type = "LOGIN"
                };

                sl.description = sl.userId + " logged in.";
                sl.status = "COMPLETED";
                SystemLogBusLogic.Logs.Add(sl);

                return RedirectToAction("Index", "Home");
            }
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);

            if (result.Succeeded)
            {
                SystemLog sl = new SystemLog();
                sl.userId = model.Email;
                sl.type = "LOGIN";
                sl.description = sl.userId + " logged in.";
                sl.status = "COMPLETED";
                SystemLogBusLogic.Logs.Add(sl);

                return RedirectToLocal(returnUrl);
            }
            else 
            {
                ModelState.AddModelError("", "Invalid login attempt.");
                return View(model);
            }
        }

        public async Task<ActionResult> LogOff()
        {
            SystemLog sl = new SystemLog
            {
                userId = User.Identity.Name,
                type = "LOGOUT",
                description = User.Identity.Name + " logged out.",
                status = "COMPLETED"
            };

            SystemLogBusLogic.Logs.Add(sl);


            await HttpContext.SignOutAsync(IdentityConstants.ApplicationScheme);
            await HttpContext.SignOutAsync(AzureADDefaults.AuthenticationScheme);

            return RedirectToAction("Login", "Account");
        }


        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }
    }
}

