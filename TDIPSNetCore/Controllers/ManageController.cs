﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TDIPS.Models;
using TDIPSNetCore.Areas.Identity.Data;

namespace TDIPSNetCore.Web.Controllers
{
	[Authorize]
    public class ManageController : Controller
    {
		private readonly UserManager<ApplicationUser> userManager;
		private readonly SignInManager<ApplicationUser> signInManager;

		public ManageController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
		{
			this.userManager = userManager;
			this.signInManager = signInManager;
		}


		[HttpPost]
		public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
		{
			var user = await userManager.FindByNameAsync(User.Identity.Name);

			var result = await userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);

			if (result.Succeeded)
			{
				return Json("SUCCESS");
			}

			return Json(result);
		}
	}
}
