﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LinqToExcel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.Business_Logic;

namespace TDIPSNetCore.Web.Controllers
{
    [Authorize(Roles = "AP")]
    public class InvoiceController : Controller
    {
        IInvoiceRepository InvoiceRepository = new InvoiceRepository();

        private readonly IWebHostEnvironment _webHostEnvironment;

        public InvoiceController(IWebHostEnvironment hostingEnvironment)
        {
            _webHostEnvironment = hostingEnvironment;
        }

        // GET: Invoice
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetInvoiceCorrectionList()
        {
            var result = InvoiceRepository.GetInvoiceCorrectionList();

            return Json(result);
        }

        public ActionResult UploadInvoiceCorrectionExcel(IFormFile FileUpload)
        {
            if (FileUpload == null)
            {
                return Json("NULL");
            }
            else if (!(FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || FileUpload.ContentType == "application/octet-stream"))
            {
                return Json("INVALID FILE");
            }

            var parsedFileName = Path.GetFileName(FileUpload.FileName);
            var filename = string.Format(DateTime.Now.ToString("yyyy-MM-dd HH_mm_ss_") + parsedFileName);
            string targetpath =  _webHostEnvironment.WebRootPath + "/SpreadSheets/";
            FileUpload.SaveAs(targetpath + filename);
            string pathToExcelFile = targetpath + filename;
            var connectionString = "";

            if (filename.EndsWith(".xls"))
            {
                connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
            }
            else if (filename.EndsWith(".xlsx"))
            {
                connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);
            }

            var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connectionString);
            var ds = new DataSet();

            adapter.Fill(ds, "ExcelTable");

            string sheetName = "Sheet1";

            var excelFile = new ExcelQueryFactory(pathToExcelFile);
            var invoices = from a in excelFile.Worksheet<InvoiceCorrection>(sheetName) select a;

            var saveResult = new ResultMessage();

            foreach (var invoice in invoices)
            {
                if (String.IsNullOrWhiteSpace(invoice.NewInvoiceNo) || String.IsNullOrWhiteSpace(invoice.OldInvoiceNo))
                {

                }
                else
                {
                    invoice.ModifiedBy = User.Identity.Name;
                    string result = InvoiceRepository.AddUpdateInvoiceNumCorrection(invoice);

                    if (result == "ADDED")
                    {
                        saveResult.RowsAdded++;
                    }
                    else
                    {
                        saveResult.RowsUpdated++;
                    }
                }
            }

            saveResult.Status = "SUCCESS";

            return Json(saveResult);
        }

        public ActionResult DeleteInvoiceCorrection(int invoiceId)
        {
            var result = InvoiceRepository.DeleteInvoiceCorrection(invoiceId);

            return Json(result);
        }

        public class ResultMessage
        {
            public string Status;
            public int RowsAdded;
            public int RowsUpdated;
        }
    }
}
