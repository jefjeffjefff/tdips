﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Hangfire;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.Business_Logic;

namespace TDIPSNetCore.Web.Controllers
{
    [Authorize(Policy = "DenyNonTreasury")]
    public class ReportController : Controller
    {
        ISystemLogRepository SystemLogRepository = new SystemLogRepository();
        IVoucherRepository VoucherRepository = new VoucherRepository();
        IMailRepository MailRepository = new MailRepository();
        IEODRepository EODRepository = new EODRepository();
        IEmployeeRepository EmployeeRepository = new EmployeeRepository();
        GlobalClass GlobalClass = new GlobalClass();

        private readonly IWebHostEnvironment _webHostEnvironment;

        public ReportController(IWebHostEnvironment hostingEnvironment)
        {
            _webHostEnvironment = hostingEnvironment;
        }

        // GET: Report
        public ActionResult Aging()
        {
            return View();
        }

        public ActionResult CollectedCheckReport()
        {
            return View();
        }

        public ActionResult DownloadExcel(string fileName)
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(_webHostEnvironment.WebRootPath +
                "/SpreadSheets/Reports/" + fileName + ".xlsx");
            string fileName2 = fileName + ".xlsx";

            BackgroundJob.Schedule(
            () => DeleteExcelFile(_webHostEnvironment.WebRootPath + "~/SpreadSheets/Reports/" + fileName + ".xlsx"),
            TimeSpan.FromMinutes(5));

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName2);
        }

        public void DeleteExcelFile(string fullpath)
        {
            if (System.IO.File.Exists(fullpath))
            {
                System.IO.File.Delete(fullpath);
            }
        }

        public string GetRowsFromRange(string range)
        {
            var result = range.Split(':');
            return result[0].Substring(1, result[0].Length - 1) + ":" + result[1].Substring(1, result[1].Length - 1);
        }

        public ActionResult WriteAgingToExcel(DateTime? asofDate = null)
        {
            DateTime asOfDate = asofDate ?? DateTime.Now;

            List<Aging> a = VoucherRepository.GetAging(asOfDate);

            try
            {
                //Name of File 
                string fileName = asOfDate.ToString("dd MMM yyyy") + " Aging Report";
                string fullPath = _webHostEnvironment.WebRootPath + "/SpreadSheets/Reports/";

                System.IO.DirectoryInfo di = new DirectoryInfo(fullPath);

                using (XLWorkbook wb = new XLWorkbook())
                {
                    a = a.OrderBy(x => x.Entity).ToList();
                    string[] entity = a.Select(x => x.Entity + " - " + x.Bank).Distinct().ToArray();

                    for (int i = 0; i < entity.Length; i++)
                    {
                        var currentEntity = a.Where(x => (x.Entity + " - " + x.Bank) == entity[i]).ToList();

                        var ws = wb.Worksheets.Add(entity[i]);
                        ws.Cell("A1").Value = "SHAKEYS PIZZA ASIA VENTURES INC";
                        ws.Cell("A2").Value = "AGING OF UNRELEASED CHECKS - " + entity[i];
                        ws.Cell("A3").Value = "AS OF " + asOfDate.ToString("MMM dd, yyyy");
                        ws.Cell("A1").Style.Font.FontSize = 13;
                        ws.Cell("A2").Style.Font.FontSize = 13;
                        ws.Cell("A3").Style.Font.FontSize = 13;
                        ws.Cell("A1").Style.Font.Bold = true;
                        ws.Cell("A2").Style.Font.Bold = true;
                        ws.Cell("A3").Style.Font.Bold = true;

                        ws.Cell("K7").SetDataType(XLDataType.Text);

                        ws.Cell("A7").Value = "CHECK";
                        ws.Cell("B7").Value = "PAYEE";
                        ws.Cell("C7").Value = "DATE RECEIVED";
                        ws.Cell("D7").Value = "NO OF DAYS";
                        ws.Cell("E7").Value = "TERM";
                        ws.Cell("F7").Value = "DUE DATE";
                        ws.Cell("G7").Value = "OVER DUE";
                        ws.Cell("H7").Value = "CHECK DATE";
                        ws.Cell("I7").Value = "AMOUNT";
                        ws.Cell("J7").Value = "CURRENT";
                        ws.Cell("K7").Value = "1 to 30";
                        ws.Cell("L7").Value = "31 to 60";
                        ws.Cell("M7").Value = "61 to 90";
                        ws.Cell("N7").Value = "91 to 120";
                        ws.Cell("O7").Value = "OVER 120";
                        ws.Cell("P7").Value = "AMOUNT";
                        ws.Cell("R7").Value = "REMARKS";

                        ws.Row(7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        ws.Row(7).Style.Font.Bold = true;
                        ws.Row(7).Style.Font.FontSize = 12;

                        ws.Cell(9, 1).Value = currentEntity.Select(x => new {
                            x.CheckNumber,
                            x.Payee,
                            x.DateReceived,
                            noOfDays = x.NoOfDays == 0 ? "" : x.NoOfDays.ToString(),
                            x.Term,
                            dueDate = x.DueDate == null ? "" : (x.DueDate ?? DateTime.Now).ToString("MM/dd/yyyy"),
                            overDue = x.OverDue == null ? "" : x.OverDue.ToString(),
                            checkDate = x.CheckDate == null ? "" : (x.CheckDate ?? DateTime.Now).ToString("MM/dd/yyyy"),
                            x.Amount,
                            current = x.Current == null ? "" : string.Format("{0:n}", Math.Round((x.Current ?? 0), 2)),
                            term1 = x.Term1 == null ? "" : string.Format("{0:n}", Math.Round((x.Term1 ?? 0), 2)),
                            term2 = x.Term2 == null ? "" : string.Format("{0:n}", Math.Round((x.Term2 ?? 0), 2)),
                            term3 = x.Term3 == null ? "" : string.Format("{0:n}", Math.Round((x.Term3 ?? 0), 2)),
                            term4 = x.Term4 == null ? "" : string.Format("{0:n}", Math.Round((x.Term4 ?? 0), 2)),
                            term5 = x.Term5 == null ? "" : string.Format("{0:n}", Math.Round((x.Term5 ?? 0), 2)),
                            amount2 = string.Format("{0:n}", Math.Round((x.Amount2 ?? 0), 2))
                        }).AsEnumerable();

                        ws.Columns("A", "Z").AdjustToContents();
                        ws.Columns("A", "Z").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        ws.Column(17).Hide();
                    }

                    wb.SaveAs(fullPath + fileName + ".xlsx");

                    using (MemoryStream stream = new MemoryStream())
                    {
                        wb.SaveAs(stream);
                        //Return xlsx Excel File  

                        SystemLog sl = new SystemLog
                        {
                            logDate = DateTime.Now,
                            userId = User.Identity.Name,
                            type = "REPORT",
                            description = "Requested an aging report (excel)",
                            status = "SUCCESS"
                        };

                        SystemLogRepository.Add(sl);

                        return Json(fileName);
                    }
                }

            }
            catch (Exception)
            {
                return Json("ERROR");
            }
        }

        public void InsertTableBorder(IXLWorksheet ws, string range, string borderColor = "#4B4B4B")
        {
            ws.Range(range).Style.Border.BottomBorder =
                XLBorderStyleValues.Thin;
            ws.Range(range).Style.Border.BottomBorderColor =
                XLColor.FromHtml(borderColor);

            ws.Range(range).Style.Border.RightBorder =
                XLBorderStyleValues.Thin;
            ws.Range(range).Style.Border.RightBorderColor =
                XLColor.FromHtml(borderColor);

            ws.Range(range).Style.Border.LeftBorder =
                XLBorderStyleValues.Thin;
            ws.Range(range).Style.Border.LeftBorderColor =
                XLColor.FromHtml(borderColor);

            ws.Range(range).Style.Border.TopBorder =
                XLBorderStyleValues.Thin;
            ws.Range(range).Style.Border.TopBorderColor =
                XLColor.FromHtml(borderColor);

            ws.Range(range).Style.Border.OutsideBorder =
                XLBorderStyleValues.Thin;
            ws.Range(range).Style.Border.OutsideBorderColor =
                XLColor.FromHtml(borderColor);
        }

        public ActionResult WriteReleasedToExcel(DateTime? dateFrom, DateTime? dateTo, bool isSendToMail = false)
        {
            if (isSendToMail == true)
            {
                BackgroundJob.Enqueue(() => SendEODMail(User.Identity.Name, dateFrom, dateTo, isSendToMail));
                return Json("SUCCESS");
            }
            else
            {

                return Json(GlobalClass.WriteReleasedToExcel(User.Identity.Name, dateFrom, dateTo, isSendToMail));
            }
        }

        public void SendEODMail(string user, DateTime? dateFrom, DateTime? dateTo, bool isSendToMail = false)
        {
            GlobalClass.
            BackgroundD65Integration(DateTime.Today.AddDays(-1),
            DateTime.Today.AddDays(1));

            GlobalClass.WriteReleasedToExcel(user, dateFrom, dateTo, isSendToMail);
        }
    }
}
