﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hangfire;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.Business_Logic;

namespace TDIPSNetCore.Web.Controllers
{
    [Authorize(Policy = "DenyNonTreasury")]
    public class MailController : Controller
    {
        IMailRepository MailRepository = new MailRepository();
        IMailComposerRepository MailComposerRepository = new MailComposerRepository();
        IVendorRepository VendorRepository = new VendorRepository();

        private readonly IWebHostEnvironment _webHostEnvironment;

        public MailController(IWebHostEnvironment hostingEnvironment)
        {
            _webHostEnvironment = hostingEnvironment;
        }

        public ActionResult GetVendorToEmailList()
        {
            return Json(VendorRepository.VendorToEmailList());
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult SaveAttachments()
        {
            if (Request.Form.Files.Count > 0)
            {
                var files = Request.Form.Files;
                var totalFiles = files.Count;
                var path = _webHostEnvironment.WebRootPath + "/Attachments/";

                for (int i = 0; i < totalFiles; i++)
                {
                    if (files[i] != null && files[i].Length > 0)
                    {
                        files[i].SaveAs(_webHostEnvironment.WebRootPath + $"~/Attachments/{files[i].FileName}");
                    }
                }
            }

            return Json("SUCCESS");
        }

        // GET: Mail
        public ActionResult SpecialAnnouncement()
        {
            return View();
        }

        public ActionResult GetDrafts()
        {
            var drafts = MailComposerRepository.GetDrafts();

            return Json(drafts);
        }

        public ActionResult DeleteDraft(int draftId)
        {
            var result = MailComposerRepository.DeleteDraft(draftId);

            return Json(result);
        }

        public ActionResult LoadDraft(int draftId)
        {
            return Json(MailComposerRepository.LoadDraft(draftId));
        }

        public class MessagePostModel
        {
            public Message message { get; set; }
            public int[] vendorId { get; set; }
            public bool _includeCollectors { get; set; }
        }

        public ActionResult SendMessage([FromBody] MessagePostModel model)
        {
            if (model.message.Attachment != null)
            {
                for (int i = 0; i < model.message.Attachment.Length; i++)
                {
                    model.message.Attachment[i] = _webHostEnvironment.WebRootPath + "/Attachments/" + model.message.Attachment[i];
                }
            }

            var jobId = BackgroundJob.Enqueue(() =>
           MailComposerRepository.SendMessage(model.message, model.vendorId, model._includeCollectors));

            return Json("SUCCESS");
        }

        [Authorize]
        [HttpPost]
        public ActionResult SendSpecialAnnouncement()
        {
            var attachments = new string[2];

            attachments[0] = _webHostEnvironment.WebRootPath + "/Docs/Template-Direct Delivery.xlsx";
            attachments[1] = _webHostEnvironment.WebRootPath + "/Docs/Template-Warehouse.xlsx";

            BackgroundJob.Enqueue(() => MailRepository.SpecialAnnouncement(attachments));

            return Json("SUCCESS");
        }

        public ActionResult SaveComposedMailToDraft([FromBody] Message draft)
        {
            var result = MailComposerRepository.SaveToDraft(draft);

            return Json(result);
        }
    }
}
