﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using TDIPS.Core.Interfaces;
using TDIPS.Infrastructure.Business_Logic;

namespace TDIPSNetCore.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly IWebHostEnvironment _webHostEnvironment;

        public HomeController(IWebHostEnvironment hostingEnvironment)
        {
            _webHostEnvironment = hostingEnvironment;
        }

        IDashboardRepository DashboardRepository = new DashboardRepository();

        public IActionResult Index()
        {
            if (User.IsInRole("AP"))
            {
                return RedirectToAction("Index", "Invoice");
            }

            return View();
        }

        public JsonResult VoucherStatus()
        {
            return Json(DashboardRepository.VoucherStatus());
        }

        public JsonResult ReleasedVouchers()
        {
            return Json(DashboardRepository.ReleasedVouchers());
        }

        public ActionResult GetBatchMailDates()
        {
            return Json(DashboardRepository.GetBatchMailDates());
        }

        public ActionResult QueueingProgress()
        {
            return Json(DashboardRepository.GetQueueProgressAsToday());
        }

        public ActionResult GetMailLog(DateTime date)
        {
            return Json(DashboardRepository.GetProcessedMails(date));
        }

        public ActionResult DownloadTemplate(string filename)
        {
            string webRootPath = _webHostEnvironment.WebRootPath; //with wwwroot
            string contentRootPath = _webHostEnvironment.ContentRootPath;

            string path = webRootPath + "/Upload_Templates/" + filename + ".xlsx";

            byte[] fileBytes = System.IO.File.ReadAllBytes(path);
            string fileName2 = filename + ".xlsx";

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName2);
        }
    }
}

public static class UrlHelperExtensions
{
    public static string ContentVersioned(this IUrlHelper self, string contentPath)
    {
        string versionedContentPath = contentPath + "?v=" + Assembly.GetAssembly(typeof(UrlHelperExtensions)).GetName().Version.ToString();
        return self.Content(versionedContentPath);
    }
}

public static class IFormFileExtension
{
    public static void SaveAs(this IFormFile file, string filepath)
    {
        using var fileStream = new FileStream(filepath, FileMode.Create);
        file.CopyTo(fileStream);
    }
}