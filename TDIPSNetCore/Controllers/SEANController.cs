﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Hangfire;
using LinqToExcel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TDIPS;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Core.Models.SS_Entities;
using TDIPS.Core.Models.SS_HELPERS;
using TDIPS.Infrastructure.Business_Logic;

namespace TDIPSNetCore.Web.Controllers
{
    [Authorize(Policy = "DenyNonTreasury")]
    public class SEANController : Controller
    {
        private readonly GenerateDynamicsData _integration = new GenerateDynamicsData();
        IVendorRepository VendorRepository = new VendorRepository();
        IVoucherRepository VoucherRepository = new VoucherRepository();
        IPaymentRepository PaymentRepository = new PaymentRepository();
        IBankRepository BankRepository = new BankRepository();
        ICollectorRepository CollectorRepository = new CollectorRepository();
        INotificationRepository NotificationRepository = new NotificationRepository();
        IUserRepository UserRepository = new UserRepository();
        IMailRepository MailRepository = new MailRepository();
        IEmployeeRepository EmployeeRepository = new EmployeeRepository();
        ISystemLogRepository SystemLogRepository = new SystemLogRepository();
        IEODRepository EODRepository = new EODRepository();

        private readonly IWebHostEnvironment _webHostEnvironment;

        public SEANController(IWebHostEnvironment hostingEnvironment)
        {
            _webHostEnvironment = hostingEnvironment;
        }

        //[CustomAuthorize("TREASURY MANAGER", "TREASURY LEAD")]
        [Authorize(Roles = "TREASURY MANAGER,TREASURY LEAD")]
        public ActionResult ApprovalRequestsIndex()
        {
            if (User.IsInRole("TREASURY LEAD") && UserRepository.IsTreasuryManagerOnLeave() == "FALSE")
            {
                return RedirectToAction("AccessDenied", "Error", new { page = "ApprovalIndex" });
            }

            return View();
        }

        public ActionResult DeleteEmployee(int employeeId)
        {
            var result = EmployeeRepository.DeleteEmployee(employeeId);

            return Json(result);
        }

        //[CustomAuthorize("!AUDIT")]

        public class VendorPostModel
        {
            public Vendor vendor { get; set; }
            public Collector[] collector { get; set; }
            public Bank[] bank { get; set; }
        }

        [Authorize(Policy = "DenyAudit")]
        public ActionResult AddOrUpdateVendor([FromBody] VendorPostModel model)
        {
            int vendorID = VendorRepository.AddOrUpdate(model.vendor, User.Identity.Name);

            //check if collector[] is null before adding
            if (model.collector != null)
            {
                CollectorRepository.AddOrUpdate(model.collector, vendorID);
            }

            //check if bank[] is null before adding
            if (model.bank != null)
            {
                BankRepository.AddOrUpdate(model.bank, vendorID);
            }

            return Json(true);
        }

        public class VoucherIds
        {
            public string[] voucherIds { get; set; }
        }

        public ActionResult ForceUncollectedVouchers([FromBody] VoucherIds voucherIds)
        {
            var result = VoucherRepository.ForceUncollectedVouchers(voucherIds.voucherIds);

            return Json(result);
        }

        public ActionResult AcceptVerificationRequest(int requestBatchId, string note)
        {
            string[] usertype = new string[1];
            usertype[0] = "TREASURY MANAGER";
            string jobId2 = BackgroundJob.Enqueue(() => MailRepository.VoucherNotification("for approval", usertype));

            int batchId = VoucherRepository.SaveVoucherStatusBatch(User.Identity.Name, note, "FOR APPROVAL");
            string result = VoucherRepository.AcceptVerificationRequest(requestBatchId, batchId, User.Identity.Name);

            if (result == "SUCCESS")
            {
                VoucherRepository.UpdateVoucherStatusBatch(requestBatchId);
            }

            return Json(result);
        }

        public void SendForReleasingCheckMail(int requestBatchId, string[] arrVoucherIdAndSeanCode)
        {
            GlobalClass GlobalClass = new GlobalClass();

            DateTime? releasingDate = VoucherRepository.GetLatestReleasingDateFromBatch(requestBatchId);

            VoucherRepository.UpdateVoucherStatusForRelease(requestBatchId, arrVoucherIdAndSeanCode);

            if (releasingDate != null)
            {
                var result = GlobalClass.WriteForReleasingToExcel("SYSTEM", releasingDate, true);
            }
        }

        public ActionResult AcceptApprovalRequest(int requestBatchId, string note)
        {
            int batchId = VoucherRepository.SaveVoucherStatusBatch(User.Identity.Name, note, "FOR RELEASING");
            string[] arrVoucherIdAndSeanCode = VoucherRepository.AcceptApprovalRequest(requestBatchId, batchId, User.Identity.Name);

            string jobId = BackgroundJob.Enqueue(() => SendForReleasingCheckMail(requestBatchId, arrVoucherIdAndSeanCode));

            List<Mail> emailArray = MailRepository.GetContactFromCollectorsAndVendors(arrVoucherIdAndSeanCode);

            string[] usertype = new string[1];
            usertype[0] = "TREASURY STAFF";


            string jobId1 = BackgroundJob.Enqueue(() => SendApprovalMail(emailArray, "APPROVAL", arrVoucherIdAndSeanCode));
            string jobId2 = BackgroundJob.Enqueue(() => MailRepository.VoucherNotification("for releasing", usertype));


            if (arrVoucherIdAndSeanCode == null)
            {
                return Json("FAILED");

            }
            else
            {
                VoucherRepository.UpdateVoucherStatusBatch(requestBatchId);
                return Json("SUCCESS");
            }
        }

        [AllowAnonymous]
        public void SendApprovalMail(List<Mail> emailArray, string type, string[] voucherId)
        {
            for (var x = 0; x < voucherId.Length; x++)
            {
                if (voucherId[x].Contains("?"))
                {
                    var vId = voucherId[x].Split('?');
                    voucherId[x] = vId[0];
                }
            }

            var dynamics365Data = JsonConvert.DeserializeObject<List<Invoice>>(_integration.GenerateInvoiceDetails(voucherId));

            MailRepository.SendMail(emailArray, type, dynamics365Data);
        }

        public static bool containsPairWithSum(int[] a, int x)
        {
            Array.Sort(a);

            var i = 0;
            var j = a.Length - 1;

            while (i < j)
            {
                int sum = a[i] + a[j];

                if (sum < x) {
                    i++;
                } else if (sum > x)
                {
                    j--;
                } else {
                    return true;
                }
            }

            return false;
        }

        //public ActionResult SendEODMail()
        //{
        //    GlobalClass GC = new GlobalClass();

        //    GC.BackgroundD65Integration(DateTime.Today, DateTime.Today.AddDays(1));

        //    string result = MailRepository.SendEODMail(User.Identity.Name);

        //    if (result == "SUCCESS") {
        //        EODRepository.Add(User.Identity.Name);
        //    }

        //    return Json(result);
        //}

        public ActionResult IsEODValidationSent()
        {
            return Json(EODRepository.IsEODValidationSent(DateTime.Today));
        }

        public ActionResult CollectedChecks()
        {
            return View();
        }

        public int DeclinedVerificationRequestNotification()
        {
            return VoucherRepository.DeclinedVerificationRequestNotification();
        }

        public ActionResult DeclineRequest(int declinedBatchId, string note)
        {
            int batchId = VoucherRepository.SaveVoucherStatusBatch(User.Identity.Name, note, "UNRELEASED", "DECLINED");
            string result = VoucherRepository.DeclineRequest(declinedBatchId, batchId, User.Identity.Name);

            string[] usertype = new string[1];
            usertype[0] = "TREASURY STAFF";
            string jobId2 = BackgroundJob.Enqueue(() => MailRepository.VoucherNotification("declined", usertype));

            if (result == "SUCCESS")
            {
                VoucherRepository.UpdateVoucherStatusBatch(declinedBatchId);
            }

            return Json(result);
        }

        //[CustomAuthorize("TREASURY STAFF")]
        [Authorize(Roles = "TREASURY STAFF")]
        public ActionResult DeclinedVerificationRequestsIndex()
        {
            return View();
        }

        public ActionResult ForReleaseIndex()
        {
            return View();
        }

        public object GetVoucherIDListFromBatch(int batchId, string status = "")
        {
            return Json(VoucherRepository.GetVoucherIDListFromBatch(batchId, status));
        }

        public ActionResult GetCollectedVoucherList(DateTime dateFrom, DateTime dateTo)
        {
            var resultData = VoucherRepository.GetCollectedVoucherList(dateFrom, dateTo);

            var result = new ContentResult
            {
                Content = JsonConvert.SerializeObject(resultData, Formatting.Indented),
                ContentType = "application/json"
            };

            return result;
        }

        public JsonResult GetNotification()
        {
            return Json(NotificationRepository.Notifications());
        }

        public JsonResult ImportPaymentExcel(IFormFile FileUpload)
        {
            if (FileUpload == null)
            {
                return Json("NULL");
            }
            else if (!(FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || FileUpload.ContentType == "application/octet-stream"))
            {
                return Json("INVALID FILE");
            }

            var parsedFileName = Path.GetFileName(FileUpload.FileName);
            var filename = string.Format(DateTime.Now.ToString("yyyy-MM-dd HH_mm_ss_") + parsedFileName);
            string targetpath = _webHostEnvironment.WebRootPath + "/SpreadSheets/";
            FileUpload.SaveAs(targetpath + filename);
            string pathToExcelFile = targetpath + filename;
            var connectionString = "";

            if (filename.EndsWith(".xls"))
            {
                connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
            }
            else if (filename.EndsWith(".xlsx"))
            {
                connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);
            }

            var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connectionString);
            var ds = new DataSet();

            adapter.Fill(ds, "ExcelTable");

            string sheetName = "Sheet1";

            List<string> duplicatedVoucher = new List<string>();
            int counter = 0;

            var excelFile = new ExcelQueryFactory(pathToExcelFile);
            var payments = from a in excelFile.Worksheet<Payment>(sheetName) select a;

            foreach (var p in payments)
            {
                //track duplicated voucher errors;
                if (p.amount > 0 && p.voucherId != null)
                {
                    string result = PaymentRepository.Add(p);
                    if (result != "SUCCESS")
                    {
                        duplicatedVoucher.Add(result);
                    }
                    else
                    {
                        counter++;
                    }
                }
            }

            //deleting excel file from folder  
            if ((System.IO.File.Exists(pathToExcelFile)))
            {
                System.IO.File.Delete(pathToExcelFile);
            }

            if (duplicatedVoucher.Count > 0)
            {
                return Json(duplicatedVoucher);
            }
            else if (counter == 0)
            {
                return Json("EMPTY");
            }
            else
            {
                SystemLog sl = new SystemLog
                {
                    userId = User.Identity.Name,
                    type = "PAYMENTS EXCEL UPLOAD",
                    description = "Uploaded a payments excel.",
                    status = "COMPLETED"
                };
                SystemLogBusLogic.Logs.Add(sl);

                return Json("SUCCESS");
            }
        }

        //[CustomAuthorize("!AUDIT")]
        public JsonResult InsertCheckDetailsExcel(IFormFile FileUpload)
        {
            if (FileUpload == null)
            {
                return Json("NULL");
            }
            else if (!(FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || FileUpload.ContentType == "application/octet-stream"))
            {
                return Json("INVALID FILE");
            }

            var parsedFileName = Path.GetFileName(FileUpload.FileName);
            var filename = string.Format(DateTime.Now.ToString("yyyy-MM-dd HH_mm_ss_") + parsedFileName);
            string targetpath = _webHostEnvironment.WebRootPath + "/SpreadSheets/";
            FileUpload.SaveAs(targetpath + filename);
            string pathToExcelFile = targetpath + filename;
            var connectionString = "";

            if (filename.EndsWith(".xls"))
            {
                connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
            }
            else if (filename.EndsWith(".xlsx"))
            {
                connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);
            }

            var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connectionString);
            var ds = new DataSet();

            adapter.Fill(ds, "ExcelTable");

            string sheetName = "Sheet1";

            List<string> voucherNotFound = new List<string>();
            int counter = 0;

            var excelFile = new ExcelQueryFactory(pathToExcelFile);
            var payments = from a in excelFile.Worksheet<Payment>(sheetName) select a;

            var total = payments.Where(x => x.chequeNumber != "").ToList().Count();

            foreach (var p in payments)
            {
                if (p.chequeDate != null && p.chequeNumber != "")
                {
                    string result = PaymentRepository.InsertCheckDetails(p);
                    if (result != "SUCCESS")
                    {
                        voucherNotFound.Add(result);
                    }
                    else
                    {
                        counter++;
                    }
                }
            }

            //deleting excel file from folder  
            if ((System.IO.File.Exists(pathToExcelFile)))
            {
                System.IO.File.Delete(pathToExcelFile);
            }

            SystemLog sl = new SystemLog
            {
                userId = User.Identity.Name,
                type = "CHECK EXCEL UPLOAD",
                description = "Uploaded a check details excel.",
                status = "COMPLETED"
            };
            SystemLogBusLogic.Logs.Add(sl);

            if (voucherNotFound.Count > 0)
            {
                return Json(voucherNotFound);
            }
            else if (counter == 0)
            {
                return Json("EMPTY");
            }
            else if (counter != total)
            {
                return Json("INCOMPLETE" + counter.ToString() + "/" + total.ToString());
            }
            else
            {
                return Json("SUCCESS");
            }
        }

        //[CustomAuthorize("!AUDIT")]
        public ActionResult IntegrateVendorsD365()
        {
            var test = _integration.GenerateVendors();

            var dynamics365Data = JsonConvert.DeserializeObject<IEnumerable<D365Vendor>>(_integration.GenerateVendors());

            SystemLog sl = new SystemLog
            {
                userId = User.Identity.Name,
                type = "INTEGRATE VENDORS",
                description = "Integrated vendors list with D365.",
                status = "COMPLETED"
            };
            SystemLogBusLogic.Logs.Add(sl);

            return Json(VendorRepository.AddOrUpdate(dynamics365Data));
        }

        //[CustomAuthorize("!AUDIT")]
        public ActionResult IntegratePaymentD365(DateTime dateFrom, DateTime dateTo)
        {
            var forReleasingPayments = VoucherRepository.GetForReleasingVoucherList();
            var cancelledVouchersArr = new List<string>();
            //these two are used to catch cancelled for releasing vouchers and mail to the treasury

            try
            {
                //var test = _integration.GeneratePayments(dateFrom, dateTo);
                var dynamics365Data = JsonConvert.DeserializeObject<IEnumerable<D365IntegratedPayment>>(_integration.GeneratePayments(dateFrom, dateTo));
                int counter = 0;

                foreach (var d in dynamics365Data)
                {
                    if (d.chequeStatus == "Paid" || d.chequeStatus == "PAID" /*|| d.paymentMode.ToUpper() == "TELEGRAPHI"*/)
                    {
                        string result = PaymentRepository.Add(d);

                        if (result == "SUCCESS")
                        {
                            counter++;
                        }
                    }
                    else
                    {
                        if (d.chequeStatus.ToUpper() == "CANCELED" &&
                            forReleasingPayments.Where(x => x.voucherId == d.voucherId).Count() > 0)
                        {
                            cancelledVouchersArr.Add(d.voucherId);
                        }
                        else if (d.chequeStatus.ToUpper() == "CREATED")
                        {
                            d.chequeStatus = "Online Released";
                        }

                        string result = VoucherRepository.AddVoucherStatus(d.voucherId, d.chequeStatus.ToUpper(), User.Identity.Name, d);

                        string jobId = BackgroundJob.Enqueue(() => VoucherRepository.CancelVoucherStatus(d.voucherId));

                        if (result == "SUCCESS")
                        {
                            counter++;
                        }
                    }
                }

                if (cancelledVouchersArr.Count > 0)
                {
                    //send canceled checks mail
                    MailRepository.SendCanceledVoucherNoticeMail(forReleasingPayments.Where(x =>
                    cancelledVouchersArr.Contains(x.voucherId)).ToList());
                }

                SystemLog sl = new SystemLog
                {
                    userId = User.Identity.Name,
                    type = "INTEGRATE PAYMENTS",
                    description = "Integrated payments details with D365 (" + dateFrom.ToString("dd MMM yyyy") + " - " +
                    dateTo.ToString("dd MMM yyyy") + ")",
                    status = "COMPLETED"
                };

                SystemLogBusLogic.Logs.Add(sl);

                return Json(counter);

            }
            catch (Exception ex)
            {
                return Json("ERROR");
            }
        }

        public JsonResult IsTreasuryManagerOnLeave()
        {
            return Json(UserRepository.IsTreasuryManagerOnLeave());
        }

        public JsonResult IsTreasuryLeadOnLeave()
        {
            return Json(UserRepository.IsTreasuryLeadOnLeave());
        }

        public JsonResult GetBankList(int vendorID)
        {
            return Json(BankRepository.List(vendorID));
        }

        public JsonResult GetCollectorList(int vendorID)
        {
            return Json(CollectorRepository.List(vendorID));
        }

        public ActionResult GetPaymentsList(string[] status = null, DateTime? statusDate = null, bool filterCheckDate = false)
        {
            var resultData = PaymentRepository.List(status, statusDate, filterCheckDate);
            var result = new ContentResult
            {
                Content = JsonConvert.SerializeObject(resultData, Formatting.Indented),
                ContentType = "application/json"
            };
            return result;
        }

        #region serverside
        [HttpPost]
        public ActionResult GetViewData(
        int draw,
        int start,
        int length,
        List<Dictionary<string, string>> columns,
        List<Dictionary<string, string>> order,
        string vendorcode,
        string companyName,
        string bank,
        string entity,
        string voucherId,
        decimal? amount,
        string payment,
        string check,
        string date,
        string status
        )
        {
            var dataTableData = new DataTableData<Payment>();
            var sortColumn = columns[ConvertHelper.ToInteger(order[0]["column"])]["data"];
            var filter = new DataFilter
            {
                Draw = draw,
                Start = start,
                Length = length,
                SortColumn = sortColumn == null ? "Id" : sortColumn.Replace(" ", string.Empty),
                SortDirection = order[0]["dir"],
                ColumnFilters = new List<ColumnFilter>()
            };

            dataTableData.dataList = PaymentRepository.SSList(
                                            filter, vendorcode, companyName, bank, entity, voucherId, amount, payment, check, ConvertHelper.ToDate(date), status);

            dataTableData.draw = filter.Draw;
            dataTableData.recordsTotal = filter.TotalRecordCount;
            dataTableData.recordsFiltered = filter.FilteredRecordCount;

            return this.Json(dataTableData);
        }
        #endregion

        public JsonResult GetVendorList(
            int draw,
            int start,
            int length,
            List<Dictionary<string, string>> columns,
            List<Dictionary<string, string>> order,
            Vendor v = null
            )
        {
            var dataTableData = new DataTableData<Vendor>();
            var sortColumn = columns[ConvertHelper.ToInteger(order[0]["column"])]["data"];
            var filter = new DataFilter
            {
                Draw = draw,
                Start = start,
                Length = length,
                SortColumn = sortColumn == null ? "Id" : sortColumn.Replace(" ", string.Empty),
                SortDirection = order[0]["dir"],
                ColumnFilters = new List<ColumnFilter>()
            };

            dataTableData.dataList = VendorRepository.List(filter, v);
            dataTableData.draw = filter.Draw;
            dataTableData.recordsTotal = filter.TotalRecordCount;
            dataTableData.recordsFiltered = filter.FilteredRecordCount;

            return Json(dataTableData);
        }

        public class VoucherBatchParamsModel
        {
            public string[] status { get; set; }
            public string[] batchStatus { get; set; }
            public string requestUse { get; set; }
        }

        public JsonResult GetVoucherBatchList(string[] status, string[] batchStatus, string requestUse)
        {
            if (requestUse == "TO APPROVE" && UserRepository.IsTreasuryLeadOnLeave() == "TRUE")
            {
                status = new string[2];
                status[0] = "FOR VERIFICATION";
                status[1] = "FOR APPROVAL";
            }

            var test = VoucherRepository.GetVoucherStatusBatchList(status, batchStatus);

            return Json(VoucherRepository.GetVoucherStatusBatchList(status, batchStatus));
        }

        public JsonResult GetVoucherList(int batchId = 0, string[] status = null, string vendorCode = "")
        {
            return Json(VoucherRepository.GetVoucherList(batchId, status, vendorCode));
        }

        public JsonResult GetVoucherListByBatchId(int batchId)
        {
            return Json(VoucherRepository.GetVoucherListByBatchId(batchId));
        }

        public string OpenDeclinedVerificationRequestNotification()
        {
            VoucherRepository.OpenDeclinedVerificationRequestNotification();
            return "SUCCESS";
        }

        public ActionResult Payment(string payment = "")
        {
            return View();
        }

        //[CustomAuthorize("!AUDIT")]
        [Authorize(Policy = "DenyAudit")]
        public ActionResult PutVoucherOnHold(string voucherId)
        {
            try
            {
                string[] usertype = new string[1];
                usertype[0] = "TREASURY STAFF";
                string jobId = BackgroundJob.Enqueue(() => MailRepository.VoucherNotification("accepted hold request", usertype));

                return Json(VoucherRepository.PutVoucherOnHold(voucherId, User.Identity.Name));
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public ActionResult DeclineHoldRequest(string voucherId)
        {
            return Json(VoucherRepository.DeclineHoldRequest(voucherId, User.Identity.Name));
        }

        public ActionResult DeclineUnholdRequest(string voucherId)
        {
            return Json(VoucherRepository.DeclineHoldRequest(voucherId, User.Identity.Name));
        }

        public ActionResult SeePreviousStatusBeforeHold(string voucherId)
        {
            return Json(VoucherRepository.SeePreviousStatusBeforeHold(voucherId));
        }

        public ActionResult UnholdVoucher(string voucherId)
        {
            string[] usertype = new string[1];
            usertype[0] = "TREASURY STAFF";
            string jobId2 = BackgroundJob.Enqueue(() => MailRepository.VoucherNotification("accepted unhold request", usertype));

            return Json(VoucherRepository.UnholdVoucher(voucherId, User.Identity.Name));
        }

        public string OpenNotification(string[] area)
        {
            NotificationRepository.OpenNotification(area);
            return "SUCCESS";
        }


        public class RequestVoucherForVerificationParamsModel
        {
            public Voucher[] Voucher { get; set; }
            public string ReleasingDate { get; set; }
            public string Note { get; set; }
        }

        [HttpPost]
        [Authorize(Roles = "TREASURY STAFF")]
        public JsonResult RequestVoucherForVerification([FromBody] RequestVoucherForVerificationParamsModel model)
        {
            string status;
            string[] usertype = new string[1];

            if (UserRepository.IsTreasuryLeadOnLeave() == "FALSE")
            {
                usertype[0] = "TREASURY LEAD";
                status = "for verification";
            }
            else
            {
                usertype[0] = "TREASURY MANAGER";
                status = "for approval";
            }

            string jobId2 = BackgroundJob.Enqueue(() => MailRepository.VoucherNotification(status, usertype));

            int batchId = VoucherRepository.SaveVoucherStatusBatch(User.Identity.Name, model.Note, "FOR VERIFICATION");

            VoucherRepository.RequestForVerification(model.Voucher, DateTime.ParseExact(model.ReleasingDate, "dd/MM/yyyy", null), batchId, User.Identity.Name);

            return Json("SUCCESS");
        }

        //[CustomAuthorize("TREASURY STAFF")]
        [Authorize(Roles = "TREASURY STAFF")]
        public ActionResult RequestVoucherForVerificationIndex()
        {
            return View();
        }

        public string UpdateVoucherStatusBatch(int batchId)
        {
            VoucherRepository.UpdateVoucherStatusBatch(batchId);
            return "SUCCESS";
        }

        //[CustomAuthorize("!AUDIT")]
        public ActionResult Vendor()
        {
            return View();
        }

        //[CustomAuthorize("TREASURY LEAD")]
        [Authorize(Roles = "TREASURY LEAD")]
        public ActionResult VerificationRequestsIndex()
        {
            return View();
        }

        public ActionResult UpdateCollectedCheck(int logId, string receiptNumber, string receiptType, string checkType)
        {
            return Json(VoucherRepository.UpdateCollectedCheck(logId, receiptNumber, receiptType, checkType));
        }

        public ActionResult ReverseVoucher(string voucherId)
        {
            return Json(VoucherRepository.ReverseVoucher(voucherId, User.Identity.Name));
        }

        public ActionResult AdverseVoucher(string voucherId)
        {
            return Json(VoucherRepository.AdverseVoucher(voucherId));
        }

        //[CustomAuthorize("TREASURY STAFF")]
        [Authorize(Roles = "TREASURY STAFF")]
        public ActionResult RequestVoucherHold()
        {
            return View();
        }

        public ActionResult RequestVoucherOnHold(string voucherId, string note)
        {
            string[] usertype = new string[1];
            usertype[0] = "TREASURY MANAGER";
            string jobId2 = BackgroundJob.Enqueue(() => MailRepository.VoucherNotification("on hold", usertype));

            return Json(VoucherRepository.RequestVoucherOnHold(voucherId, User.Identity.Name, note));
        }

        public ActionResult RequestVoucherUnhold(string voucherId, string note)
        {
            string[] usertype = new string[1];
            usertype[0] = "TREASURY MANAGER";
            string jobId2 = BackgroundJob.Enqueue(() => MailRepository.VoucherNotification("unhold", usertype));

            return Json(VoucherRepository.RequestVoucherUnhold(voucherId, User.Identity.Name, note));
        }

        public class MergeVendorParamModel
        {
            public int refVendorId { get; set; }
            public int[] vendorIds { get; set; }
            public string mergedVendorCode { get; set; }
            public string mergedEntity { get; set; }
        }

        //[CustomAuthorize("!AUDIT")]
        public ActionResult MergeVendor([FromBody]MergeVendorParamModel model)
        {
            return Json(VendorRepository.MergeVendor(model.refVendorId, model.vendorIds, 
                model.mergedVendorCode, model.mergedEntity, User.Identity.Name));
        }

        //[CustomAuthorize("!AUDIT")]
        public ActionResult CancelMerge(string vendorCode, string companyName)
        {
            return Json(VendorRepository.CancelMerge(vendorCode, User.Identity.Name, companyName));
        }

        [RequestSizeLimit(40000000)]
        public ActionResult GetVoucherListWithHoldRequest(int draw,
        int start,
        int length,
        List<Dictionary<string, string>> columns,
        List<Dictionary<string, string>> order,
        string vendorcode,
        string companyName,
        string bank,
        string entity,
        string voucherId,
        decimal? amount,
        string payment,
        string check,
        string date,
        string status,
        string[] statusArr = null
            )
        {
            var dataTableData = new DataTableData<Payment>();
            var sortColumn = columns[ConvertHelper.ToInteger(order[0]["column"])]["data"];
            var filter = new DataFilter
            {
                Draw = draw,
                Start = start,
                Length = length,
                SortColumn = sortColumn == null ? "Id" : sortColumn.Replace(" ", string.Empty),
                SortDirection = order[0]["dir"],
                ColumnFilters = new List<ColumnFilter>()
            };

            dataTableData.dataList = VoucherRepository.GetVoucherListWithHoldRequest(
                                            filter, vendorcode, companyName, bank, entity, voucherId, amount, payment, check, ConvertHelper.ToDate(date), status, statusArr);

            dataTableData.draw = filter.Draw;
            dataTableData.recordsTotal = filter.TotalRecordCount;
            dataTableData.recordsFiltered = filter.FilteredRecordCount;

            return this.Json(dataTableData);
        }

        //[CustomAuthorize("TREASURY MANAGER")]
        [Authorize(Roles = "TREASURY MANAGER")]
        public ActionResult HoldRequests()
        {
            return View();
        }

        //[CustomAuthorize("!AUDIT")]
        [Authorize(Policy = "DenyNonTreasury")]
        public ActionResult Employee()
        {
            return View();
        }

        //[CustomAuthorize("!AUDIT")]
        [Authorize(Policy = "DenyNonTreasury")]
        public ActionResult GetEmployeeList()
        {
            return Json(EmployeeRepository.List());
        }

        //[CustomAuthorize("!AUDIT")]
        [Authorize(Policy = "DenyNonTreasury")]
        public ActionResult AddOrUpdateEmployee([FromBody]Employee e)
        {
            return Json(EmployeeRepository.Add(e, User.Identity.Name));
        }

        //[CustomAuthorize("!AUDIT")]
        [Authorize(Policy = "DenyNonTreasury")]
        public JsonResult ImportEmployeeExcel(IFormFile FileUpload)
        {
            if (FileUpload == null)
            {
                return Json("NULL");
            }
            else if (!(FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || FileUpload.ContentType == "application/octet-stream"))
            {
                return Json("INVALID FILE");
            }

            var parsedFileName = Path.GetFileName(FileUpload.FileName);
            var filename = string.Format(DateTime.Now.ToString("yyyy-MM-dd HH_mm_ss_") + parsedFileName);
            string targetpath = _webHostEnvironment.WebRootPath + "~/SpreadSheets/";
            FileUpload.SaveAs(targetpath + filename);
            string pathToExcelFile = targetpath + filename;
            var connectionString = "";

            if (filename.EndsWith(".xls"))
            {
                connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
            }
            else if (filename.EndsWith(".xlsx"))
            {
                connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);
            }

            var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connectionString);
            var ds = new DataSet();

            adapter.Fill(ds, "ExcelTable");

            string sheetName = "Sheet1";

            var excelFile = new ExcelQueryFactory(pathToExcelFile);
            var employees = from a in excelFile.Worksheet<EmployeeExcelUpload>(sheetName) select a;

            if (employees.Count() == 0)
            {
                return Json("EMPTY");
            }
            else
            {
                var result = EmployeeRepository.Add(employees.ToList());

                if ((System.IO.File.Exists(pathToExcelFile)))
                {
                    System.IO.File.Delete(pathToExcelFile);
                }

                SystemLog sl = new SystemLog
                {
                    userId = User.Identity.Name,
                    type = "EMPLOYEE EXCEL UPLOAD",
                    description = "Uploaded an employee records excel.",
                    status = "COMPLETED"
                };

                SystemLogBusLogic.Logs.Add(sl);

                return Json(result);
            }
        }

        //[CustomAuthorize("!AUDIT")]
        [Authorize(Policy = "DenyNonTreasury")]
        public JsonResult ImportVendorExcel(IFormFile FileUpload)
        {
            if (FileUpload == null)
            {
                return Json("NULL");
            }
            else if (!(FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || FileUpload.ContentType == "application/octet-stream"))
            {
                return Json("INVALID FILE");
            }

            var parsedFileName = Path.GetFileName(FileUpload.FileName);
            var filename = string.Format(DateTime.Now.ToString("yyyy-MM-dd HH_mm_ss_") + parsedFileName);
            string targetpath = _webHostEnvironment.WebRootPath + "/SpreadSheets/";
            FileUpload.SaveAs(targetpath + filename);
            string pathToExcelFile = targetpath + filename;
            var connectionString = "";

            if (filename.EndsWith(".xls"))
            {
                connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
            }
            else if (filename.EndsWith(".xlsx"))
            {
                connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);
            }

            var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connectionString);
            var ds = new DataSet();

            adapter.Fill(ds, "ExcelTable");

            string sheetName = "Sheet1";

            var excelFile = new ExcelQueryFactory(pathToExcelFile);
            var vendors = from a in excelFile.Worksheet<VendorExcelUpload>(sheetName) select a;

            if (vendors.Count() == 0)
            {
                return Json("EMPTY");
            }
            else
            {
                var result = VendorRepository.AddOrUpdate(vendors.ToList());

                if ((System.IO.File.Exists(pathToExcelFile)))
                {
                    System.IO.File.Delete(pathToExcelFile);
                }

                SystemLog sl = new SystemLog
                {
                    userId = User.Identity.Name,
                    type = "VENDOR EXCEL UPLOAD",
                    description = "Uploaded an vendor records excel.",
                    status = "COMPLETED"
                };

                SystemLogBusLogic.Logs.Add(sl);

                return Json(result);
            }
        }
        public ActionResult GetEmployeeWithOnlinePaymentsList(DateTime? from = null, DateTime? to = null)
        {
            var resultData = EmployeeRepository.GetEmployeeWithOnlinePaymentsList(from, to);
            var result = new ContentResult
            {
                Content = JsonConvert.SerializeObject(resultData, Formatting.Indented),
                ContentType = "application/json"
            };
            return result;
        }


        public class OnlinePaymentsPostModel
        {
            public List<EmployeeOnlinePayment> onlinePayments{ get; set; }
            public bool isWithEmployee { get; set; }
        }

        public ActionResult SaveOnlinePayments([FromBody]OnlinePaymentsPostModel model)
        {
            foreach (var p in model.onlinePayments)
            {
                string result = EmployeeRepository.OnlinePayment(p, model.isWithEmployee, true);
            }

            if (model.isWithEmployee)
            {
                string jobId = BackgroundJob.Enqueue(() => MailRepository.OnlinePaymentMail(model.onlinePayments));
            }

            return Json("SUCCESS");
        }

        //[CustomAuthorize("!AUDIT")]
        [Authorize(Policy = "DenyNonTreasury")]
        public JsonResult OnlinePaymentExcel(IFormFile FileUpload)
        {
            if (FileUpload == null)
            {
                return Json("NULL");
            }
            else if (!(FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || FileUpload.ContentType == "application/octet-stream"))
            {
                return Json("INVALID FILE");
            }

            var parsedFileName = Path.GetFileName(FileUpload.FileName);
            var filename = string.Format(DateTime.Now.ToString("yyyy-MM-dd HH_mm_ss_") + parsedFileName);
            string targetpath = _webHostEnvironment.WebRootPath + "/SpreadSheets/";
            FileUpload.SaveAs(targetpath + filename);
            string pathToExcelFile = targetpath + filename;
            var connectionString = "";

            if (filename.EndsWith(".xls"))
            {
                connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
            }
            else if (filename.EndsWith(".xlsx"))
            {
                connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);
            }

            var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connectionString);
            var ds = new DataSet();

            adapter.Fill(ds, "ExcelTable");

            string sheetName = "Sheet1";

            int counter = 0;
            int index = 0;
            int totalRecord = 0;

            var excelFile = new ExcelQueryFactory(pathToExcelFile);
            var onlinePayments = from a in excelFile.Worksheet<EmployeeOnlinePayment>(sheetName) select a;

            var headers = from c in excelFile.WorksheetRangeNoHeader("A1", "F1") //Selects headers
                          select c;

            var isWithEmployee = headers.ToList().FirstOrDefault()[0] == "employeeId";

            List<string> employeeNotFound = new List<string>();
            List<string> incomplete = new List<string>();
            List<string> blankEmail = new List<string>();
            ResultMessage rm = new ResultMessage();

            List<EmployeeOnlinePayment> op = new List<EmployeeOnlinePayment>();

            foreach (var p in onlinePayments)
            {
                //Validations if there is employee
                if (isWithEmployee)
                {
                    if (p.amount == null && p.bank == null && p.employeeAccNum == null && p.employeeId == null &&
                        p.employeeAccNum == null && p.paymentDate == null && p.entity == null)
                    {
                        continue;
                    }

                    totalRecord++;

                    if (String.IsNullOrWhiteSpace(p.employeeId) || p.amount == null ||
                        p.paymentDate == null || String.IsNullOrWhiteSpace(p.employeeAccNum) ||
                        String.IsNullOrWhiteSpace(p.bank) || String.IsNullOrWhiteSpace(p.entity))
                    {
                        incomplete.Add((index++ + 1).ToString());
                        continue;
                    }

                    string result = EmployeeRepository.OnlinePayment(p, true, false);
                    if (result == "SUCCESS")
                    {
                        counter++;
                        op.Add(p);
                    }
                    else if (result == "NOT FOUND")
                    {
                        if (!employeeNotFound.Contains(p.employeeId))
                            employeeNotFound.Add(p.employeeId);
                    }
                    else if (result == "NO EMAIL")
                    {
                        if (!blankEmail.Contains(p.employeeId))
                            blankEmail.Add(p.employeeId);
                    }
                }
                else
                {
                    if (p.amount == null && p.bank == null && p.paymentDate == null && p.entity == null)
                    {
                        continue;
                    }

                    totalRecord++;

                    if (p.amount == null || p.paymentDate == null ||
                        String.IsNullOrWhiteSpace(p.bank) ||
                        String.IsNullOrWhiteSpace(p.entity))
                    {
                        incomplete.Add((index++ + 1).ToString());
                        continue;
                    }

                    string result = EmployeeRepository.OnlinePayment(p, false, false);
                    if (result == "SUCCESS")
                    {
                        counter++;
                        op.Add(p);
                    }
                }
            }

            //deleting excel file from folder  
            if ((System.IO.File.Exists(pathToExcelFile)))
            {
                System.IO.File.Delete(pathToExcelFile);
            }

            SystemLog sl = new SystemLog
            {
                userId = User.Identity.Name,
                type = "ONLINE PAYMENT EXCEL UPLOAD",
                description = "Uploaded an online payments excel.",
                status = "COMPLETED"
            };

            SystemLogBusLogic.Logs.Add(sl);

            //if (counter > 0 && isWithEmployee)
            //{
            //    string jobId = BackgroundJob.Enqueue(() => MailRepository.OnlinePaymentMail(op));
            //}

            var employees = EmployeeRepository.List();
            var ops = new List<EmployeeOnlinePayment>();

            if (isWithEmployee)
            {
                ops = op.Join(
                employees,
                tblOP => tblOP.employeeId,
                tblE => tblE.employee_no,
                (tblOP, tblE) => new EmployeeOnlinePayment
                {
                    Employee = new Employee
                    {
                        employee_no = tblE.employee_no ?? "-",
                        accountNo = tblE.accountNo ?? "-",
                        employeeName = tblE.employeeName ?? "No Employee is Set",
                        email = tblE.email ?? "-"
                    },
                    employeeAccNum = tblE.accountNo ?? "-",
                    amount = tblOP.amount,
                    transDate = tblOP.paymentDate?.ToString(),
                    paymentDate = tblOP.paymentDate,
                    logDate = tblOP.logDate,
                    strLogDate = tblOP.logDate?.ToString(),
                    paymentId = tblOP.paymentId,
                    entity = tblOP.entity ?? "",
                    bank = tblOP.bank ?? "",
                    employeeId = tblE.employee_no
                }).ToList();
            }
            else
            {
                ops = op.Select(x => new EmployeeOnlinePayment
                {
                    Employee = new Employee
                    {
                        employee_no = "-",
                        accountNo = "-",
                        employeeName = "-",
                        email = "-",
                    },
                    employeeAccNum = "-",
                    amount = x.amount,
                    transDate = x.paymentDate?.ToString(),
                    paymentDate = x.paymentDate,
                    logDate = DateTime.Now,
                    strLogDate = DateTime.Now.ToString(),
                    entity = x.entity ?? "",
                    bank = x.bank ?? "",
                    employeeId = "-"
                }).ToList();
            }



            rm.EmployeeNotFound = employeeNotFound;
            rm.BlankEmail = blankEmail;
            rm.Incomplete = incomplete;
            rm.RowsAdded = counter;
            rm.TotalRows = totalRecord;
            rm.IsWithEmployee = isWithEmployee;
            rm.OnlinePayments = ops;

            if (counter == 0)
            {
                rm.Status = "EMPTY";
            }
            else
            {
                rm.Status = "SUCCESS";
            }

            return Json(rm);
        }

        public class MarkVouchersAsReleasedParamsModel
        {
            public string[] voucherIdArr { get; set; }
            public DateTime releaseDate { get; set; }
            public Voucher[] voucher { get; set; }
            public VoucherCollector c { get; set; }
        }

        public ActionResult MarkVouchersAsReleased([FromBody]MarkVouchersAsReleasedParamsModel model)
        {
            //Mark vouchers as released without verification and approval requests, even without receipt details.
            string jobId2 = BackgroundJob.Enqueue(() => VoucherRepository.ForceReleaseVoucher(model.voucher, model.releaseDate, model.c));

            SystemLogRepository.Add(new SystemLog
            {
                logDate = DateTime.Now,
                userId = User.Identity.Name,
                type = "RELEASE",
                description = "The user marked vouchers (" + String.Join(",", model.voucherIdArr) + ") as released",
                status = "SUCCESS"
            });

            return Json(VoucherRepository.ForceReleaseVoucher(model.voucherIdArr, model.releaseDate, User.Identity.Name));
        }

        public ActionResult GetForReleasingVoucherList(string vendorId = "")
        {
            return Json(VoucherRepository.GetForReleasingVoucherList(vendorId, DateTime.Today));
        }

        public ActionResult GetCollectedChecks(DateTime? dateFrom, DateTime? dateTo, string[] checkType = null)
        {
            if (dateFrom == dateTo)
            {
                dateTo = (dateFrom ?? DateTime.Today).AddDays(1);
            }

            var resultData = VoucherRepository.GetCollectedChecks(dateFrom ?? DateTime.Today, dateTo ?? DateTime.Today.AddDays(1), checkType);
            var result = new ContentResult
            {
                Content = JsonConvert.SerializeObject(resultData),
                ContentType = "application/json"
            };

            return result;
        }

        //[CustomAuthorize("!AUDIT")]
        [Authorize(Policy = "DenyNonTreasury")]
        public JsonResult ImportReceiptExcel(IFormFile FileUpload)
        {
            if (FileUpload == null)
            {
                return Json("NULL");
            }
            else if (!(FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || FileUpload.ContentType == "application/octet-stream"))
            {
                return Json("INVALID FILE");
            }

            var parsedFileName = Path.GetFileName(FileUpload.FileName);
            var filename = string.Format(DateTime.Now.ToString("yyyy-MM-dd HH_mm_ss_") + parsedFileName);
            string targetpath = _webHostEnvironment.WebRootPath + "/SpreadSheets/";
            FileUpload.SaveAs(targetpath + filename);
            string pathToExcelFile = targetpath + filename;
            var connectionString = "";

            if (filename.EndsWith(".xls"))
            {
                connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
            }
            else if (filename.EndsWith(".xlsx"))
            {
                connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);
            }

            var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connectionString);
            var ds = new DataSet();

            adapter.Fill(ds, "ExcelTable");

            string sheetName = "Sheet1";

            var excelFile = new ExcelQueryFactory(pathToExcelFile);
            var receipts = from a in excelFile.Worksheet<Receipt>(sheetName) select a;

            string result = VoucherRepository.UpdateReceiptDetailsFromExcel(receipts);

            //deleting excel file from folder  
            if ((System.IO.File.Exists(pathToExcelFile)))
            {
                System.IO.File.Delete(pathToExcelFile);
            }

            if (result == "SUCCESS")
            {
                SystemLog sl = new SystemLog
                {
                    userId = User.Identity.Name,
                    type = "RECEIPT DETAILS EXCEL UPLOAD",
                    description = "Uploaded a receipt details excel.",
                    status = "COMPLETED"
                };
                SystemLogBusLogic.Logs.Add(sl);
            }

            return Json(result);
        }

        public ActionResult DownloadExcel(string fileName)
        {
            fileName += ".xlsx";
            var filepath = _webHostEnvironment.WebRootPath + "/SpreadSheets/Reports/" + fileName;

            byte[] fileBytes = System.IO.File.ReadAllBytes(filepath);
            
            BackgroundJob.Schedule(
            () => DeleteExcelFile(filepath),
            TimeSpan.FromMinutes(5));

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public ActionResult UploadVendorBankDetails(IFormFile FileUpload)
        {
            if (FileUpload == null)
            {
                return Json("NULL");
            }
            else if (!(FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || FileUpload.ContentType == "application/octet-stream"))
            {
                return Json("INVALID FILE");
            }

            var parsedFileName = Path.GetFileName(FileUpload.FileName);
            var filename = string.Format(DateTime.Now.ToString("yyyy-MM-dd HH_mm_ss_") + parsedFileName);
            string targetpath = _webHostEnvironment.WebRootPath + "/SpreadSheets/";

            FileUpload.SaveAs(targetpath + filename);

            string pathToExcelFile = targetpath + filename;
            var connectionString = "";

            if (filename.EndsWith(".xls"))
            {
                connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
            }
            else if (filename.EndsWith(".xlsx"))
            {
                connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);
            }

            var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connectionString);
            var ds = new DataSet();

            adapter.Fill(ds, "ExcelTable");

            string sheetName = "Sheet1";

            var excelFile = new ExcelQueryFactory(pathToExcelFile);
            var banks = from a in excelFile.Worksheet<VendorBanksExcelUpload>(sheetName) select a;

            var results = new ExcelUploadResult();

            int index = 1;
            int totalRows = 0;
            results.NotFound = new List<string>();
            results.Incomplete = new List<int>();

            foreach (var bank in banks)
            {
                if (String.IsNullOrWhiteSpace(bank.BankCompany) && String.IsNullOrWhiteSpace(bank.VendorCode)
                    && String.IsNullOrWhiteSpace(bank.BankAccount) && String.IsNullOrWhiteSpace(bank.BankBranch)
                    && String.IsNullOrWhiteSpace(bank.AccountName))
                {
                    continue;
                }
                else
                {
                    totalRows++;

                    var result = BankRepository.AddOrUpdateByUpload(bank);

                    if (result == "EMPTY")
                    {
                        results.Incomplete.Add(index + 1);
                    }
                    else if (result == "ADDED")
                    {
                        results.RowsAdded++;
                    }
                    else if (result == "UPDATED")
                    {
                        results.RowsUpdated++;
                    }
                    else
                    {
                        results.NotFound.Add(result);
                    }

                    index++;
                }
            }

            results.TotalRows = totalRows;

            return Json(results);
        }

        public void DeleteExcelFile(string fullpath)
        {
            if (System.IO.File.Exists(fullpath))
            {
                System.IO.File.Delete(fullpath);
            }
        }

        public class ExportVoucherToExcelModel
        {
            public List<VoucherExcelExport> voucherArr { get; set; }
        }

        public JsonResult ExportVoucherToExcel([FromBody]ExportVoucherToExcelModel model)
        {
            try
            {
                //Name of File 
                string fileName = "Selected Voucher List";
                string fullPath = _webHostEnvironment.WebRootPath + "/SpreadSheets/Reports/";

                System.IO.DirectoryInfo di = new DirectoryInfo(fullPath);

                using (XLWorkbook wb = new XLWorkbook())
                {
                    var ws = wb.Worksheets.Add("Sheet 1");

                    ws.Row(1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    ws.Row(1).Style.Font.Bold = true;
                    ws.Row(1).Style.Font.FontSize = 12;
                    ws.Cell("A1").Value = "Entity";
                    ws.Cell("B1").Value = "Vendor Code";
                    ws.Cell("C1").Value = "Company Name";
                    ws.Cell("D1").Value = "Bank Name";
                    ws.Cell("E1").Value = "Voucher Number";
                    ws.Cell("F1").Value = "Amount";
                    ws.Cell("G1").Value = "Check Number";
                    ws.Cell("H1").Value = "Check Date";

                    ws.Cell(2, 1).Value = model.voucherArr.AsEnumerable();

                    ws.Columns("A", "Z").AdjustToContents();
                    ws.Columns("A", "Z").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    ws.Column(17).Hide();

                    wb.SaveAs(fullPath + fileName + ".xlsx");

                    using (MemoryStream stream = new MemoryStream())
                    {
                        wb.SaveAs(stream);
                        //Return xlsx Excel File  

                        SystemLog sl = new SystemLog
                        {
                            logDate = DateTime.Now,
                            userId = User.Identity.Name,
                            type = "EXPORT",
                            description = "Requested to export the selected vouchers to excel",
                            status = "SUCCESS"
                        };

                        SystemLogRepository.Add(sl);

                        return Json(fileName);
                    }
                }

            }
            catch (Exception ex)
            {
                return Json("ERROR");
            }
        }

        public ActionResult ImportVoucherNoExcel(IFormFile FileUpload)
        {
            if (FileUpload == null)
            {
                return Json("NULL");
            }
            else if (!(FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || FileUpload.ContentType == "application/octet-stream"))
            {
                return Json("INVALID FILE");
            }

            var parsedFileName = Path.GetFileName(FileUpload.FileName);
            var filename = string.Format(DateTime.Now.ToString("yyyy-MM-dd HH_mm_ss_") + parsedFileName);
            string targetpath = _webHostEnvironment.WebRootPath + "~/SpreadSheets/";
            FileUpload.SaveAs(targetpath + filename);
            string pathToExcelFile = targetpath + filename;
            var connectionString = "";

            if (filename.EndsWith(".xls"))
            {
                connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
            }
            else if (filename.EndsWith(".xlsx"))
            {
                connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);
            }

            var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connectionString);
            var ds = new DataSet();

            adapter.Fill(ds, "ExcelTable");

            string sheetName = "Sheet1";

            var excelFile = new ExcelQueryFactory(pathToExcelFile);
            var vouchers = from a in excelFile.Worksheet<VoucherNumber>(sheetName) select a;

            if (vouchers.Count() == 0)
            {
                return Json("EMPTY");
            }
            else
            {
                var result = vouchers.ToList().Where(x => x.VoucherNo != null).Select(x => "#" + x.VoucherNo).ToArray();

                if ((System.IO.File.Exists(pathToExcelFile)))
                {
                    System.IO.File.Delete(pathToExcelFile);
                }

                SystemLog sl = new SystemLog
                {
                    userId = User.Identity.Name,
                    type = "PAYMENT EXCEL IMPORT",
                    description = "Imported voucher numbers to be selected for verification request.",
                    status = "COMPLETED"
                };

                SystemLogBusLogic.Logs.Add(sl);

                return Json(result);
            }
        }

        public ActionResult GetVoucherStatusLogs(string voucherId)
        {
            return Json(VoucherRepository.GetVoucherStatusLogs(voucherId));
        }

        public ActionResult GetCollectedCheckDetails(string voucherId)
        {
            return Json(VoucherRepository.GetCollectedCheckDetails(voucherId));
        }

        public ActionResult ExportOnlinePayments(DateTime from, DateTime to)
        {
            GlobalClass GlobalClass = new GlobalClass();
            string[] checkType = new string[1] { "ONLINE" };

            return Json(GlobalClass.WriteReleasedToExcel(User.Identity.Name, from, to, false, checkType));
        }

        public class ExcelUploadResult
        {
            public int RowsAdded;
            public int TotalRows;
            public List<int> Incomplete;
            public int RowsUpdated;
            public List<string> NotFound;
        }

        public class ResultMessage
        {
            public List<string> BlankEmail;
            public string Status;
            public List<string> EmployeeNotFound;
            public List<string> Incomplete;
            public int RowsAdded;
            public int TotalRows;
            public bool IsWithEmployee { get; set; }
            public List<EmployeeOnlinePayment> OnlinePayments { get; set; }
        }
    }
}
