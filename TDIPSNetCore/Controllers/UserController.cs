﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TDIPS.Core.Interfaces;
using TDIPS.Infrastructure.Business_Logic;
using TDIPS.Models;
using TDIPSNetCore.Areas.Identity.Data;

namespace TDIPSNetCore.Web.Controllers
{
	[Authorize(Policy = "DenyNonTreasury")]
	public class UserController : Controller
    {
		IUserRepository UserRepository = new UserRepository();
		private readonly UserManager<ApplicationUser> userManager;
		private readonly SignInManager<ApplicationUser> signInManager;

		// GET: User

		//[CustomAuthorize("!AUDIT")]
		[Authorize(Policy = "DenyAudit")]
		public ActionResult Index()
		{
			return View();
		}

		public UserController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
		{
			this.userManager = userManager;
			this.signInManager = signInManager;
		}

		public ActionResult GetUsersList()
		{
			return Json(UserRepository.List());
		}

		public ActionResult IsUserOnLeave()
		{
			var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

			return Json(UserRepository.IsUserOnLeave(userId));
		}

		[AllowAnonymous]
		[HttpPost]
		public async Task<ActionResult> AddUser([FromBody]RegisterViewModel model)
		{
			if (UserRepository.IsEmailExisting(model.Email))
			{
				return Json("Email address " + model.Email + " is already taken.");
			}

			//var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));

			var user = new ApplicationUser
			{
				UserName = model.UserName,
				Email = model.Email
			};

			var result = await userManager.CreateAsync(user, model.Password);

			if (result.Succeeded)
			{
				await userManager.AddToRoleAsync(user, model.Role);
				return Json("SUCCESS");
			}
			else
			{
				return Json(result.Errors);
			}
		}

		public void SetUsersLeaveStatus(bool isOnLeave)
		{
			UserRepository.SetUsersLeaveStatus(User.Identity.Name, isOnLeave);
		}
	}
}
