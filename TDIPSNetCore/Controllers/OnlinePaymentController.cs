﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Hangfire;
using LinqToExcel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TDIPS;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.Business_Logic;
using TDIPS.Models;

namespace TDIPSNetCore.Web.Controllers
{
    [Authorize]
    public class OnlinePaymentController : Controller
    {
        private readonly GenerateDynamicsData _integration = new GenerateDynamicsData();
        IOnlinePaymentRepository OnlinePaymentRepository = new OnlinePaymentRepository();
        IVendorRepository VendorRepository = new VendorRepository();
        IMailRepository MailRepository = new MailRepository();
        IBankRepository BankRepository = new BankRepository();
        IEODRepository EODRepository = new EODRepository();

        private readonly IWebHostEnvironment _webHostEnvironment;

        public OnlinePaymentController(IWebHostEnvironment hostingEnvironment)
        {
            _webHostEnvironment = hostingEnvironment;
        }

        public ActionResult SendOtherOnlinePaymentEODMail()
        {
            BackgroundJob.Enqueue(() => MailRepository.SendOtherOnlinePaymentsEODMail(User.Identity.Name, false));

            return Json("SUCCESS");
        }

        public ActionResult IsEODValidationSent()
        {
            return Json(EODRepository.IsEODValidationSent(DateTime.Today, "OTHER ONLINE PAYMENT"));
        }

        public ActionResult Employees()
        {
            return View();
        }

        public ActionResult GetOtherOnlinePayments(int batchId, string accountType = "")
        {
            return Json(OnlinePaymentRepository.GetOtherOnlinePayments(batchId, accountType));
        }

        public ActionResult GetOnlinePaymentBatches(DateTime dateFrom, DateTime dateTo)
        {
            var result = OnlinePaymentRepository.GetOtherOnlinePaymentBatches(dateFrom, dateTo);

            return Json(result);
        }

        public class PostOtherOnlinePaymentModel 
        {
            public OtherOnlinePaymentBatch batch { get; set; }
            public OtherOnlinePayment[] paymentArr { get; set; }
        }

        //chanfdafdasfaf

        public ActionResult PostOtherOnlinePayment([FromBody] PostOtherOnlinePaymentModel model)
        {
            var result = new PostResult();

            var postResult = _integration.PostOtherOnlinePayments(model.batch.description, model.batch.entity, model.paymentArr);

            postResult = Regex.Replace(postResult, @"\""", "");

            if (postResult.Contains("Success:"))
            {
                var generalJournalNumber = Regex.Replace(postResult, @"Success: ", "");
                model.batch.uploadDate = DateTime.Now;
                model.batch.uploadedBy = User.Identity.Name;
                model.batch.generalJournalNum = generalJournalNumber;

                OnlinePaymentRepository.SaveOtherOnlinePayments(model.batch, model.paymentArr);

                result.Status = "SUCCESS";
                result.Message = generalJournalNumber;
            }
            else
            {
                result.Status = "FAILED";
                result.Message = postResult;
            }

            return Json(result);
        }

        public ActionResult GetGeneralOnlinePaymentList(DateTime? from = null, DateTime? to = null, bool isByTransdate = true)
        {
            return Json(OnlinePaymentRepository.GetGeneralOnlinePaymentList(from, to, isByTransdate));
        }

        public class OnlinePaymentPostModel
        {
            public GeneralOnlinePayment[] onlinePayments { get; set; }
        }

        public ActionResult SubmitOnlinePayment([FromBody] OnlinePaymentPostModel model)
        {
            OnlinePaymentRepository.SaveGeneralOnlinePayment(model.onlinePayments);

            BackgroundJob.Enqueue(() => MailRepository.SendOnlinePaymentMail(model.onlinePayments));

            return Json("SUCCESS");
        }

        public ActionResult ImportOtherOnlinePayment(IFormFile FileUpload)
        {
            if (FileUpload == null)
            {
                return Json("NULL");
            }
            else if (!(FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || FileUpload.ContentType == "application/octet-stream"))
            {
                return Json("INVALID FILE");
            }

            var parsedFileName = Path.GetFileName(FileUpload.FileName);
            var filename = string.Format(DateTime.Now.ToString("yyyy-MM-dd HH_mm_ss_") + parsedFileName);
            string targetpath = _webHostEnvironment.WebRootPath + "/SpreadSheets/";
            FileUpload.SaveAs(targetpath + filename);
            string pathToExcelFile = targetpath + filename;
            var connectionString = "";

            if (filename.EndsWith(".xls"))
            {
                connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
            }
            else if (filename.EndsWith(".xlsx"))
            {
                connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);
            }

            var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connectionString);
            var ds = new DataSet();

            adapter.Fill(ds, "ExcelTable");

            string sheetName = "Sheet1";

            var excelFile = new ExcelQueryFactory(pathToExcelFile);
            var otherOnlinePayments = new List<OtherOnlinePayment>
                (from a in excelFile.Worksheet<OtherOnlinePayment>(sheetName) select a);
            var results = new List<OtherOnlinePaymentExcelUploadResult>();

            var hasOffsetAccount = otherOnlinePayments
                .Any(x => !String.IsNullOrWhiteSpace(x.offsetAccountType));

            if (hasOffsetAccount)
            {
                var hasNoOffsetAccount = otherOnlinePayments
                .Any(x => String.IsNullOrWhiteSpace(x.offsetAccountType));

                if (hasNoOffsetAccount)
                {
                    var result = new OtherOnlinePaymentExcelUploadResult
                    {
                        LineNumber = 0,
                        Message = new List<string> {
                            "Some lines are missing an Offset Account"
                        },
                        Status = "ERROR"
                    };

                    results.Add(result);
                }
            }
            else
            {
                var debit = otherOnlinePayments.AsEnumerable().Sum(x => x.debit);
                var credit = otherOnlinePayments.AsEnumerable().Sum(x => x.credit);

                if (debit != credit)
                {
                    var result = new OtherOnlinePaymentExcelUploadResult
                    {
                        LineNumber = 0,
                        Message = new List<string> {
                            "Total debit amount is not balanced with total credit amount."
                        },
                        Status = "ERROR"
                    };

                    results.Add(result);
                }
            }

            var index = 2;

            foreach (var payment in otherOnlinePayments)
            {
                if (payment.transDate == null &&
                    string.IsNullOrWhiteSpace(payment.description) &&
                    string.IsNullOrWhiteSpace(payment.accountType) &&
                    string.IsNullOrWhiteSpace(payment.account) &&
                    string.IsNullOrWhiteSpace(payment.stores) &&
                    string.IsNullOrWhiteSpace(payment.department) &&
                    string.IsNullOrWhiteSpace(payment.businessUnit) &&
                    string.IsNullOrWhiteSpace(payment.purpose) &&
                    string.IsNullOrWhiteSpace(payment.salesSegment) &&
                    string.IsNullOrWhiteSpace(payment.worker) &&
                    string.IsNullOrWhiteSpace(payment.worker) &&
                    string.IsNullOrWhiteSpace(payment.costCenter) &&
                    payment.debit == null &&
                    payment.credit == null &&
                    string.IsNullOrWhiteSpace(payment.offsetAccount) &&
                    string.IsNullOrWhiteSpace(payment.offsetAccountType) &&
                    string.IsNullOrWhiteSpace(payment.oStores) &&
                    string.IsNullOrWhiteSpace(payment.oDepartment) &&
                    string.IsNullOrWhiteSpace(payment.oBusinessUnit) &&
                    string.IsNullOrWhiteSpace(payment.oPurpose) &&
                    string.IsNullOrWhiteSpace(payment.oSalesSegment) &&
                    string.IsNullOrWhiteSpace(payment.oWorker) &&
                    string.IsNullOrWhiteSpace(payment.oCostCenter))
                {
                    continue;
                }

                var result = new OtherOnlinePaymentExcelUploadResult
                {
                    OtherOnlinePayment = payment,
                    LineNumber = index,
                    Message = new List<string>()
                };

                if (payment.transDate == null ||
                    string.IsNullOrWhiteSpace(payment.description) ||
                    string.IsNullOrWhiteSpace(payment.accountType) ||
                    string.IsNullOrWhiteSpace(payment.account) ||
                    (payment.debit == null && payment.credit == null)
                   )
                {
                    result.Status = "ERROR";
                    result.Message.Add("There is an incomplete fields.");
                }

                var accountType = payment.accountType?.ToUpper();
                var offsetAccountType = payment.offsetAccountType?.ToUpper();

                if (!(accountType == null || accountType == "") &&
                    !(accountType == "LEDGER" || accountType == "BANK" ||
                    accountType == "VENDOR" || accountType == "CUSTOMER"))
                {
                    result.Status = "ERROR";
                    result.Message.Add("Invalid account type. Please choose among ledger, bank, vendor or customer");
                }

                if (!(offsetAccountType == null || offsetAccountType == "") &&
                    !(offsetAccountType == "LEDGER" || offsetAccountType == "BANK" ||
                    offsetAccountType == "VENDOR" || offsetAccountType == "CUSTOMER"))
                {
                    result.Status = "ERROR";
                    result.Message.Add("Invalid offset account type. Please choose among ledger, bank, vendor or customer");
                }

                if (payment.debit > 0 && payment.credit > 0)
                {
                    result.Status = "ERROR";
                    result.Message.Add("Debit and credit amount shouldn't be filled at the same line.");
                }

                if (!string.IsNullOrWhiteSpace(payment.offsetAccountType) &&
                    string.IsNullOrWhiteSpace(payment.offsetAccount))
                {
                    result.Status = "ERROR";
                    result.Message.Add("Please set the offsetAccount");
                }

                if (String.IsNullOrWhiteSpace(result.Status))
                {
                    result.Status = "SUCCESS";
                }

                results.Add(result);

                index++;
            }

            if ((System.IO.File.Exists(pathToExcelFile)))
            {
                System.IO.File.Delete(pathToExcelFile);
            }

            return Json(results);
        }

        public ActionResult ImportGeneralOnlinePayment(IFormFile FileUpload)
        {
            if (FileUpload == null)
            {
                return Json("NULL");
            }
            else if (!(FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || FileUpload.ContentType == "application/octet-stream"))
            {
                return Json("INVALID FILE");
            }

            var parsedFileName = Path.GetFileName(FileUpload.FileName);
            var filename = string.Format(DateTime.Now.ToString("yyyy-MM-dd HH_mm_ss_") + parsedFileName);
            string targetpath = _webHostEnvironment + "/SpreadSheets/";
            FileUpload.SaveAs(targetpath + filename);
            string pathToExcelFile = targetpath + filename;
            var connectionString = "";

            if (filename.EndsWith(".xls"))
            {
                connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
            }
            else if (filename.EndsWith(".xlsx"))
            {
                connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);
            }

            var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connectionString);
            var ds = new DataSet();

            adapter.Fill(ds, "ExcelTable");

            string sheetName = "Sheet1";

            var excelFile = new ExcelQueryFactory(pathToExcelFile);
            var select = from a in excelFile.Worksheet<GeneralOnlinePayment>(sheetName) select a;

            var GeneralOnlinePayments = select.ToList();

            GeneralOnlinePayments.ForEach(x => {
                x.vendorCode = Regex.Replace((x.vendorCode?.Trim() ?? ""), @"[^\u0009\u000A\u000D\u0020-\u007E]", "");
                x.bankAccountNumber = Regex.Replace((x.bankAccountNumber?.Trim() ?? ""), @"[^\u0009\u000A\u000D\u0020-\u007E]", "");
            });

            var results = new List<GeneralOnlinePaymentExcelUploadResult>();

            var bankAccounts = JsonConvert.DeserializeObject<IEnumerable<BankAccount>>(_integration.GenerateBanks());

            var paymentModes = GetPaymentModeList();

            var vendorCodeArr = GeneralOnlinePayments.Select(x => x.vendorCode).ToArray();

            var vendors = VendorRepository.GetVendorListByVendorCodes(vendorCodeArr);

            var vendorIds = vendors.Select(x => x.vendorID).ToList();

            var banks = BankRepository.GetBankListByVendorIds(vendorIds);

            var index = 2;

            foreach (var payment in GeneralOnlinePayments)
            {
                if (String.IsNullOrWhiteSpace(payment.vendorCode) &&
                    String.IsNullOrWhiteSpace(payment.invoiceVouchers) &&
                    String.IsNullOrWhiteSpace(payment.entity) &&
                    String.IsNullOrWhiteSpace(payment.bank) &&
                    payment.transDate == null &&
                    payment.amount == null &&
                    String.IsNullOrWhiteSpace(payment.modeOfPayment))
                {
                    continue;
                }

                var vendor = vendors.FirstOrDefault(x => x.vendorCode.Contains(payment.vendorCode));

                payment.vendor = vendor;

                var result = new GeneralOnlinePaymentExcelUploadResult
                {
                    GeneralOnlinePayment = payment,
                    LineNumber = index,
                    Message = new List<string>()
                };

                if (String.IsNullOrWhiteSpace(payment.vendorCode) ||
                    String.IsNullOrWhiteSpace(payment.invoiceVouchers) ||
                    String.IsNullOrWhiteSpace(payment.entity) ||
                    String.IsNullOrWhiteSpace(payment.bank) ||
                    payment.transDate == null ||
                    payment.amount == null ||
                    String.IsNullOrWhiteSpace(payment.modeOfPayment) ||
                    String.IsNullOrWhiteSpace(payment.bankAccountNumber))
                {
                    result.Status = "ERROR";
                    result.Message.Add("There is an incomplete fields.");
                }

                if (
                    !String.IsNullOrWhiteSpace(payment.modeOfPayment) &&
                    !String.IsNullOrWhiteSpace(payment.entity) &&
                    !(
                    paymentModes.Any(x => x.Entity == payment.entity?.ToUpper() &&
                    x.Mode.Contains(payment.modeOfPayment?.ToUpper()))
                    )
                   )
                {
                    result.Status = "ERROR";
                    result.Message.Add($"The specified mode of payment is not allowed in the " +
                        $"{payment.entity?.ToUpper()} entity.");
                }

                if (!String.IsNullOrWhiteSpace(payment.vendorCode) && vendor == null)
                {
                    result.Status = "ERROR";
                    result.Message.Add("The vendor code " + payment.vendorCode + " is not found in the system.");
                }
                else
                {
                    if (String.IsNullOrWhiteSpace(vendor.companyEmail) &&
                  String.IsNullOrWhiteSpace(vendor.contactNumber))
                    {
                        result.Status = "ERROR";
                        result.Message.Add("The vendor has no email address nor contact number.");
                    }

                    var bank = banks.FirstOrDefault(x => x.vendorID == vendor.vendorID &&
                    x.bankAccount == payment.bankAccountNumber);

                    if (!String.IsNullOrWhiteSpace(payment.bankAccountNumber) && bank == null)
                    {
                        result.Status = "ERROR";
                        result.Message.Add("The bank account number " + payment.bankAccountNumber +
                            " is not found in the vendor's bank account numbers list.");
                    }
                    else
                    {
                        result.GeneralOnlinePayment.BankDetails = bank;
                    }
                }

                if (String.IsNullOrWhiteSpace(result.Status))
                {
                    result.Status = "SUCCESS";
                }

                index++;
                results.Add(result);
            }

            if ((System.IO.File.Exists(pathToExcelFile)))
            {
                System.IO.File.Delete(pathToExcelFile);
            }

            return Json(results);
        }

        public List<PaymentMode> GetPaymentModeList()
        {
            var paymentModes = new List<PaymentMode>
            {
                new PaymentMode {
                    Entity = "SPAV",
                    Mode = new string[] { "CHECK", "CREDITCARD", "FUNDTRANSF", "ONLINE", "TELEGRAPHI" }
                },
                new PaymentMode {
                    Entity = "SPCI",
                    Mode = new string[] { "AUTOCREDIT", "CHECK", "CREDITCARD", "ONLINE" }
                },
                new PaymentMode {
                    Entity = "WBHI",
                    Mode = new string[] { "CHECK", "CREDITCARD", "FUNDTRANSF", "ONLINE", "TELEGRAPHI" }
                },
                new PaymentMode {
                    Entity = "NAF",
                    Mode = new string[] { "CHECK", "CREDITCARD", "FUNDTRANSF", "ONLINE", "TELEGRAPHI" }
                },
                new PaymentMode{
                    Entity = "SIL",
                    Mode = new string[] { "CHECK", "CREDITCARD", "FUNDTRANSF", "ONLINE", "TELEGRAPHI"}
                },
                new PaymentMode{
                    Entity = "SPRF",
                    Mode = new string[] { "CHECK", "CREDITCARD", "FUNDTRANSF", "ONLINE", "TELEGRAPHI"}
                },
                new PaymentMode{
                    Entity = "SSI",
                    Mode = new string[] { "CHECK", "CREDITCARD", "FUNDTRANSF", "ONLINE", "TELEGRAPHI" }
                }
            };

            return paymentModes;
        }

        public bool IsPaymentModeValid(string entity, string mode)
        {
            if (entity?.ToUpper() == "SPAV")
            {

            }
            else if (entity?.ToUpper() == "SPCI")
            {

            }
            else if (entity?.ToUpper() == "WBHI")
            {

            }
            else if (entity?.ToUpper() == "NAF")
            {

            }
            else if (entity?.ToUpper() == "SIL")
            {

            }
            else if (entity?.ToUpper() == "SPRF")
            {

            }
            else if (entity?.ToUpper() == "SSI")
            {

            }

            return false;
        }

        public class PaymentMode
        {
            public string[] Mode { get; set; }
            public string Entity { get; set; }
        }

        public class GeneralOnlinePaymentExcelUploadResult
        {
            public GeneralOnlinePayment GeneralOnlinePayment { get; set; }
            public string Status { get; set; }
            public List<string> Message { get; set; }
            public int LineNumber { get; set; }
        }

        public class PostResult
        {
            public string Status { get; set; }
            public string Message { get; set; }
        }

        public class OtherOnlinePaymentExcelUploadResult
        {
            public OtherOnlinePayment OtherOnlinePayment { get; set; }
            public string Status { get; set; }
            public List<string> Message { get; set; }
            public int LineNumber { get; set; }
        }
    }
}
