﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hangfire;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.CognitiveServices.Speech;
using Microsoft.CognitiveServices.Speech.Audio;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.Business_Logic;

namespace TDIPSNetCore.Web.Controllers
{
    [Authorize(Policy = "DenyNonTreasury")]
    public class ECREAController : Controller
    {
        IQueueRepository QueueRepository = new QueueRepository();
        IQueueingWindow IQueueingWindow = new QueueingWindowRepository();
        IVoucherRepository VoucherRepository = new VoucherRepository();
        IVendorRepository VendorRepository = new VendorRepository();
        IECREAManagerRepository ECREAManagerRepository = new ECREAManagerRepository();

        private readonly IWebHostEnvironment _webHostEnvironment;

        private readonly IHubContext<CompanyCallHub> _hub;

        public ECREAController(IWebHostEnvironment hostingEnvironment, IHubContext<CompanyCallHub> hub)
        {
            _webHostEnvironment = hostingEnvironment;
            _hub = hub;
        }

        private static Random random = new Random();

        static async Task SynthesizeAudioAsync(string text, string filepath)
        {
            var config = SpeechConfig.FromSubscription("92be2c4978d341e387049d55db64c40d", "southeastasia");
            using var audioConfig = AudioConfig.FromWavFileOutput(filepath);
            using var synthesizer = new SpeechSynthesizer(config, audioConfig);
            await synthesizer.SpeakTextAsync(text);
        }

        public void Speak(string textToSpeech, string filename)
        {
        //    // Command to execute PS  

        //    var cmd = $@"
        //        Add-Type -AssemblyName System.Speech
        //        $soundFilePath = ""C:\Users\dizon\Desktop\Projects\TDIPSNetCore\TDIPSNetCore\wwwroot\t2s\test.wav""
        //        [System.Speech.Synthesis.SpeechSynthesizer] $voice = $null
        //        Try {{
        //        $voice = New-Object -TypeName System.Speech.Synthesis.SpeechSynthesizer
        //        $voice.SetOutputToWaveFile($soundFilePath)
        //        $voice.SelectVoice(""Microsoft Zira Desktop"")
        //        $voice.Speak(""testing fdalksfafkjalkfjalkfjkads"")
        //        }}
        //                    Finally {{
        //                        if ($voice) {{
        //        $voice.Dispose()
        //                        }}
        //    }};";

        //    Execute(cmd);
        

        //void Execute(string command)
        //    {
        //        // create a temp file with .ps1 extension  
        //        var cFile = _webHostEnvironment.ContentRootPath + "/wwwroot/t2s/test.ps1";

        //        //Write the .ps1  
        //        using var tw = new System.IO.StreamWriter(cFile, false, Encoding.UTF8);
        //        tw.Write(command);

        //        // Setup the PS  
        //        var start =
        //            new System.Diagnostics.ProcessStartInfo()
        //            {
        //                FileName = "C:\\windows\\system32\\windowspowershell\\v1.0\\powershell.exe",  // CHUPA MICROSOFT 02-10-2019 23:45                    
        //                LoadUserProfile = false,
        //                UseShellExecute = false,
        //                CreateNoWindow = true,
        //                Arguments = $"-executionpolicy bypass -File {cFile}",
        //                WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden
        //            };

        //        //Init the Process  

        //        try
        //        {
        //            var p = System.Diagnostics.Process.Start(start);
        //            p.WaitForExit();
        //        }
        //        catch (Exception ex)
        //        {

        //            throw;
        //        }
        //    }
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        // GET: ECREA
        [AllowAnonymous]
        public ActionResult QueueingWindow()
        {
            return View();
        }
        [AllowAnonymous]
        public JsonResult GetQueueList(int rowNumber = -1, string status = "")
        {
            return Json(QueueRepository.GetQueueList(rowNumber, status));
        }

        //[CustomAuthorize("!AUDIT")]
        [Authorize(Policy = "DenyAudit")]
        public ActionResult Management()
        {
            ViewBag.IsKioskRatingActivated = ECREAManagerRepository.IsKioskRatingActivated();

            return View();
        }

        public ActionResult GetLastECREARatings()
        {
            return Json(ECREAManagerRepository.GetLastECREARatings());
        }

        private string ReplaceFirstOccurrence(string Source, string Find, string Replace)
        {
            int Place = Source.IndexOf(Find, StringComparison.CurrentCultureIgnoreCase);
            string result = Source;

            if (Place > -1)
            {
                result = Source.Remove(Place, Find.Length).Insert(Place, Replace);
            }

            return result;
        }

        public ActionResult IsKioskRatingActivated()
        {
            return Json(ECREAManagerRepository.IsKioskRatingActivated());
        }

        public async Task<ActionResult> TTS(string text, string companyName, string collectorName, int queueNumber, string queueType, int repeat = 0)
        {
            text = ReplaceFirstOccurrence(text, "INC.", "incorporated. ");

            for (int i = 0; i < repeat; i++)
            {
                text += text;
            }

            NumberCall n = new NumberCall
            {
                Type = "NUMBER CALL",
                CompanyName = companyName,
                QueueType = queueType,
                QueueNumber = queueNumber,
                Status = "READY",
                Text = text,
                CollectorName = collectorName
            };

            var callid = IQueueingWindow.AddNumberCall(n);
            string fileName = "TTS" + callid;

            //Speak(text, fileName + ".wav");

            BackgroundJob.Schedule(
            () => DeleteAudioWhenDone(callid),
            TimeSpan.FromMinutes(5));

            var filepath = _webHostEnvironment.WebRootPath + "\\Queueing Window\\TTS\\" + fileName + ".wav";

            await SynthesizeAudioAsync(text, filepath);

            await _hub.Clients.All.SendAsync("CompanyCall", "SYSTEM", callid.ToString() + "_" + queueType);

            return Json(fileName + ".wav");
        }

        [HttpPost]
        public string SaveVideo(IFormFile videofile)
        {
            var splitfilename = videofile.FileName.Split('\\');
            var filename = splitfilename[splitfilename.Length - 1];

            if (!(filename.EndsWith(".mp4") || filename.EndsWith(".avi") || filename.EndsWith(".mov") || filename.EndsWith(".flv") || filename.EndsWith(".wmv")))
            {
                return "invalid format";
            }

            if (videofile != null)
            {
                var filenames = videofile.FileName.ToString().Split('.');
                string fileextension = filenames[filenames.Length - 1];
                int videoid = IQueueingWindow.SaveVideo(filename, fileextension, User.Identity.Name);
                videofile.SaveAs(_webHostEnvironment.WebRootPath + "//Queueing Window//Videos//" + videoid.ToString() + "." + fileextension);
                
                return "SUCCESS";
            }
            return "FAILED";
        }

        [AllowAnonymous]
        public ActionResult GetVideoList()
        {
            return Json(IQueueingWindow.VideoList());
        }

        public ActionResult SetKioskRatingActivation(bool isActivate)
        {
            ECREAManagerRepository.SetKioskRatingActivation(isActivate);

            return Json("SUCCESS");
        }

        public string SaveMarqueeText(string text)
        {
            return IQueueingWindow.SaveMarqueeText(text, User.Identity.Name);
        }

        [AllowAnonymous]
        public string GetMarqueeText()
        {
            return IQueueingWindow.GetMarqueeText();
        }

        public string DeleteVideo(int videoId, string fileExtension)
        {
            string fullPath = _webHostEnvironment.WebRootPath + "/Queueing Window/Videos/" +  videoId + "." + fileExtension.ToString();

            if (System.IO.File.Exists(fullPath))
            {
                IQueueingWindow.DeleteVideo(videoId, User.Identity.Name);
                System.IO.File.Delete(fullPath);

                return "SUCCESS";
            }

            return "FAILED";
        }

        public JsonResult ReleaseVoucher(ReleasedVoucher[] r, int queueId, bool isInQueue = true, VoucherCollector c = null, string vendorCode = "")
        {
            if (isInQueue)
            {
                QueueRepository.DeleteFromQueue(queueId);
            }

            string jobId = BackgroundJob.Enqueue(() => VoucherRepository.UpdateCollectedVouchers(r, c));

            return Json(VoucherRepository.ReleaseVoucher(r, User.Identity.Name));
        }

        public ActionResult Release()
        {
            return View();
        }

        public ActionResult Dequeue(int queueId)
        {
            return Json(QueueRepository.Dequeue(queueId, User.Identity.Name));
        }

        public ActionResult Queue(int queueId)
        {
            return Json(QueueRepository.Queue(queueId, User.Identity.Name));
        }

        public ActionResult GetVendorListWithVoucherForRelease()
        {
            return Json(VendorRepository.GetVendorListWithVoucherForRelease());
        }

        [AllowAnonymous]
        public ActionResult GetCall()
        {
            return Json(IQueueingWindow.GetCall());
        }

        [AllowAnonymous]
        public ActionResult DeleteAudioWhenDone(int callid)
        {
            string fullPath = _webHostEnvironment.WebRootPath + "/Queueing Window/TTS/TTS" + callid + ".wav";

            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
            }

            return Json("SUCCESS");
        }

        public ActionResult CheckCompany()
        {
            return View();
        }

        public ActionResult CheckList()
        {
            return View();
        }
    }
}
