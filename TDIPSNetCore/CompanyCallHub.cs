﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TDIPSNetCore.Web
{
    public class CompanyCallHub : Hub
    {
        public async Task CallCompany(string message)
        {
            await Clients.All.SendAsync("CompanyCall", "test", message);
        }
    }
}
