using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TDIPSNetCore.Areas.Identity.Data;
using TDIPSNetCore.Data;
using Hangfire;
using Hangfire.PostgreSql;
using System.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication.AzureAD.UI;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Serialization;
using TDIPSNetCore.Web;
using TDIPS.Core.Interfaces;
using TDIPS.Infrastructure.Business_Logic;
using TDIPS;
using Newtonsoft.Json;
using TDIPS.Core.Models;

namespace TDIPSNetCore
{
    public class Startup
    {
        IVoucherRepository VoucherRepository = new VoucherRepository();
        IHangfireRepository HangfireRepository = new HangfireRepository();
        ISystemLogRepository SystemRepository = new SystemLogRepository();
        IMailRepository MailRepository = new MailRepository();
        IDynamicsRepository DynamicsRepository = new DynamicsRepository();
        GlobalClass GlobalClass = new GlobalClass();
        ICashflowRepository CashflowRepository = new CashflowRepository();
        private readonly GenerateDynamicsData _integration = new GenerateDynamicsData();


        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void SendEODMail()
        {
            GlobalClass GC = new GlobalClass();

            GC.BackgroundD65Integration(DateTime.Today, DateTime.Today.AddDays(1));

            GC.WriteReleasedToExcel("SYSTEM", DateTime.Today, DateTime.Today.AddDays(1), true);
        }

        public void IntegrateLedgerTransactions()
        {
            var cashflowMappings = CashflowRepository.GetCashflowAccountMappings();
            var dataAreaIdArr = new string[] { "SPAV", "SPCI", "SIL", "WBHI", "SPRF", "SSI" };

            foreach (var map in cashflowMappings)
            {
                for (int i = 0; i < dataAreaIdArr.Length; i++)
                {
                    var dynamics365Data = JsonConvert.DeserializeObject<IEnumerable<D365LedgerTransaction>>(_integration
                    .GenerateLedgerTransactionList(DateTime.Today.AddDays(-30), DateTime.Today.AddDays(1), dataAreaIdArr[i],
                    map.MainAccount, map.PurposeDimension));

                    DynamicsRepository.UpdateLedgerTransaction(map, dynamics365Data, dataAreaIdArr[i]);
                }
            }
        }

        public void IntegrateGeneralJournalOnlinePayments()
        {
            var mappings = DynamicsRepository.GetGeneralJournalMaps();

            var dataAreaIdArr = new string[] { "SPAV", "SPCI", "SIL", "WBHI", "SPRF", "SSI" };

            foreach (var map in mappings)
            {
                for (int i = 0; i < dataAreaIdArr.Length; i++)
                {
                    var dynamics365Data = JsonConvert.DeserializeObject<IEnumerable<D365LedgerTransaction>>(_integration
                    .GenerateLedgerTransactionList(DateTime.Today, DateTime.Today.AddDays(1), dataAreaIdArr[i],
                    map.MainAccount, null));

                    DynamicsRepository.UpdateOnlinePayments(map, dynamics365Data, dataAreaIdArr[i]);
                }
            }
        }

        public void IntegrateEmployeesReimbursements()
        {
            var reimbursements = JsonConvert.DeserializeObject<IEnumerable<EmployeesReimbursement>>
                (_integration.GenerateEmployeesReimbursements(DateTime.Today.AddDays(-90), DateTime.Today));

            DynamicsRepository.UpdateEmployeesReimbursements(reimbursements);
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews()
                .AddNewtonsoftJson(options => {
                    options.SerializerSettings.ContractResolver = null;
                })
                .AddRazorRuntimeCompilation();

            services.AddDbContext<TDIPSDbContext>(options => {
                options.UseNpgsql(Configuration.GetConnectionString("TDIPSDbContextConnection"));
            });

            services.AddHangfire(configuration => configuration
            .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
            .UseSimpleAssemblyNameTypeSerializer()
            .UseRecommendedSerializerSettings()
            .UsePostgreSqlStorage(Configuration.GetConnectionString("TDIPSDbContextConnection"), new PostgreSqlStorageOptions
            {
                //CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                //SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                //QueuePollInterval = TimeSpan.Zero,
                //UseRecommendedIsolationLevel = true,
                //DisableGlobalLocks = true
            }));

            JobStorage.Current = new PostgreSqlStorage(Configuration.GetConnectionString("TDIPSDbContextConnection"));

            services.AddHangfireServer();

            HangfireRepository.Reset();
            
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddSignalR();

            services.AddAuthentication()
                .AddAzureAD(options =>
                {
                    Configuration.Bind("AzureAd", options);
                });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("DenyNonTreasury", policy =>
                    policy.RequireRole("TREASURY MANAGER", "TREASURY LEAD", "TREASURY STAFF", "AUDIT"));
                options.AddPolicy("DenyAudit", policy =>
                    policy.RequireRole("TREASURY MANAGER", "TREASURY LEAD", "TREASURY STAFF"));
            });

            services.Configure<OpenIdConnectOptions>(AzureADDefaults.OpenIdScheme, options =>
            {
                options.Authority = options.Authority + "/v2.0/";
                options.TokenValidationParameters.ValidateIssuer = false;
                options.TokenValidationParameters.NameClaimType = "preferred_username";
            });

            services.ConfigureApplicationCookie(options => options.LoginPath = "/Account/LogIn");

            services.AddMvc(options =>
            {
                var defaultPolicy = new AuthorizationPolicyBuilder(new[] { AzureADDefaults.AuthenticationScheme, 
                    IdentityConstants.ApplicationScheme })
                  .RequireAuthenticatedUser()
                  .Build();
                options.Filters.Add(new AuthorizeFilter(defaultPolicy));
                options.EnableEndpointRouting = false;
            })

//                .AddRazorPagesOptions(o => o.Conventions.AddAreaFolderRouteModelConvention("Identity", "/Account/", model =>
//                {
//                    foreach (var selector in model.Selectors)
//                    {
//                        var attributeRouteModel = selector.AttributeRouteModel;
//                        attributeRouteModel.Order = -1;
//                        attributeRouteModel.Template = attributeRouteModel.Template.Remove(0, "Identity".Length);
//                    }
//                })
//)/*.SetCompatibilityVersion(CompatibilityVersion.Version_2_1);*/
            .ConfigureApiBehaviorOptions(options =>
            {
                options.SuppressInferBindingSourcesForParameters = true;
            })
            .AddJsonOptions(jsonOptions =>
            {
                jsonOptions.JsonSerializerOptions.PropertyNamingPolicy = null;
            })
              .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);


            RecurringJob.AddOrUpdate(() => SendEODMail(), "0 20 * * 1,2,3,4,5", TimeZoneInfo.Local);

            RecurringJob.AddOrUpdate(() => MailRepository.SendOtherOnlinePaymentsEODMail("SYSTEM", true),
                "0 20 * * 1,2,3,4,5", TimeZoneInfo.Local);

            RecurringJob.AddOrUpdate(() => VoucherRepository.CleanVouchers(), "30 0 * * *", TimeZoneInfo.Local);

            RecurringJob.AddOrUpdate(() => GlobalClass.
            BackgroundD65Integration(DateTime.Today.AddDays(-30),
            DateTime.Today.AddDays(1)), "0 19 * * *", TimeZoneInfo.Local);

            RecurringJob.AddOrUpdate(() => VoucherRepository.UpdateUncollectedVouchers(), "10 0 * * *", TimeZoneInfo.Local);

            RecurringJob.AddOrUpdate(() => DynamicsRepository.UpdateDataToBrains(), "30 22 * * *", TimeZoneInfo.Local);

            RecurringJob.AddOrUpdate(() => DynamicsRepository.UpdateCashflowsToBrains(), "0 22 * * *", TimeZoneInfo.Local);

            RecurringJob.AddOrUpdate(() => IntegrateLedgerTransactions(), "0 23 * * *", TimeZoneInfo.Local);

            RecurringJob.AddOrUpdate(() => IntegrateGeneralJournalOnlinePayments(), "0 23 * * *", TimeZoneInfo.Local);

            RecurringJob.AddOrUpdate(() => IntegrateEmployeesReimbursements(), "30 23 * * *", TimeZoneInfo.Local);


            //services.AddIdentity<ApplicationUser, IdentityRole>()
            //    .AddEntityFrameworkStores<TDIPSDbContext>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IBackgroundJobClient test)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseHangfireDashboard();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseMvc();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action}");
                endpoints.MapHub<CompanyCallHub>("/companycallhub");
            });
        }
    }
}
