﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TDIPS.Infrastructure.TDIPSDBContextModels;
using TDIPSNetCore.Areas.Identity.Data;

namespace TDIPSNetCore.Data
{
    public class TDIPSDbContext : IdentityDbContext<ApplicationUser>
    {
        public TDIPSDbContext(DbContextOptions<TDIPSDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

            builder.Entity<AspNetUserLogin>().HasKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId});



            //builder.Entity<AspNetUser>().HasMany(a => a.AspNetRoles)
            //               .WithRequired().HasForeignKey(con => con.EndCityId);
        }
    }
}
