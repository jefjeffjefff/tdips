﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace TDIPSNetCore.Areas.Identity.Data
{
    // Add profile data for application users by adding properties to the TDIPSUser class
    public class ApplicationUser : IdentityUser
    {
        public bool IsUserOnLeave { get; set; }
    }
}
