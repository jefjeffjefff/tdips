﻿using ClosedXML.Excel;
using Hangfire;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TDIPS;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.Business_Logic;

namespace TDIPSNetCore.Web
{
    public class GlobalClass
    {
        private readonly GenerateDynamicsData _integration = new GenerateDynamicsData();
        ISystemLogRepository SystemLogRepository = new SystemLogRepository();
        IVoucherRepository VoucherRepository = new VoucherRepository();
        IMailRepository MailRepository = new MailRepository();
        IEODRepository EODRepository = new EODRepository();
        IPaymentRepository PaymentRepository = new PaymentRepository();
        IEmployeeRepository EmployeeRepository = new EmployeeRepository();
        IOnlinePaymentRepository OnlinePaymentRepository = new OnlinePaymentRepository();
        IDynamicsRepository DynamicsRepository = new DynamicsRepository();

        public void InsertTableBorder(IXLWorksheet ws, string range, string borderColor = "#4B4B4B")
        {
            ws.Range(range).Style.Border.BottomBorder =
                XLBorderStyleValues.Thin;
            ws.Range(range).Style.Border.BottomBorderColor =
                XLColor.FromHtml(borderColor);

            ws.Range(range).Style.Border.RightBorder =
                XLBorderStyleValues.Thin;
            ws.Range(range).Style.Border.RightBorderColor =
                XLColor.FromHtml(borderColor);

            ws.Range(range).Style.Border.LeftBorder =
                XLBorderStyleValues.Thin;
            ws.Range(range).Style.Border.LeftBorderColor =
                XLColor.FromHtml(borderColor);

            ws.Range(range).Style.Border.TopBorder =
                XLBorderStyleValues.Thin;
            ws.Range(range).Style.Border.TopBorderColor =
                XLColor.FromHtml(borderColor);

            ws.Range(range).Style.Border.OutsideBorder =
                XLBorderStyleValues.Thin;
            ws.Range(range).Style.Border.OutsideBorderColor =
                XLColor.FromHtml(borderColor);
        }

        public string WriteForReleasingToExcel(string user, DateTime? date, bool isSendToMail = false)
        {
            DateTime Date = date ?? DateTime.Now;

            string projectDirectory = AppDomain.CurrentDomain.BaseDirectory;

            try
            {
                //Name of File 
                string fileName = Date.ToString("MMM dd, yyyy") + " For Releasing Check Report";
                string fullPath = projectDirectory + "SpreadSheets\\Reports\\";
                int nextRow = 0;
                int rangeFrom = 0;
                int rangeTo = 0;

                //Excel Areas to Format in Range Format
                Dictionary<string, string> dictAreas = new Dictionary<string, string>();

                var vouchers = VoucherRepository.GetForReleasingVoucherList("", DateTime.Today);

                using (XLWorkbook wb = new XLWorkbook())
                {
                    var filteredVouchers = vouchers
                    .Select(x => new
                    {
                        forReleasingDate = (x.releasingDate ?? DateTime.Now).ToString("MM/dd/yyyy"),
                        x.companyName,
                        x.voucherId,
                        x.entity,
                        x.amount,
                        x.bank
                    }).ToList();

                    var ws = wb.Worksheets.Add("Sheet1");

                    ws.Cell("A1").Value = "For Releasing Vouchers for the date of " + Date.ToString("MMM dd, yyyy");

                    rangeFrom = 3;

                    ws.Cell("A3").Value = "Entity";
                    ws.Cell("B3").Value = "Total Amount";

                    var entities = filteredVouchers.OrderBy(x => x.entity).ThenBy(x => x.bank)
                        .Select(x => x.entity + " - " + x.bank).Distinct().ToArray();
                    nextRow = ws.LastRowUsed().RowNumber() + 1;


                    for (int i = 0; i < entities.Length; i++)
                    {
                        var totalAmount = filteredVouchers.Where(x => (x.entity + " - " + x.bank) == entities[i]).AsEnumerable().Sum(x => x.amount);

                        ws.Cell("A" + nextRow).Value = entities[i];
                        ws.Cell("B" + nextRow).Value = String.Format("{0:n}", Math.Round(totalAmount ?? 0, 2));
                        nextRow = ws.LastRowUsed().RowNumber() + 1;
                    }

                    var grandTotal = filteredVouchers.AsEnumerable().Sum(x => x.amount);

                    nextRow = ws.LastRowUsed().RowNumber() + 1;
                    ws.Cell("A" + nextRow).Value = "Grand Total";
                    ws.Cell("B" + nextRow).Value = String.Format("{0:n}", Math.Round(grandTotal ?? 0, 2));

                    rangeTo = ws.LastRowUsed().RowNumber();

                    dictAreas["tblEntity"] = "A" + rangeFrom.ToString() + ":B" + rangeTo.ToString();

                    ws.Rows(rangeFrom + ":" + rangeTo).Style.Font.FontColor = XLColor.Red;

                    ws.Rows(rangeFrom + ":" + rangeTo).Style.Font.FontSize = 13;

                    InsertTableBorder(ws, "A" + rangeFrom.ToString() + ":B" + rangeTo.ToString());

                    ws.Rows(rangeTo.ToString()).Style.Font.Bold = true;

                    nextRow += 2;

                    ws.Range("A" + nextRow + ":" + "E" + nextRow).Style.Font.FontColor = XLColor.FromHtml("#ecf0f1");
                    ws.Range("A" + nextRow + ":" + "E" + nextRow).Style.Fill.BackgroundColor = XLColor.FromHtml("#333F4F");

                    ws.Cell("A" + nextRow).Value = "Releasing Date";
                    ws.Cell("B" + nextRow).Value = "Company Name";
                    ws.Cell("C" + nextRow).Value = "Voucher Id";
                    ws.Cell("D" + nextRow).Value = "Entity";
                    ws.Cell("E" + nextRow).Value = "Amount";

                    ws.Rows(nextRow.ToString()).Style.Font.FontSize = 12;

                    dictAreas["entireTblVoucher"] = "A" + nextRow + ":";
                    dictAreas["tblVoucher"] = "A" + (nextRow + 1) + ":";

                    nextRow++;

                    for (int i = 0; i < entities.Length; i++)
                    {
                        var totalAmount = filteredVouchers.Where(x => (x.entity + " - " + x.bank) == entities[i])
                            .AsEnumerable().Sum(x => x.amount);

                        ws.Cell("A" + nextRow).Value = filteredVouchers
                        .Where(x => (x.entity + " - " + x.bank) == entities[i])
                        .Select(x => new
                        {
                            forReleasingDate = x.forReleasingDate,
                            companyName = x.companyName,
                            voucherId = x.voucherId,
                            entity = x.entity + " - " + x.bank,
                            amount = String.Format("{0:n}", Math.Round(x.amount ?? 0, 2))
                        }).AsEnumerable();

                        nextRow = ws.LastRowUsed().RowNumber() + 1;
                        ws.Cell("D" + nextRow).Value = entities[i] + " Total";
                        ws.Cell("E" + nextRow).Value = String.Format("{0:n}", Math.Round(totalAmount ?? 0, 2));
                        ws.Rows(nextRow.ToString()).Style.Font.Bold = true;
                        ws.Rows(nextRow.ToString()).Style.Font.FontColor = XLColor.Red;

                        nextRow = ws.LastRowUsed().RowNumber() + 1;
                    }

                    dictAreas["tblVoucher"] = dictAreas["tblVoucher"] + "E" + ws.LastRowUsed().RowNumber();
                    dictAreas["entireTblVoucher"] = dictAreas["entireTblVoucher"] + "E" + ws.LastRowUsed().RowNumber();

                    nextRow = ws.LastRowUsed().RowNumber() + 2;
                    ws.Cell("D" + nextRow).Value = "Grand Total";
                    ws.Cell("E" + nextRow).Value = String.Format("{0:n}", Math.Round(grandTotal ?? 0, 2));
                    ws.Rows(nextRow.ToString()).Style.Font.FontColor = XLColor.Red;
                    ws.Rows(nextRow.ToString()).Style.Font.Bold = true;
                    ws.Rows(nextRow.ToString()).Style.Font.FontSize = 12;
                    ws.Range("D" + nextRow.ToString() + ":E" + nextRow.ToString()).Style.Fill.BackgroundColor =
                        XLColor.FromHtml("#F4F8FF");

                    dictAreas["grandTotal"] = "D" + nextRow.ToString() + ":E" + nextRow.ToString();

                    ws.Range(dictAreas["grandTotal"]).Style.Border.OutsideBorder =
                        XLBorderStyleValues.Thin;

                    ws.Range(dictAreas["grandTotal"]).Style.Border.OutsideBorderColor =
                        XLColor.Black;

                    ws.Row(1).Style.Font.FontSize = 15;
                    ws.Row(1).Style.Font.Bold = true;

                    ws.Rows().AdjustToContents();
                    ws.Columns().AdjustToContents();

                    ws.Rows().Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);


                    ws.Row(1).Height = 40;

                    ws.Rows(GetRowsFromRange(dictAreas["tblEntity"])).Height = 22;
                    ws.Rows(GetRowsFromRange(dictAreas["entireTblVoucher"])).Height = 20;
                    ws.Rows(GetRowsFromRange(dictAreas["grandTotal"])).Height = 24;

                    ws.Rows("3").Height = 24;
                    ws.Rows("3").Style.Font.FontColor = XLColor.FromHtml("#ecf0f1");

                    ws.Range(dictAreas["tblEntity"]).Style.Fill.BackgroundColor = XLColor.FromHtml("#F4F8FF");
                    ws.Range("A3:B3").Style.Fill.BackgroundColor = XLColor.FromHtml("#333F4F");

                    ws.Columns("A").Width = 23;

                    ws.Ranges(dictAreas["tblVoucher"]).Style.Fill.BackgroundColor = XLColor.FromHtml("#F4F8FF");
                    InsertTableBorder(ws, dictAreas["entireTblVoucher"]);

                    ws.Columns("A", "Z").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    ws.Rows("1").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);

                    wb.SaveAs(fullPath + fileName + ".xlsx");

                    using (MemoryStream stream = new MemoryStream())
                    {
                        wb.SaveAs(stream);

                        if (isSendToMail)
                        {
                            string result = MailRepository.SendForReleasingMail(user, Date, fullPath + fileName + ".xlsx");

                            BackgroundJob.Schedule(
                            () => DeleteExcelFile(fullPath + fileName + ".xlsx"),
                            TimeSpan.FromDays(1));

                            return result;
                        }

                        SystemLog sl = new SystemLog
                        {
                            logDate = DateTime.Now,
                            userId = user,
                            type = "REPORT",
                            description = "Sent a collected checks report.",
                            status = "SUCCESS"
                        };

                        SystemLogRepository.Add(sl);

                    }
                    return fileName;
                }
            }
            catch (Exception ex)
            {
                return "ERROR";
            }
        }

        public string GenerateSheetTitle(string checkType)
        {
            if (checkType == "STANDARD")
            {
                return "Standard Checks";
            }
            else if (checkType == "MANAGER")
            {
                return "Manager's Checks";
            }
            else
            {
                return checkType;
            }
        }

        public string WriteReleasedToExcel(string user, DateTime? dateFrom, DateTime? dateTo,
            bool isSendToMail = false, string[] checkTypes = null)
        {
            DateTime datefrom = dateFrom ?? DateTime.Today;
            DateTime dateto = dateTo ?? DateTime.Today.AddDays(1);

            string projectDirectory = AppDomain.CurrentDomain.BaseDirectory;

            try
            {
                //Name of File 
                string fileName = datefrom.ToString("MMM dd, yyyy") + " - " + dateto.ToString("MMM dd, yyyy hh-mm-ss") + " Collected Payments Report";
                string fullPath = projectDirectory + "SpreadSheets\\Reports\\";
                int nextRow = 0;
                int rangeFrom = 0;
                int rangeTo = 0;

                //Excel Areas to Format in Range Format
                Dictionary<string, string> dictAreas = new Dictionary<string, string>();

                var vouchers = VoucherRepository.GetCollectedChecks(datefrom, dateto);

                var employeeOnlinePayments = EmployeeRepository.GetEmployeeWithOnlinePaymentsList(datefrom, dateto);

                if (checkTypes == null)
                {
                    checkTypes = new string[3] { "STANDARD", "MANAGER", "EMPLOYEES"/*, "TELEGRAPHI" */};
                }

                //var generalOnlinePayments = OnlinePaymentRepository.GetGeneralOnlinePaymentList(DateTime.Today, DateTime.Today, false);

                //var modesArr = generalOnlinePayments.Select(x => x.modeOfPayment).Distinct().ToArray();


                foreach (var op in employeeOnlinePayments)
                {
                    var cc = new CollectedChecks
                    {
                        releasedDate = op.logDate,
                        companyName = op.Employee.employeeName,
                        entity = op.entity,
                        amount = op.amount,
                        bank = op.bank,
                        checkType = "EMPLOYEES"
                    };

                    vouchers.Add(cc);
                }

                var createdOnlinePayments = VoucherRepository.GetCreatedOnlinePayments(DateTime.Today.AddDays(-10), DateTime.Today.AddDays(1));

                foreach (var op in createdOnlinePayments)
                {
                    vouchers.Add(op);
                }

                //foreach (var op in generalOnlinePayments)
                //{
                //    var cc = new CollectedChecks
                //    {
                //        releasedDate = op.logDate,
                //        companyName = op.companyName,
                //        entity = op.entity,
                //        amount = op.amount,
                //        bank = op.bank,
                //        checkType = op.modeOfPayment,
                //        voucherId = op.invoiceVouchers
                //    };
                //    vouchers.Add(cc);
                //}


                var mappings = DynamicsRepository.GetGeneralJournalMaps();

                var dataAreaIdArr = new string[] { "SPAV", "SPCI", "SIL", "WBHI", "SPRF", "SSI" };

                //foreach (var map in mappings)
                //{
                //    for (int i = 0; i < dataAreaIdArr.Length; i++)
                //    {
                //        var dynamics365Data = JsonConvert.DeserializeObject<IEnumerable<D365LedgerTransaction>>(_integration
                //        .GenerateLedgerTransactionList(DateTime.Today.AddDays(-10), DateTime.Today, dataAreaIdArr[i],
                //        map.MainAccount, null));

                //        foreach (var item in dynamics365Data.ToList())
                //        {
                //            var cc = new CollectedChecks
                //            {
                //                releasedDate = item.transDate,
                //                companyName = item.accountName,
                //                entity = dataAreaIdArr[i],
                //                amount = item.amount,
                //                bank = map.Subsegment,
                //                segment = map.Segment,
                //                subsegment = map.Subsegment,
                //                checkType = "ONLINE",
                //                voucherId = item.voucher
                //            };  

                //            vouchers.Add(cc);
                //        }
                //    }
                //}

                var paymentTypes = vouchers.Select(x => x.checkType).Distinct().ToArray();

                using (XLWorkbook wb = new XLWorkbook())
                {
                    for (int c = 0; c < paymentTypes.Length; c++)
                    {
                        var filteredVouchers = vouchers.OrderBy(x => x.releasedDate)
                        .Select(x => new
                        {
                            releasedDate = (x.releasedDate ?? DateTime.Now).ToString("MM/dd/yyyy"),
                            x.companyName,
                            x.voucherId,
                            x.entity,
                            x.amount,
                            x.bank,
                            checkType = x.checkType ?? "STANDARD",
                            segment = x.segment ?? "",
                            subSegment = x.subsegment ?? "",
                            checkDate = x.checkDate,
                            checkNum = x.checkNum,
                            receipt = x.receiptNum
                        }).Where(x => x.checkType == paymentTypes[c]).ToList();


                        var ws = wb.Worksheets.Add(GenerateSheetTitle(paymentTypes[c]));

                        if (filteredVouchers.Count() == 0)
                        {
                            ws.Cell("A1").Value = "No Data.";
                            ws.Cell("A1").Style.Font.FontSize = 13;
                            ws.Rows().AdjustToContents();
                            ws.Columns().AdjustToContents();

                            continue;
                        }

                        ws.Cell("A1").Value = " Released Vouchers " + datefrom.ToString("MMM dd, yyyy") + " - " + dateto.ToString("MMM dd, yyyy");

                        rangeFrom = 3;

                        ws.Cell("A3").Value = "Entity";

                        ws.Cell("B3").Value = "Total Amount";

                        var entities = filteredVouchers.OrderBy(x => x.entity).ThenBy(x => x.bank)
                            .Select(x => x.entity + " - " + x.bank).Distinct().ToArray();
                        nextRow = ws.LastRowUsed().RowNumber() + 1;


                        for (int i = 0; i < entities.Length; i++)
                        {
                            var totalAmount = filteredVouchers.Where(x => (x.entity + " - " + x.bank) == entities[i]).AsEnumerable().Sum(x => x.amount);

                            ws.Cell("A" + nextRow).Value = entities[i];
                            ws.Cell("B" + nextRow).Value = String.Format("{0:n}", Math.Round(totalAmount ?? 0, 2));
                            nextRow = ws.LastRowUsed().RowNumber() + 1;
                        }

                        var grandTotal = filteredVouchers.AsEnumerable().Sum(x => x.amount);

                        nextRow = ws.LastRowUsed().RowNumber() + 1;
                        ws.Cell("A" + nextRow).Value = "Grand Total";
                        ws.Cell("B" + nextRow).Value = String.Format("{0:n}", Math.Round(grandTotal ?? 0, 2));

                        rangeTo = ws.LastRowUsed().RowNumber();

                        dictAreas["tblEntity"] = "A" + rangeFrom.ToString() + ":B" + rangeTo.ToString();

                        ws.Rows(rangeFrom + ":" + rangeTo).Style.Font.FontColor = XLColor.Red;

                        ws.Rows(rangeFrom + ":" + rangeTo).Style.Font.FontSize = 13;

                        InsertTableBorder(ws, "A" + rangeFrom.ToString() + ":B" + rangeTo.ToString());

                        ws.Rows(rangeTo.ToString()).Style.Font.Bold = true;

                        nextRow += 2;

                        ws.Cell("A" + nextRow).Value = "Released Date";
                        ws.Cell("B" + nextRow).Value = paymentTypes[c] == "EMPLOYEES" ? "Employee Name" : "Company Name";
                        ws.Cell("C" + nextRow).Value = "Voucher Id";
                        ws.Cell("D" + nextRow).Value = "Entity";

                        if (paymentTypes[c] == "STANDARD" || paymentTypes[c] == "MANAGER")
                        {
                            ws.Cell("E" + nextRow).Value = "Segment";
                            ws.Cell("F" + nextRow).Value = "Subsegment";
                            ws.Cell("G" + nextRow).Value = "Check Date";
                            ws.Cell("H" + nextRow).Value = "Check Number";
                            ws.Cell("I" + nextRow).Value = "Receipt";
                            ws.Cell("J" + nextRow).Value = "Amount";
                        }
                        else
                        {
                            ws.Cell("E" + nextRow).Value = "Subsegment";
                            ws.Cell("F" + nextRow).Value = "Amount";
                        }

                        ws.Range("A" + nextRow + ":" + GetExcelColumnName(ws.LastColumnUsed().ColumnNumber()) + nextRow).Style.Font.FontColor = XLColor.FromHtml("#ecf0f1");
                        ws.Range("A" + nextRow + ":" + GetExcelColumnName(ws.LastColumnUsed().ColumnNumber()) + nextRow).Style.Fill.BackgroundColor = XLColor.FromHtml("#333F4F");

                        ws.Rows(nextRow.ToString()).Style.Font.FontSize = 12;

                        dictAreas["entireTblVoucher"] = "A" + nextRow + ":";
                        dictAreas["tblVoucher"] = "A" + (nextRow + 1) + ":";

                        nextRow++;

                        for (int i = 0; i < entities.Length; i++)
                        {
                            var totalAmount = filteredVouchers.Where(x => (x.entity + " - " + x.bank) == entities[i])
                                .AsEnumerable().Sum(x => x.amount);

                            if (paymentTypes[c] == "STANDARD" || paymentTypes[c] == "MANAGER")
                            {
                                ws.Cell("A" + nextRow).Value = filteredVouchers
                                .Where(x => (x.entity + " - " + x.bank) == entities[i])
                                .Select(x => new
                                {
                                    releasedDate = x.releasedDate,
                                    companyName = x.companyName,
                                    voucherId = x.voucherId,
                                    entity = x.entity + " - " + x.bank,
                                    segment = x.segment,
                                    subsegment = x.subSegment,
                                    checknum = x.checkNum,
                                    checkdate = x.checkDate,
                                    receipt = x.receipt,
                                    amount = String.Format("{0:n}", Math.Round(x.amount ?? 0, 2))
                                }).AsEnumerable();
                            }
                            else
                            {
                                ws.Cell("A" + nextRow).Value = filteredVouchers
                                .Where(x => (x.entity + " - " + x.bank) == entities[i])
                                .Select(x => new
                                {
                                    releasedDate = x.releasedDate,
                                    companyName = x.companyName,
                                    voucherId = x.voucherId,
                                    entity = x.entity + " - " + x.bank,
                                    subsegment = x.subSegment,
                                    amount = String.Format("{0:n}", Math.Round(x.amount ?? 0, 2))
                                }).AsEnumerable();
                            }



                            nextRow = ws.LastRowUsed().RowNumber() + 1;
                            ws.Cell(GetExcelColumnName(ws.LastColumnUsed().ColumnNumber() - 1) + nextRow).Value = entities[i] + " Total";
                            ws.Cell(GetExcelColumnName(ws.LastColumnUsed().ColumnNumber()) + nextRow).Value = String.Format("{0:n}", Math.Round(totalAmount ?? 0, 2));
                            ws.Rows(nextRow.ToString()).Style.Font.Bold = true;
                            ws.Rows(nextRow.ToString()).Style.Font.FontColor = XLColor.Red;

                            nextRow = ws.LastRowUsed().RowNumber() + 1;
                        }

                        dictAreas["tblVoucher"] = dictAreas["tblVoucher"] + GetExcelColumnName(ws.LastColumnUsed().ColumnNumber())
                            + ws.LastRowUsed().RowNumber();
                        dictAreas["entireTblVoucher"] = dictAreas["entireTblVoucher"] + GetExcelColumnName(ws.LastColumnUsed().ColumnNumber())
                            + ws.LastRowUsed().RowNumber();

                        nextRow = ws.LastRowUsed().RowNumber() + 2;
                        ws.Cell(GetExcelColumnName(ws.LastColumnUsed().ColumnNumber() - 1) + nextRow)
                            .Value = "Grand Total";
                        ws.Cell(GetExcelColumnName(ws.LastColumnUsed().ColumnNumber()) + nextRow)
                            .Value = String.Format("{0:n}", Math.Round(grandTotal ?? 0, 2));
                        ws.Rows(nextRow.ToString()).Style.Font.FontColor = XLColor.Red;
                        ws.Rows(nextRow.ToString()).Style.Font.Bold = true;
                        ws.Rows(nextRow.ToString()).Style.Font.FontSize = 12;
                        ws.Range(GetExcelColumnName(ws.LastColumnUsed().ColumnNumber() - 1) + nextRow.ToString() + ":" +
                            GetExcelColumnName(ws.LastColumnUsed().ColumnNumber()) +
                            nextRow.ToString()).Style.Fill.BackgroundColor =
                            XLColor.FromHtml("#F4F8FF");

                        dictAreas["grandTotal"] = GetExcelColumnName(ws.LastColumnUsed().ColumnNumber() - 1)
                            + nextRow.ToString() + ":" + GetExcelColumnName(ws.LastColumnUsed().ColumnNumber())
                            + nextRow.ToString();

                        ws.Range(dictAreas["grandTotal"]).Style.Border.OutsideBorder =
                            XLBorderStyleValues.Thin;

                        ws.Range(dictAreas["grandTotal"]).Style.Border.OutsideBorderColor =
                            XLColor.Black;

                        ws.Row(1).Style.Font.FontSize = 15;
                        ws.Row(1).Style.Font.Bold = true;

                        ws.Rows().AdjustToContents();
                        ws.Columns().AdjustToContents();

                        ws.Rows().Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);


                        ws.Row(1).Height = 40;

                        ws.Rows(GetRowsFromRange(dictAreas["tblEntity"])).Height = 22;
                        ws.Rows(GetRowsFromRange(dictAreas["entireTblVoucher"])).Height = 20;
                        ws.Rows(GetRowsFromRange(dictAreas["grandTotal"])).Height = 24;

                        ws.Rows("3").Height = 24;
                        ws.Rows("3").Style.Font.FontColor = XLColor.FromHtml("#ecf0f1");

                        ws.Range(dictAreas["tblEntity"]).Style.Fill.BackgroundColor = XLColor.FromHtml("#F4F8FF");
                        ws.Range("A3:B3").Style.Fill.BackgroundColor = XLColor.FromHtml("#333F4F");

                        ws.Columns("A").Width = 23;

                        ws.Ranges(dictAreas["tblVoucher"]).Style.Fill.BackgroundColor = XLColor.FromHtml("#F4F8FF");
                        InsertTableBorder(ws, dictAreas["entireTblVoucher"]);


                        ws.Columns("A", "Z").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        ws.Rows("1").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);

                        if (paymentTypes[c] == "EMPLOYEES")
                        {
                            ws.Column(3).Delete();
                        }
                    }

                    var totalsByTypes = vouchers
                    .GroupBy(x => x.checkType).Select(y => new
                    {
                        checkType = y.First().checkType,
                        total = y.Sum(t => t.amount)
                    });

                    var ws2 = wb.AddWorksheet("Grand Total");

                    ws2.Cell("A1").Value = "Released Vouchers " + datefrom.ToString("MMM dd, yyyy") + " - " + dateto.ToString("MMM dd, yyyy");
                    ws2.Row(1).Height = 40;
                    ws2.Row(1).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                    ws2.Row(1).Style.Font.Bold = true;
                    ws2.Row(1).Style.Font.FontSize = 15;
                    ws2.Range("A1:C1").Merge();

                    ws2.Cell("A3").Value = "Type";
                    ws2.Cell("B3").Value = "Total Amount";

                    ws2.Row(3).Height = 25;
                    ws2.Row(3).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                    ws2.Row(3).Style.Font.FontColor = XLColor.FromHtml("#ECF0F1");
                    ws2.Range("A3:B3").Style.Fill.BackgroundColor = XLColor.FromHtml("#2C3541");

                    nextRow = 4;

                    for (int i = 0; i < paymentTypes.Length; i++)
                    {
                        var typeDetails = totalsByTypes.FirstOrDefault(x => x.checkType == paymentTypes[i]);

                        var totalAmountByType = (typeDetails == null ? new decimal(0) : typeDetails.total);

                        ws2.Cell("A" + nextRow).Value = GenerateSheetTitle(paymentTypes[i]);
                        ws2.Cell("B" + nextRow).Value = string.Format("{0:n}", Math.Round((totalAmountByType ?? 0), 2));

                        nextRow++;
                    }

                    var totalAmountCT = totalsByTypes.AsEnumerable().Sum(x => x.total);

                    ws2.Cell("A" + nextRow).Value = "Grand Total";
                    ws2.Cell("B" + nextRow).Value = string.Format("{0:n}", Math.Round((totalAmountCT ?? 0), 2));

                    ws2.Row(nextRow).Style.Font.Bold = true;

                    ws2.Range("A3:B" + nextRow);

                    ws2.Rows("3:" + nextRow).Style.Font.FontSize = 13;
                    ws2.Rows("4:" + nextRow).Style.Font.FontSize = 13;
                    ws2.Range("A4:" + "B" + nextRow).Style.Fill.BackgroundColor = XLColor.FromHtml("#F4F8FF");
                    ws2.Rows(3 + ":" + nextRow).Height = 22;

                    ws2.Rows(4 + ":" + nextRow).Style.Font.FontColor = XLColor.Red;

                    ws2.Column(1).Width = 22;
                    ws2.Column(2).Width = 20;

                    ws2.Rows("3:" + nextRow).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                    InsertTableBorder(ws2, "A3:B" + nextRow);

                    ws2.Rows().Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);

                    wb.SaveAs(fullPath + fileName + ".xlsx");

                    using (MemoryStream stream = new MemoryStream())
                    {
                        wb.SaveAs(stream);
                        //Return xlsx Excel File

                        if (isSendToMail)
                        {
                            string result = MailRepository.SendEODMail(vouchers, user, user == "SYSTEM" ? true : false, fullPath + fileName + ".xlsx");

                            if (result == "SUCCESS")
                            {
                                EODRepository.Add(user);
                            }

                            BackgroundJob.Schedule(
                            () => DeleteExcelFile(fullPath + fileName + ".xlsx"),
                            TimeSpan.FromDays(1));

                            return result;
                        }

                        SystemLog sl = new SystemLog
                        {
                            logDate = DateTime.Now,
                            userId = user,
                            type = "REPORT",
                            description = "Requested a report for collected checks (excel)",
                            status = "SUCCESS"
                        };

                        SystemLogRepository.Add(sl);

                        return fileName;
                    }
                }
            }
            catch (Exception ex)
            {
                return "ERROR";
            }
        }

        public void DeleteExcelFile(string fullpath)
        {
            if (System.IO.File.Exists(fullpath))
            {
                System.IO.File.Delete(fullpath);
            }
        }

        public string GetRowsFromRange(string range)
        {
            var result = range.Split(':');
            return result[0].Substring(1, result[0].Length - 1) + ":" + result[1].Substring(1, result[1].Length - 1);
        }

        public void BackgroundD65Integration(DateTime dateFrom, DateTime dateTo)
        {
            var forReleasingPayments = VoucherRepository.GetForReleasingVoucherList();
            var cancelledVouchersArr = new List<string>();

            //var dateFrom = DateTime.Today.AddDays(-30);
            //var dateTo = DateTime.Today.AddDays(1);

            SystemLog sl = new SystemLog
            {
                userId = "SYSTEM",
                type = "INTEGRATE PAYMENTS",
                description = "Integrated payments details with D365 (" + dateFrom.ToString("dd MMM yyyy") + " - " +
                    dateTo.ToString("dd MMM yyyy") + ")",
                status = "COMPLETED"
            };

            try
            {
                var dynamics365Data = JsonConvert.DeserializeObject<IEnumerable<D365IntegratedPayment>>
                (_integration.GeneratePayments(dateFrom, dateTo));

                int counter = 0;

                foreach (var d in dynamics365Data)
                {
                    //track duplicated voucher errors;
                    //if mode of payment is check status should be null or paid only
                    //if mode of payment is not check any status will be accepted

                    if (d.chequeStatus == "Paid" || d.chequeStatus == "PAID")
                    {
                        string result = PaymentRepository.Add(d);

                        if (result == "SUCCESS")
                        {
                            counter++;
                        }
                    }
                    else
                    {
                        if (d.chequeStatus.ToUpper() == "CANCELED" &&
                            forReleasingPayments.Where(x => x.voucherId == d.voucherId).Count() > 0)
                        {
                            cancelledVouchersArr.Add(d.voucherId);
                        }

                        string result = VoucherRepository.AddVoucherStatus(d.voucherId, d.chequeStatus.ToUpper(), "SYSTEM", d);

                        string jobId = BackgroundJob.Enqueue(() => VoucherRepository.CancelVoucherStatus(d.voucherId));

                        if (result == "SUCCESS")
                        {
                            counter++;
                        }
                    }
                }

                if (cancelledVouchersArr.Count > 0)
                {
                    MailRepository.SendCanceledVoucherNoticeMail(forReleasingPayments.Where(x =>
                    cancelledVouchersArr.Contains(x.voucherId)).ToList());
                }
            }
            catch (Exception ex)
            {
                sl.description = ex.Message;
            }

            SystemLogBusLogic.Logs.Add(sl);
        }

        private string GetExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return columnName;
        }
    }
}
