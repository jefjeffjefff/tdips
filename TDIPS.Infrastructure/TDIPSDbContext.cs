﻿using TDIPS.Infrastructure.TDIPSDBContextModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDIPS.Infrastructure
{
    public class TDIPSDbContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql("Server=localhost;Port=5432;Database=TDIPS;User Id=postgres;Password=Wushu147;", builder => {
                    builder.EnableRetryOnFailure(20, TimeSpan.FromSeconds(10), null);
                });
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

            builder.HasDefaultSchema("dbo");
            builder.Entity<Hash>().HasKey(t => new { t.Key, t.Field });
            //builder.Entity<AspNetUserLogin>().HasNoKey();
        }

        public virtual DbSet<Hash> Hashes { get; set; }

        //[NotMapped]
        //public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        //[NotMapped]
        //public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        //[NotMapped]
        //public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        //[NotMapped]
        //public virtual DbSet<AspNetUser> AspNetUsers { get; set; }

        public virtual DbSet<tblAnnouncement> tblAnnouncements { get; set; }
        public virtual DbSet<tblBank> tblBanks { get; set; }
        public virtual DbSet<tblBatchMail> tblBatchMails { get; set; }
        public virtual DbSet<tblCashflowAccountMapping> tblCashflowAccountMappings { get; set; }
        public virtual DbSet<tblCollectedCheck> tblCollectedChecks { get; set; }
        public virtual DbSet<tblCollector> tblCollectors { get; set; }
        public virtual DbSet<tblComposedMail> tblComposedMails { get; set; }
        public virtual DbSet<tblD365IntegratedPayment> tblD365IntegratedPayment { get; set; }
        public virtual DbSet<tblDailyCashFlow> tblDailyCashFlows { get; set; }
        public virtual DbSet<tblDailyCashFlowBank> tblDailyCashFlowBanks { get; set; }
        public virtual DbSet<tblDailyCashFlowBankTotal> tblDailyCashFlowBankTotals { get; set; }
        public virtual DbSet<tblDailyCashFlowBankTransaction> tblDailyCashFlowBankTransactions { get; set; }
        public virtual DbSet<tblECREARating> tblECREARatings { get; set; }
        public virtual DbSet<tblEmployee> tblEmployees { get; set; }
        public virtual DbSet<tblEmployeeOnlinePayment> tblEmployeeOnlinePayments { get; set; }
        public virtual DbSet<tblEODValidationLog> tblEODValidationLogs { get; set; }
        public virtual DbSet<tblExcelUploadedPayment> tblExcelUploadedPayments { get; set; }
        public virtual DbSet<tblGeneralJournalMapping> tblGeneralJournalMappings { get; set; }
        public virtual DbSet<tblGeneralOnlinePayment> tblGeneralOnlinePayments { get; set; }
        public virtual DbSet<tblGJBank> tblGJBanks { get; set; }
        public virtual DbSet<tblGJTransaction> tblGJTransactions { get; set; }
        public virtual DbSet<tblHoldRequest> tblHoldRequests { get; set; }
        public virtual DbSet<tblInvoiceCorrection> tblInvoiceCorrections { get; set; }
        public virtual DbSet<tblMail> tblMails { get; set; }
        public virtual DbSet<tblMailLog> tblMailLogs { get; set; }
        public virtual DbSet<tblNumberCall> tblNumberCalls { get; set; }
        public virtual DbSet<tblOtherOnlinePayment> tblOtherOnlinePayments { get; set; }
        public virtual DbSet<tblOtherOnlinePaymentBatch> tblOtherOnlinePaymentBatches { get; set; }
        public virtual DbSet<tblOtherRecipientEmail> tblOtherRecipientEmails { get; set; }
        public virtual DbSet<tblProjectionCashflow> tblProjectionCashflows { get; set; }
        public virtual DbSet<tblProjectionCashflowSegment> tblProjectionCashflowSegments { get; set; }
        public virtual DbSet<tblProjectionCashflowTransaction> tblProjectionCashflowTransactions { get; set; }
        public virtual DbSet<tblQueue> tblQueues { get; set; }
        public virtual DbSet<tblReceipt> tblReceipts { get; set; }
        public virtual DbSet<tblSystemConfiguration> tblSystemConfigurations { get; set; }
        public virtual DbSet<tblSystemLog> tblSystemLogs { get; set; }
        public virtual DbSet<tblUnreleasedGeneralJournal> tblUnreleasedGeneralJournals { get; set; }
        public virtual DbSet<tblVendor> tblVendors { get; set; }
        public virtual DbSet<tblVideo> tblVideos { get; set; }
        public virtual DbSet<tblVoucherRequest> tblVoucherRequests { get; set; }
        public virtual DbSet<tblVoucherStatu> tblVoucherStatus { get; set; }
        public virtual DbSet<tblVoucherStatusBatch> tblVoucherStatusBatches { get; set; }
    }
}
