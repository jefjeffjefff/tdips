﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Npgsql;
using TDIPS.Core.Models;

namespace TDIPS.Infrastructure
{
    public class PSQLControl
    {
        // CREATE YOUR DB CONNECTION
        private NpgsqlConnection DBCon = new NpgsqlConnection("Host=localhost;Username=postgres;Password=Wushu147;Database=TDIPS");

        // PREPARE DB COMMAND
        private NpgsqlCommand DBCmd;

        // DB DATA
        public NpgsqlDataAdapter DBDA;
        public DataTable DBDT;

        // QUERY PARAMETERS
        public List<NpgsqlParameter> Params = new List<NpgsqlParameter>();

        // QUERY STATISTICS
        public int RecordCount;
        public string Exception;


        //public List<Parameter> Parameters = new List<Parameter>();

        //public class Parameter
        //{
        //    public string Name { get; set; }
        //    public string Value { get; set; }
        //}

        public void ExecQuery(string Query)
        {
            // RESET QUERY STATS
            RecordCount = 0;
            Exception = "";

            //if (SystemConfiguration.Environment == "LOCAL" || SystemConfiguration.Environment == "TESTING")
            //{
            //    Query = "USE [TDIPSTEST] " + Query;
            //}
            //else if (SystemConfiguration.Environment == "PRODUCTION")
            //{
            //    Query = "USE [TDIPS] " + Query;
            //}

            try
            {
                // OPEN A CONNECTION
                DBCon.Open();

                // CREATE DB COMMAND
                DBCmd = new NpgsqlCommand(Query, DBCon);

                //foreach (var param in Parameters)
                //{
                //    DBCmd.Parameters.AddWithValue(param.Name, param.Value);
                //}

                DBCmd.CommandTimeout = 600;

                // LOAD PARAMS INTO DB COMMAND
                Params.ForEach(p => DBCmd.Parameters.Add(p));

                // CLEAR PARAMS LIST
                Params.Clear();

                // EXECUTE COMMAND & FILL DATATABLE
                DBDT = new DataTable();
                DBDA = new NpgsqlDataAdapter(DBCmd);
                RecordCount = DBDA.Fill(DBDT);
            }
            catch (System.Exception ex)
            {
                Exception = ex.Message;
            }

            // CLOSE YOUR CONNECTION
            if (DBCon.State == ConnectionState.Open)
                DBCon.Close();
        }

        // INCLUDE QUERY & COMMAND PARAMETERS
        public void AddParam(string Name, object Value)
        {
            //Parameters.Add(new Parameter { 
            //    Name = Name,
            //    Value = Value
            //});

            NpgsqlParameter NewParam = new NpgsqlParameter(Name, Value ?? DBNull.Value);
            Params.Add(NewParam);
        }
    }
}
