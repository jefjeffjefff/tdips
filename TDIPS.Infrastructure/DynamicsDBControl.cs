﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using TDIPS.Core.Models;


namespace TDIPS.Infrastructure
{
    public class DynamicsDBControl
    {
        // CREATE YOUR DB CONNECTION
        private SqlConnection DBCon = new SqlConnection("Data source=dynamicsutilv2.southeastasia.cloudapp.azure.com; " +
                    "Database=TDIPSTEST;User Id=Dynamics;Password=EW=XU9e~&nq&");

        // PREPARE DB COMMAND
        private SqlCommand DBCmd;

        // DB DATA
        public SqlDataAdapter DBDA;
        public DataTable DBDT;

        // QUERY PARAMETERS
        public List<SqlParameter> Params = new List<SqlParameter>();

        // QUERY STATISTICS
        public int RecordCount;
        public string Exception;

        public void ExecQuery(string Query)
        {
            // RESET QUERY STATS
            RecordCount = 0;
            Exception = "";

            if (SystemConfiguration.Environment == "LOCAL" || SystemConfiguration.Environment == "TESTING")
            {
                Query = "USE [TDIPSTEST] " + Query;
            }
            else if (SystemConfiguration.Environment == "PRODUCTION")
            {
                Query = "USE [TDIPS] " + Query;
            }

            try
            {
                // OPEN A CONNECTION
                DBCon.Open();

                // CREATE DB COMMAND
                DBCmd = new SqlCommand(Query, DBCon);
                DBCmd.CommandTimeout = 600;

                // LOAD PARAMS INTO DB COMMAND
                Params.ForEach(p => DBCmd.Parameters.Add(p));

                // CLEAR PARAMS LIST
                Params.Clear();

                // EXECUTE COMMAND & FILL DATATABLE
                DBDT = new DataTable();
                DBDA = new SqlDataAdapter(DBCmd);
                RecordCount = DBDA.Fill(DBDT);
            }
            catch (System.Exception ex)
            {
                Exception = ex.Message;
            }

            // CLOSE YOUR CONNECTION
            if (DBCon.State == ConnectionState.Open)
                DBCon.Close();
        }

        // INCLUDE QUERY & COMMAND PARAMETERS
        public void AddParam(string Name, object Value)
        {
            SqlParameter NewParam = new SqlParameter(Name, (Value ?? DBNull.Value));
            Params.Add(NewParam);
        }
    }
}
