﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Core.Models.SS_Entities;
using TDIPS.Core.Models.SS_HELPERS;
using TDIPS.Infrastructure.TDIPSDBContextModels;

namespace TDIPS.Infrastructure.Business_Logic
{
    public class VoucherRepository : IVoucherRepository
    {
        TDIPSDbContext db = new TDIPSDbContext();
        tblVoucherStatu tblVs;
        tblVoucherStatusBatch tblVSB;
        tblD365IntegratedPayment tblD365;
        tblReceipt tblR;
        tblVoucherRequest tblVR;
        IMailRepository MailRepository = new MailRepository();
        IPaymentRepository PaymentRepository = new PaymentRepository();

        public string[] AcceptApprovalRequest(int requestedBatchId, int batchId, string userId)
        {
            if (db.tblVoucherStatusBatches.SingleOrDefault(x => x.batchId == requestedBatchId).isProcessed == true)
            {
                return null;
            }

            List<Voucher> vouchers = db.tblVoucherStatus.Select(x => new Voucher
            {
                batchId = x.batchId,
                voucherId = x.voucherId,
                releasingDate = x.releasingDate,
                status = x.status ?? "UNRELEASED"
            }
            ).Where(x => x.batchId == requestedBatchId && (x.status == "FOR VERIFICATION" || x.status == "FOR APPROVAL")).ToList<Voucher>();

            try
            {
                string[] arrVoucher = new string[vouchers.Count()];
                var index = 0;

                string[] activeSEANCodeList = db.tblVoucherStatus.Where(x => x.status.ToUpper() == "FOR RELEASING").Select(x => x.SEANCode).ToArray();
                foreach (var v in vouchers)
                {
                    bool isExisting = true;

                    tblVs = new tblVoucherStatu
                    {
                        voucherId = v.voucherId,
                        batchId = batchId,
                        releasingDate = v.releasingDate,
                        logDate = DateTime.Now,
                        status = "FOR RELEASING",
                        userId = userId
                    };

                    while (isExisting == true)
                    {
                        var code = RandomString(6);

                        if (activeSEANCodeList.Contains(code))
                        {
                            continue;
                        }

                        isExisting = false;
                        tblVs.SEANCode = code;
                    }

                    db.tblVoucherStatus.Add(tblVs);
                    db.SaveChanges();

                    arrVoucher[index] = v.voucherId + "?" + tblVs.SEANCode;

                    index++;
                }

                SystemLog sl = new SystemLog();
                sl.userId = userId;
                sl.type = "ACCEPT APPROVAL REQUEST";
                sl.description = "Accepted an approval request.";
                sl.status = "COMPLETED";
                SystemLogBusLogic.Logs.Add(sl);

                if (index == 0)
                {
                    return null;
                }
                else
                {
                    return arrVoucher;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string AcceptVerificationRequest(int requestedBatchId, int batchId, string userId)
        {
            if (db.tblVoucherStatusBatches.SingleOrDefault(x => x.batchId == requestedBatchId && x.status == "FOR VERIFICATION").isProcessed == true)
            {
                return "REQUEST IS ALREADY DONE";
            }

            List<Voucher> vouchers = db.tblVoucherStatus.Select(x => new Voucher
            {
                batchId = x.batchId,
                voucherId = x.voucherId,
                releasingDate = x.releasingDate,
                status = x.status
            }
            ).Where(x => x.batchId == requestedBatchId && x.status == "FOR VERIFICATION").ToList<Voucher>();

            try
            {
                foreach (var v in vouchers)
                {
                    tblVs = new tblVoucherStatu();
                    tblVs.voucherId = v.voucherId;
                    tblVs.batchId = batchId;
                    tblVs.releasingDate = v.releasingDate;
                    tblVs.logDate = DateTime.Now;
                    tblVs.status = "FOR APPROVAL";
                    tblVs.userId = userId;
                    db.tblVoucherStatus.Add(tblVs);
                }

                db.tblVoucherRequests
               .Where(x => x.batchId == requestedBatchId)
               .ToList()
               .ForEach(a => a.batchId = batchId);

                db.SaveChanges();

                SystemLog sl = new SystemLog();
                sl.userId = userId;
                sl.type = "ACCEPT VERIFICATION REQUEST";
                sl.description = "Accepted a verification request.";
                sl.status = "COMPLETED";
                SystemLogBusLogic.Logs.Add(sl);

                return "SUCCESS";
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int DeclinedVerificationRequestNotification()
        {
            return db.tblVoucherStatusBatches.Where(x => x.isProcessed != true && x.isOpened != true && x.batchStatus == "DECLINED").ToList().Count();
        }

        public string DeclineRequest(int declinedBatchId, int batchId, string userId)
        {
            tblVSB = db.tblVoucherStatusBatches.FirstOrDefault(x => x.batchId == declinedBatchId && (x.status == "FOR VERIFICATION" || x.status == "FOR APPROVAL"));

            if (tblVSB.isProcessed ?? false == true)
            {
                return "REQUEST IS ALREADY DONE";
            }

            List<Voucher> vouchers = db.tblVoucherStatus.Select(x => new Voucher
            {
                batchId = x.batchId,
                voucherId = x.voucherId,
                releasingDate = x.releasingDate,
                status = x.status
            }
            ).Where(x => (x.batchId == declinedBatchId) && (x.status == "FOR VERIFICATION" || x.status == "FOR APPROVAL")).ToList<Voucher>();

            try
            {
                foreach (var v in vouchers)
                {
                    tblVs = new tblVoucherStatu();
                    tblVs.voucherId = v.voucherId;
                    tblVs.batchId = batchId;
                    tblVs.releasingDate = v.releasingDate;
                    tblVs.logDate = DateTime.Now;
                    tblVs.status = "UNRELEASED";
                    tblVs.userId = userId;
                    db.tblVoucherStatus.Add(tblVs);
                }

                db.tblVoucherRequests
               .Where(x => x.batchId == declinedBatchId)
               .ToList()
               .ForEach(a => a.batchId = batchId);

                db.SaveChanges();

                SystemLog sl = new SystemLog();
                sl.userId = userId;
                sl.type = "DECLINED REQUEST";
                sl.description = "Declined a voucher releasing request.";
                sl.status = "COMPLETED";
                SystemLogBusLogic.Logs.Add(sl);

                return "SUCCESS";
            }
            catch (Exception)
            {
                throw;
            }
        }

        public object GetVoucherIDListFromBatch(int batchId = 0, string status = "")
        {
            return db.tblVoucherRequests.Where(x => x.batchId == batchId).Select(x => x.voucherId).ToArray();
        }

        public object GetVoucherList(int batchId = 0, string[] status = null, string vendorCode = "", bool includeHoldRequest = false)
        {
            if (status == null)
            {
                status = new string[9];
                status[0] = "UNRELEASED";
                status[1] = "FOR VERIFICATION";
                status[2] = "FOR APPROVAL";
                status[3] = "FOR RELEASING";
                status[4] = "RELEASED";
                status[5] = "UNCOLLECTED";
                status[6] = "ON HOLD";
                status[7] = "REVERSED";
                status[8] = "REQUESTED FOR HOLD";
            }

            object fullList = db.tblExcelUploadedPayments.Select(x => new
            {
                dataAreaId = x.dataAreaId.ToUpper(),
                vendorCode = db.tblVendors.FirstOrDefault(v => v.vendorCode.Contains(x.vendorCode) && v.isDeleted == false).vendorCode ?? "-",
                paymentId = x.paymentId,
                bankName = x.bankName,
                voucherId = x.voucherId,
                amount = x.amount,
                methodOfPayment = x.methodOfPayment,
                chequeNumber = x.chequeNumber,
                chequeDate = x.chequeDate,
                companyName = (db.tblVendors.FirstOrDefault(v => v.vendorCode.Contains(x.vendorCode)).companyName) ?? "Company not found.",
                source = "EXCEL",
                status = (db.tblVoucherStatus.Where(vi => vi.voucherId == x.voucherId && (includeHoldRequest ? vi.status != "Jeff" : vi.status != "REQUESTED FOR HOLD"))
                .OrderByDescending(d => d.logDate).FirstOrDefault().status) ?? "UNRELEASED"
            }).Union(db.tblD365IntegratedPayment.Select(y => new
            {
                dataAreaId = y.dataAreaId.ToUpper(),
                vendorCode = db.tblVendors.FirstOrDefault(v => v.vendorCode.Contains(y.recipientAccountNum) && v.isDeleted == false).vendorCode,
                paymentId = y.paymentId,
                bankName = y.accountId,
                voucherId = y.voucherId,
                amount = y.amountCur,
                methodOfPayment = "",
                chequeNumber = y.chequeNum,
                chequeDate = y.chequeDate,
                companyName = y.bankRecipientName,
                source = "D365",
                status = (db.tblVoucherStatus.Where(vi => vi.voucherId == y.voucherId).OrderByDescending(d => d.logDate)
                            .FirstOrDefault().status) ?? "UNRELEASED"
            })).Join(
                db.tblVoucherStatus.GroupBy(a => a.voucherId).Select(b => b.OrderByDescending(a => a.logDate).FirstOrDefault()),
                tblP => tblP.voucherId,
                tblVS => tblVS.voucherId,
                (tblP, tblVS) => new
                {
                    companyName = tblP.companyName,
                    vendorCode = tblP.vendorCode,
                    paymentId = tblP.paymentId,
                    bankName = tblP.bankName ?? "Unknown-Bank",
                    voucherId = tblP.voucherId,
                    amount = tblP.amount,
                    methodOfPayment = tblP.methodOfPayment,
                    chequeNumber = tblP.chequeNumber,
                    chequeDate = tblP.chequeDate,
                    status = tblP.status,
                    batchId = tblVS.batchId,
                    releasingDate = tblVS.releasingDate,
                    SEANCode = tblVS.SEANCode,
                    dataAreaId = tblP.dataAreaId == "" ? "Unknown-Entity" : tblP.dataAreaId.ToUpper()
                }
            ).Where(z => (batchId == 0 ? z.batchId >= 0 : z.batchId == batchId) && (status.Contains(z.status))
            && (vendorCode == "" ? z.vendorCode != "" : z.vendorCode.Contains(vendorCode))).ToList();
            return fullList;
        }

        public List<VoucherStatusBatch> GetVoucherStatusBatchList(string[] status = null, string[] batchStatus = null, bool includeCompleted = false)
        {
            if (status == null)
            {
                status = new string[8];
                status[0] = "UNRELEASED";
                status[1] = "FOR VERIFICATION";
                status[2] = "FOR APPROVAL";
                status[3] = "VERIFIED";
                status[4] = "RELEASED";
                status[5] = "UNCOLLECTED";
                status[6] = "ON HOLD";
                status[7] = "REVERSED";
            }

            if (batchStatus == null || batchStatus.Length == 0)
            {
                batchStatus = new string[3];
                batchStatus[0] = "";
                batchStatus[1] = "NOT PROCESSED";
                batchStatus[2] = "DECLINED";
            }

            var voucherBatches = db.tblVoucherStatusBatches.Select(x => new VoucherStatusBatch
            {
                batchId = x.batchId,
                logDate = x.logDate,
                userId = x.userId,
                note = x.note,
                status = x.status,
                batchStatus = x.batchStatus ?? "NOT PROCESSED",
                isOpened = x.isOpened,
                isProcessed = x.isProcessed ?? false
            }
            ).Where(x => status.Contains(x.status) && batchStatus.Contains(x.batchStatus) && x.isProcessed == includeCompleted).ToList();
            return voucherBatches;
        }

        public void OpenDeclinedVerificationRequestNotification()
        {
            db.tblVoucherStatusBatches.Where(x => x.batchStatus == "DECLINED").ToList()
                .ForEach(y => y.isOpened = true);

            db.SaveChanges();
        }

        public string RequestForVerification(Voucher[] vouchers, DateTime releasingDate, int batchId, string userId)
        {
            try
            {
                foreach (var v in vouchers)
                {
                    if (v.status.ToUpper() == "UNRELEASED" || v.status.ToUpper() == "UNCOLLECTED" || v.status.ToUpper() == "DECLINED" || v.status == "REQUESTED FOR HOLD")
                    {
                        tblVs = new tblVoucherStatu();
                        tblVs.voucherId = v.voucherId;
                        tblVs.batchId = batchId;
                        tblVs.releasingDate = releasingDate;
                        tblVs.logDate = DateTime.Now;
                        tblVs.userId = userId;
                        tblVs.status = "FOR VERIFICATION";
                        db.tblVoucherStatus.Add(tblVs);

                        tblVR = new tblVoucherRequest();
                        tblVR.batchId = batchId;
                        tblVR.voucherId = v.voucherId;
                        tblVR.vendorCode = v.vendorCode;
                        tblVR.companyName = v.companyName;
                        tblVR.entity = v.entity;
                        tblVR.bank = v.bank;
                        tblVR.amount = v.amount;
                        tblVR.logDate = DateTime.Now;
                        tblVR.status = "FOR VERIFICATION";
                        tblVR.releasingDate = releasingDate;
                        tblVR.checkDate = v.checkDate;
                        tblVR.checkNum = v.checkNum;
                        db.tblVoucherRequests.Add(tblVR);
                    }
                }

                db.SaveChanges();

                SystemLog sl = new SystemLog();
                sl.userId = userId;
                sl.type = "REQUEST VERIFICATION";
                sl.description = "Requested for a verification.";
                sl.status = "COMPLETED";
                SystemLogBusLogic.Logs.Add(sl);

                return "SUCCESS";
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int SaveVoucherStatusBatch(string userId, string note, string status, string batchStatus = "")
        {
            tblVSB = new tblVoucherStatusBatch();
            tblVSB.logDate = DateTime.Now;
            tblVSB.userId = userId;
            tblVSB.note = note;
            tblVSB.status = status;
            tblVSB.batchStatus = batchStatus;
            db.tblVoucherStatusBatches.Add(tblVSB);
            db.SaveChanges();
            return tblVSB.batchId;
        }

        public void UpdateVoucherStatusBatch(int batchId, string batchStatus = "")
        {
            tblVSB = db.tblVoucherStatusBatches.SingleOrDefault(x => x.batchId == batchId);
            tblVSB.isProcessed = true;
            if (batchStatus != "")
            {
                tblVSB.batchStatus = batchStatus;
            }

            db.SaveChanges();
        }

        private static Random random = new Random();


        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHJKLMNPQRSTUVWXYZ23456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public object ReleaseVoucher(ReleasedVoucher[] model, string userId)
        {
            try
            {
                foreach (var r in model)
                {
                    tblVs = new tblVoucherStatu
                    {
                        voucherId = r.VoucherId,
                        status = "RELEASED",
                        logDate = DateTime.Now,
                        userId = userId
                    };
                    db.tblVoucherStatus.Add(tblVs);
                    db.SaveChanges();

                    tblR = new tblReceipt
                    {
                        voucherId = r.VoucherId,
                        receiptNo = r.ReceiptNo,
                        receiptType = r.ReceiptType
                    };
                    db.tblReceipts.Add(tblR);
                    db.SaveChanges();
                }

                SystemLog sl = new SystemLog();
                sl.userId = userId;
                sl.type = "RELEASE VOUCHER";
                sl.description = "Released voucher/s.";
                sl.status = "COMPLETED";
                SystemLogBusLogic.Logs.Add(sl);
                return "SUCCESS";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public object GetCollectedVoucherList(DateTime dateFrom, DateTime dateTo)
        {
            var fullList = db.tblExcelUploadedPayments.Select(x => new
            {
                dataAreaId = x.dataAreaId.ToUpper(),
                vendorCode = x.vendorCode ?? "-",
                paymentId = x.paymentId,
                bankName = x.bankName,
                voucherId = x.voucherId,
                amount = x.amount,
                methodOfPayment = x.methodOfPayment,
                chequeNumber = x.chequeNumber,
                chequeDate = x.chequeDate,
                companyName = (db.tblVendors.FirstOrDefault(v => v.vendorCode.Contains(x.vendorCode)).companyName) ?? "Company not found.",
                source = "EXCEL",
                status = (db.tblVoucherStatus.Where(vi => vi.voucherId == x.voucherId).OrderByDescending(d => d.logDate)
                            .FirstOrDefault().status) ?? "UNRELEASED"
            }).Union(db.tblD365IntegratedPayment.Select(y => new
            {
                dataAreaId = y.dataAreaId.ToUpper(),
                vendorCode = y.recipientAccountNum,
                paymentId = y.paymentId,
                bankName = y.accountId,
                voucherId = y.voucherId,
                amount = y.amountCur,
                methodOfPayment = "",
                chequeNumber = y.chequeNum,
                chequeDate = y.chequeDate,
                companyName = y.bankRecipientName,
                source = "D365",
                status = (db.tblVoucherStatus.Where(vi => vi.voucherId == y.voucherId).OrderByDescending(d => d.logDate)
                            .FirstOrDefault().status) ?? "UNRELEASED"
            })).Join(
                db.tblVoucherStatus.GroupBy(a => a.voucherId).Select(b => b.OrderByDescending(a => a.logDate).FirstOrDefault()),
                tblP => tblP.voucherId,
                tblVS => tblVS.voucherId,
                (tblP, tblVS) => new { tblP, tblVS }
                ).Join(
                db.tblReceipts.DefaultIfEmpty(),
                twoTables => twoTables.tblP.voucherId,
                tblR => tblR.voucherId,
                (twoTables, tblR) => new
                {
                    companyName = twoTables.tblP.companyName,
                    vendorCode = twoTables.tblP.vendorCode,
                    paymentId = twoTables.tblP.paymentId,
                    bankName = twoTables.tblP.bankName,
                    voucherId = twoTables.tblP.voucherId,
                    amount = twoTables.tblP.amount,
                    methodOfPayment = twoTables.tblP.methodOfPayment,
                    chequeNumber = twoTables.tblP.chequeNumber,
                    chequeDate = twoTables.tblP.chequeDate,
                    status = twoTables.tblP.status,
                    batchId = twoTables.tblVS.batchId,
                    releasingDate = twoTables.tblVS.releasingDate,
                    SEANCode = twoTables.tblVS.SEANCode,
                    dataAreaId = twoTables.tblP.dataAreaId.ToUpper(),
                    receiptId = tblR.receiptId,
                    receiptNumber = tblR.receiptNo,
                    receiptType = tblR.receiptType,
                    releasedDate = twoTables.tblVS.logDate,
                    collector = db.tblQueues.OrderByDescending(x => x.queueID).FirstOrDefault(x => x.vendorCode == twoTables.tblP.vendorCode)
                }).Where(x => x.status == "RELEASED").ToList();

            return (fullList.Where(x => x.releasedDate >= dateFrom && x.releasedDate <= dateTo)
                .OrderByDescending(x => x.releasedDate).ToList());
        }

        public string UpdateReceiptDetails(int receiptId, string receiptNumber, string receiptType, string userID)
        {
            try
            {
                tblR = db.tblReceipts.FirstOrDefault(x => x.receiptId == receiptId);

                tblR.receiptNo = receiptNumber;
                tblR.receiptType = receiptType;

                db.SaveChanges();

                SystemLog sl = new SystemLog();
                sl.userId = userID;
                sl.type = "UPDATE RECEIPT DETAILS";
                sl.description = "Updated a receipt details ("+ tblR.voucherId +")";
                sl.status = "COMPLETED";
                SystemLogBusLogic.Logs.Add(sl);

                return "SUCCESS";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public void UpdateUncollectedVouchers()
        {
            var lastThreeDays = DateTime.Today.AddDays(-5);

            var voucherStatuses = db.tblVoucherStatus.Where(x => x.logDate > lastThreeDays).ToList();

            var rows = voucherStatuses.GroupBy(x => x.voucherId, (key, g)=>g.OrderByDescending(e=>e.logDate).FirstOrDefault()).Where(x => x.releasingDate < DateTime.Today && x.status == "FOR RELEASING")
                .ToList();

            if (rows.Count > 0)
            {
                foreach (var r in rows)
                {
                    tblVs = new tblVoucherStatu();
                    tblVs.voucherId = r.voucherId;
                    tblVs.batchId = 0;
                    tblVs.logDate = DateTime.Now;
                    tblVs.status = "UNCOLLECTED";
                    tblVs.userId = "SYSTEM";
                    db.tblVoucherStatus.Add(tblVs);
                    
                    var oldrequests = db.tblVoucherRequests.Where(x => x.voucherId == r.voucherId);
                    db.tblVoucherRequests.RemoveRange(oldrequests);

                    db.SaveChanges();
                }
                
                string[] vouchersIds = rows.Select(a => a.voucherId).ToArray();
                List<Mail> Mails = MailRepository.GetContactFromCollectorsAndVendors(vouchersIds);
            }

            SystemLog sl = new SystemLog();
            sl.userId = "SYSTEM";
            sl.type = "UNCOLLECTED UPDATE";
            sl.description = "Updated uncollected vouchers.";
            sl.status = "COMPLETED";
            SystemLogBusLogic.Logs.Add(sl);
        }

        public string PutVoucherOnHold(string voucherId, string userId)
        {
            try
            {
                var otherLists = db.tblVoucherRequests.Where(x => x.voucherId == voucherId).ToList();
                db.tblVoucherRequests.RemoveRange(otherLists);

                tblVs = new tblVoucherStatu();

                tblVs.voucherId = voucherId;
                tblVs.batchId = 0;
                tblVs.logDate = DateTime.Now;
                tblVs.status = "ON HOLD";
                tblVs.userId = userId;
                db.tblVoucherStatus.Add(tblVs);

                db.SaveChanges();

                tblHoldRequest tblHR = new tblHoldRequest();
                tblHR.status = "HOLD REQUEST ACCEPTED";
                tblHR.logdate = DateTime.Now;
                tblHR.voucherId = voucherId;

                db.tblHoldRequests.Add(tblHR);
                
                db.SaveChanges();

                SystemLog sl = new SystemLog();
                sl.userId = userId;
                sl.type = "HOLD VOUCHER";
                sl.description = "Put voucher ("+ voucherId +") on hold.";
                sl.status = "COMPLETED";
                SystemLogBusLogic.Logs.Add(sl);

                return "SUCCESS";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string UnholdVoucher(string voucherId, string userId)
        {
            try
            {
                string status = "";

                if (db.tblVoucherStatus.Where(x => x.status != "ON HOLD" && x.voucherId == voucherId).ToList().Count() == 0)
                {
                    status = "UNRELEASED";
                }
                else
                {
                    tblVs = db.tblVoucherStatus.OrderByDescending(x => x.logDate).FirstOrDefault(x => x.status != "ON HOLD" && x.voucherId == voucherId);

                    if (tblVs.status == "FOR RELEASING" && tblVs.releasingDate < DateTime.Today)
                    {
                        status = "UNCOLLECTED";
                    }
                    else
                    {
                        status = "UNRELEASED";
                    }
                }

                tblVs = new tblVoucherStatu();
                tblVs.userId = userId;
                tblVs.voucherId = voucherId;
                tblVs.logDate = DateTime.Now;
                tblVs.status = status;
                tblVs.batchId = 0;

                db.tblVoucherStatus.Add(tblVs);

                db.SaveChanges();

                tblHoldRequest tblHR = new tblHoldRequest();
                tblHR.status = "UNHOLD REQUEST ACCEPTED";
                tblHR.logdate = DateTime.Now;
                tblHR.voucherId = voucherId;

                db.tblHoldRequests.Add(tblHR);
                db.SaveChanges();

                SystemLog sl = new SystemLog();
                sl.userId = userId;
                sl.type = "UNHOLD VOUCHER";
                sl.description = "Unhold voucher("+ voucherId +").";
                sl.status = "COMPLETED";
                SystemLogBusLogic.Logs.Add(sl);

                return "SUCCESS";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string ReverseVoucher(string voucherId, string userId)
        {
            try
            {
                var otherReleasedData = db.tblCollectedChecks.Where(x => x.voucherId == voucherId).ToList();

                db.tblCollectedChecks.RemoveRange(otherReleasedData);


                tblVs = new tblVoucherStatu();
                tblVs.voucherId = voucherId;
                tblVs.logDate = DateTime.Now;
                tblVs.status = "REVERSED";
                tblVs.batchId = 0;
                tblVs.userId = userId;
                db.tblVoucherStatus.Add(tblVs);

                tblVs = new tblVoucherStatu();
                tblVs.voucherId = voucherId;
                tblVs.logDate = DateTime.Now;
                tblVs.status = "UNRELEASED";
                tblVs.batchId = 0;

                db.tblVoucherStatus.Add(tblVs);

                db.SaveChanges();

                SystemLog sl = new SystemLog();
                sl.userId = userId;
                sl.type = "REVERSE VOUCHER";
                sl.description = "Reversed voucher status ("+ voucherId +")";
                sl.status = "COMPLETED";
                SystemLogBusLogic.Logs.Add(sl);

                return "SUCCESS";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string AdverseVoucher(string voucherId)
        {
            try
            {
                tblVs = new tblVoucherStatu();
                tblVs.voucherId = voucherId;
                tblVs.logDate = DateTime.Now;
                tblVs.status = "RELEASED";
                tblVs.batchId = 0;

                db.tblVoucherStatus.Add(tblVs);

                db.SaveChanges();

                return "SUCCESS";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string RequestVoucherOnHold(string voucherId, string userId, string note)
        {
            try
            {
                tblHoldRequest tblHR = new tblHoldRequest();

                tblHR.voucherId = voucherId;
                tblHR.userId = userId;
                tblHR.logdate = DateTime.Now;
                tblHR.note = note;
                tblHR.status = "REQUESTED FOR HOLD";

                db.tblHoldRequests.Add(tblHR);

                db.SaveChanges();

                SystemLog sl = new SystemLog();
                sl.userId = userId;
                sl.type = "REQUEST HOLD";
                sl.description = "Requested voucher on hold ("+ voucherId +")";
                sl.status = "COMPLETED";
                SystemLogBusLogic.Logs.Add(sl);

                return "SUCCESS";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public List<Payment> GetVoucherListWithHoldRequest(
            DataFilter filter,
            string vendorcode,
            string companyName,
            string bank,
            string entity,
            string voucherId,
            decimal? amount,
            string payment,
            string check,
            DateTime? date,
            string status,
            string[] statusArr = null)
        {
            if (statusArr == null || statusArr.Length == 0)
            {
                statusArr = new string[16];
                statusArr[0] = "UNRELEASED";
                statusArr[1] = "FOR VERIFICATION";
                statusArr[2] = "FOR APPROVAL";
                statusArr[3] = "FOR RELEASING";
                statusArr[5] = "UNCOLLECTED";
                statusArr[6] = "ON HOLD";
                statusArr[7] = "HOLD REQUEST DECLINED";
                statusArr[8] = "REQUESTED FOR HOLD";
                statusArr[9] = "REQUESTED FOR UNHOLD";
                statusArr[10] = "REQUESTED";
                statusArr[11] = "ACCEPTED";
                statusArr[12] = "DECLINED";
                statusArr[13] = "HOLD REQUEST ACCEPTED";
                statusArr[14] = "UNHOLD REQUEST ACCEPTED";
                statusArr[15] = "UNHOLD REQUEST DECLINED";
            }
            
            var payments = db.tblExcelUploadedPayments.Select(x => new Payment
            {
                vendorCode = x.vendorCode ?? "-",
                dataAreaId = x.dataAreaId == "" ? "Unknown-Entity" : x.dataAreaId.ToUpper(),
                paymentId = x.paymentId,
                bankName = x.bankName ?? "Unknown-Bank",
                voucherId = x.voucherId,
                amount = x.amount,
                methodOfPayment = x.methodOfPayment,
                chequeNumber = x.chequeNumber,
                chequeDate = x.chequeDate,
                companyName = (db.tblVendors.FirstOrDefault(v => v.vendorCode.Contains(x.vendorCode)).companyName) ?? "Company not found.",
                source = "EXCEL",
                status = db.tblHoldRequests.Where(a => a.voucherId == x.voucherId).OrderByDescending(a => a.logdate).FirstOrDefault() != null
                ? db.tblHoldRequests.Where(a => a.voucherId == x.voucherId).OrderByDescending(a => a.logdate).FirstOrDefault().status : (db.tblVoucherStatus.Where(vi => vi.voucherId == x.voucherId).OrderByDescending(d => d.logDate)
                            .FirstOrDefault().status) ?? "UNRELEASED",
                requestDetails = db.tblHoldRequests.Where(a => a.voucherId == x.voucherId).OrderByDescending(a => a.logdate).FirstOrDefault()
            }).ToList().Union(db.tblD365IntegratedPayment.Select((y => new Payment
            {
                vendorCode = y.recipientAccountNum,
                dataAreaId = y.dataAreaId == "" ? "Unknown-Entity" : y.dataAreaId.ToUpper(),
                paymentId = y.paymentId,
                bankName = y.accountId ?? "Unknown-Bank",
                voucherId = y.voucherId,
                amount = y.amountCur,
                methodOfPayment = y.paymentMode,
                chequeNumber = y.chequeNum,
                chequeDate = y.chequeDate,
                companyName = y.bankRecipientName,
                source = "D365",
                status = db.tblHoldRequests.Where(a => a.voucherId == y.voucherId).OrderByDescending(x => x.logdate).FirstOrDefault() != null
                ? db.tblHoldRequests.Where(a => a.voucherId == y.voucherId).OrderByDescending(x => x.logdate).FirstOrDefault().status : (db.tblVoucherStatus.Where(vi => vi.voucherId == y.voucherId).OrderByDescending(d => d.logDate)
                            .FirstOrDefault().status) ?? "UNRELEASED",
                requestDetails = db.tblHoldRequests.Where(a => a.voucherId == y.voucherId).OrderByDescending(x => x.logdate).FirstOrDefault()
            }))
            ).Where(a => statusArr.Contains(a.status) && a.status != "COLLECTED");

            filter.TotalRecordCount = payments.Count();

            if (!string.IsNullOrEmpty(vendorcode))
            {
                payments = payments.Where(p => p.vendorCode.Contains(vendorcode));
            }

            if (!string.IsNullOrEmpty(voucherId))
            {
                payments = payments.Where(p => p.voucherId.Contains(voucherId));
            }

            if (!string.IsNullOrEmpty(companyName))
            {
                payments = payments.Where(p => p.companyName.Contains(companyName));
            }

            if (!string.IsNullOrEmpty(bank))
            {
                payments = payments.Where(p => p.bankName.Contains(bank));
            }

            if (!string.IsNullOrEmpty(entity))
            {
                payments = payments.Where(p => p.dataAreaId.Contains(entity));
            }

            if (!string.IsNullOrEmpty(vendorcode))
            {
                payments = payments.Where(p => p.vendorCode.Contains(vendorcode));
            }

            if (amount > 0)
            {
                payments = payments.Where(p => p.amount == amount);
            }

            if (!string.IsNullOrEmpty(payment))
            {
                payments = payments.Where(p => p.methodOfPayment.Contains(payment));
            }

            if (!string.IsNullOrEmpty(check))
            {
                payments = payments.Where(p => p.chequeNumber.Contains(check));
            }

            if (!string.IsNullOrEmpty(status))
            {
                payments = payments.Where(p => status.ToUpper().Contains(p.status.ToUpper()));
            }

            if (date != null)
            {
                payments = payments.Where(p => p.chequeDate == date);
            }

            filter.FilteredRecordCount = payments.Count();

            //sort
            payments = QueryHelper.Ordering(payments.AsQueryable(), filter.SortColumn, filter.SortDirection != "asc", false);

            //// data length
            payments = filter.Length > 0 ? payments.Skip(filter.Start).Take(filter.Length) : payments;

            return payments.ToList();
        }

        public string SeePreviousStatusBeforeHold(string voucherId)
        {
            if (db.tblVoucherStatus.Where(x => x.status != "ON HOLD" && x.voucherId == voucherId).ToList().Count() == 0)
            {
                return "UNRELEASED";
            } else
            {
                tblVs = db.tblVoucherStatus.OrderByDescending(x => x.logDate).FirstOrDefault(x => x.status != "ON HOLD" && x.voucherId == voucherId);

                if (tblVs.status == "FOR RELEASING" && tblVs.releasingDate < DateTime.Today)
                {
                    return "UNCOLLECTED";
                } else
                {
                    return "UNRELEASED";
                }
            }
        }

        public string DeclineHoldRequest(string voucherId, string userId)
        {
            try
            {
                tblHoldRequest tblHR = new tblHoldRequest();

                tblHR.voucherId = voucherId;
                tblHR.status = "HOLD REQUEST DECLINED";
                tblHR.logdate = DateTime.Now;

                db.tblHoldRequests.Add(tblHR);

                db.SaveChanges();

                SystemLog sl = new SystemLog();
                sl.userId = userId;
                sl.type = "DECLINE HOLD REQUEST";
                sl.description = "Declined hold request ("+ voucherId +")";
                sl.status = "COMPLETED";
                SystemLogBusLogic.Logs.Add(sl);

                return "SUCCESS";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string RequestVoucherUnhold(string voucherId, string userId, string note)
        {
            try
            {
                tblHoldRequest tblHR = new tblHoldRequest();

                tblHR.voucherId = voucherId;
                tblHR.userId = userId;
                tblHR.logdate = DateTime.Now;
                tblHR.note = note;
                tblHR.status = "REQUESTED FOR UNHOLD";

                db.tblHoldRequests.Add(tblHR);

                db.SaveChanges();

                SystemLog sl = new SystemLog();
                sl.userId = userId;
                sl.type = "UNHOLD REQUEST";
                sl.description = "Requested to unhold voucher ("+ voucherId +").";
                sl.status = "COMPLETED";
                SystemLogBusLogic.Logs.Add(sl);

                return "SUCCESS";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string DeclineUnholdRequest(string voucherId, string userId)
        {
            try
            {
                tblHoldRequest tblHR = new tblHoldRequest();

                tblHR.voucherId = voucherId;
                tblHR.status = "UNHOLD REQUEST DECLINED";
                tblHR.logdate = DateTime.Now;

                db.tblHoldRequests.Add(tblHR);

                db.SaveChanges();

                SystemLog sl = new SystemLog();
                sl.userId = userId;
                sl.type = "DECLINED UNHOLD REQUEST";
                sl.description = "Declined an unhold request ("+ voucherId +").";
                sl.status = "COMPLETED";
                SystemLogBusLogic.Logs.Add(sl);

                return "SUCCESS";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string AddVoucherStatus(string voucherId, string status, string userId = null, D365IntegratedPayment d = null, bool isAddNew = true)
        {
            try
            {
                if (isAddNew == true)
                {
                    var isVoucherExisting = db.tblD365IntegratedPayment.FirstOrDefault(x => x.voucherId == voucherId);

                    if (isVoucherExisting == null)
                    {
                        tblD365 = new tblD365IntegratedPayment()
                        {
                            voucherId = d.voucherId,
                            accountId = d.accountId,
                            amountCur = Math.Abs(d.bankTransAmountCur ?? 0),
                            bankCurrencyAmount = d.bankTransAmountCur,
                            bankRecipientName = d.dimensionName,
                            recipientAccountNum = d.displayValue,
                            paymentMode = d.paymentMode,
                            chequeDate = d.transDate,
                            transDate = d.transDate,
                            dataAreaId = d.dataAreaId.ToUpper()
                        };
                        db.tblD365IntegratedPayment.Add(tblD365);
                        db.SaveChanges();
                    }
                }
                
                var currentStatus = db.tblVoucherStatus.Where(x => x.voucherId == voucherId).OrderByDescending(x => x.logDate)
                               .FirstOrDefault();

                if (currentStatus == null || currentStatus.status != status)
                {
                    tblVs = new tblVoucherStatu();
                    tblVs.status = status;
                    tblVs.userId = userId ?? "SYSTEM";
                    tblVs.voucherId = voucherId;
                    tblVs.logDate = DateTime.Now;
                    tblVs.batchId = 0;

                    db.tblVoucherStatus.Add(tblVs);
                    db.SaveChanges();

                    return "SUCCESS";
                } else
                {
                    return "DUPLICATE";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string ForceReleaseVoucher(string[] voucherId, DateTime releaseDate, string userId)
        {
            try
            {
                for (int i = 0; i < voucherId.Length; i++)
                {
                    tblVs = new tblVoucherStatu();

                    tblVs.batchId = 0;
                    tblVs.logDate = releaseDate;
                    tblVs.voucherId = voucherId[i];
                    tblVs.status = "RELEASED";
                    tblVs.userId = userId;
                    db.tblVoucherStatus.Add(tblVs);

                    tblR = new tblReceipt();
                    tblR.receiptNo = "";
                    tblR.receiptType = "OR";
                    tblR.voucherId = voucherId[i];
                    db.tblReceipts.Add(tblR);
                }

                db.SaveChanges();
                return "SUCCESS";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public List<Voucher> GetVoucherListByBatchId(int batchId)
        {
            var vouchers = db.tblVoucherRequests.Where(x => x.batchId == batchId).Select(x => new Voucher
            {
                batchId = x.batchId,
                voucherId = x.voucherId,
                vendorCode = x.vendorCode,
                companyName = x.companyName,
                entity = x.entity,
                bank = x.bank,
                amount = x.amount,
                logDate = x.logDate,
                status = x.status,
                checkNum = x.checkNum,
                checkDate = x.checkDate,
                releasingDate = x.releasingDate
            }).ToList();

            return vouchers;
        }

        public void UpdateVoucherStatusForRelease(int batchId, string[] arrVoucherIdAndSeanCode)
        {
            for (int i = 0; i < arrVoucherIdAndSeanCode.Length; i++)
            {
                var vId = arrVoucherIdAndSeanCode[i].Split('?');

                if (vId.Length == 2)
                {
                    var voucherId = vId[0];
                    var SEANCode = vId[1];

                    tblVR = db.tblVoucherRequests.FirstOrDefault(x => x.voucherId == voucherId && x.batchId == batchId);

                    if (tblVR != null)
                    {
                        tblVR.SEANCode = SEANCode;
                        tblVR.status = "FOR RELEASING";
                        db.SaveChanges();
                    }
                }
            }
        }

        public List<Voucher> GetForReleasingVoucherList(string vendorId = "", DateTime? releasingDate = null)
        {
            if (vendorId != "")
            {
                var tblV = db.tblVendors.Where(x => x.vendorCode.Contains(vendorId)).OrderByDescending(x => x.vendorCode).FirstOrDefault();
                if (tblV != null)
                {
                    vendorId = tblV.vendorCode;
                }
            }

            var vouchers = db.tblVoucherRequests.Where(x => x.status == "FOR RELEASING"
            && (vendorId == "" ? x.logId > 0 : ((x.vendorCode.Contains(vendorId) || vendorId.Contains(x.vendorCode)) && 
            (releasingDate == null ? x.logId > 0 : x.releasingDate == releasingDate)))).Select(x => new Voucher
            {
                batchId = x.batchId,
                voucherId = x.voucherId,
                vendorCode = x.vendorCode,
                companyName = x.companyName,
                entity = x.entity,
                bank = x.bank,
                amount = x.amount,
                status = x.status,
                checkNum = x.checkNum,
                checkDate = x.checkDate,
                releasingDate = x.releasingDate,
                SEANCode = x.SEANCode
            }).ToList();

            return vouchers;
        }

        public void UpdateCollectedVouchers(ReleasedVoucher[] model, VoucherCollector c)
        {
            foreach (var item in model)
            {
                tblVR = db.tblVoucherRequests.FirstOrDefault(x => x.voucherId == item.VoucherId && x.status == "FOR RELEASING");

                if (tblVR != null)
                {
                    tblCollectedCheck tblCC = new tblCollectedCheck
                    {
                        companyName = tblVR.companyName,
                        vendorCode = tblVR.vendorCode,
                        entity = tblVR.entity,
                        bank = tblVR.bank,
                        voucherId = tblVR.voucherId,
                        checkNum = tblVR.checkNum,
                        checkDate = tblVR.checkDate,
                        amount = tblVR.amount,
                        receiptNum = item.ReceiptNo,
                        receiptType = item.ReceiptType,
                        collectorName = c.CollectorName,
                        collectorType = c.CollectorType,
                        thirdPartyCompany = c.ThirdPartyCompany,
                        releasedDate = DateTime.Now,
                        checkType = item.CheckType
                    };

                    db.tblCollectedChecks.Add(tblCC);

                    var oldStatus = db.tblVoucherRequests.Where(x => x.voucherId == item.VoucherId).ToList();

                    db.tblVoucherRequests.RemoveRange(oldStatus);
                }
            }
            db.SaveChanges();
        }

        public List<CollectedChecks> GetCollectedChecks(DateTime dateFrom, DateTime dateTo, string[] checkType = null)
        {
            if (dateFrom == dateTo)
            {
                dateTo = dateTo.AddDays(1);
            }

            if (checkType == null)
            {
                checkType = new string[3];
                checkType[0] = "STANDARD";
                checkType[1] = "MANAGER";
                checkType[2] = "TELEGRAPHI";
            }

            var data = db.tblCollectedChecks
                .Select(x => new CollectedChecks
                {
                    logId = x.logId,
                    companyName = x.companyName,
                    vendorCode = x.vendorCode,
                    entity = x.entity,
                    bank = x.bank,
                    voucherId = x.voucherId,
                    checkNum = x.checkNum,
                    checkDate = x.checkDate,
                    checkType = x.checkType ?? "STANDARD",
                    amount = x.amount,
                    releasedDate = x.releasedDate,
                    receiptNum = x.receiptNum ?? "",
                    receiptType = x.receiptType,
                    collectorType = x.collectorType,
                    collectorName = x.collectorName,
                    thirdPartyCompany = x.thirdPartyCompany,
                    segment = db.tblVendors.FirstOrDefault(v => v.vendorCode.Contains(x.vendorCode)).segment,
                    subsegment = db.tblVendors.FirstOrDefault(v => v.vendorCode.Contains(x.vendorCode)).subSegment
                }).Where(x => checkType.Contains(x.checkType) && x.releasedDate >= dateFrom && x.releasedDate <= dateTo);
            
            return data.ToList();
        }

        public string UpdateCollectedCheck(int logId, string receiptNumber, string receiptType, string checkType)
        {
            try
            {
                tblCollectedCheck tblCC = db.tblCollectedChecks.FirstOrDefault(x => x.logId == logId);
                if (tblCC == null)
                {
                    return "SUCCESS";
                }
                else
                {
                    tblCC.receiptNum = receiptNumber;
                    tblCC.receiptType = receiptType;
                    tblCC.checkType = checkType;
                    db.SaveChanges();
                    return "SUCCESS";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public void CancelVoucherStatus(string voucherId)
        {
            var tblRequests = db.tblVoucherRequests.Where(x => x.voucherId == voucherId).ToList();
            db.tblVoucherRequests.RemoveRange(tblRequests);

            var tblCollectedChecks = db.tblCollectedChecks.Where(x => x.voucherId == voucherId).ToList();
            db.tblCollectedChecks.RemoveRange(tblCollectedChecks);

            db.SaveChanges();
        }

        public void ForceReleaseVoucher(Voucher[] voucher, DateTime releaseDate, VoucherCollector c = null)
        {
            tblCollectedCheck tblCC = new tblCollectedCheck();

            foreach (var v in voucher)
            {
                tblCC = new tblCollectedCheck
                {
                    checkType = v.CheckType,
                    voucherId = v.voucherId,
                    vendorCode = v.vendorCode,
                    companyName = v.companyName,
                    entity = v.entity,
                    bank = v.bank,
                    amount = v.amount,
                    releasedDate = releaseDate,
                    checkDate = v.checkDate,
                    checkNum = v.checkNum,
                    receiptNum = v.ReceiptNo ?? "",
                    receiptType = v.ReceiptType ?? "",
                    collectorName = c.CollectorName ?? "",
                    collectorType = c.CollectorType ?? "",
                    thirdPartyCompany = c.ThirdPartyCompany ?? ""
                };
                db.tblCollectedChecks.Add(tblCC);
            }

            db.SaveChanges();
        }

        public string UpdateReceiptDetailsFromExcel(IQueryable<Receipt> receipt)
        {
            tblCollectedCheck tblCC;

            try
            {
                foreach (var r in receipt)
                {
                    tblCC = db.tblCollectedChecks.FirstOrDefault(x => x.voucherId == r.VOUCHERID);

                    if (tblCC != null)
                    {
                        tblCC.receiptNum = r.RECEIPTNUMBER;
                        tblCC.receiptType = r.RECEIPTTYPE;
                        db.SaveChanges();
                    }
                }

                return "SUCCESS";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            
        }

        public object GetVoucherStatusLogs(string voucherId)
        {
            return db.tblVoucherStatus
                .Where(x => x.voucherId == voucherId)
                .OrderBy(x => x.logId)
                .Select(x => new
                {
                    logDate = x.logDate.ToString(),
                    x.status,
                    x.userId
                }).ToList();
        }

        public List<Aging> GetAging(DateTime asOfDate)
        {
            Nullable<DateTime> dtNull = null;
            Nullable<int> intNull = null;

            string[] statusArr = new string[8] { "UNRELEASED", "FOR VERIFIFICATION", "FOR APPROVAL", "FOR RELEASING",
                "UNCOLLECTED","ON HOLD","REVERSED","REQUESTED FOR HOLD" };
            var payments = PaymentRepository.List(statusArr, asOfDate, true);

            List<Aging> lstAging = new List<Aging>();

            foreach (var p in payments)
            {
                Aging a = new Aging
                {
                    Entity = p.dataAreaId,
                    CheckNumber = p.chequeNumber,
                    Payee = p.companyName,
                    DateReceived = p.chequeDate == null ? "Not Set" : (p.chequeDate ?? DateTime.Today).ToString("MM/dd/yyyy"),
                    NoOfDays = p.chequeDate == null ? 0 : GetTwoDatesDiff(asOfDate, (p.chequeDate ?? DateTime.Today)) - 1,
                    DueDate = p.chequeDate == null ? dtNull : (p.chequeDate ?? DateTime.Today).Date.AddDays(30),
                    OverDue = p.chequeDate == null ? intNull : GetTwoDatesDiff(asOfDate, (p.chequeDate ?? DateTime.Today).AddDays(30)) - 1,
                    CheckDate = p.chequeDate == null ? dtNull : (p.chequeDate ?? DateTime.Today).Date.AddDays(30),
                    Amount = p.amount,
                    Amount2 = p.amount,
                    Term = 30,
                    Bank = p.bankName
                };

                var overdue = a.OverDue;

                if (overdue != null)
                {
                    if (overdue < 1)
                    {
                        a.Current = a.Amount;
                    }
                    else if (overdue < 31)
                    {
                        a.Term1 = a.Amount;
                    }
                    else if (overdue < 61)
                    {
                        a.Term2 = a.Amount;
                    }
                    else if (overdue < 91)
                    {
                        a.Term3 = a.Amount;
                    }
                    else if (overdue < 121)
                    {
                        a.Term4 = a.Amount;
                    }
                    else if (overdue >= 121)
                    {
                        a.Term5= a.Amount;
                    }
                }
                lstAging.Add(a);
            }

            return lstAging;
        }

        public int GetTwoDatesDiff(DateTime dt1, DateTime dt2)
        {
            TimeSpan diff = dt1.Date - dt2.Date;
            return diff.Days;
        }

        public DateTime? GetLatestReleasingDateFromBatch(int batchId)
        {
            var result = db.tblVoucherRequests.OrderByDescending(x => x.releasingDate).FirstOrDefault(x => x.batchId == batchId);

            if (result == null)
            {
                return null;
            }

            return result.releasingDate;
        }

        public CollectedChecks GetCollectedCheckDetails(string voucherId)
        {
            tblCollectedCheck tblCC = db.tblCollectedChecks
                .FirstOrDefault(x => x.voucherId == voucherId);

            if (tblCC == null)
            {
                return null;
            }
            else
            {
                return new CollectedChecks
                {
                    receiptNum = tblCC.receiptNum,
                    receiptType = tblCC.receiptType,
                    checkType = tblCC.checkType,
                    releasedDate = tblCC.releasedDate
                };
            }
        }

        public void CleanVouchers()
        {
            var unreleasedVouchers = db.tblVoucherStatus
                .Where(x => x.releasingDate > DateTime.Now &&
                (x.status == "FOR APPROVAL" || 
                x.status == "FOR VERIFICATION"))
                .ToList();

            unreleasedVouchers.ForEach(x => x.status = "UNRELEASED");

            var requests = db.tblVoucherStatusBatches
                .Where(x => x.isProcessed == false && 
                (x.status == "FOR APPROVAL" || x.status == "FOR VERIFICATION"));
            
            foreach (var request in requests)
            {
                bool hasVouchers = db.tblVoucherStatus
                    .Any(x => x.batchId == request.batchId && 
                    x.status == request.status);

                if (!hasVouchers) {
                    request.isProcessed = true;
                    request.isOpened = true;
                }
            }

            db.SaveChanges();
        }

        public string ForceUncollectedVouchers(string[] voucherIds)
        {
            var rows = db.tblVoucherStatus
                .GroupBy(x => x.voucherId, (key, g) => g.OrderByDescending(e => e.logDate)
                .FirstOrDefault())
                .Where(x => voucherIds.Contains(x.voucherId) && x.status == "FOR RELEASING")
                .ToList();

            if (rows.Count > 0)
            {
                foreach (var r in rows)
                {
                    tblVs = new tblVoucherStatu();
                    tblVs.voucherId = r.voucherId;
                    tblVs.batchId = 0;
                    tblVs.logDate = DateTime.Now;
                    tblVs.status = "UNCOLLECTED";
                    tblVs.userId = "SYSTEM";
                    db.tblVoucherStatus.Add(tblVs);

                    var oldrequests = db.tblVoucherRequests.Where(x => x.voucherId == r.voucherId);
                    db.tblVoucherRequests.RemoveRange(oldrequests);

                    db.SaveChanges();
                }
            }

            return "SUCCESS";
        }

        public List<CollectedChecks> GetCreatedOnlinePayments(DateTime dateFrom, DateTime dateTo)
        {
            var payments = db.tblExcelUploadedPayments.Select(x => new
            {
                dataAreaId = x.dataAreaId == "" ? "Unknown-Entity" : x.dataAreaId.ToUpper(),
                //paymentId = x.paymentId,
                bankName = x.bankName ?? "Unknown-Bank",
                transdate = x.chequeDate,
                logdate = x.logDate,
                voucherId = x.voucherId,
                amount = x.amount,
                methodOfPayment = x.methodOfPayment,
                chequeNumber = x.chequeNumber,
                chequeDate = x.chequeDate,
                companyName = (db.tblVendors.FirstOrDefault(v => v.vendorCode.Contains(x.vendorCode)).companyName) ?? "Company not found.",
                source = "EXCEL",
                status = "",
                vendorCode = x.vendorCode,
            }).Union(db.tblD365IntegratedPayment.Select((y => new
            {
                dataAreaId = y.dataAreaId == "" ? "Unknown-Entity" : y.dataAreaId.ToUpper(),
                //paymentId = y.paymentId,
                bankName = y.accountId ?? "Unknown-Bank",
                transdate = y.transDate,
                logdate = y.logDate,
                voucherId = y.voucherId,
                amount = y.amountCur,
                methodOfPayment = y.paymentMode,
                chequeNumber = y.chequeNum,
                chequeDate = y.chequeDate,
                companyName = y.bankRecipientName,
                source = "D365",
                status = "",
                vendorCode = y.recipientAccountNum
            }))
            ).GroupJoin(
                db.tblVoucherStatus
                .GroupBy(a => a.voucherId).Select(b => b.OrderByDescending(a => a.logId).FirstOrDefault()),
                tblV => tblV.voucherId,
                tblVS => tblVS.voucherId,
                (tblV, tblVS) => new
                {
                    tblV,
                    tblVS
                }
                )
                .SelectMany(
                joined => joined.tblVS.DefaultIfEmpty(),
                (joined, tblVS) => new
                {
                    //vendorCode = joined.tblV.vendorCode,
                    //dataAreaId = joined.tblV.dataAreaId,
                    //paymentId = joined.tblV.paymentId,
                    //bankName = joined.tblV.bankName,
                    bank = joined.tblV.bankName,
                    logdate = tblVS.logDate ?? joined.tblV.logdate,
                    voucherId = joined.tblV.voucherId,
                    amount = joined.tblV.amount,
                    methodOfPayment = joined.tblV.methodOfPayment ?? "",
                    chequeNumber = joined.tblV.chequeNumber,
                    chequeDate = joined.tblV.chequeDate,
                    vendorCode = joined.tblV.vendorCode,
                    companyName = joined.tblV.companyName,
                    source = joined.tblV.source,
                    status = tblVS.status ?? "UNRELEASED",
                    releasingDate = tblVS.releasingDate,
                    statusLogDate = tblVS.logDate,
                    transdate = joined.tblV.transdate,
                    entity = joined.tblV.dataAreaId,
                }
            ).Where(x => x.transdate >= dateFrom && 
            x.transdate < dateTo && 
            (x.status.ToUpper() == "CREATED" || 
            x.status.ToUpper() == "ONLINE")).ToList();


            return payments.Select(x => new CollectedChecks
            {
                voucherId = x.voucherId,
                releasedDate = x.releasingDate,
                companyName = x.companyName,
                entity = x.entity,
                bank = x.bank,
                amount = x.amount,
                checkType = "Synced Online Payments",
                segment = "Synced with D365"
            }).ToList();
        }
    }
}
