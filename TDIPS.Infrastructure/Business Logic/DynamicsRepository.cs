﻿//using BrainsProject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.TDIPSDBContextModels;

namespace TDIPS.Infrastructure.Business_Logic
{
    public class DynamicsRepository : IDynamicsRepository
    {
        TDIPSDbContext db = new TDIPSDbContext();
        DynamicsDBControl dbDynamics = new DynamicsDBControl();
        IOnlinePaymentRepository OnlinePaymentRepository = new OnlinePaymentRepository();

        public void UpdateCashflowsToBrains()
        {
            var from = DateTime.Today;
            var to = DateTime.Today.AddDays(1);

            //DCF -> DailyCashFlows
            //DCFB -> DailyCashFlowBanks
            //DCFBT -> DailyCashFlowBankTotals

            var dailyCashFlows = db.tblDailyCashFlows
                .Join(
                db.tblDailyCashFlowBanks,
                DCF => DCF.Id,
                DCFB => DCFB.DailyCashFlowId,
                (DCF, DCFB) => new { DCF, DCFB })
                .Join(
                db.tblDailyCashFlowBankTotals,
                DCFDCFB => DCFDCFB.DCFB.Id,
                DCFBT => DCFBT.DailyCashFlowBankId,
                (DCFDCFB, DCFBT) => new { DCFDCFB, DCFBT }
                ).Where(x => x.DCFDCFB.DCF.DateOfUpload > from && x.DCFDCFB.DCFB.Bank != "TOTAL" && x.DCFBT.Type.ToUpper() == "ENDING BALANCE").ToList();

            foreach (var cashflow in dailyCashFlows)
            {
                dbDynamics.AddParam("@bank", cashflow.DCFDCFB.DCFB.Bank);
                dbDynamics.AddParam("@date", cashflow.DCFDCFB.DCF.DailyCashDate);

                dbDynamics.ExecQuery("SELECT * from DailyCashFlow " +
                    "WHERE Bank = @bank AND [Date] = @date");

                if (dbDynamics.DBDT.Rows.Count == 0)
                {
                    dbDynamics.AddParam("@bank", cashflow.DCFDCFB.DCFB.Bank);
                    dbDynamics.AddParam("@date", cashflow.DCFDCFB.DCF.DailyCashDate);
                    dbDynamics.AddParam("@amount", cashflow.DCFBT.Amount);
                    dbDynamics.AddParam("@type", cashflow.DCFBT.Type);
                    dbDynamics.AddParam("@segment", "CASH, END");
                    dbDynamics.AddParam("@bankclassification", cashflow.DCFDCFB.DCFB.BankClassification);

                    dbDynamics.ExecQuery("INSERT INTO DailyCashFlow(" +
                        "Bank," +
                        "Date," +
                        "Amount," +
                        "Type," +
                        "Segment," +
                        "BankClassification) " +

                        "VALUES(" +
                        "@bank," +
                        "@date," +
                        "@amount," +
                        "@type," +
                        "@segment," +
                        "@bankclassification" +
                        ")");
                }
                else
                {
                    dbDynamics.AddParam("@amount", cashflow.DCFBT.Amount);
                    dbDynamics.AddParam("@type", cashflow.DCFBT.Type);
                    dbDynamics.AddParam("@segment", "CASH, END");
                    dbDynamics.AddParam("@bankclassification", cashflow.DCFDCFB.DCFB.BankClassification);

                    dbDynamics.AddParam("@bank", cashflow.DCFDCFB.DCFB.Bank);
                    dbDynamics.AddParam("@date", cashflow.DCFDCFB.DCF.DailyCashDate);

                    dbDynamics.ExecQuery("UPDATE DailyCashFlow SET " +
                        "Amount=@amount," +
                        "Type=@type," +
                        "Segment=@segment," +
                        "BankClassification=@bankclassification " +
                        "WHERE Bank=@bank AND [Date] = @date");
                }
            }

            var cashflowprojections = db.tblProjectionCashflows.Join(
                db.tblProjectionCashflowTransactions,
                PCF => PCF.Id,
                PCFT => PCFT.ProjectionCashflowId,
                (PCF, PCFT) => new { PCF, PCFT })
                .Join(
                    db.tblProjectionCashflowSegments,
                    PCFPCFT => PCFPCFT.PCFT.SegmentId,
                    PCFS => PCFS.Id,
                    (PCFPCFT, PCFS) => new
                    {
                        uploadDate = PCFPCFT.PCF.DateOfUpload,
                        projectionMonth = PCFPCFT.PCF.ProjectionMonth,
                        projectionDateRange = PCFPCFT.PCF.ProjectionDateRange,
                        segment = PCFS.Segment,
                        amount = PCFPCFT.PCFT.Amount,
                        flow = PCFS.InflowOutflow,
                        activity = PCFS.Activity
                    }
                ).ToList();

            foreach (var cashflow in cashflowprojections)
            {
                var segment = cashflow.segment;

                if (segment == "Co-owned")
                {
                    segment = "Shakeys Co-Owned Sales";
                }
                else if (segment == "Franchise")
                {
                    segment = "Shakeys Franchise Sales";
                }
                else if (segment == "Co-owned -P2")
                {
                    segment = "WBHI Co-Owned Sales";
                }
                else if (segment == "Franchise - P2")
                {
                    segment = "WBHI Franchise Sales";
                }

                dbDynamics.AddParam("@projectionmonth", cashflow.projectionMonth.Split(' ')[0]);
                dbDynamics.AddParam("@projectiondaterange", cashflow.projectionDateRange);
                dbDynamics.AddParam("@projectionyear", cashflow.projectionMonth.Split(' ')[1]);
                dbDynamics.AddParam("@segment", segment);

                dbDynamics.ExecQuery("SELECT * from CashflowProjection " +
                    "WHERE ProjectionMonth = @projectionmonth AND ProjectionDateRange = @projectiondaterange " +
                    "AND ProjectionYear = @projectionyear AND Segment = @segment");

                if (dbDynamics.DBDT.Rows.Count == 0)
                {
                    dbDynamics.AddParam("@uploaddate", cashflow.uploadDate);
                    dbDynamics.AddParam("@projectionmonth", cashflow.projectionMonth.Split(' ')[0]);
                    dbDynamics.AddParam("@projectiondaterange", cashflow.projectionDateRange);
                    dbDynamics.AddParam("@projectionyear", cashflow.projectionMonth.Split(' ')[1]);
                    dbDynamics.AddParam("@segment", segment);
                    dbDynamics.AddParam("@amount", cashflow.amount);
                    dbDynamics.AddParam("@flow", cashflow.flow);
                    dbDynamics.AddParam("@activity", cashflow.activity);

                    dbDynamics.ExecQuery("INSERT INTO CashflowProjection(" +
                        "UploadDate," +
                        "ProjectionMonth," +
                        "ProjectionDateRange," +
                        "ProjectionYear," +
                        "Segment," +
                        "Amount," +
                        "Flow," +
                        "Activity) " +
                        "VALUES(" +
                        "@uploaddate," +
                        "@projectionmonth," +
                        "@projectiondaterange," +
                        "@projectionyear," +
                        "@segment," +
                        "@amount," +
                        "@flow," +
                        "@activity" +
                        ")");
                }
                else
                {
                    dbDynamics.AddParam("@uploaddate", cashflow.uploadDate);
                    dbDynamics.AddParam("@amount", cashflow.amount);
                    dbDynamics.AddParam("@flow", cashflow.flow);
                    dbDynamics.AddParam("@activity", cashflow.activity);

                    dbDynamics.AddParam("@projectionmonth", cashflow.projectionMonth.Split(' ')[0]);
                    dbDynamics.AddParam("@projectiondaterange", cashflow.projectionDateRange);
                    dbDynamics.AddParam("@projectionyear", cashflow.projectionMonth.Split(' ')[1]);
                    dbDynamics.AddParam("@segment", segment);

                    dbDynamics.ExecQuery("UPDATE CashflowProjection SET " +
                        "UploadDate=@uploaddate," +
                        "Amount=@amount," +
                        "Flow=@flow," +
                        "Activity=@activity " +
                        "WHERE ProjectionMonth = @projectionmonth AND ProjectionDateRange = @projectiondaterange " +
                        "AND ProjectionYear = @projectionyear AND Segment = @segment");
                }
            }
        }

        public class OtherOnlinePaymentMap
        {
            public string MainAccount { get; set; }

            public string Segment { get; set; }

            public string Subsegment { get; set; }
        }

        public List<OtherOnlinePaymentMap> OtherOnlinePaymentsMapping()
        {
            return new List<OtherOnlinePaymentMap>()
            {
                new OtherOnlinePaymentMap() {
                    MainAccount = "2-01-00001",
                    Segment = "Raw Materials",
                    Subsegment = "Local Raw Materials-Milk/Dairy/Dessert"
                },
                new OtherOnlinePaymentMap() {
                    MainAccount = "1-0009002",
                    Segment = "Raw Materials",
                    Subsegment = "Local Raw Materials-Milk/Dairy/Dessert"
                },
                new OtherOnlinePaymentMap() {
                    MainAccount = "2-01-02060",
                    Segment = "Salaries & Wages",
                    Subsegment = "Payroll-CSO"
                },
                new OtherOnlinePaymentMap() {
                    MainAccount = "1-01-02000",
                    Segment = "Other Expense",
                    Subsegment = "Other Expense"
                },
                new OtherOnlinePaymentMap() {
                    MainAccount = "2-01-00501",
                    Segment = "Other Emp Benefits",
                    Subsegment = "SSS, Philhealth & Pag-ibig"
                },
                new OtherOnlinePaymentMap() {
                    MainAccount = "2-01-00502",
                    Segment = "Other Emp Benefits",
                    Subsegment = "SSS, Philhealth & Pag-ibig"
                },
                new OtherOnlinePaymentMap() {
                    MainAccount = "5-30-00720",
                    Segment = "Other Emp Benefits",
                    Subsegment = "Other Emp Benefits"
                },
                new OtherOnlinePaymentMap() {
                    MainAccount = "4-08-00100",
                    Segment = "Other Emp Benefits",
                    Subsegment = "SSS, Philhealth, PAG-IBIG Payable"
                },
            };
        }

        public void UpdateDataToBrains()
        {
            var from = DateTime.Today;
            var to = DateTime.Today.AddDays(1);

            //var payments = db.tblD365IntegratedPayment
            //    .Where(x => x.logDate >= from && x.logDate < to)
            //    .ToList();

            var payments = db.tblExcelUploadedPayments.Select(x => new
            {
                dataAreaId = x.dataAreaId == "" ? "Unknown-Entity" : x.dataAreaId.ToUpper(),
                //paymentId = x.paymentId,
                //bankName = x.bankName ?? "Unknown-Bank",
                transdate = x.chequeDate,
                logdate = x.logDate,
                voucherId = x.voucherId,
                amount = x.amount,
                methodOfPayment = x.methodOfPayment,
                chequeNumber = x.chequeNumber,
                chequeDate = x.chequeDate,
                source = "EXCEL",
                status = "",
                vendorCode = x.vendorCode,
            }).Union(db.tblD365IntegratedPayment.Select((y => new
            {
                dataAreaId = y.dataAreaId == "" ? "Unknown-Entity" : y.dataAreaId.ToUpper(),
                //paymentId = y.paymentId,
                //bankName = y.accountId ?? "Unknown-Bank",
                transdate = y.transDate,
                logdate = y.logDate,
                voucherId = y.voucherId,
                amount = y.amountCur,
                methodOfPayment = y.paymentMode,
                chequeNumber = y.chequeNum,
                chequeDate = y.chequeDate,
                source = "D365",
                status = "",
                vendorCode = y.recipientAccountNum
            }))
            ).GroupJoin(
                db.tblVoucherStatus
                .GroupBy(a => a.voucherId).Select(b => b.OrderByDescending(a => a.logId).FirstOrDefault()),
                tblV => tblV.voucherId,
                tblVS => tblVS.voucherId,
                (tblV, tblVS) => new
                {
                    tblV,
                    tblVS
                }
                )
                .SelectMany(
                joined => joined.tblVS.DefaultIfEmpty(),
                (joined, tblVS) => new
                {
                    //vendorCode = joined.tblV.vendorCode,
                    //dataAreaId = joined.tblV.dataAreaId,
                    //paymentId = joined.tblV.paymentId,
                    //bankName = joined.tblV.bankName,
                    logdate = tblVS.logDate ?? joined.tblV.logdate,
                    voucherId = joined.tblV.voucherId,
                    amount = joined.tblV.amount,
                    methodOfPayment = joined.tblV.methodOfPayment ?? "",
                    chequeNumber = joined.tblV.chequeNumber,
                    chequeDate = joined.tblV.chequeDate,
                    vendorCode = joined.tblV.vendorCode,
                    //companyName = joined.tblV.companyName,
                    source = joined.tblV.source,
                    status = tblVS.status ?? "UNRELEASED",
                    releasingDate = tblVS.releasingDate,
                    statusLogDate = tblVS.logDate,
                    transdate = joined.tblV.transdate,
                    entity = joined.tblV.dataAreaId
                }
            ).Where(x => x.logdate >= from && (x.status == "UNRELEASED" || x.status == "UNCOLLECTED" || 
            x.status == "FOR VERIFICATION" || x.status == "FOR RELEASING" ||
                    x.status == "FOR APPROVAL" || x.status == "RELEASED" || x.status == "REIMBURSED" || 
                    x.status.ToUpper() == "CREATED" || x.status.ToUpper() == "ONLINE RELEASED") && x.entity != "NAF" /*&& x.logdate < to*/).ToList();

            var vendorPayments = payments.Select(x => new
            {
                logdate = x.logdate,
                voucherId = x.voucherId,
                amount = x.amount,
                methodOfPayment = x.methodOfPayment ?? "",
                chequeNumber = x.chequeNumber,
                chequeDate = x.chequeDate,
                vendorCode = x.vendorCode,
                //companyName = x.companyName,
                source = x.source,
                status = x.status,
                releasingDate = x.releasingDate,
                statusLogDate = x.statusLogDate,
                transdate = x.transdate,
                entity = x.entity,
                vendor = db.tblVendors.FirstOrDefault(v => v.vendorCode.Contains(x.vendorCode) && v.isDeleted == false)
            })
           .ToList();

            var brainVouchers = new List<BrainVoucher>();
            var unreleasedBrainVouchers = new List<BrainVoucher>();

            foreach (var payment in vendorPayments)
            {
                if (payment.status == "RELEASED" && (payment.methodOfPayment?.ToUpper() == "CHECK"
                    || payment.methodOfPayment?.ToUpper() == "CHECKS"))
                {
                    var brainVoucher = new BrainVoucher()
                    {
                        Cashflow = "Outflow",
                        MainAccount = payment?.vendor?.companyName ?? "",
                        Voucher = payment.voucherId,
                        Amount = payment.amount,
                        Segment = payment?.vendor?.segment ?? "",
                        Subsegment = payment?.vendor?.subSegment ?? "",
                        Entity = payment.entity,
                        Transdate = payment.statusLogDate,
                        Source = "Checks"
                    };


                    brainVouchers.Add(brainVoucher);
                }
                else if (payment.status == "UNRELEASED" || payment.status == "UNCOLLECTED" || payment.status == "FOR VERIFICATION" || payment.status == "FOR RELEASING" ||
                    payment.status == "FOR APPROVAL")
                {
                    var brainVoucher = new BrainVoucher()
                    {
                        Cashflow = "Unreleased",
                        MainAccount = payment?.vendor?.companyName ?? "",
                        Voucher = payment.voucherId,
                        Amount = payment.amount,
                        Segment = "Others (Outstanding Checks)",
                        Subsegment = "Unreleased",
                        Entity = payment.entity,
                        Transdate = DateTime.Today,
                        Source = "Unreleased"
                    };

                    unreleasedBrainVouchers.Add(brainVoucher);
                }
                else if (payment.status == "REIMBURSED")
                {
                    var brainVoucher = new BrainVoucher()
                    {
                        Cashflow = "Outflow",
                        MainAccount = payment?.vendor?.vendorCode ?? "",
                        Voucher = payment.voucherId,
                        Amount = payment.amount,
                        Segment = "Reimbursement",
                        Subsegment = "Customer",
                        Entity = payment.entity,
                        Transdate = DateTime.Today,
                        Source = "Online SFOS"
                    };

                    unreleasedBrainVouchers.Add(brainVoucher);
                }
                else if (payment.methodOfPayment?.ToUpper() != "CHECK" && payment.methodOfPayment?.ToUpper() != "CHECKS")
                {
                    var brainVoucher = new BrainVoucher()
                    {
                        Cashflow = "Outflow",
                        MainAccount = payment?.vendor?.vendorCode ?? "",
                        Voucher = payment.voucherId,
                        Amount = payment.amount,
                        Segment = payment?.vendor?.segment ?? "",
                        Subsegment = payment?.vendor?.subSegment ?? "",
                        Entity = payment.entity,
                        Transdate = DateTime.Today,
                        Source = "OnlinePaym"
                    };

                    brainVouchers.Add(brainVoucher);
                }
            }

            #region Removed Online Payments
            //var generalOnlinePayments = db.tblGeneralOnlinePayments.Select(x => new
            //{
            //    vendor = db.tblVendors.FirstOrDefault(v => v.vendorCode.Contains(x.vendorCode) && v.isDeleted == false),
            //    logdate = x.logDate,
            //    voucherId = x.invoiceVouchers,
            //    entity = x.entity,
            //    transdate = x.transDate,
            //    amount = x.amount,
            //    modeOfPayment = x.modeOfPayment
            //})
            //.Where(x => x.logdate >= from && x.entity != "NAF").ToList();

            //foreach (var payment in generalOnlinePayments)
            //{
            //    var brainVoucher = new BrainVoucher()
            //    {
            //        Cashflow = "Outflow",
            //        MainAccount = payment.vendor.companyName,
            //        Voucher = payment.voucherId,
            //        Amount = payment.amount,
            //        Segment = payment.vendor.segment,
            //        Subsegment = payment.vendor.subSegment,
            //        Entity = payment.entity,
            //        Source = "ONLINETDIPS",
            //        Transdate = payment.transdate
            //    };
            //    brainVouchers.Add(brainVoucher);
            //}

            //var otherOnlinePayments = OnlinePaymentRepository.GetOtherOnlinePayments(0, "", from, to);
            //var otherOnlinePaymentsMapping = OtherOnlinePaymentsMapping();

            //foreach (var payment in otherOnlinePayments)
            //{
            //    if (payment.entity?.ToUpper() != "NAF")
            //    {
            //        var map = otherOnlinePaymentsMapping.FirstOrDefault(x => x.MainAccount == payment.account);

            //        if (map != null)
            //        {
            //            brainVouchers.Add(new BrainVoucher()
            //            {
            //                Cashflow = "Outflow",
            //                MainAccount = payment.account,
            //                Voucher = payment.genJournNum + "-" + payment.paymentId + "-" + payment.account + "-account",
            //                Amount = payment.debit ?? ((payment.credit ?? 0) * -1),
            //                Segment = map.Segment,
            //                Subsegment = map.Subsegment,
            //                Entity = payment.entity,
            //                Source = "GJTDIPS",
            //                Transdate = payment.transDate
            //            });

            //            brainVouchers.Add(new BrainVoucher()
            //            {
            //                Cashflow = "Outflow",
            //                MainAccount = payment.account,
            //                Voucher = payment.genJournNum + "-" + payment.paymentId + "-" + payment.account + "-account",
            //                Amount = payment.credit ?? ((payment.debit ?? 0) * -1),
            //                Segment = map.Segment,
            //                Subsegment = map.Subsegment,
            //                Entity = payment.entity,
            //                Source = "GJTDIPS",
            //                Transdate = payment.transDate
            //            });
            //        }
            //    }
            //}

            //IEmployeeRepository EmployeeRepository = new EmployeeRepository();

            //var employeeOnlinePayments = EmployeeRepository.GetEmployeeWithOnlinePaymentsList(DateTime.Today, DateTime.Today).Where(x => x.entity != "NAF");

            //foreach (var payment in employeeOnlinePayments)
            //{
            //    var brainVoucher = new BrainVoucher()
            //    {
            //        Cashflow = "Outflow",
            //        MainAccount = payment.Employee.employee_no,
            //        Voucher = payment.Employee.employee_no + " " + payment.logDate,
            //        Amount = payment.amount,
            //        Segment = "Salaries & Benefits",
            //        Subsegment = "",
            //        Entity = payment.entity,
            //        Source = "EmpOnline",
            //        Transdate = payment.logDate
            //    };
            //    brainVouchers.Add(brainVoucher);
            //}
            #endregion
            
            foreach (var voucher in brainVouchers)
            {
                dbDynamics.AddParam("@voucherId", voucher.Voucher);

                dbDynamics.ExecQuery("SELECT Voucher from BrainVoucher " +
                    "WHERE Voucher = @voucherId");

                if (dbDynamics.DBDT.Rows.Count == 0)
                {
                    dbDynamics.AddParam("@cashflow", voucher.Cashflow);
                    dbDynamics.AddParam("@transDate", voucher.Transdate);
                    dbDynamics.AddParam("@mainAccount", voucher.MainAccount);
                    dbDynamics.AddParam("@voucher", voucher.Voucher);
                    dbDynamics.AddParam("@amount", voucher.Amount);
                    dbDynamics.AddParam("@segment", voucher.Segment);
                    dbDynamics.AddParam("@subsegment", voucher.Subsegment);
                    dbDynamics.AddParam("@activity", voucher.Segment == "CAPEX" ? "INVESTING" : "OPERATING");
                    dbDynamics.AddParam("@source", voucher.Source);
                    dbDynamics.AddParam("@entity", voucher.Entity);

                    dbDynamics.ExecQuery("INSERT INTO BrainVoucher(" +
                        "Cashflow," +
                        "Transdate," +
                        "MainAccount," +
                        "Voucher," +
                        "Amount," +
                        "Segment," +
                        "Subsegment," +
                        "Activity," +
                        "Source," +
                        "Entity) " +
                        "VALUES(" +
                        "@cashflow," +
                        "@transdate," +
                        "@mainAccount," +
                        "@voucher," +
                        "@amount," +
                        "@segment," +
                        "@subsegment," +
                        "@activity," +
                        "@source," +
                        "@entity" +
                        ")");
                }
                else
                {
                    dbDynamics.AddParam("@cashflow", voucher.Cashflow);
                    dbDynamics.AddParam("@transDate", voucher.Transdate);
                    dbDynamics.AddParam("@mainAccount", voucher.MainAccount);
                    dbDynamics.AddParam("@amount", voucher.Amount);
                    dbDynamics.AddParam("@segment", voucher.Segment);
                    dbDynamics.AddParam("@subsegment", voucher.Subsegment);
                    dbDynamics.AddParam("@activity", voucher.Segment == "CAPEX" ? "INVESTING" : "OPERATING");
                    dbDynamics.AddParam("@source", voucher.Source);
                    dbDynamics.AddParam("@entity", voucher.Entity);
                    dbDynamics.AddParam("@voucher", voucher.Voucher);

                    dbDynamics.ExecQuery("UPDATE BrainVoucher SET " +
                        "Cashflow=@cashflow," +
                        "Transdate=@transDate," +
                        "MainAccount=@mainAccount," +
                        "Amount=@amount," +
                        "Segment=@segment," +
                        "Subsegment=@subsegment," +
                        "Activity=@activity," +
                        "source=@source," +
                        "Entity=@entity " +
                        "WHERE Voucher=@Voucher");
                }
            }

            foreach (var voucher in unreleasedBrainVouchers)
            {
                dbDynamics.AddParam("@voucherId", voucher.Voucher);
                dbDynamics.AddParam("@transDate", voucher.Transdate);

                dbDynamics.ExecQuery("SELECT Voucher from UnreleasedBrainVoucher " +
                    "WHERE Voucher = @voucherId AND Transdate = @transDate");

                if (dbDynamics.DBDT.Rows.Count == 0)
                {
                    dbDynamics.AddParam("@cashflow", voucher.Cashflow);
                    dbDynamics.AddParam("@transDate", voucher.Transdate);
                    dbDynamics.AddParam("@mainAccount", voucher.MainAccount);
                    dbDynamics.AddParam("@voucher", voucher.Voucher);
                    dbDynamics.AddParam("@amount", voucher.Amount);
                    dbDynamics.AddParam("@segment", voucher.Segment);
                    dbDynamics.AddParam("@subsegment", voucher.Subsegment);
                    dbDynamics.AddParam("@activity", voucher.Segment == "CAPEX" ? "INVESTING" : "OPERATING");
                    dbDynamics.AddParam("@source", voucher.Source);
                    dbDynamics.AddParam("@entity", voucher.Entity);

                    dbDynamics.ExecQuery("INSERT INTO UnreleasedBrainVoucher(" +
                        "Cashflow," +
                        "Transdate," +
                        "MainAccount," +
                        "Voucher," +
                        "Amount," +
                        "Segment," +
                        "Subsegment," +
                        "Activity," +
                        "Source," +
                        "Entity) " +
                        "VALUES(" +
                        "@cashflow," +
                        "@transdate," +
                        "@mainAccount," +
                        "@voucher," +
                        "@amount," +
                        "@segment," +
                        "@subsegment," +
                        "@activity," +
                        "@source," +
                        "@entity" +
                        ")");
                }
            }
        }

        public void UpdateLedgerTransaction(CashflowAccountMapping map, IEnumerable<D365LedgerTransaction> transactions, string entity)
        {
            if (transactions.Count() == 0) {
                return;
            }
            
            if (map.Segment?.ToUpper() == "DIVIDENDS PAYABLE" && entity?.ToUpper() != "SPAV")
            {
                return;
            }

            string cashflow = "Outflow";

            if (map.Segment?.ToUpper() == "AVAILMENT OF BANK LOANS") {
                cashflow = "Inflow";
            }
            
            foreach (var transaction in transactions)
            {
                if ((transaction.amount <= 0 || transaction.isCredit.ToUpper() == "YES") && map?.Segment?.ToUpper() == "DIVIDENDS PAYABLE")
                {
                    continue;
                }

                if (string.IsNullOrWhiteSpace(transaction.journalCategory)) continue;
                
                if (transaction.journalCategory?.ToUpper() == "PAYMENT")
                {
                    map.Segment = "Availment of Bank Loans";
                }
                else if (map.Segment?.ToUpper() != "DIVIDENDS PAYABLE" && transaction.isCredit?.ToUpper() != "YES")
                {
                    continue;
                }

                dbDynamics.AddParam("@voucherId", transaction.voucher);
                dbDynamics.AddParam("@recId", transaction.recId);
                
                dbDynamics.ExecQuery("SELECT Voucher from BrainVoucher " +
                    "WHERE Voucher = @voucherId AND RecId = @recId");

                if (dbDynamics.DBDT.Rows.Count == 0)
                {
                    dbDynamics.AddParam("@cashflow", cashflow);
                    dbDynamics.AddParam("@transDate", transaction.transDate);
                    dbDynamics.AddParam("@mainAccount", transaction.accountName);
                    dbDynamics.AddParam("@voucher", transaction.voucher);
                    dbDynamics.AddParam("@amount", Math.Abs(transaction.amount));
                    dbDynamics.AddParam("@segment", map.Segment);
                    dbDynamics.AddParam("@subsegment", map.Subsegment);
                    dbDynamics.AddParam("@activity", "Financing");
                    dbDynamics.AddParam("@source", "D365");
                    dbDynamics.AddParam("@entity", entity);
                    dbDynamics.AddParam("@recId", transaction.recId);

                    dbDynamics.ExecQuery("INSERT INTO BrainVoucher(" +
                        "Cashflow," +
                        "Transdate," +
                        "MainAccount," +
                        "Voucher," +
                        "Amount," +
                        "Segment," +
                        "Subsegment," +
                        "Activity," +
                        "Source," +
                        "Entity," +
                        "RecId) " +
                        "VALUES(" +
                        "@cashflow," +
                        "@transdate," +
                        "@mainAccount," +
                        "@voucher," +
                        "@amount," +
                        "@segment," +
                        "@subsegment," +
                        "@activity," +
                        "@source," +
                        "@entity," +
                        "@recId" +
                        ")");
                }
                else
                {
                    dbDynamics.AddParam("@cashflow", cashflow);
                    dbDynamics.AddParam("@transDate", transaction.transDate);
                    dbDynamics.AddParam("@mainAccount", transaction.accountName);
                    dbDynamics.AddParam("@amount", transaction.amount);
                    dbDynamics.AddParam("@segment", map.Segment);
                    dbDynamics.AddParam("@subsegment", map.Subsegment);
                    dbDynamics.AddParam("@activity", "Financing");
                    dbDynamics.AddParam("@source", "D365");
                    dbDynamics.AddParam("@entity", entity);
                   
                    dbDynamics.AddParam("@voucher", transaction.voucher);
                    dbDynamics.AddParam("@recId", transaction.recId);

                    dbDynamics.ExecQuery("UPDATE BrainVoucher SET " +
                        "Cashflow=@cashflow," +
                        "Transdate=@transDate," +
                        "MainAccount=@mainAccount," +
                        "Amount=@amount," +
                        "Segment=@segment," +
                        "Subsegment=@subsegment," +
                        "Activity=@activity," +
                        "source=@source," +
                        "Entity=@entity " +
                        "WHERE Voucher=@Voucher AND RecId=@recId");
                }
            }
        }

        public void UpdateEmployeesReimbursements(IEnumerable<EmployeesReimbursement> reimbursements)
        {
            var brainVouchers = new List<BrainVoucher>();

            reimbursements = reimbursements.Where(x => x.accountType.ToUpper() == "VENDOR" || x.accountType.ToUpper() == "CUSTOMER");

            foreach (var reimb in reimbursements)
            {
                dbDynamics.AddParam("@voucherId", reimb.voucher + "_" + reimb.lineNum);
                dbDynamics.AddParam("@segment", "Reimbursement");
                
                dbDynamics.ExecQuery("SELECT Voucher from BrainVoucher " +
                    "WHERE Voucher = @voucherId AND Segment= @segment");

                if (dbDynamics.DBDT.Rows.Count == 0)
                {
                    dbDynamics.AddParam("@cashflow", "Outflow");
                    dbDynamics.AddParam("@transDate", reimb.transdate);
                    dbDynamics.AddParam("@mainAccount", reimb.account);
                    dbDynamics.AddParam("@voucher", reimb.voucher + "_" + reimb.lineNum);
                    dbDynamics.AddParam("@amount", reimb.amountcurdebit > 0 ? reimb.amountcurdebit : (reimb.amountcurcredit * -1));
                    dbDynamics.AddParam("@segment", "Reimbursement");
                    dbDynamics.AddParam("@subsegment", reimb.accountType.ToUpper() == "VENDOR" ? "Employee" : reimb.accountType);
                    dbDynamics.AddParam("@activity", "Operating");
                    dbDynamics.AddParam("@source", "GenLed");
                    dbDynamics.AddParam("@entity", reimb.dataAreaId);

                    dbDynamics.ExecQuery("INSERT INTO BrainVoucher(" +
                        "Cashflow," +
                        "Transdate," +
                        "MainAccount," +
                        "Voucher," +
                        "Amount," +
                        "Segment," +
                        "Subsegment," +
                        "Activity," +
                        "Source," +
                        "Entity) " +
                        "VALUES(" +
                        "@cashflow," +
                        "@transdate," +
                        "@mainAccount," +
                        "@voucher," +
                        "@amount," +
                        "@segment," +
                        "@subsegment," +
                        "@activity," +
                        "@source," +
                        "@entity" +
                        ")");
                }
            }
        }

        public List<GeneralJournalMap> GetGeneralJournalMaps()
        {
            return db.tblGeneralJournalMappings.Select(x => new GeneralJournalMap
            {
                MainAccount = x.MainAccount,
                Segment = x.Segment,
                Subsegment = x.Subsegment
            }).ToList();
        }

        public void UpdateOnlinePayments(GeneralJournalMap map, IEnumerable<D365LedgerTransaction> transactions, string entity)
        {
            if (transactions.Count() == 0)
            {
                return;
            }

            foreach (var transaction in transactions)
            {
                if (map.Subsegment.ToUpper() == "PCR" && transaction.voucher.ToUpper().Contains("-APV-"))
                {
                    continue;
                }
                else if (map.Subsegment.ToUpper() == "BEREAVEMENT" && !transaction.voucher.ToUpper().Contains("-GJ-"))
                {
                    continue;
                }
                
                if (transaction.isCredit?.ToUpper() == "YES")
                {
                    transaction.amount = Math.Abs(transaction.amount) * -1;
                }
                else {
                    transaction.amount = Math.Abs(transaction.amount);
                }


                dbDynamics.AddParam("@voucherId", transaction.voucher);
                dbDynamics.AddParam("@recId", transaction.recId);

                dbDynamics.ExecQuery("SELECT Voucher from BrainVoucher " +
                    "WHERE Voucher = @voucherId AND RecId = @recId");

                if (dbDynamics.DBDT.Rows.Count == 0)
                {
                    dbDynamics.AddParam("@cashflow", "Outflow");
                    dbDynamics.AddParam("@transDate", transaction.transDate);
                    dbDynamics.AddParam("@mainAccount", transaction.accountName);
                    dbDynamics.AddParam("@voucher", transaction.voucher);
                    dbDynamics.AddParam("@amount", transaction.amount);
                    dbDynamics.AddParam("@segment", map.Segment);
                    dbDynamics.AddParam("@subsegment", map.Subsegment);
                    dbDynamics.AddParam("@activity", "Operating");
                    dbDynamics.AddParam("@source", "GenLed");
                    dbDynamics.AddParam("@entity", entity);
                    dbDynamics.AddParam("@recId", transaction.recId);

                    dbDynamics.ExecQuery("INSERT INTO BrainVoucher(" +
                        "Cashflow," +
                        "Transdate," +
                        "MainAccount," +
                        "Voucher," +
                        "Amount," +
                        "Segment," +
                        "Subsegment," +
                        "Activity," +
                        "Source," +
                        "Entity," +
                        "RecId) " +
                        "VALUES(" +
                        "@cashflow," +
                        "@transdate," +
                        "@mainAccount," +
                        "@voucher," +
                        "@amount," +
                        "@segment," +
                        "@subsegment," +
                        "@activity," +
                        "@source," +
                        "@entity," +
                        "@recId" +
                        ")");
                }
                //else
                //{
                //    dbDynamics.AddParam("@cashflow", "Outflow");
                //    dbDynamics.AddParam("@transDate", transaction.transDate);
                //    dbDynamics.AddParam("@mainAccount", transaction.accountName);
                //    dbDynamics.AddParam("@amount", transaction.amount);
                //    dbDynamics.AddParam("@segment", map.Segment);
                //    dbDynamics.AddParam("@subsegment", map.Subsegment);
                //    dbDynamics.AddParam("@activity", "Operating");
                //    dbDynamics.AddParam("@source", "GenLed");
                //    dbDynamics.AddParam("@entity", entity);

                //    dbDynamics.AddParam("@voucher", transaction.voucher);
                //    dbDynamics.AddParam("@recId", transaction.recId);

                //    dbDynamics.ExecQuery("UPDATE BrainVoucher SET " +
                //        "Cashflow=@cashflow," +
                //        "Transdate=@transDate," +
                //        "MainAccount=@mainAccount," +
                //        "Amount=@amount," +
                //        "Segment=@segment," +
                //        "Subsegment=@subsegment," +
                //        "Activity=@activity," +
                //        "source=@source," +
                //        "Entity=@entity " +
                //        "WHERE Voucher=@Voucher AND RecId=@recId");
                //}
            }
        }
    }
}

