﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDIPS.Core.Interfaces;
using TDIPS.Infrastructure.TDIPSDBContextModels;

namespace TDIPS.Infrastructure.Business_Logic
{
    public class EODRepository : IEODRepository
    {
        TDIPSDbContext db = new TDIPSDbContext();

        public string Add(string username, string type = "VOUCHER")
        {
            try
            {
                tblEODValidationLog tblEODVL = new tblEODValidationLog
                {
                    logDate = DateTime.Now,
                    userId = username,
                    type = type
                };
                db.tblEODValidationLogs.Add(tblEODVL);
                db.SaveChanges();
                
                return "SUCCESS";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            
        }

        public object IsEODValidationSent(DateTime? date, string type = "VOUCHER")
        {
            DateTime dateFrom = date ?? DateTime.Today;
            DateTime dateTo = dateFrom.AddDays(1);
            
            return db.tblEODValidationLogs.Where(x => x.logDate > dateFrom && x.logDate < dateTo && x.type == type)
                .Select(x => new {
                    logDate = x.logDate,
                    user = x.userId,
                    x.logId
                })
                .OrderByDescending(x => x.logId);
        }
    }
}
