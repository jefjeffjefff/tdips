﻿using System;
using System.Collections.Generic;
using System.Linq;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Core.Models.SS_Entities;
using TDIPS.Core.Models.SS_HELPERS;
using TDIPS.Infrastructure.TDIPSDBContextModels;

namespace TDIPS.Infrastructure.Business_Logic
{
	public class VendorRepository : IVendorRepository
	{
		TDIPSDbContext db = new TDIPSDbContext();
		tblVendor tblV;

		public int AddOrUpdate(Vendor v, string userID)
		{
            int returnMsg;
            SystemLog sl = new SystemLog();
            sl.userId = userID;
            sl.status = "COMPLETED";
            
            try
			{
				if (v.vendorID > 0)
				{
					tblV = db.tblVendors.SingleOrDefault(x => x.vendorID == v.vendorID);
                    sl.type = "UPDATE VENDOR";
                    sl.description = "Updated vendor record (" + v.vendorCode + ")";
                }
				else
				{
                    sl.type = "ADD VENDOR";
                    sl.description = "Added a new vendor";
                    
                    tblV = new tblVendor();
				}

                tblV.segment = v.segment;
                tblV.subSegment = v.subSegment;
				tblV.vendorCode = v.vendorCode;
				tblV.companyName = v.companyName;
				tblV.tinNumber = v.tinNumber;
				tblV.companyEmail = v.companyEmail;
				tblV.contactNumber = v.contactNumber;
				tblV.companyAddress = v.companyAddress;
				tblV.vendorType = v.vendorType;
                tblV.dataAreaId = v.dataAreaId;
                tblV.type = v.type;

                tblV.isDeleted = false;

				if (!(v.vendorID > 0))
				{
					db.tblVendors.Add(tblV);
				}

				db.SaveChanges();
                SystemLogBusLogic.Logs.Add(sl);
                returnMsg = tblV.vendorID;
			}
			catch (Exception ex)
			{
                returnMsg = 0;
			}

            return returnMsg;
		}

        public string[] AddOrUpdate(IEnumerable<D365Vendor> vendor)
        {
            try
            {
                var vendorsAdded = new List<string>();

                foreach (var v in vendor)
                {
                    tblV = db.tblVendors.FirstOrDefault(x => x.vendorCode.Contains(v.accountNum));

                    if (tblV == null)
                    {
                        tblV = new tblVendor
                        {
                            vendorCode = v.accountNum,
                            companyName = v.name,
                            isDeleted = false,
                            dataAreaId = v.dataAreaId,
                            subSegment = v.subsegment,
                            type = v.vendGroup,
                            segment = v.segment
                        };
                        db.tblVendors.Add(tblV);

                        vendorsAdded.Add(v.accountNum);
                    }
                    else
                    {
                        tblV.companyName = v.name;

                        if (tblV.dataAreaId != null && !tblV.dataAreaId.Contains(v.dataAreaId))
                        {
                            tblV.dataAreaId = tblV.dataAreaId + "," + v.dataAreaId;
                        }

                        if (string.IsNullOrWhiteSpace(tblV.segment) && !string.IsNullOrWhiteSpace(v.segment))
                        {
                            tblV.segment = v.segment;
                        }

                        if (string.IsNullOrWhiteSpace(tblV.subSegment) && !string.IsNullOrWhiteSpace(v.subsegment))
                        {
                            tblV.subSegment = v.subsegment;
                        }
                    }
                }

                db.SaveChanges();
                return vendorsAdded.ToArray();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string AddOrUpdate(IList<VendorExcelUpload> vendor)
        {
            int vendorId = 0;
            tblCollector tblC;
            tblBank tblB;
            string duplicates = "";
            bool isEmpty = true;

            try
            {
                foreach (var v in vendor)
                {
                    if (string.IsNullOrEmpty(v.VendorCode))
                    {
                        continue;
                    }

                    isEmpty = false;

                    tblV = db.tblVendors.FirstOrDefault(x => x.vendorCode.Contains(v.VendorCode));

                    var isExisting = false;

                    if (tblV == null)
                    {
                        tblV = new tblVendor();
                    }
                    else {
                        isExisting = true;
                    }

                    tblV.vendorCode = v.VendorCode;
                    tblV.companyName = v.CompanyName;
                    tblV.companyAddress = v.CompanyAddress;
                    tblV.companyEmail = v.CompanyEmail;
                    tblV.contactNumber = v.CompanyMobileNumber;
                    tblV.dataAreaId = v.Entity;
                    tblV.tinNumber = v.TinNumber;
                    tblV.subSegment = v.Subsegment;
                    tblV.segment = v.Segment;

                    if (!isExisting)
                    {
                        db.tblVendors.Add(tblV);
                    }
                    
                    db.SaveChanges();

                    vendorId = tblV.vendorID;

                    if (!(string.IsNullOrEmpty(v.C1_Name) && string.IsNullOrEmpty(v.C1_Position) && string.IsNullOrEmpty(v.C1_Email) && string.IsNullOrEmpty(v.C1_Mobile)))
                    {
                        tblC = new tblCollector
                        {
                            collectorName = v.C1_Name,
                            position = v.C1_Position,
                            email = v.C1_Email,
                            contactNumber = v.C1_Mobile,
                            vendorID = vendorId
                        };
                        db.tblCollectors.Add(tblC);
                    }

                    if (!(string.IsNullOrEmpty(v.C2_Name) && string.IsNullOrEmpty(v.C2_Position) && string.IsNullOrEmpty(v.C2_Email) && string.IsNullOrEmpty(v.C2_Mobile)))
                    {
                        tblC = new tblCollector
                        {
                            collectorName = v.C2_Name,
                            position = v.C2_Position,
                            email = v.C2_Email,
                            contactNumber = v.C2_Mobile,
                            vendorID = vendorId
                        };
                        db.tblCollectors.Add(tblC);
                    }

                    if (!(string.IsNullOrEmpty(v.C3_Name) && string.IsNullOrEmpty(v.C3_Position) && string.IsNullOrEmpty(v.C3_Email) && string.IsNullOrEmpty(v.C3_Mobile)))
                    {
                        tblC = new tblCollector
                        {
                            collectorName = v.C3_Name,
                            position = v.C3_Position,
                            email = v.C3_Email,
                            contactNumber = v.C3_Mobile,
                            vendorID = vendorId
                        };
                        db.tblCollectors.Add(tblC);
                    }

                    tblB = new tblBank
                    {
                        vendorID = vendorId,
                        bankCompany = v.BankName,
                        bankBranch = v.BankBranch,
                        accountName = v.AccountName,
                        bankAccount = v.BankAccountNo
                    };

                    db.tblBanks.Add(tblB);
                }

                db.SaveChanges();

                if (isEmpty == true)
                {
                    return "EMPTY";
                } else if (duplicates != "") {
                    return duplicates.Substring(0, duplicates.Length - 1);
                } else {
                    return "SUCCESS";
                }
            }
            catch (Exception ex)
            {
                return "ERROR";
            }
        }

        public string CancelMerge(string mergedVendorCode, string userID, string companyName = "")
        {
            string returnMsg;
            SystemLog sl = new SystemLog();

            try
            {
                string[] vendorCodes = mergedVendorCode.Split(',');
                
                sl.userId = userID;
                sl.type = "CANCEL MERGE";
                sl.description = "Cancelled vendors merge ("+ mergedVendorCode +")";

                for (int i = 0; i < vendorCodes.Length; i++)
                {
                    if (vendorCodes[i].Contains('_'))
                    {
                        string[] vc = vendorCodes[i].Split('_');
                        var vendorId = Int32.Parse(vc[0]);
                        var vendorCode = vc[vc.Length - 1];

                        tblV = db.tblVendors.FirstOrDefault(x => x.vendorID == vendorId);

                        if (tblV == null)
                        {
                            tblV = new tblVendor();
                            tblV.vendorID = vendorId;
                            tblV.vendorCode = vendorCode;
                            tblV.isDeleted = false;
                        }
                        else
                        {
                            tblV.dataAreaId = tblV.origDataAreaId;
                            tblV.vendorCode = vendorCode;
                            tblV.isDeleted = false;
                        }
                    } else
                    {
                        tblV = new tblVendor();
                        tblV.vendorCode = vendorCodes[i];
                        tblV.companyName = companyName;
                        
                        db.tblVendors.Add(tblV);
                    }

                    if (!mergedVendorCode.Contains('_'))
                    {
                        var oldRecord = db.tblVendors.Where(a => a.vendorCode == mergedVendorCode).ToList();
                        db.tblVendors.RemoveRange(oldRecord);
                    }
                    
                    db.SaveChanges();
                }
                

                sl.status = "COMPLETED";
                returnMsg = "SUCCESS";
            }
            catch (Exception ex)
            {
                sl.status = "FAILED";
                returnMsg = ex.Message;
            }
            SystemLogBusLogic.Logs.Add(sl);
            return returnMsg;
        }

        public List<Vendor> VendorToEmailList()
        {
            var vendors = db.tblVendors.Where(x => x.isDeleted != true && x.companyEmail != ""
            && x.companyEmail != null).Select(x => new Vendor {
                dataAreaId = x.dataAreaId ?? "",
                vendorCode = x.vendorCode,
                companyEmail = x.companyEmail,
                companyName = x.companyName,
                vendorID = x.vendorID
            }).ToList();

            return vendors;
        }

        public object Delete(int vendorid, string userID)
		{
			try
			{
				tblV = db.tblVendors.SingleOrDefault(x => x.vendorID == vendorid);

				tblV.isDeleted = true;

				db.SaveChanges();

				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        public object GetVendorListWithVoucherForRelease()
        {
            DateTime currentDate = DateTime.Now;

            string[] vendorsAlreadyInQueueArr = db.tblQueues.Where(x => x.status != "DONE" &&
            (x.queueDateTime.Value.Year == currentDate.Year && x.queueDateTime.Value.Month == currentDate.Month && 
            x.queueDateTime.Value.Day == currentDate.Day)).Select(x => x.vendorCode).ToArray();

            var vendors = db.tblExcelUploadedPayments.Select(x => new
            {
                vendorCode = db.tblVendors.FirstOrDefault(v => v.vendorCode.Contains(x.vendorCode) && v.isDeleted == false).vendorCode,
                voucherId = x.voucherId
            }).Union(db.tblD365IntegratedPayment.Select(y => new
            {
                vendorCode = db.tblVendors.FirstOrDefault(v => v.vendorCode.Contains(y.recipientAccountNum) && v.isDeleted == false).vendorCode,
                voucherId = y.voucherId
            })).Join(
                db.tblVoucherStatus.GroupBy(a => a.voucherId).Select(b => b.OrderByDescending(a => a.logDate).FirstOrDefault()),
                tblP => tblP.voucherId,
                tblVS => tblVS.voucherId,
                (tblP, tblVS) => new { tblP, tblVS }
                ).Join(
                db.tblVendors,
                tblPVS => tblPVS.tblP.vendorCode,
                tblV => tblV.vendorCode,
                (tblPVS, tblV) => new
                {
                    companyName = tblV.companyName ?? "Not Set",
                    vendorCode = tblV.vendorCode,
                    companyEmail = tblV.companyEmail,
                    companyAddress = tblV.companyAddress,
                    tinNumber = tblV.tinNumber,
                    status = tblPVS.tblVS.status,
                    releasingDate = tblPVS.tblVS.releasingDate,
                    SEANCode = tblPVS.tblVS.SEANCode,
                    dataAreaId = tblV.dataAreaId == "" ? "Unknown-Entity" : tblV.dataAreaId.ToUpper()
                }
                ).ToList();

            vendors = vendors.Where(a => a.status == "FOR RELEASING" && !(vendorsAlreadyInQueueArr.Contains(a.vendorCode)) &&
                a.releasingDate == DateTime.Today).GroupBy(b => b.vendorCode).Select(c => c.FirstOrDefault()).ToList();

            return vendors;
        }
        

        public string MergeVendor(int refVendorId, int[] vendorIds, string mergedVendorCode, string mergedEntity, string userID)
        {
            string returnMsg;

            SystemLog sl = new SystemLog();
            sl.userId = userID;
            sl.type = "MERGE VENDORS";
            sl.description = "Merge vendors' records ("+ mergedVendorCode +")";

            try
            {
                string[] entityArr = mergedEntity.Split(',');
                entityArr.Distinct().ToArray();

                var excessRows = db.tblVendors.Where(x => x.vendorID != refVendorId && vendorIds.Contains(x.vendorID)).ToList();
                //db.tblVendors.RemoveRange(excessRows);
                excessRows.ForEach(a => { a.isDeleted = true; a.vendorCode = ""; a.origDataAreaId = a.dataAreaId; });

                tblV = db.tblVendors.FirstOrDefault(x => x.vendorID == refVendorId);
                tblV.origDataAreaId = tblV.dataAreaId;
                tblV.vendorCode = mergedVendorCode;
                tblV.dataAreaId = mergedEntity;
                
                db.SaveChanges();
                sl.status = "COMPLETED";
                returnMsg = "SUCCESS";
            }
            catch (Exception ex)
            {
                sl.status = "FAILED";
                returnMsg = ex.Message;
            }

            SystemLogBusLogic.Logs.Add(sl);
            return returnMsg;
        }

        List<Vendor> IVendorRepository.List(DataFilter filter, Vendor v)
        {
            var vendors = db.tblVendors.Select(x => new Vendor
			{
				vendorID = x.vendorID,
				vendorCode = x.vendorCode,
				companyName = x.companyName ?? "",
				tinNumber = x.tinNumber ?? "",
				companyEmail = x.companyEmail ?? "",
				contactNumber = x.contactNumber ?? "",
				companyAddress = x.companyAddress ?? "",
                dataAreaId = x.dataAreaId.ToUpper() ?? "",
                segment = x.segment ?? "",
                subSegment = x.subSegment ?? "",
                type = x.type ?? "",
                isDeleted = x.isDeleted
			}
			).Where(x => x.isDeleted != true);

            filter.TotalRecordCount = vendors.Count();

            if (!string.IsNullOrEmpty(v.dataAreaId))
            {
                vendors = vendors.Where(x => x.dataAreaId.Contains(v.dataAreaId));
            }

            if (!string.IsNullOrEmpty(v.vendorCode))
            {
                vendors = vendors.Where(x => x.vendorCode.Contains(v.vendorCode));
            }

            if (!string.IsNullOrEmpty(v.companyName))
            {
                vendors = vendors.Where(x => x.companyName.Contains(v.companyName));
            }

            if (!string.IsNullOrEmpty(v.companyEmail))
            {
                vendors = vendors.Where(x => x.companyEmail.Contains(v.companyEmail));
            }

            if (!string.IsNullOrEmpty(v.contactNumber))
            {
                vendors = vendors.Where(x => x.contactNumber.Contains(v.contactNumber));
            }

            if (!string.IsNullOrEmpty(v.companyAddress))
            {
                vendors = vendors.Where(x => x.companyAddress.Contains(v.companyAddress));
            }

            filter.FilteredRecordCount = vendors.Count();

            vendors = QueryHelper.Ordering(vendors.AsQueryable(), filter.SortColumn, filter.SortDirection != "asc", false);

            vendors = filter.Length > 0 ? vendors.Skip(filter.Start).Take(filter.Length) : vendors;
            
			return vendors.ToList();
		}

        public List<Vendor> GetVendorListByVendorCodes(string[] vendorCodes)
        {
            var vendors = db.tblVendors
                .Where(x => vendorCodes.Any(y => x.vendorCode.Contains(y)) && x.isDeleted != true)
                .Select(x => new Vendor {
                    dataAreaId = x.dataAreaId ?? "",
                    vendorCode = x.vendorCode,
                    companyEmail = x.companyEmail,
                    vendorID = x.vendorID,
                    companyName = x.companyName,
                    contactNumber = x.contactNumber,
                    segment = x.segment,
                    subSegment = x.subSegment
                }).ToList();

            return vendors;
        }
    }
}
