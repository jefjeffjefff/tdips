﻿using System;
using System.Collections.Generic;
using System.Linq;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.TDIPSDBContextModels;

namespace TDIPS.Infrastructure.Business_Logic
{
	public class QueueRepository : IQueueRepository
	{
		TDIPSDbContext db = new TDIPSDbContext();
		tblQueue tblQ;

        public void AddQueue(CollectorDetails c, string vendorCode)
        {
            tblQ = new tblQueue();
            tblQ.collectorName = c.CollectorName;
            tblQ.vendorCode = vendorCode;
            tblQ.collectorType = c.CollectorType;
            tblQ.thirdPartyCompany = c.ThirdPartyCompany;
            tblQ.queueDateTime = DateTime.Now;
            tblQ.queueType = c.QueueType;
            tblQ.status = c.Status;

            db.tblQueues.Add(tblQ);
            db.SaveChanges();
        }

        public void DeleteFromQueue(int queueId = 0, string vendorCode = "")
		{
            if (queueId == 0)
            {
                tblQ = db.tblQueues.OrderByDescending(x => x.vendorCode).FirstOrDefault();

                if (tblQ != null)
                {
                    tblQ.status = "DONE";
                }

                db.SaveChanges();
            } else
            {
                tblQ = db.tblQueues.FirstOrDefault(x => x.queueID == queueId);

                if (tblQ != null)
                {
                    tblQ.status = "DONE";
                    db.SaveChanges();
                }
            }
		}

        public string Dequeue(int queueId, string userID)
        {
            try
            {
                tblQ = db.tblQueues.FirstOrDefault(x => x.queueID == queueId);
                tblQ.status = "DEQUEUED";
                db.SaveChanges();

                return "SUCCESS";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public object GetQueueList(int rowNumber = -1, string status = "")
		{
			DateTime currentDate = DateTime.Now;

			object tblQ;
			
			if (rowNumber == -1)
			{
				tblQ = db.tblQueues.Where(x => (status == "" ? x.status != "" : x.status == status) && (x.queueDateTime.Value.Year == currentDate.Year &&
				x.queueDateTime.Value.Month == currentDate.Month && x.queueDateTime.Value.Day == currentDate.Day)).Select(x => new
				{
					queueId = x.queueID,
					vendorCode = x.vendorCode,
					queueNumber = x.queueNumber,
					collectorName = x.collectorName,
					queueDateTime = x.queueDateTime,
					collectorType = x.collectorType,
					queueType = x.queueType,
					thirdPartyCompany = x.thirdPartyCompany,
					status = x.status,
					companyName = db.tblVendors.FirstOrDefault(y => y.vendorCode.Contains(x.vendorCode)).companyName
				})
				.OrderBy(x => x.queueNumber).ToList();
			} else
			{
				tblQ = db.tblQueues.Where(x => (status == "" ? x.status != "" : x.status == status) && (x.queueDateTime.Value.Year == currentDate.Year &&
				x.queueDateTime.Value.Month == currentDate.Month && x.queueDateTime.Value.Day == currentDate.Day)).Select(x => new
				{
					vendorCode = x.vendorCode,
					queueNumber = x.queueNumber,
					collectorName = x.collectorName,
					queueDateTime = x.queueDateTime,
					collectorType = x.collectorType,
					queueType = x.queueType,
					thirdPartyCompany = x.thirdPartyCompany,
					status = x.status
				})
				.OrderBy(x => x.queueNumber).Take(rowNumber).ToList();
			}
			return tblQ;
		}

        public string Queue(int queueId, string userID)
        {
            try
            {
                tblQ = db.tblQueues.FirstOrDefault(x => x.queueID == queueId);
                tblQ.status = "QUEUEING";
                db.SaveChanges();

                return "SUCCESS";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
