﻿using System;
using System.Collections.Generic;
using System.Linq;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.TDIPSDBContextModels;

namespace TDIPS.Infrastructure.Business_Logic
{
	public class QueueingWindowRepository : IQueueingWindow
	{
		TDIPSDbContext db = new TDIPSDbContext();
		tblVideo tblV;
		tblAnnouncement tblA;

		public string DeleteVideo(int videoId, string userID)
		{
            string returnMsg;

            SystemLog sl = new SystemLog();
            sl.userId = userID;
            sl.type = "DELETE VIDEO";
            sl.description = "Deleted an advertisement video.";

            try
			{
				var video = db.tblVideos.Where(x => x.videoId == videoId).ToList();
				db.tblVideos.RemoveRange(video);
				db.SaveChanges();
                sl.status = "COMPLETED";
				returnMsg =  "SUCCESS";
			}
			catch (Exception ex)
			{
                sl.status = "FAILED";
				returnMsg = ex.Message;
			}
            SystemLogBusLogic.Logs.Add(sl);
            return returnMsg;
		}

		public string SaveMarqueeText(string text, string userID)
		{
            string returnMsg;
            SystemLog sl = new SystemLog();
            sl.userId = userID;
            sl.type = "MODIFY ADS TEXT";
            sl.description = "Modified advertisement text.";

            try
			{
				int row = db.tblAnnouncements.ToList().Count;

				if (row == 0)
				{
					tblA = new tblAnnouncement();
				}
				else
				{
					tblA = db.tblAnnouncements.FirstOrDefault();
				}

				tblA.announcement = text;

				if (row == 0)
				{
					db.tblAnnouncements.Add(tblA);
				}

				db.SaveChanges();
                sl.status = "COMPLETED";
                returnMsg = "SUCCESS";
			}
			catch (Exception ex)
			{
                sl.status = "FAILED";
                returnMsg = ex.Message;
			}
            SystemLogBusLogic.Logs.Add(sl);
            return returnMsg;
		}

		public int SaveVideo(string fileName, string fileExtension, string userID)
		{
            int returnMsg;
            SystemLog sl = new SystemLog();
            sl.userId = userID;
            sl.type = "ADD VIDEO";
            sl.description = "Added a new video.";

            try
            {
                tblV = new tblVideo();
                tblV.dateUploaded = DateTime.Now;
                tblV.fileName = fileName;
                tblV.fileExtension = fileExtension;
                db.tblVideos.Add(tblV);
                db.SaveChanges();
                returnMsg = tblV.videoId;
                sl.status = "COMPLETED";
            }
            catch (Exception)
            {
                sl.status = "FAILED";
                returnMsg = 0;
            }

            SystemLogBusLogic.Logs.Add(sl);
            return returnMsg;
		}

		public string GetMarqueeText()
		{
			tblA = db.tblAnnouncements.FirstOrDefault();

			if (tblA == null)
			{
				return "";
			} else
			{
				return tblA.announcement;
			}
		}

		public object VideoList()
		{
			return db.tblVideos.ToList();
		}

        public int AddNumberCall(NumberCall n)
        {
            tblNumberCall tblN = new tblNumberCall
            {
                type = n.Type,
                companyName = n.CompanyName,
                queueType = n.QueueType,
                queueNumber = n.QueueNumber,
                status = n.Status,
                text = n.Text,
                collectorName = n.CollectorName
            };

            db.tblNumberCalls.Add(tblN);
            db.SaveChanges();

            return tblN.id;
        }

        public object GetCall()
        {
            var calls = db.tblNumberCalls.ToList();

            var firstCall = calls.LastOrDefault(x => x.status == "READY");

            db.tblNumberCalls.RemoveRange(calls);

            db.SaveChanges();

            return firstCall;
        }

        public void SetCallReady(int id)
        {
            tblNumberCall tblN = db.tblNumberCalls.FirstOrDefault(x => x.id == id);

            if (tblN != null)
            {
                tblN.status = "READY";
                db.SaveChanges();
            }
        }
    }
}
