﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.TDIPSDBContextModels;

namespace TDIPS.Infrastructure.Business_Logic
{
    public class DashboardRepository : IDashboardRepository
    {
        TDIPSDbContext db = new TDIPSDbContext();

        public object GetBatchMailDates()
        {
            var dates = db.tblBatchMails.OrderBy(x => x.logDate).Select(x => new
            {
                date = (x.logDate ?? DateTime.Now).Year.ToString() + "-" + 
                (x.logDate ?? DateTime.Now).Month.ToString() + "-" + (x.logDate ?? DateTime.Now).Day.ToString(),
                dt = x.logDate
            }).OrderByDescending(x => x.dt).Distinct().ToList();

            return dates;
        }

        public object GetProcessedMails(DateTime date)
        {
            var dateTo = date.AddDays(1);

            var ml = db.tblMailLogs.Join(
                db.tblBatchMails,
                tblML => tblML.batchId,
                tblBM => tblBM.batchMailId,
                (tblML, tblBM) => new
                {
                    tblML.type,
                    tblML.status,
                    tblBM.logDate,
                    batchStatus = tblBM.status
                }
            ).Where(x => x.logDate > date && x.logDate < dateTo);

            return ml;
        }

        public object GetQueueProgressAsToday()
        {
            DateTime currentDate = DateTime.Now;

            return db.tblQueues.Where(x => (x.queueDateTime.Value.Year == currentDate.Year &&
                x.queueDateTime.Value.Month == currentDate.Month && x.queueDateTime.Value.Day == currentDate.Day)).Select(x => new
                {
                    queueNumber = x.queueNumber,
                    queueDateTime = x.queueDateTime,
                    queueType = x.queueType,
                    status = x.status
                })
                .OrderBy(x => x.queueNumber).ToList();
        }

        public object ReleasedVouchers()
        {
            var last4weeks = DateTime.Now.AddDays(-28);

            try
            {
                var logs = db.tblCollectedChecks.Where(x => x.releasedDate > last4weeks).Select(x => new
                {
                    releasedDate = (x.releasedDate ?? DateTime.Now).Year.ToString() + "-" + 
                    (x.releasedDate ?? DateTime.Now).Month.ToString() + "-" + (x.releasedDate ?? DateTime.Now).Day.ToString(),
                    x.amount,
                    x.entity,
                    date = x.releasedDate
                }).OrderByDescending(x => x.date).ThenBy(x => x.entity);

                return logs;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            
        }

        public VoucherStatus VoucherStatus()
        {
            var voucherstat = db.tblExcelUploadedPayments.Select(x => new
            {
                voucherId = x.voucherId,
                stats = db.tblVoucherStatus.Where(z => z.voucherId == x.voucherId).OrderByDescending(z => z.logId).FirstOrDefault().status ?? "UNRELEASED"
            }).ToList().Union(db.tblD365IntegratedPayment.Select((y => new
            {
                y.voucherId,
                stats = db.tblVoucherStatus.Where(z => z.voucherId == y.voucherId).OrderByDescending(z => z.logId).FirstOrDefault().status ?? "UNRELEASED"
            }))).ToList();

            return new VoucherStatus()
            {
                Unreleased = voucherstat.Where(x => x.stats == "UNRELEASED").Count(),
                ForVerification = voucherstat.Where(x => x.stats == "FOR VERIFICATION").Count(),
                ForApproval = voucherstat.Where(x => x.stats == "FOR APPROVAL").Count(),
                ForRelease = voucherstat.Where(x => x.stats == "FOR RELEASING").Count(),
                Released = voucherstat.Where(x => x.stats == "RELEASED").Count(),
                Uncollected = voucherstat.Where(x => x.stats == "UNCOLLECTED").Count()
            };
        }
    }
}
