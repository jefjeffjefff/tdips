﻿using System;
using System.Collections.Generic;
using System.Linq;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.TDIPSDBContextModels;


namespace TDIPS.Infrastructure.Business_Logic
{
	public class NotificationRepository : INotificationRepository
	{
		TDIPSDbContext db = new TDIPSDbContext();
		IUserRepository UserRepository = new UserRepository();


		public Notification Notifications()
		{
			Notification notif = new Notification();

			var notification = db.tblVoucherStatusBatches.Where(x => x.isProcessed != true && x.isOpened != true).ToList();

			notif.DeclinedRequest = notification.Where(x => x.batchStatus == "DECLINED").ToList().Count();
			notif.VerificationRequest = notification.Where(x => x.status == "FOR VERIFICATION").ToList().Count();

			notif.ApprovalRequest = notification.Where(x => (UserRepository.IsTreasuryLeadOnLeave() == "FALSE" ? x.status == "FOR APPROVAL"
			: (x.status == "FOR VERIFICATION" || x.status == "FOR APPROVAL"))).ToList().Count();


			notif.ReadyForReleasing = notification.Where(x => x.status == "FOR RELEASING").ToList().Count();

			return notif;
		}

		public void OpenNotification(string[] area)
		{
			string firstArea = area[0];

			if (firstArea == "FOR APPROVAL" &&  UserRepository.IsTreasuryLeadOnLeave() == "TRUE")
			{
				area = new string[2];
				area[0] = firstArea;
				area[1] = "FOR VERIFICATION";
			}

			if (area.Contains("DECLINED"))
			{
				db.tblVoucherStatusBatches.Where(x => area.Contains(x.batchStatus)).ToList().ForEach(y => y.isOpened = true);
			} else
			{
				db.tblVoucherStatusBatches.Where(x => area.Contains(x.status)).ToList().ForEach(y => y.isOpened = true);
			}

			db.SaveChanges();
		}
	}
}
