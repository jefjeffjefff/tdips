﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.TDIPSDBContextModels;

namespace TDIPS.Infrastructure.Business_Logic
{
    public class OnlinePaymentRepository : IOnlinePaymentRepository
    {
        TDIPSDbContext db = new TDIPSDbContext();

        public List<GeneralOnlinePayment> GetGeneralOnlinePaymentList(DateTime? from = null, DateTime? to = null, bool isByTransdate = true)
        {
            if (from == null)
            {
                from = DateTime.Today;
            }

            if (to == null)
            {
                to = DateTime.Today.AddDays(1);
            }
            else
            {
                to = to?.AddDays(1);
            }

            var result = db.tblGeneralOnlinePayments
                .Select(x => new GeneralOnlinePayment
                {
                    Id = x.Id,
                    invoiceVouchers = x.invoiceVouchers,
                    entity = x.entity,
                    bank = x.bank,
                    transDate = x.transDate,
                    amount = x.amount,
                    modeOfPayment = x.modeOfPayment.ToUpper(),
                    logDate = x.logDate,
                    vendorCode = x.vendorCode,
                    bankAccountNumber = x.bankAccountNumber,
                    companyName = (db.tblVendors.FirstOrDefault(v => v.vendorCode.Contains(x.vendorCode)).companyName) ?? "Company not found.",
                })
                .Where(x => isByTransdate == true ? (x.transDate >= from && x.transDate <= to) : (x.logDate >= from && x.transDate <= to))
                .ToList();

            return result;
        }

        public List<OtherOnlinePaymentBatch> GetOtherOnlinePaymentBatches(DateTime dateFrom, DateTime dateTo)
        {
            dateTo = dateTo.AddDays(1);

            var batches = db.tblOtherOnlinePaymentBatches
                .Where(x => x.uploadDate >= dateFrom && x.uploadDate <= dateTo)
                .Select(x => new OtherOnlinePaymentBatch {
                    batchId = x.batchId,
                    uploadDate = x.uploadDate,
                    uploadedBy = x.uploadedBy,
                    entity = x.entity,
                    transactionType = x.transactionType,
                    description = x.description,
                    generalJournalNum = x.generalJournalNum
                }).ToList();

            return batches;
        }

        public List<OtherOnlinePayment> GetOtherOnlinePayments(int batchId = 0, string accountType = "", DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            if (dateFrom == null)
            {
                dateFrom = DateTime.Today;
            }

            if (dateTo == null)
            {
                dateTo = DateTime.Today.AddDays(1);
            }
            else
            {
                dateTo = dateTo?.AddDays(1);
            }

            var results = db.tblOtherOnlinePaymentBatches
                .Join(
                    db.tblOtherOnlinePayments,
                    tblOOPB => tblOOPB.batchId,
                    tblOOP => tblOOP.batchId,
                    (tblOOPB, tblOOP) => new OtherOnlinePayment
                    {
                        transDate = tblOOP.transDate,
                        description = tblOOP.description,
                        accountType = tblOOP.accountType ?? "",
                        account = tblOOP.account,
                        stores = tblOOP.stores,
                        department = tblOOP.department,
                        businessUnit = tblOOP.businessUnit,
                        purpose = tblOOP.purpose,
                        salesSegment = tblOOP.salesSegment,
                        worker = tblOOP.costCenter,
                        debit = tblOOP.debit,
                        credit = tblOOP.credit,
                        offsetAccountType = tblOOP.offsetAccountType,
                        offsetAccount = tblOOP.offsetAccount,
                        oStores = tblOOP.oStores,
                        oDepartment = tblOOP.oDepartment,
                        oBusinessUnit = tblOOP.oBusinessUnit,
                        oPurpose = tblOOP.oPurpose,
                        oSalesSegment = tblOOP.oSalesSegment,
                        oWorker = tblOOP.oWorker,
                        oCostCenter = tblOOP.oCostCenter,
                        batchId = tblOOPB.batchId,
                        transactionType = tblOOPB.transactionType,
                        uploadDate = tblOOPB.uploadDate,
                        entity = tblOOPB.entity,
                        genJournNum = tblOOPB.generalJournalNum,
                        paymentId = tblOOP.paymentId
                    }
                ).Where(x => (batchId == 0 ? (x.uploadDate >= dateFrom && x.uploadDate < dateTo && 
                (accountType != "" ? 
                (x.accountType.ToUpper() == accountType.ToUpper() || x.offsetAccountType.ToUpper() == accountType.ToUpper())
                : x.batchId > 0)) 
                : x.batchId == batchId)).ToList();

            return results;
        }

        public void SaveGeneralOnlinePayment(GeneralOnlinePayment[] model)
        {
            if (model == null) return;

            foreach (var payment in model)
            {
                tblGeneralOnlinePayment gop = new tblGeneralOnlinePayment
                {
                    logDate = DateTime.Now,
                    vendorCode = payment.vendorCode,
                    invoiceVouchers = payment.invoiceVouchers,
                    entity = payment.entity,
                    bank = payment.bank,
                    transDate = payment.transDate,
                    amount = payment.amount,
                    modeOfPayment = payment.modeOfPayment,
                    bankAccountNumber = payment.bankAccountNumber
                };

                db.tblGeneralOnlinePayments.Add(gop);
            }

            db.SaveChanges();
        }

        public string SaveOtherOnlinePayments(OtherOnlinePaymentBatch batch, OtherOnlinePayment[] paymentsArr)
        {
            var tblBatch = new tblOtherOnlinePaymentBatch
            {
                description = batch.description,
                entity = batch.entity,
                uploadDate = batch.uploadDate,
                uploadedBy = batch.uploadedBy,
                transactionType = batch.transactionType,
                generalJournalNum = batch.generalJournalNum
            };

            db.tblOtherOnlinePaymentBatches.Add(tblBatch);
            db.SaveChanges();

            var batchId = tblBatch.batchId;

            for (int i = 0; i < paymentsArr.Length; i++)
            {
                var payment = new tblOtherOnlinePayment
                {
                    batchId = batchId,
                    transDate = paymentsArr[i].transDate,
                    description = paymentsArr[i].description,
                    accountType = paymentsArr[i].accountType,
                    account = paymentsArr[i].account,
                    stores = paymentsArr[i].stores,
                    department = paymentsArr[i].department,
                    businessUnit = paymentsArr[i].businessUnit,
                    purpose = paymentsArr[i].purpose,
                    salesSegment = paymentsArr[i].salesSegment,
                    worker = paymentsArr[i].worker,
                    costCenter = paymentsArr[i].costCenter,
                    debit = paymentsArr[i].debit,
                    credit = paymentsArr[i].credit,
                    offsetAccountType = paymentsArr[i].offsetAccountType,
                    offsetAccount = paymentsArr[i].offsetAccount,
                    oStores = paymentsArr[i].oStores,
                    oDepartment = paymentsArr[i].oDepartment,
                    oBusinessUnit = paymentsArr[i].oBusinessUnit,
                    oPurpose = paymentsArr[i].oPurpose,
                    oSalesSegment = paymentsArr[i].oSalesSegment,
                    oWorker = paymentsArr[i].oWorker,
                    oCostCenter = paymentsArr[i].oCostCenter
                };

                db.tblOtherOnlinePayments.Add(payment);
            }

            db.SaveChanges();

            return "SUCCESS";
        }
    }
}
