﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.TDIPSDBContextModels;

namespace TDIPS.Infrastructure.Business_Logic
{
    public class GeneralJournalRepository : IGeneralJournalRepository
    {
        TDIPSDbContext db = new TDIPSDbContext();
        tblGJBank tblGJB;
        tblGJTransaction tblGJT;
        
        public string AddBank(GJBank gjbank)
        {
            bool isExisting = false;
            tblGJB = null;

            gjbank.AccountNo = GetNumbers(gjbank.AccountNo);

            if (!String.IsNullOrWhiteSpace(gjbank.AccountNo) || gjbank.AccountNo != "")
            {
                tblGJB = db.tblGJBanks.FirstOrDefault(x => x.accountNo == gjbank.AccountNo);
            }
            
            if (tblGJB == null)
            {
                tblGJB = new tblGJBank();
            }
            else {
                isExisting = true;
            }

            tblGJB.accountNo = gjbank.AccountNo;
            tblGJB.accountName = gjbank.AccountName;
            tblGJB.accountType = gjbank.AccountType;
            tblGJB.store = gjbank.Store;
            tblGJB.department = gjbank.Department;
            tblGJB.businessUnit = gjbank.BusinessUnit;

            if (isExisting)
            {
                db.SaveChanges();
                return "OVERWRITTEN";
            }
            else {
                db.tblGJBanks.Add(tblGJB);
                db.SaveChanges();
                return "SUCCESS";
            }
        }

        public List<GJBank> GetBankList()
        {
            return db.tblGJBanks.Select(x => new GJBank {
                BankId = x.bankId,
                AccountName = x.accountName,
                AccountNo = x.accountNo,
                AccountType = x.accountType,
                Store = x.store,
                Department = x.department,
                BusinessUnit = x.businessUnit
            }).ToList();
        }

        private static string GetNumbers(string input)
        {
            if (input == null) return "";
            return new string(input.Where(c => char.IsDigit(c)).ToArray());
        }

        public string AddTransaction(GJTransaction gjtransaction)
        {
            tblGJT = new tblGJTransaction
            {
                PostingDate = gjtransaction.PostingDate,
                UploadedDT = DateTime.Now,
                Debit = gjtransaction.Debit,
                Credit = gjtransaction.Credit,
                AccountNo = gjtransaction.AccountNo
            };

            db.tblGJTransactions.Add(tblGJT);
            db.SaveChanges();

            return "SUCCESS";
        }

        public List<GJTransaction> JoinGJTandGJB(List<GJTransaction> lstGjtransaction)
        {
            var result = lstGjtransaction.Join(
                db.tblGJBanks.DefaultIfEmpty(),
                tblgjt => tblgjt.AccountNo,
                tblgjb => tblgjb.accountNo,
                (tblgjt, tblgjb) => new GJTransaction
                {
                    AccountNo = tblgjt.AccountNo,
                    PostingDate = tblgjt.PostingDate,
                    Debit = tblgjt.Debit,
                    Credit = tblgjt.Credit,
                    Gjbank = new GJBank {
                        AccountName = tblgjb.accountName,
                        AccountNo = tblgjb.accountNo,
                        Store = tblgjb.store,
                        Department = tblgjb.department,
                        BusinessUnit = tblgjb.businessUnit,
                        AccountType = tblgjb.accountType
                    }
                }
            ).ToList();

            return result;
        }

        public List<UnreleasedGJEntry> GetUnreleasedGJEntries(string entity, DateTime from, DateTime to, DateTime? asOf = null)
        {
            var unreleasedStatuses = new string[] { "UNCOLLECTED", "UNRELEASED", "FOR VERIFICATION", "FOR APPROVAL", "FOR RELEASING"};
            
            var unreleasedPayments = db.tblExcelUploadedPayments.Select(x => new
            {
                vendorCode = x.vendorCode,
                vendor = db.tblVendors.FirstOrDefault(v => v.vendorCode.Contains(x.vendorCode) && v.isDeleted == false),
                dataAreaId = x.dataAreaId == "" ? "Unknown-Entity" : x.dataAreaId.ToUpper(),
                paymentId = x.paymentId,
                bankName = x.bankName ?? "Unknown-Bank",
                voucherId = x.voucherId,
                amount = x.amount,
                methodOfPayment = x.methodOfPayment,
                chequeNumber = x.chequeNumber,
                chequeDate = x.chequeDate,
                companyName = (db.tblVendors.FirstOrDefault(v => v.vendorCode.Contains(x.vendorCode)).companyName) ?? "Company not found.",
                source = "EXCEL",
                status = ""
            }).Union(db.tblD365IntegratedPayment.Select((y => new
            {
                vendorCode = y.recipientAccountNum,
                vendor = db.tblVendors.FirstOrDefault(v => v.vendorCode.Contains(y.recipientAccountNum) && v.isDeleted == false),
                dataAreaId = y.dataAreaId == "" ? "Unknown-Entity" : y.dataAreaId.ToUpper(),
                paymentId = y.paymentId,
                bankName = y.accountId ?? "Unknown-Bank",
                voucherId = y.voucherId,
                amount = y.amountCur,
                methodOfPayment = y.paymentMode,
                chequeNumber = y.chequeNum,
                chequeDate = y.chequeDate,
                companyName = y.bankRecipientName,
                source = "D365",
                status = ""
            }))
            ).GroupJoin(
                db.tblVoucherStatus.Where(d => (asOf == null ? d.logId > 0 : d.logDate < asOf))
                .GroupBy(a => a.voucherId).Select(b => b.OrderByDescending(a => a.logId).FirstOrDefault()),
                tblV => tblV.voucherId,
                tblVS => tblVS.voucherId,
                (tblV, tblVS) => new
                {
                    tblV,
                    tblVS
                }
                )
                .SelectMany(
                joined => joined.tblVS.DefaultIfEmpty(),
                (joined, tblVS) => new
                {
                    vendorCode = joined.tblV.vendorCode,
                    vendor = joined.tblV.vendor,
                    dataAreaId = joined.tblV.dataAreaId,
                    paymentId = joined.tblV.paymentId,
                    bankName = joined.tblV.bankName,
                    voucherId = joined.tblV.voucherId,
                    amount = joined.tblV.amount,
                    methodOfPayment = joined.tblV.methodOfPayment ?? "",
                    chequeNumber = joined.tblV.chequeNumber,
                    chequeDate = joined.tblV.chequeDate,
                    companyName = joined.tblV.companyName,
                    source = joined.tblV.source,
                    status = tblVS.status ?? "UNRELEASED",
                }
            ).Where(x => unreleasedStatuses.Contains(x.status) &&
            x.chequeDate >= from && x.chequeDate < to && x.dataAreaId.ToUpper() == entity);
            
            var entries = new List<UnreleasedGJEntry>();

            foreach (var payment in unreleasedPayments)
            {
                var entryStatus = "";

                var entryDescription = new List<string>();

                var isEmployee = payment.vendor?.type?.ToUpper() == "EMPLOYEE";
                    
                if (payment.vendor == null)
                {
                    entryDescription.Add("Vendor doensn't exist in the system.");
                    entryStatus = "ERROR";
                }

                if (string.IsNullOrWhiteSpace(payment.chequeNumber))
                {
                    entryDescription.Add("Please put the check number.");
                    entryStatus = "ERROR";
                }
                
                entries.Add(new UnreleasedGJEntry
                {
                    AccountType = payment.vendor?.type ?? "VENDOR",
                    Account = payment.vendorCode,
                    Transdate = payment.chequeDate,
                    Department = isEmployee == true ? "FINANCE" : "",
                    BusinessUnit = isEmployee == true ? "CSO" : "",
                    Purpose = isEmployee == true ? "EMPLOYEEES" : "",
                    TXT = entryStatus == "ERROR" ? "Couldn't generate TXT field." : payment.chequeNumber + 
                        "-UNRELEASED CHECK BDO CHKPRI " + to.ToString("MMMM YYYY") + "-" + payment.vendor.companyName,
                    Credit = payment.amount ?? 0,
                    OffsetAccountType = "Bank",
                    OffsetAccount = payment.bankName,
                    PostingProfile = "DEFAULT",
                    DueDate = payment.chequeDate,
                    Document = entryStatus == "ERROR" ? "Couldn't generate Document field." : payment.chequeNumber +
                        "-UNRELEASED CHECK BDO CHKPRI " + to.ToString("MMMM YYYY") + "-" + payment.vendor.companyName,
                    VoucherNum = payment.voucherId,
                    EntryStatus = entryStatus,
                    EntryDescription = entryDescription,
                    strTransdate = payment.chequeDate?.ToString("MM-dd-yyyy")
                });
            }

            return entries;
        }

        public string AddGeneralJournal(string entity, string description, string genJourNum, string user, string month, string year)
        {
            var genjour = new tblUnreleasedGeneralJournal
            {
                entity = entity,
                description = description,
                generalJournalNum = genJourNum,
                postedBy = user,
                month = month,
                year = year,
                postDate = DateTime.Now
            };

            db.tblUnreleasedGeneralJournals.Add(genjour);

            db.SaveChanges();

            return "SUCCESS";
        }

        public List<UnreleasedGeneralJournal> GetPostedUnreleasedGeneralJournal()
        {
            return db.tblUnreleasedGeneralJournals.Select(x => new UnreleasedGeneralJournal
            {
                id = x.id,
                postDate = x.postDate,
                postedBy = x.postedBy,
                generalJournalNum = x.generalJournalNum,
                description = x.description,
                entity = x.entity,
                month = x.month,
                year = x.year
            }).ToList();
        }
    }
}
