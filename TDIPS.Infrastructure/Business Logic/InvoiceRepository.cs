﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.TDIPSDBContextModels;

namespace TDIPS.Infrastructure.Business_Logic
{
    public class InvoiceRepository : IInvoiceRepository
    {
        TDIPSDbContext db = new TDIPSDbContext();
        tblInvoiceCorrection tblIC;


        public string AddUpdateInvoiceNumCorrection(InvoiceCorrection invoiceCorrection)
        {
            tblIC = db.tblInvoiceCorrections.FirstOrDefault(x => x.OldInvoiceNo == invoiceCorrection.OldInvoiceNo);
            string result;

            if (tblIC != null)
            {
                tblIC.NewInvoiceNo = invoiceCorrection.NewInvoiceNo;
                tblIC.ModifiedBy = invoiceCorrection.ModifiedBy;
                
                result = "UPDATED";
            }
            else
            {
                tblIC = new tblInvoiceCorrection
                {
                    OldInvoiceNo = invoiceCorrection.OldInvoiceNo,
                    NewInvoiceNo = invoiceCorrection.NewInvoiceNo,
                    ModifiedBy = invoiceCorrection.ModifiedBy
                };

                
                result = "ADDED";

                db.tblInvoiceCorrections.Add(tblIC);
            }

            db.SaveChanges();
            return result;
        }

        public string DeleteInvoiceCorrection(int invoiceId)
        {
            var invoice = db.tblInvoiceCorrections.FirstOrDefault(x => x.Id == invoiceId);

            db.tblInvoiceCorrections.Remove(invoice);

            db.SaveChanges();

            return "SUCCESS";
        }

        public List<InvoiceCorrection> GetInvoiceCorrectionList()
        {
            var invoices = db.tblInvoiceCorrections.Select(x => new InvoiceCorrection
            {
                Id = x.Id,
                OldInvoiceNo = x.OldInvoiceNo,
                NewInvoiceNo = x.NewInvoiceNo,
                ModifiedBy = x.ModifiedBy
            }).ToList();

            return invoices;
        }
    }
}
