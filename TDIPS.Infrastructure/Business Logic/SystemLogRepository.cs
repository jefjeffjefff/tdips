﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.TDIPSDBContextModels;

namespace TDIPS.Infrastructure.Business_Logic
{
    public class SystemLogRepository : ISystemLogRepository
    {
        TDIPSDbContext db = new TDIPSDbContext();

        public void Add(SystemLog sl)
        {
            try
            {
                tblSystemLog tblSL = new tblSystemLog
                {
                    logDate = DateTime.Now,
                    userId = sl.userId,
                    type = sl.type,
                    description = sl.description,
                    status = sl.status
                };
                db.tblSystemLogs.Add(tblSL);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            
        }

        public List<SystemLog> List(Nullable<DateTime> dateFrom = null, Nullable<DateTime> dateTo = null, string type = "")
        {
            if (dateFrom != null && dateTo != null && dateFrom == dateTo)
            {
                dateTo = (dateFrom ?? DateTime.Today).AddDays(1);
            }

            List<SystemLog> list = db.tblSystemLogs.Where(x => (dateFrom == null ? x.logId != 0 : (x.logDate > dateFrom && x.logDate < dateTo)) &&
            (type == "" ? x.logId != 0 : x.type == type))
                .Select(y => new SystemLog
                {
                    logId = y.logId,
                    logDate = y.logDate,
                    userId = y.userId,
                    type = y.type,
                    description = y.description,
                    status = y.description,
                    strTime = (y.logDate ?? DateTime.Now).ToString()
            }).OrderByDescending(a => a.logDate).ToList();

            return list;
        }
    }

    public static class SystemLogBusLogic
    {
        public class Logs
        {
            public static void Add(SystemLog sl)
            {
                try
                {
                    TDIPSDbContext db = new TDIPSDbContext();

                    tblSystemLog tblSL = new tblSystemLog();

                    tblSL.logDate = DateTime.Now;
                    tblSL.userId = sl.userId;
                    tblSL.type = sl.type;
                    tblSL.description = sl.description;
                    tblSL.status = sl.status;
                    db.tblSystemLogs.Add(tblSL);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }
        }
    }
}
