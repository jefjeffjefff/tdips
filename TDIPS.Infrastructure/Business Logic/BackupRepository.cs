﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDIPS.Core.Interfaces;

namespace TDIPS.Infrastructure.Business_Logic
{
    public class BackupRepository : IBackupRepository
    {
        TDIPSDbContext db = new TDIPSDbContext();

        public string Restore(string filepath)
        {
            try
            {
                string query = "USE master " +
                                "ALTER DATABASE MyTempCopy " +
                                "SET OFFLINE WITH ROLLBACK IMMEDIATE " +
                                "RESTORE DATABASE MyTempCopy FROM DISK=@filepath " +
                                "WITH " +
                                "MOVE 'DBTDIPS' TO 'C:\\Backup\\MyTempCopy.mdf'," +
                                "MOVE 'DBTDIPS_log' TO 'C:\\Backup\\MyTempCopy_log.ldf'," +
                                "REPLACE " +
                                "USE master " +
                                "ALTER DATABASE[MyTempCopy] " +
                                "SET ONLINE " +
                                "USE[DBTDIPS] " +
                                "drop table DBTDIPS.dbo.tblBank " +
                                "select * into DBTDIPS.dbo.tblBank from MyTempCopy.dbo.tblBank " +
                                "drop table DBTDIPS.dbo.tblBatchMail " +
                                "select * into DBTDIPS.dbo.tblBatchMail from MyTempCopy.dbo.tblBatchMail " +
                                "drop table DBTDIPS.dbo.tblCollectedCheck " +
                                "select * into DBTDIPS.dbo.tblCollectedCheck from MyTempCopy.dbo.tblCollectedCheck " +
                                "drop table DBTDIPS.dbo.tblCollector " +
                                "select * into DBTDIPS.dbo.tblCollector from MyTempCopy.dbo.tblCollector " +
                                "drop table DBTDIPS.dbo.tblD365IntegratedPayment " +
                                "select * into DBTDIPS.dbo.tblD365IntegratedPayment from MyTempCopy.dbo.tblD365IntegratedPayment " +
                                "drop table DBTDIPS.dbo.tblEmployee " +
                                "select * into DBTDIPS.dbo.tblEmployee from MyTempCopy.dbo.tblEmployee " +
                                "drop table DBTDIPS.dbo.tblExcelUploadedPayment " +
                                "select * into DBTDIPS.dbo.tblExcelUploadedPayment from MyTempCopy.dbo.tblExcelUploadedPayment " +
                                "drop table DBTDIPS.dbo.tblHoldRequest " +
                                "select * into DBTDIPS.dbo.tblHoldRequest from MyTempCopy.dbo.tblHoldRequest " +
                                "drop table DBTDIPS.dbo.tblMailLog " +
                                "select * into DBTDIPS.dbo.tblMailLog from MyTempCopy.dbo.tblMailLog " +
                                "drop table DBTDIPS.dbo.tblOnlinePayment " +
                                "select * into DBTDIPS.dbo.tblOnlinePayment from MyTempCopy.dbo.tblOnlinePayment " +
                                "drop table DBTDIPS.dbo.tblQueue " +
                                "select * into DBTDIPS.dbo.tblQueue from MyTempCopy.dbo.tblQueue " +
                                "drop table DBTDIPS.dbo.tblReceipt " +
                                "select * into DBTDIPS.dbo.tblReceipt from MyTempCopy.dbo.tblReceipt " +
                                "drop table DBTDIPS.dbo.tblVendor " +
                                "select * into DBTDIPS.dbo.tblVendor from MyTempCopy.dbo.tblVendor " +
                                "drop table DBTDIPS.dbo.tblVoucherRequest " +
                                "select * into DBTDIPS.dbo.tblVoucherRequest from MyTempCopy.dbo.tblVoucherRequest " +
                                "drop table DBTDIPS.dbo.tblVoucherStatus " +
                                "select * into DBTDIPS.dbo.tblVoucherStatus from MyTempCopy.dbo.tblVoucherStatus " +
                                "drop table DBTDIPS.dbo.tblVoucherStatusBatch " +
                                "select * into DBTDIPS.dbo.tblVoucherStatusBatch from MyTempCopy.dbo.tblVoucherStatusBatch " +
                                "alter table DBTDIPS.dbo.tblMailLog add primary key (logId) " +
                                "alter table DBTDIPS.dbo.tblBank add primary key(bankId) " +
                                "alter table DBTDIPS.dbo.tblBatchMail add primary key(batchMailId) "+
                                "alter table DBTDIPS.dbo.tblCollectedCheck add primary key(logId) "+
                                "alter table DBTDIPS.dbo.tblCollector add primary key(collectorId) "+
                                "alter table DBTDIPS.dbo.tblD365IntergratedPayment add primary key(paymentId) "+
                                "alter table DBTDIPS.dbo.tblEmployee add primary key(id) "+
                                "alter table DBTDIPS.dbo.tblExcelUploadedPayment add primary key(paymentId) "+
                                "alter table DBTDIPS.dbo.tblHoldRequest add primary key(logId) "+
                                "alter table DBTDIPS.dbo.tblOnlinePayment add primary key(paymentId) "+
                                "alter table DBTDIPS.dbo.tblQueue add primary key(queueId) "+
                                "alter table DBTDIPS.dbo.tblReceipt add primary key(receiptId) "+
                                "alter table DBTDIPS.dbo.tblVendor add primary key(vendorId) "+
                                "alter table DBTDIPS.dbo.tblVoucherRequest add primary key(logId) "+
                                "alter table DBTDIPS.dbo.tblVoucherStatus add primary key(logId) "+
                                "alter table DBTDIPS.dbo.tblVoucherStatusBatch add primary key(batchId) ";


                //var results = db.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, query,
                // new SqlParameter("@filepath", filepath));
                return "SUCCESS";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
