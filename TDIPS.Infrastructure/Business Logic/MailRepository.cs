﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.TDIPSDBContextModels;
using System.Net.Http;
using System.Text.RegularExpressions;
//using System.Windows.Forms.DataVisualization.Charting;
using System.IO;
using Microsoft.EntityFrameworkCore;

namespace TDIPS.Infrastructure.Business_Logic
{
    public class MailRepository : IMailRepository
    {
        PSQLControl PSQLControl = new PSQLControl();

        const string SHORT_CODE = "21589907";
        const string APP_ID = "EkA5ugoxGEu67igXz5cxxzu8ok6pur64";
        const string APP_SECRET = "e9590c8847c2ccc5b5529f0620802e9bbc008d70b158bc61783707e3c5628642";
        const string ACCESS_TOKEN = "bsaipgYZqOPywZrbG3tZZQ54W7yK81J3r_s4o3ZD1n8";
        
        //IVoucherRepository VoucherRepository = new VoucherRepository();
        TDIPSDbContext db = new TDIPSDbContext();
        tblEmployee tblE;
        tblMailLog tblML;
        tblBatchMail tblBM;
        IEmployeeRepository EmployeeRepository = new EmployeeRepository();
        IOnlinePaymentRepository OnlinePaymentRepository = new OnlinePaymentRepository();
        IUserRepository UserRepository = new UserRepository();

        public List<Mail> GetContactFromCollectorsAndVendors(string[] voucherId = null)
        {
            //this removes SEAN Code from Array
            for (var x = 0; x < voucherId.Length; x++)
            {
                if (voucherId[x].Contains("?"))
                {
                    var vId = voucherId[x].Split('?');
                    voucherId[x] = vId[0];
                }
            }

            string pquery = @$"SELECT ROW_NUMBER() OVER (ORDER BY tblVC.Email) as id, tblVC.""companyName"", tblVC.Email, tblVC.""contactNumber"", tblVC.""vendorCode"", tblVC.receiverName, tblVC.EmailFrom, tblVouchers.""voucherId"",tblVouchers.amount, tblVouchers.""chequeNumber"", tblVouchers.""chequeDate"", tblVouchers.""dataAreaId"", tblVouchers.bank FROM  
                        (SELECT ""companyName"", ""companyEmail"" AS Email, ""contactNumber"", ""vendorCode"", ""companyName"" AS receiverName, 'VENDOR' AS EmailFrom FROM dbo.""tblVendor"" UNION
                        (SELECT ""tblVendor"".""companyName"", ""tblCollector"".""email"", ""tblCollector"".""contactNumber"", ""tblVendor"".""vendorCode"", ""tblCollector"".""collectorName"" as receiverName, 'COLLECTOR' AS EmailFrom FROM dbo.""tblCollector""
                        INNER JOIN dbo.""tblVendor"" ON dbo.""tblCollector"".""vendorID"" = dbo.""tblVendor"".""vendorID"")) AS tblVC RIGHT JOIN
                        (SELECT ""voucherId"", ""vendorCode"", ""amount"", ""chequeNumber"", ""chequeDate"", ""dataAreaId"", ""bankName"" as bank FROM dbo.""tblExcelUploadedPayment"" UNION
                        (SELECT ""voucherId"", ""recipientAccountNum"" AS vendorCode, ""amountCur"" AS amount, ""chequeNum"" AS chequeNumber, ""chequeDate"", ""dataAreaId"", ""accountId"" as bank FROM dbo.""tblD365IntegratedPayment""))  
                        AS tblVouchers ON tblVC.""vendorCode"" LIKE CONCAT('%',tblvouchers.""vendorCode"",'%') ORDER BY ""vendorCode""";


            //string query = "SELECT ROW_NUMBER() OVER (ORDER BY tblVC.Email) as id, tblVC.companyName, tblVC.Email, tblVC.contactNumber, tblVC.vendorCode, tblVC.receiverName, tblVC.EmailFrom, tblVouchers.voucherId,tblVouchers.amount, tblVouchers.chequeNumber, tblVouchers.chequeDate, tblVouchers.dataAreaId, tblVouchers.bank FROM " +
            //            "(SELECT companyName, companyEmail AS Email, contactNumber, vendorCode, companyName AS receiverName, 'VENDOR' AS EmailFrom FROM tblVendor UNION " +
            //            "(SELECT tblVendor.companyName, tblCollector.email, tblCollector.contactNumber, tblVendor.vendorCode, tblCollector.collectorName as receiverName, 'COLLECTOR' AS EmailFrom FROM tblCollector " +
            //            "INNER JOIN tblVendor ON tblCollector.vendorID = tblVendor.vendorID)) AS tblVC RIGHT JOIN " +
            //            "(SELECT voucherId, vendorCode, amount, chequeNumber, chequeDate, dataAreaId, bankName as bank FROM tblExcelUploadedPayment UNION " +
            //            "(SELECT voucherId, recipientAccountNum AS vendorCode, amountCur AS amount, chequeNum AS chequeNumber, chequeDate, dataAreaId, accountId as bank FROM tblD365IntegratedPayment)) " +
            //            "AS tblVouchers ON tblVC.vendorCode LIKE '%' + tblvouchers.vendorCode + '%' ORDER BY vendorCode";

            PSQLControl.ExecQuery(pquery);

            var voucherVendorsCollectors = db.tblMails.FromSqlRaw(pquery).ToList();

            var voucherStatuses = db.tblVoucherStatus.Where(x => voucherId.Contains(x.voucherId)).ToList();

            List<Mail> mails = voucherVendorsCollectors.Join(
                voucherStatuses.GroupBy(a => a.voucherId).Select(b => b.OrderByDescending(a => a.logId).FirstOrDefault()).AsEnumerable(),
                tblN => tblN.voucherId,
                tblVS => tblVS.voucherId,
                (tblN, tblVS) => new Mail
                {
                    dateAreaId = tblN.dataAreaid,
                    companyName = tblN.companyName,
                    receiverIdentification = tblN.EmailFrom + "_" + tblN.receiverName,
                    receiverContactNumber = tblN.contactNumber,
                    amount = tblN.amount,
                    receiverEmail = tblN.Email,
                    SEANCODE = tblVS.SEANCode,
                    vendorCode = tblN.vendorCode,
                    voucherId = tblN.voucherId,
                    receiverName = tblN.receiverName ?? tblN.companyName,
                    releasingDate = tblVS.releasingDate ?? DateTime.Now,
                    chequeDate = tblN.chequeDate ?? null,
                    chequeNumber = tblN.chequeNumber,
                    bank = tblN.bank
                }).OrderBy(v => v.vendorCode).ThenBy(e => e.receiverEmail)
                .ToList();

            return mails;
        }

        public void SendMail(List<Mail> e, string noticeType, List<Invoice> invoice = null)
        {
            var batchId = 0;

            try
            {
                string[] vendorIds = e.Select(x => x.vendorCode).Distinct().ToArray();
                //const string htmlValue = "font-size:14px;font-weight:600;margin-left:7px;";
                const string htmlText = "font-size:14px;font-weight:600;";
                const string htmlDiv = "display:flex;margin-bottom:6px;";
                const string border = "border: 1px solid black;";
                //const string tdPadding = "padding:10px;";

                var allContacts = e.Select(x => new { x.vendorCode, x.receiverName, x.receiverEmail, x.receiverContactNumber }).Distinct().ToList();
                tblBM = new tblBatchMail
                {
                    logDate = DateTime.Now,
                    status = "PROCESSING"
                };
                db.tblBatchMails.Add(tblBM);
                db.SaveChanges();

                batchId = tblBM.batchMailId;

                foreach (var c in allContacts)
                {
                    if (!string.IsNullOrWhiteSpace(c.receiverEmail))
                    {
                        tblML = new tblMailLog
                        {
                            type = "EMAIL",
                            receiverName = c.receiverName,
                            status = "SENDING",
                            contact = c.receiverEmail,
                            description = "Voucher Release Notice",
                            batchId = batchId
                        };

                        db.tblMailLogs.Add(tblML);
                    }

                    if (!string.IsNullOrWhiteSpace(c.receiverContactNumber))
                    {
                        tblML = new tblMailLog
                        {
                            type = "SMS",
                            receiverName = c.receiverName,
                            status = "SENDING",
                            contact = c.receiverContactNumber,
                            description = "Voucher Release Notice",
                            batchId = batchId
                        };

                        db.tblMailLogs.Add(tblML);
                    }
                }

                db.SaveChanges();

                for (var x = 0; x < vendorIds.Length; x++)
                {
                    List<Mail> mails = e.Where(y => y.vendorCode == vendorIds[x]).ToList();
                    decimal paymentTotal = mails.GroupBy(a => a.voucherId).Select(b => b.FirstOrDefault()).Sum(c => c.amount) ?? 0;

                    string[] voucherIds = e.Where(y => y.vendorCode == vendorIds[x]).OrderBy(a => a.dateAreaId).Select(v => v.voucherId).Distinct().ToArray();

                    string SEANCode = mails.FirstOrDefault().SEANCODE;
                    string CompanyName = mails.FirstOrDefault().companyName;
                    string releasingDate = mails.FirstOrDefault().releasingDate.ToString("MMM dd yyyy");

                    mails = mails.GroupBy(y => y.receiverIdentification).Select(y => y.FirstOrDefault()).ToList();

                    List<ContactDetails> receiverContactDetails = mails.Select(y => new ContactDetails
                    {
                        ContactNumber = y.receiverContactNumber,
                        EmailAddress = y.receiverEmail,
                        ReceiverName = y.receiverName
                    }).ToList();

                    if (noticeType == "APPROVAL")
                    {
                        foreach (var cd in receiverContactDetails)
                        {
                            StringBuilder body = new StringBuilder("<div style='display:flex;margin-top:20px;'><div style='font-size:16px;font-weight:700;display:inline-block;'>To:</div> <div style='color:#bf1200;font-weight:700;margin-left:7px;font-size:16px !important;display:inline-block;'>" + CompanyName + "</div></div>");
                            StringBuilder smsbody = new StringBuilder();

                            body.Append("<div style='margin-bottom:30px;margin-top:35px;font-weight:600;font-size:15px;'>Please be informed that our weekly payment releasing will continue till further notice. But here are some precautionary reminders for everyone's safety:</div>");
                            body.Append("<ol style='margin-top:10px;margin-bottom:10px;font-size:14px;'>");
                            body.Append("<li>Shakeys will be taking the body temperature of all visitors at the lobby. Kindly submit yourself to be checked.</li>");
                            body.Append("<li>Highly advised to please wear a mask and practice proper coughing etiquette. </li>");
                            body.Append("<li>Practice hand sanitation diligently before entering the premises and while on the Shakeys building. We had provided hand sanitizers and soap in our Comfort Rooms for the use of everyone. </li>");
                            body.Append("<li>Avoid close physical contact and greet each other without handshakes as much as possible. </li>");
                            body.Append("</ol>");

                            body.Append("<div style='margin-bottom:10px;font-size:14px;'>Please see the details below for your payments:</div>");

                            body.Append("<div style='" + htmlDiv + "'><div style='" + htmlText + "display:inline-block;'>SEAN Code:</div> <div style='font-size:15px;font-weight:600;margin-left:7px;" + "color:#bf1200;display:inline-block;'>" + SEANCode + "</div> </div>");
                            body.Append("<div style='margin-top:11px;margin-bottom:13px;font-weight:500;font-size:14px;'><u>IMPORTANT</u>: Kindly prepare your Official Receipt beforehand using the details in the tables below. This will result in faster releasing of your payment.</div>");
                            body.Append("<div style='" + htmlDiv + "'><div style='margin-bottom:6px;display:inline-block;" + htmlText + "'>Releasing Date : </div> <div style='font-size:14px;font-weight:600;display:inline-block;'>" + releasingDate + "</div></div>");
                            body.Append("<div style='" + htmlDiv + "'><div style='" + htmlText + "width:58px;display:inline-block;'>Address:</div> <div style='font-size:14px;font-weight:600;display:inline;'> KM15 East Service Rd cor. Marian Road 2, San Martin De Porres, Paranaque City, 1700 </div></div>");

                            body.Append("<div style='margin-top:10px;font-weight:500;font-size:15px;'><u>NOTE</u>: If you fail to collect the payment on the above - mentioned date, the payment will be re - scheduled on another day.Please wait for notifications of the next payment releasing.</div>");

                            body.Append("<div style='" + "display:flex;margin-bottom:-9px;margin-top:30px;" + "'><div style='" + "font-size:19px;font-weight:700;" + "'>Total Payment: ₱" + String.Format("{0:n}", Math.Round(paymentTotal, 2)) + "</div></div>");

                            smsbody.Append("Payment Release Notice\n\n");
                            smsbody.Append("To: " + CompanyName + "\n\n");

                            smsbody.Append("Please be informed that our weekly payment releasing will continue till further notice. But here are some precautionary reminders for everyone's safety:.\n\n");
                            smsbody.Append("1. Shakeys will be taking the body temperature of all visitors at the lobby. Kindly submit yourself to be checked. \n");
                            smsbody.Append("2. Highly advised to please wear a mask and practice proper coughing etiquette. \n");
                            smsbody.Append("3. Practice hand sanitation diligently before entering the premises and while on the Shakeys building. We had provided hand sanitizers and soap in our Comfort Rooms for the use of everyone. \n");
                            smsbody.Append("4. Avoid close physical contact and greet each other without handshakes as much as possible. \n\n");

                            smsbody.Append("Please see the details below for your payments: \n");

                            smsbody.Append("SEAN Code: " + SEANCode);
                            smsbody.Append("\nIMPORTANT: Kindly prepare your Official Receipt beforehand using the details in the list below. This will result in faster releasing of your payment.\n");
                            smsbody.Append("\nReleasing Date : " + releasingDate);
                            smsbody.Append("\nAddress: KM15 East Service Rd cor. Marian Road 2, San Martin De Porres, Paranaque City,1700");
                            smsbody.Append("\nNOTE: If you fail to collect the payment on the above - mentioned date, the payment will be re - scheduled on another day.Please wait for notifications of the next payment releasing.\n");

                            smsbody.Append("\n\nTotal Payment: ₱" + String.Format("{0:n}", Math.Round(paymentTotal, 2)) + "\n");

                            body.Append("<div style='margin-top:25px;margin-bottom:20px;font-weight:700;font-size:16px;text-decoration:underline;'>Paying Company Details:</div></div>");
                            smsbody.Append("\n\nPaying Company Details:\n");

                            var prevEntity = "";
                            var currentEntity = "";
                            string bank = "";
                            string company;
                            string tin;
                            string businessStyle;
                            string address;

                            for (int i = 0; i < voucherIds.Length; i++)
                            {
                                Mail voucherDetails = e.Where(a => a.voucherId == voucherIds[i]).FirstOrDefault();
                                var invoiceNumbers = String.Join(", ", invoice.Where(a => a.offSetTransVoucher == voucherIds[i]).Select(a => a.invoiceId).ToArray());
                                decimal amount = voucherDetails.amount ?? 0;
                                var dataAreaId = voucherDetails.dateAreaId.ToUpper();


                                if (dataAreaId == "SPAV" || dataAreaId == "NAF")
                                {
                                    company = "Shakeys Pizza Asia Ventures Inc";
                                    tin = "000-163-396-000";
                                    businessStyle = "SHAKEYS";
                                    address = "KM15 East Service";
                                }
                                else if (dataAreaId == "SPCI")
                                {
                                    company = "Shakeys Pizza Commerce Inc";
                                    tin = "009-891-171-000";
                                    businessStyle = "SPCI";
                                    address = "Road San Martin";
                                }
                                else if (dataAreaId == "WBHI")
                                {
                                    company = "Wow Brand Holdings Inc";
                                    tin = "010-314-863-000";
                                    businessStyle = "WOW BRAND HOLDINGS INC";
                                    address = "Parañaque";
                                }
                                else if (dataAreaId == "DBE")
                                {
                                    company = "DBE Project INC";
                                    tin = "008-453-049-000";
                                    businessStyle = "DBE PROJECT INC";
                                    address = "De Porres";
                                }
                                else
                                {
                                    company = "-";
                                    tin = "-";
                                    businessStyle = "-";
                                    address = "-";
                                }

                                if (dataAreaId == "SPAV" || dataAreaId == "NAF")
                                {
                                    currentEntity = "SPAV/NAF";
                                }
                                else
                                {
                                    currentEntity = dataAreaId;
                                }

                                if (prevEntity != currentEntity)
                                {
                                    if (prevEntity != "")
                                    {
                                        body.Append("</tbody></table></div>");
                                        smsbody.Append("\n\n");
                                    }

                                    body.Append("<h1 style='font-size:15px;margin-top:25px;'>Paying Company: " + company + " (" + currentEntity + ")</h1>");
                                    body.Append("<h1 style='font-size:15px;margin-top:7px;'>Tin: " + tin + "</h1>");
                                    body.Append("<h1 style='font-size:15px;margin-top:7px;'>Business Style: " + businessStyle + "</h1>");
                                    body.Append("<h1 style='font-size:15px;margin-top:7px;'>Address: " + address + "</h1>");
                                    body.Append("<div style='overflow-x:auto'>");
                                    body.Append("<table style='margin-top:8px;width:100%;" + border + "border-collapse:collapse;text-align:center;'>");
                                    body.Append("<thead style='font-weight:700;'><tr style='" + border + "'>");
                                    body.Append("<td style='width:50px; " + border + "'>Company</td>");
                                    body.Append("<td style='width:100px; " + border + "'>Payment Voucher</td>");
                                    body.Append("<td style='width:100px; " + border + "'>Check Number</td>");
                                    body.Append("<td style='width:90px; " + border + "'>Check Date</td>");
                                    body.Append("<td style='width:90px; " + border + "'>Check Amount</td>");
                                    body.Append("<td style='width:90px; " + border + "'>Bank</td>");
                                    body.Append("<td style='" + border + "'>Invoices</td>");
                                    body.Append("</tr></thead><tbody style='font-weight:600;'>");

                                    smsbody.Append("\nPaying Company: " + company + " (" + currentEntity + ")");
                                    smsbody.Append("\nTin: " + tin);
                                    smsbody.Append("\nBusiness Style: " + businessStyle);
                                    smsbody.Append("\nAddress: " + address);

                                    prevEntity = currentEntity;
                                }

                                if (voucherDetails.bank.Contains("-"))
                                {
                                    bank = voucherDetails.bank.Split('-')[0];
                                }
                                else
                                {
                                    bank = voucherDetails.bank;
                                }

                                DateTime chequeDate = voucherDetails.chequeDate ?? DateTime.Today;

                                body.Append("<tr style='" + border + "'>");
                                body.Append("<td valign='top' style='" + border + "'>" + currentEntity + "</td>");
                                body.Append("<td valign='top' style='" + border + "'>" + voucherDetails.voucherId + "</td>");
                                body.Append("<td valign='top' style='" + border + "'>" + (voucherDetails.chequeNumber ?? "") + "</td>");
                                body.Append("<td valign='top' style='" + border + "'>" + chequeDate.ToString("MMM dd yyyy") + "</td>");
                                body.Append("<td valign='top' style='" + border + "'>₱" + String.Format("{0:n}", Math.Round(amount, 2)) + "</td>");
                                body.Append("<td valign='top' style='" + border + "'>" + bank + "</td>");
                                body.Append("<td valign='top' style='" + border + "'>" + (invoiceNumbers ?? " ") + "</td>");
                                body.Append("</tr>");

                                smsbody.Append("\n\nCompany: " + currentEntity);
                                smsbody.Append("\nVoucher: " + voucherDetails.voucherId);
                                smsbody.Append("\nCheck No: " + voucherDetails.chequeNumber ?? "");
                                smsbody.Append("\nCheck Date: " + chequeDate.ToString("MMM dd yyyy"));
                                smsbody.Append("\nAmount: ₱" + String.Format("{0:n}", Math.Round(amount, 2)));
                                smsbody.Append("\nBank: " + bank);
                                smsbody.Append("\nInvoice: " + (invoiceNumbers ?? " "));
                            }


                            body.Append("</tbody></table></div>");

                            body.Append("<div style='margin-top:40px;font-size:15px;font-weight:500;'>If you have any inquiries regarding your payment, please email at mmdechavez@shakeys.biz. </div>");
                            body.Append("<div style='width:95%;'><p style='margin-top:65px;margin-bottom:0px;font-weight:700;font-size:18px;'>Kind Regards,</p><p style='font-weight:700;font-size:18px;font-style:italic;margin-top:12px;'>Shakeys Treasury</p></div>");
                            body.Append("<p style='text-indent:28px;display:inline-block;font-style:italic;margin-top:45px;line-height:20px;margin-bottom:50px;'>CONFIDENTIALITY NOTICE: This is a system generated email – please do not reply to it. The contents of this email/sms is confidential and intended for the recipient specified in message only and may be legally protected from disclosure. If you received this message by mistake, please reply to this email mmdechavez@shakeys.biz and follow with its deletion, so that we can ensure such a mistake does not occur in the future. It is strictly forbidden to share any part of this message with any third party, without a written consent of the sender.  </p>");

                            smsbody.Append("\n\nIf you have any inquiries regarding your payment, please email at mmdechavez@shakeys.biz.");

                            smsbody.Append("\n\nANNOUNCEMENT: Effective since October 18, 2019, the Countering of Invoices will be every Friday starting 10:00 AM to 4:00 PM. Please make sure to have your complete attachment with you for countering to avoid delays.");
                            smsbody.Append("\n\nKind Regards,\nShakeys Treasury\n\n\n");
                            smsbody.Append("CONFIDENTIALITY NOTICE: This is a system generated email – please do not reply to it. The contents of this email/sms is confidential and intended for the recipient specified in message only and may be legally protected from disclosure. If you received this message by mistake, please reply to this email mmdechavez@shakeys.biz and follow with its deletion, so that we can ensure such a mistake does not occur in the future. It is strictly forbidden to share any part of this message with any third party, without a written consent of the sender.  ");

                            Message msg = new Message
                            {
                                Subject = "Payment Release Notice",

                                Body = body.ToString(),
                                ReceiverEmail = cd.EmailAddress
                            };

                            string emailResult = SendToEmail(msg);
                            string smsResult = SendSMSAsync(cd.ContactNumber, smsbody.ToString()).Result;


                            tblML = db.tblMailLogs.FirstOrDefault(y => y.contact == cd.EmailAddress &&
                            y.type == "EMAIL" && y.receiverName == cd.ReceiverName && y.status == "SENDING" && y.batchId == batchId);

                            if (!String.IsNullOrWhiteSpace(cd.EmailAddress))
                            {
                                if (tblML != null)
                                {
                                    tblML.status = emailResult;
                                    tblML.dateTimeSent = DateTime.Now;
                                    tblML.receiverName = cd.ReceiverName;
                                    tblML.contact = cd.EmailAddress;
                                }
                                else
                                {
                                    tblML = new tblMailLog
                                    {
                                        type = "EMAIL",
                                        dateTimeSent = DateTime.Now,
                                        receiverName = cd.ReceiverName,
                                        status = emailResult,
                                        description = "Voucher Release Notice"
                                    };
                                }

                                db.SaveChanges();
                            }

                            if (!String.IsNullOrWhiteSpace(cd.ContactNumber))
                            {
                                tblML = db.tblMailLogs.FirstOrDefault(y => y.contact == cd.ContactNumber &&
                            y.type == "SMS" && y.receiverName == cd.ReceiverName && y.status == "SENDING" && y.batchId == batchId);

                                if (tblML != null)
                                {
                                    tblML.status = smsResult;
                                    tblML.dateTimeSent = DateTime.Now;
                                    tblML.receiverName = cd.ReceiverName;
                                    tblML.contact = cd.ContactNumber;
                                }
                                else
                                {
                                    tblML = new tblMailLog
                                    {
                                        type = "SMS",
                                        dateTimeSent = DateTime.Now,
                                        receiverName = cd.ReceiverName,
                                        status = emailResult,
                                        contact = cd.ContactNumber,
                                        description = "Voucher Release Notice"
                                    };
                                }

                                db.SaveChanges();
                            }
                        }

                        tblBM = db.tblBatchMails.FirstOrDefault(y => y.batchMailId == batchId);
                        tblBM.status = "COMPLETED";
                        db.SaveChanges();
                    }
                }

            }
            catch (Exception ex)
            {
                tblBM = db.tblBatchMails.FirstOrDefault(y => y.batchMailId == batchId);
                if (tblBM != null)
                {
                    tblBM.status = "STOPPED";
                    db.SaveChanges();
                }

                var sendingMails = db.tblMailLogs.Where(x => x.status == "SENDING" && x.batchId == batchId).ToList();
                sendingMails.ForEach(x => x.status = "STOPPED");
                db.SaveChanges();

                SystemLog sl = new SystemLog
                {
                    logDate = DateTime.Now,
                    userId = "SYSTEM",
                    type = "ERROR",
                    description = ex.Message,
                    status = "ERROR"
                };

                ISystemLogRepository SystemLogRepository = new SystemLogRepository();
                SystemLogRepository.Add(sl);
            }
        }

        static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        static string SendToEmail(Message msg)
        {
            try
            {
                if (msg.ReceiverEmail == "" || msg.ReceiverEmail == null)
                {
                    return "EMAIL IS EMPTY";
                }

                msg.ReceiverEmail = Regex.Replace(msg.ReceiverEmail, @"\s+", "");

                var message = new MailMessage();
                message.To.Add(new MailAddress(msg.ReceiverEmail));  // replace with valid value 
                message.From = new MailAddress("tdipsnoreply@shakeys.biz");  // replace with valid value
                message.Subject = msg.Subject;
                message.Body = msg.Body;
                message.IsBodyHtml = true;

                if (msg.Attachment != null)
                {
                    for (int i = 0; i < msg.Attachment.Length; i++)
                    {
                        if (System.IO.File.Exists(msg.Attachment[i]))
                        {
                            Attachment attachment = new Attachment(msg.Attachment[i]);
                            message.Attachments.Add(attachment);
                        }
                    }
                }

                using (var smtp = new SmtpClient())
                {
                    var credential = new NetworkCredential
                    {
                        UserName = "tdipsnoreply@shakeys.biz",  // replace with valid value,
                        Password = "td1p$@dm1n"
                    };
                    smtp.Credentials = credential;
                    smtp.Host = "smtp-mail.outlook.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.Send(message);
                }

                return "SUCCESS";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        static async System.Threading.Tasks.Task<string> SendSMSAsync(string mobileNumber, string message)
        {
            if (mobileNumber == null || mobileNumber == "")
            {
                return "FAILED:NUMBER IS EMPTY";
            }

            mobileNumber = Regex.Replace(mobileNumber, @"\s+", "");

            if (mobileNumber.Length == 10 && mobileNumber.Substring(0, 1) == "9")
            {
                mobileNumber = "+63" + mobileNumber;
            }

            try
            {
                var values = new Dictionary<string, string>
                {
                    { "message", message},
                    { "address", mobileNumber },
                    { "passphrase", "Qnkg1p6E1K" },
                    { "app_id", "K5pMtbEyoGhRdiEdR5cyMXhR95G6t4Gk" },
                    { "app_secret", "68e41664bfd4424dd4c22e3849c67a5b875fe5e435b35cbb77dd8563ccd29974" }
                };

                var content = new FormUrlEncodedContent(values);

                using (var client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromMinutes(5);

                    var response = await client.PostAsync("https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/21587678/requests/", content);

                    var responseString = await response.Content.ReadAsStringAsync();

                    if (response.IsSuccessStatusCode == true)
                    {
                        return "SUCCESS";
                    }

                    return responseString;
                }
            }
            catch (Exception ex)
            {
                return ex.Message; ;
            }
        }

        public void VoucherNotification(string status, string[] usertype)
        {
            var users = UserRepository.GetUsersList();

            foreach (var u in users)
            {
                if (u.Email != "")
                {
                    Message msg = new Message
                    {
                        Body = "You have a new " + status + " request now. Please check your account at <a href='tdips.shakeys.solutions'>TDIPS</a>",
                        Subject = status.ToUpper() + " REQUEST",
                        ReceiverEmail = u.Email
                    };

                    SendToEmail(msg);
                }
            }
        }

        public void OnlinePaymentMail(List<EmployeeOnlinePayment> op)
        {
            if (op.FirstOrDefault() != null && op.FirstOrDefault().employeeId == null)
            {
                return;
            }

            List<Employee> allContacts = op.Join(
                db.tblEmployees.DefaultIfEmpty(),
                tblOP => tblOP.employeeId,
                tblE => tblE.employee_no,
                (tblOP, tblE) => new Employee
                {
                    employee_no = tblE.employee_no,
                    employeeName = tblE.employeeName ?? "",
                    email = tblE.email ?? ""
                }
                ).ToList();

            tblBM = new tblBatchMail
            {
                logDate = DateTime.Now,
                status = "PROCESSING"
            };
            db.tblBatchMails.Add(tblBM);
            db.SaveChanges();

            var batchId = tblBM.batchMailId;

            foreach (var c in allContacts)
            {
                tblML = new tblMailLog
                {
                    type = "EMAIL",
                    receiverName = "(" + c.employee_no + ") " + c.employeeName,
                    status = "SENDING",
                    contact = c.email,
                    description = "Online Payment Notice",
                    batchId = batchId
                };

                db.tblMailLogs.Add(tblML);
            }

            db.SaveChanges();


            string[] employeeIdArr = op.Select(x => x.employeeId).Distinct().ToArray();

            for (int i = 0; i < employeeIdArr.Length; i++)
            {
                var employeeId = employeeIdArr[i];

                tblE = db.tblEmployees.FirstOrDefault(x => x.employee_no == employeeId);
                tblML = db.tblMailLogs.FirstOrDefault(x => x.receiverName.Contains(employeeId) && x.batchId == batchId);

                if (tblE == null)
                {
                    tblML.status = "FAILED: EMPLOYEE NOT FOUND";
                }
                else
                {
                    string result = "";

                    try
                    {
                        var totalAmount = op.Where(x => x.employeeId == employeeId).AsEnumerable().Sum(x => x.amount);

                        var bank = "BPI";

                        var entity = op.Where(x => x.employeeId == employeeId).FirstOrDefault()?.entity;

                        if (entity == "WBHI") {
                            bank = "BDO";
                        }

                        StringBuilder body = new StringBuilder("<div style='font-size:16px;font-weight:600'>To: " + tblE.employeeName + "</div>");
                        body.Append("<br /><div style='font-weight:600;'>Good Day!</div> <br />");
                        body.Append("<div style='text-indent:15px;line-height:22px;'>Please be informed that an amount of ₱" +
                            string.Format("{0:0.00}", totalAmount) + " has been deposited into your account number " +
                            " <u>" + tblE.accountNo + "</u>.</div>");
                        body.Append("<br /> <div style='text-indent:15px;line-height:22px;'>Kindy check your "+ bank +" payroll account and if you have any questions and inquiries, " +
                            "kindly email jggonzales@shakeys.biz.</div>");
                        body.Append("<br /> <br /> <div style='font-weight:600;font-size:16px;'>Kind Regards,</div>");
                        body.Append("<div style='font-weight:600;font-size:16px;margin-top:2px;'>Shakey's Treasury</div>");

                        body.Append("<p style='display:inline-block;font-style:italic;margin-top:45px;line-height:20px;margin-bottom:50px;'>CONFIDENTIALITY NOTICE: This is a system generated email – please do not reply to it. The contents of this email/sms is confidential and intended for the recipient specified in message only and may be legally protected from disclosure. If you received this message by mistake, please reply to this email mmdechavez@shakeys.biz and follow with its deletion, so that we can ensure such a mistake does not occur in the future. It is strictly forbidden to share any part of this message with any third party, without a written consent of the sender.  </p>");

                        Message msg = new Message
                        {
                            Body = body.ToString(),
                            Subject = "ONLINE PAYMENT NOTICE",
                            ReceiverEmail = tblE.email
                        };

                        result = SendToEmail(msg);
                    }
                    catch (Exception ex)
                    {
                        result = ex.Message;
                    }

                    tblML.status = result;
                    tblML.dateTimeSent = DateTime.Now;
                }
                db.SaveChanges();
            }

            var sendingMails = db.tblMailLogs.Where(x => x.status == "SENDING" && x.batchId == batchId).ToList();
            sendingMails.ForEach(x => x.status = "STOPPED");

            tblBM = db.tblBatchMails.FirstOrDefault(y => y.batchMailId == batchId);
            tblBM.status = "COMPLETED";
            db.SaveChanges();
        }

        public object MailLogs(DateTime dateFrom, DateTime dateTo)
        {
            object ml = db.tblMailLogs.Join(
                db.tblBatchMails,
                tblML => tblML.batchId,
                tblBM => tblBM.batchMailId,
                (tblML, tblBM) => new
                {
                    tblML.logId,
                    tblML.batchId,
                    tblML.type,
                    dateTimeSent = (tblML.dateTimeSent ?? new DateTime(2000, 1, 1)).ToString(),
                    tblML.receiverName,
                    tblML.status,
                    contact = tblML.contact ?? "",
                    tblML.description,
                    tblBM.logDate,
                    batchStatus = tblBM.status
                }
            ).Where(x => x.logDate >= dateFrom && x.logDate <= dateTo).OrderByDescending(x => x.batchStatus).ThenBy(x => x.status);

            return ml;
        }

        public MailStatus TrackMailStatus()
        {
            var ml = db.tblMailLogs.Join(
                db.tblBatchMails,
                tblML => tblML.batchId,
                tblBM => tblBM.batchMailId,
                (tblML, tblBM) => new
                {
                    tblML.logId,
                    tblML.batchId,
                    tblML.type,
                    dateTimeSent = tblML.dateTimeSent.ToString(),
                    tblML.receiverName,
                    tblML.status,
                    contact = tblML.contact ?? "",
                    tblML.description,
                    tblBM.logDate,
                    batchStatus = tblBM.status
                }
            ).Where(x => x.batchStatus == "PROCESSING");

            MailStatus MS = new MailStatus
            {
                Success = ml.Where(x => x.status == "SUCCESS").Count(),
                Sending = ml.Where(x => x.status == "SENDING").Count(),
                Failed = ml.Where(x => x.status != "SUCCESS" && x.status != "SENDING").Count()
            };

            return MS;
        }

        public List<Voucher> GetForReleasingVoucherList(string vendorId = "", DateTime? releasingDate = null)
        {
            if (vendorId != "")
            {
                var tblV = db.tblVendors.Where(x => x.vendorCode.Contains(vendorId)).OrderByDescending(x => x.vendorCode).FirstOrDefault();
                if (tblV != null)
                {
                    vendorId = tblV.vendorCode;
                }
            }

            var vouchers = db.tblVoucherRequests.Where(x => x.status == "FOR RELEASING"
            && (vendorId == "" ? x.logId > 0 : ((x.vendorCode.Contains(vendorId) || vendorId.Contains(x.vendorCode)) &&
            (x.releasingDate == (releasingDate ?? DateTime.Today))))).Select(x => new Voucher
            {
                batchId = x.batchId,
                voucherId = x.voucherId,
                vendorCode = x.vendorCode,
                companyName = x.companyName,
                entity = x.entity,
                bank = x.bank,
                amount = x.amount,
                status = x.status,
                checkNum = x.checkNum,
                checkDate = x.checkDate,
                releasingDate = x.releasingDate,
                SEANCode = x.SEANCode
            }).ToList();

            return vouchers;
        }

        public List<Mail> GetContactFromCollectorsAndVendorsWithNoCollection(string[] vendorCodes = null)
        {
            string query = "SELECT ROW_NUMBER() OVER(ORDER BY tblVC.Email) as id, NULL as amount, '' as chequeNumber, '' as dataAreaId, '' as bank, NULL as chequeDate, MAX(tblVC.companyName) AS companyname, tblVC.Email, tblVC.contactNumber, MAX(tblVC.vendorCode) as vendorCode, MAX(tblVC.receiverName) as receiverName, MAX(tblVC.EmailFrom) as emailFrom, voucherId as voucherId FROM " +
                       "(SELECT companyName, companyEmail AS Email, contactNumber, vendorCode, companyName AS receiverName, 'VENDOR' AS EmailFrom FROM tblVendor UNION " +
                       "(SELECT tblVendor.companyName, tblCollector.email, tblCollector.contactNumber, tblVendor.vendorCode, tblCollector.collectorName as receiverName, 'COLLECTOR' AS EmailFrom FROM tblCollector " +
                       "INNER JOIN tblVendor ON tblCollector.vendorID = tblVendor.vendorID)) AS tblVC LEFT JOIN " +
                       "(SELECT voucherId, vendorCode, amount, chequeNumber, chequeDate, dataAreaId, bankName as bank FROM tblExcelUploadedPayment UNION " +
                       "(SELECT voucherId, recipientAccountNum AS vendorCode, amountCur AS amount, chequeNum AS chequeNumber, chequeDate, dataAreaId, accountId as bank FROM tblD365IntegratedPayment)) " +
                        "AS tblVouchers ON tblVC.vendorCode LIKE '%' + tblvouchers.vendorCode + '%' " +
                        "WHERE((tblVC.email is not null AND tblVC.email != '') OR(tblVC.contactNumber is not null AND tblVC.contactNumber != '')) " +
                        "GROUP BY tblVC.contactNumber, tblVC.Email, voucherId";


            //List<Mail> mails = db.tblMails.SqlQuery(query).Where(x => vendorCodes.Any(x.vendorCode.Contains)).Join(
            //    db.tblVoucherStatus.GroupBy(a => a.voucherId).Select(b => b.OrderByDescending(a => a.logDate).FirstOrDefault()),
            //    tblN => tblN.voucherId,
            //    tblVS => tblVS.voucherId,
            //    (tblN, tblVS) => new Mail
            //    {
            //        dateAreaId = tblN.dataAreaid,
            //        companyName = tblN.companyName,
            //        receiverIdentification = tblN.EmailFrom + "_" + tblN.receiverName,
            //        receiverContactNumber = tblN.contactNumber,
            //        amount = tblN.amount,
            //        receiverEmail = tblN.Email,
            //        SEANCODE = tblVS.SEANCode,
            //        vendorCode = tblN.vendorCode,
            //        voucherId = tblN.voucherId,
            //        receiverName = tblN.receiverName,
            //        releasingDate = tblVS.releasingDate ?? DateTime.Now,
            //        chequeDate = tblN.chequeDate ?? null,
            //        chequeNumber = tblN.chequeNumber,
            //        bank = tblN.bank
            //    }).OrderBy(v => v.vendorCode).ThenBy(e => e.receiverEmail)
            //    .ToList();
            return null;
        }

        public List<CollectedChecks> GetCollectedChecks(DateTime dateFrom, DateTime dateTo)
        {
            List<CollectedChecks> data = db.tblCollectedChecks.Where(x => x.releasedDate >= dateFrom && x.releasedDate <= dateTo)
                .OrderByDescending(x => x.releasedDate).Select(x => new CollectedChecks
                {
                    checkType = x.checkType,
                    logId = x.logId,
                    companyName = x.companyName,
                    vendorCode = x.vendorCode,
                    entity = x.entity,
                    bank = x.bank,
                    voucherId = x.voucherId,
                    checkNum = x.checkNum,
                    checkDate = x.checkDate,
                    amount = x.amount,
                    releasedDate = x.releasedDate,
                    receiptNum = x.receiptNum ?? "",
                    receiptType = x.receiptType,
                    collectorType = x.collectorType,
                    collectorName = x.collectorName,
                    thirdPartyCompany = x.thirdPartyCompany
                }).ToList();

            return data;
        }

        public string GenerateTableTitle(string checkType)
        {
            if (checkType == "STANDARD")
            {
                return "Standard Checks";
            }
            else if (checkType == "MANAGER")
            {
                return "Manager's Checks";
            }
            else
            {
                return checkType;
            }
        }

        public string SendEODMail(List<CollectedChecks> collectedChecks, string user, bool isBySystem = false, string filePath = "")
        {
            if (isBySystem)
            {
                var dateFrom = DateTime.Today;
                var dateTo = DateTime.Today.AddDays(1);

                if (db.tblEODValidationLogs.Where(x => x.logDate > dateFrom && x.logDate < dateTo && x.type == "VOUCHER").Count() > 0)
                {
                    return "ALREADY SENT";
                }
            }

            string chartPath = AppDomain.CurrentDomain.BaseDirectory + "//Charts//chart.jpeg";
            Byte[] bytes;
            String file;

            try
            {
                const string border = "border-bottom: 1px solid #464242;padding:8px;";
                string body = "";
                bool isCheck = false;

                var releasedTodayChecks = collectedChecks;
                //var employeeOnlinePayments = EmployeeRepository.GetEmployeeWithOnlinePaymentsList(DateTime.Today, DateTime.Today);

                //var generalOnlinePayments = OnlinePaymentRepository.GetGeneralOnlinePaymentList(DateTime.Today, DateTime.Today, false);

                //foreach (var op in employeeOnlinePayments)
                //{
                //    var cc = new CollectedChecks
                //    {
                //        releasedDate = op.logDate,
                //        companyName = op.Employee.employeeName,
                //        entity = op.entity,
                //        amount = op.amount,
                //        bank = op.bank,
                //        checkType = "EMPLOYEES"
                //    };

                //    releasedTodayChecks.Add(cc);
                //}

                //foreach (var op in generalOnlinePayments)
                //{
                //    var cc = new CollectedChecks
                //    {
                //        releasedDate = op.logDate,
                //        companyName = op.companyName,
                //        entity = op.entity,
                //        amount = op.amount,
                //        bank = op.bank,
                //        checkType = op.modeOfPayment
                //    };

                //    releasedTodayChecks.Add(cc);
                //}

                decimal? totalAmount = 0;
                string[] checkTypes = new string[3] { "STANDARD", "MANAGER", "EMPLOYEES" };

                //string[] paymentModes = generalOnlinePayments.Select(x => x.modeOfPayment).Distinct().ToArray();

                string[] paymentTypes = collectedChecks.Select(x => x.checkType).Distinct().ToArray();

                //Array.Copy(checkTypes, paymentTypes, checkTypes.Length);
                //Array.Copy(paymentModes, 0, paymentTypes, checkTypes.Length, paymentModes.Length);


                string[] htmlTable = new string[paymentTypes.Length];

                if (releasedTodayChecks.Count() > 0)
                {
                    isCheck = true;

                    for (int i = 0; i < paymentTypes.Length; i++)
                    {
                        totalAmount = releasedTodayChecks.Where(x => x.checkType == paymentTypes[i])
                            .AsEnumerable().Sum(x => x.amount);

                        if (totalAmount == 0)
                        {
                            continue;
                        }
                        
                        htmlTable[i] = "<div style='padding:15px;border:#595959 1px solid;margin-bottom:40px;'>" +
                            "<div style='text-transform:capitalize;font-size:17px;font-weight:700;margin-bottom:25px;'>" +
                            GenerateTableTitle(paymentTypes[i]) + "</div>" +
                            "<table style='width:100%;margin-top:8px;margin-bottom:10px;width:100%;" +
                        "border-collapse:collapse;'><thead><tr style='font-size:15px;font-weight:500;'><td style='padding:0px 0px 6px 7px;border-bottom:1px solid #464242;'>" +
                        "Entity</td><td style='padding:0px 0px 6px 7px;border-bottom:1px solid #464242;'>"+(paymentTypes[i] == "ONLINE" ? "Segments" : "Banks")+"</td><td style='padding:0px 0px 6px 7px;border-bottom:1px solid #464242;'>Amount</td></tr></thead style='font-size:14px;'><tbody>";

                        string[] bankArr = releasedTodayChecks.Where(x => x.checkType == paymentTypes[i])
                            .GroupBy(x => x.bank)
                            .Select(x => x.First()).Select(x => x.bank).ToArray();

                        string[] entityArr = releasedTodayChecks.Where(x => x.checkType == paymentTypes[i])
                            .GroupBy(x => x.entity)
                            .Select(x => x.First()).Select(x => x.entity).ToArray();

                        //var entitiesTotalsChart = new Chart();
                        //var entitiesTotalsChartArea = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
                        //entitiesTotalsChart.ChartAreas.Add(entitiesTotalsChartArea);
                        //var entitiesTotalsChartSeries = new Series("values");
                        //entitiesTotalsChartSeries.IsVisibleInLegend = false;
                        //entitiesTotalsChart.Series.Add(entitiesTotalsChartSeries);

                        for (int a = 0; a < entityArr.Length; a++)
                        {
                            var entityTotalAmount = releasedTodayChecks.Where(x => x.entity == entityArr[a] && x.checkType == paymentTypes[i])
                                .AsEnumerable().Sum(x => x.amount);

                            //entitiesTotalsChartSeries.Points.AddXY(entityArr[a], entityTotalAmount);

                            //entitiesTotalsChart.SaveImage(chartPath, ChartImageFormat.Jpeg);

                            bytes = File.ReadAllBytes(chartPath);
                            file = Convert.ToBase64String(bytes);

                            htmlTable[i] += "<tr style='font-size:14px;'><td style='" + border + "'>" + entityArr[a] +
                                " - ₱" + string.Format("{0:n}", Math.Round((entityTotalAmount ?? 0), 2)) + "</td>";

                            var totalsByBanks = releasedTodayChecks.Where(x => x.entity == entityArr[a] && x.checkType == paymentTypes[i])
                                .GroupBy(x => x.bank).Select(y => new
                                {
                                    bank = y.First().bank,
                                    total = y.Sum(t => t.amount)
                                });

                            var index = 0;
                            foreach (var bankDetails in totalsByBanks)
                            {
                                if (index == 0)
                                {
                                    htmlTable[i] += "<td style='" + border + "'>" + bankDetails.bank + "</td>" + "<td style='" + border + "'>₱" + string.Format("{0:n}", Math.Round((bankDetails.total ?? 0), 2)) + "</td></tr>";
                                    index = 1;
                                }
                                else
                                {
                                    htmlTable[i] += "<tr style='font-size:14px;'><td style='" + border + "'></td><td style='" + border + "'>" + bankDetails.bank + "</td>" + "<td style='" + border + "'>₱" + string.Format("{0:n}", Math.Round((bankDetails.total ?? 0), 2)) + "</td></tr>";
                                }
                            }
                        }
                        htmlTable[i] += "<tr style='font-weight:700;font-size:14px;text-decoration:underline;'><td style='text-align:right;padding-top:10px;' colspan='2'>" +
                            "Total Amount : </td><td style='padding-left:8px;padding-top:10px;'> ₱" +
                            string.Format("{0:n}", Math.Round((totalAmount ?? 0), 2)) + "</td></tr>";

                        htmlTable[i] += "</tbody></table>";


                        //entitiesTotalsChart.SaveImage(chartPath, ChartImageFormat.Jpeg);

                        bytes = File.ReadAllBytes(chartPath);
                        file = Convert.ToBase64String(bytes);

                        htmlTable[i] += $"<div style='display:flex;'>" +
                        $"<img style='margin-left:auto;margin-right:auto;margin-top:25px;' src='data:image/jpeg;base64,{file}' />" +
                        $"</div>";

                        htmlTable[i] += "</div>";
                    }

                    body = "<div style='margin-bottom:25px;font-size:16px;'>Good day treasury team! </div>" +
                        "<div style='margin-bottom:35px;font-size:16px;'>Here is the <label style='color:#af0505;'>End of Day "
                        + (isBySystem == true ? "System Generated " : "") + "Released Payments Summarized Report</label> as of " +
                        DateTime.Now.ToString("MMM dd, yyyy hh:mm tt") + ". " +
                        (isBySystem == true ? "Note that this is a system generated report." : "") + "</div>";

                    for (int i = 0; i < htmlTable.Length; i++)
                    {
                        if (htmlTable[i] != "")
                        {
                            body += htmlTable[i];
                        }
                    }

                    var totalsByTypes = releasedTodayChecks
                    .GroupBy(x => x.checkType).Select(y => new
                    {
                        checkType = y.First().checkType,
                        total = y.Sum(t => t.amount)
                    });

                    StringBuilder table = new StringBuilder();

                    table.Append("<div style='padding:14px; border:#e20b0b 1px solid; margin-bottom:40px;'>");
                    table.Append("<div style='font-weight:700;font-size:17px;margin-bottom:25px;'>Grand Total</div>");
                    table.Append("<table style='width:100%;margin-top:8px;margin-bottom:10px;border-collapse:collapse'>");
                    table.Append("<thead style='font-weight:700'>");
                    table.Append("<tr style='font-size:15px;'>");
                    table.Append("<td style='padding:0px 0px 6px 7px;border-bottom:1px solid #464242;width:70%;'>Types</td>");
                    table.Append("<td style='padding:0px 0px 6px 7px;border-bottom:1px solid #464242;'>Total Amount</td>");
                    table.Append("</tr>");
                    table.Append("</thead>");
                    table.Append("<tbody>");


                    //var grandTotalChart = new Chart();
                    //var grandTotalChartArea = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
                    //grandTotalChart.ChartAreas.Add(grandTotalChartArea);
                    //var grandTotalChartSeries = new Series("values");
                    //grandTotalChartSeries.IsVisibleInLegend = false;
                    //grandTotalChart.Series.Add(grandTotalChartSeries);
                    
                    for (int i = 0; i < paymentTypes.Length; i++)
                    {
                        var typeDetails = totalsByTypes.FirstOrDefault(x => x.checkType == paymentTypes[i]);

                        var totalAmountByType = (typeDetails == null ? new decimal(0) : typeDetails.total);

                        //grandTotalChartSeries.Points.AddXY(paymentTypes[i], totalAmountByType);
                        
                        table.Append("<tr style='font-weight:600'>");
                        table.Append("<td style='padding:8px; border-bottom:1px solid #464242'>");
                        table.Append(GenerateTableTitle(paymentTypes[i]));
                        table.Append("</td>");
                        table.Append("<td style='padding:8px; border-bottom:1px solid #464242'>");
                        table.Append("₱" + string.Format("{0:n}", Math.Round((totalAmountByType ?? 0), 2)));
                        table.Append("</td>");
                        table.Append("</tr>");
                    }

                    totalAmount = totalsByTypes.AsEnumerable().Sum(x => x.total);

                    table.Append("<tr style='font-weight:700;font-size:14px;text-decoration:underline;'>");
                    table.Append("<td style='text-align:right;padding-top:10px;'>");
                    table.Append("Grand Total");
                    table.Append("</td>");
                    table.Append("<td style='padding-left:8px;padding-top:10px;'>");
                    table.Append("₱" + string.Format("{0:n}", Math.Round((totalAmount ?? 0), 2)));
                    table.Append("</td>");
                    table.Append("</tr>");

                    table.Append("</tbody>");

                    table.Append("</table>");

                    //grandTotalChart.SaveImage(chartPath, ChartImageFormat.Jpeg);

                    bytes = File.ReadAllBytes(chartPath);
                    file = Convert.ToBase64String(bytes);

                    table.Append($"<div style='display:flex;'>" +
                        $"<img style='margin-left:auto;margin-right:auto;margin-top:25px;' src='data:image/jpeg;base64,{file}' />" +
                        $"</div>");

                    table.Append("</div>");

                    body += table.ToString();
                }
                else
                {
                    body = "<div style='margin-bottom:25px;font-size:16px;'>Good day treasury team!</div>" +
                        "<div style='margin-bottom:50px;font-size:16px;'> There are no payments released for today's cut off <label style='color:#af0505;'>" +
                        DateTime.Now.ToString("MMM dd, yyyy hh:mm tt") + "</label>.</div>";
                }

                var users = UserRepository.GetUsersList();

                var emailArr = "";

                foreach (var u in users)
                {
                    if (!String.IsNullOrWhiteSpace(u.Email))
                    {
                        emailArr = emailArr + "," + u.Email;
                    }
                }

                var otherRecipients = db.tblOtherRecipientEmails.Select(x => new
                {
                    email = x.recipientEmail
                }).ToList();

                foreach (var or in otherRecipients)
                {
                    emailArr = emailArr + "," + or.email;
                }

                var email = new MailMessage("tdipsnoreply@shakeys.biz", emailArr.Substring(1))
                {
                    Subject = "End of Day Process Report" + (isBySystem == true ? " (System Generated)" : ""),
                    Body = body,
                    IsBodyHtml = true
                };

                if (isCheck && filePath != "")
                {
                    if (System.IO.File.Exists(filePath))
                    {
                        Attachment attachment = new Attachment(filePath);
                        email.Attachments.Add(attachment);
                    }
                }

                using (var smtp = new SmtpClient())
                {
                    var credential = new NetworkCredential
                    {
                        UserName = "tdipsnoreply@shakeys.biz",  // replace with valid value,
                        Password = "td1p$@dm1n"
                    };
                    smtp.Credentials = credential;
                    smtp.Host = "smtp-mail.outlook.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.Send(email);
                }

                SystemLog sl = new SystemLog
                {
                    userId = user,
                    type = "EOD VALIDATION",
                    description = "Sent an EOD Validation Mail.",
                    status = "COMPLETED"
                };
                SystemLogBusLogic.Logs.Add(sl);

                return "SUCCESS";
            }
            catch (Exception ex)
            {
                SystemLog sl = new SystemLog
                {
                    userId = user,
                    type = "EOD VALIDATION",
                    description = ex.Message,
                    status = "ERROR"
                };
                SystemLogBusLogic.Logs.Add(sl);

                return ex.Message;
            }
        }

        public string SendForReleasingMail(string user, DateTime? dtReleasingDate = null, string filePath = "")
        {
            try
            {
                const string border = "border-bottom: 1px solid #464242;padding:8px;";
                string body = "";
                bool isCheck = false;

                var forReleasingChecks = GetForReleasingVoucherList("", dtReleasingDate);
                decimal? totalAmount = 0;
                string[] checkTypes = new string[2] { "STANDARD", "MANAGER" };
                string htmlTable = "";

                if (forReleasingChecks.Count() > 0)
                {
                    isCheck = true;

                    totalAmount = forReleasingChecks.AsEnumerable().Sum(x => x.amount);

                    htmlTable = "<div style='padding:15px;border:#595959 1px solid;margin-bottom:40px;'>" +
                        "<div style='text-transform:capitalize;font-size:17px;font-weight:700;margin-bottom:25px;'>" +
                        "For Releasing Checks</div>" +
                        "<table style='width:100%;margin-top:8px;margin-bottom:10px;width:100%;" +
                    "border-collapse:collapse;'><thead><tr style='font-size:15px;font-weight:500;'><td style='padding:0px 0px 6px 7px;border-bottom:1px solid #464242;'>" +
                    "Entity</td><td style='padding:0px 0px 6px 7px;border-bottom:1px solid #464242;'>Banks</td><td style='padding:0px 0px 6px 7px;border-bottom:1px solid #464242;'>Amount</td></tr></thead style='font-size:14px;'><tbody>";

                    string[] bankArr = forReleasingChecks
                        .GroupBy(x => x.bank)
                        .Select(x => x.First()).Select(x => x.bank).ToArray();

                    string[] entityArr = forReleasingChecks
                        .GroupBy(x => x.entity)
                        .Select(x => x.First()).Select(x => x.entity).ToArray();

                    for (int a = 0; a < entityArr.Length; a++)
                    {
                        var entityTotalAmount = forReleasingChecks.Where(x => x.entity == entityArr[a])
                            .AsEnumerable().Sum(x => x.amount);

                        htmlTable += "<tr style='font-size:14px;'><td style='" + border + "'>" + entityArr[a] +
                            " - ₱" + string.Format("{0:n}", Math.Round((entityTotalAmount ?? 0), 2)) + "</td>";

                        var totalsByBanks = forReleasingChecks.Where(x => x.entity == entityArr[a])
                            .GroupBy(x => x.bank).Select(y => new
                            {
                                bank = y.First().bank,
                                total = y.Sum(t => t.amount)
                            });

                        var index = 0;
                        foreach (var bankDetails in totalsByBanks)
                        {
                            if (index == 0)
                            {
                                htmlTable += "<td style='" + border + "'>" + bankDetails.bank + "</td>" + "<td style='" + border + "'>₱" + string.Format("{0:n}", Math.Round((bankDetails.total ?? 0), 2)) + "</td></tr>";
                                index = 1;
                            }
                            else
                            {
                                htmlTable += "<tr style='font-size:14px;'><td style='" + border + "'></td><td style='" + border + "'>" + bankDetails.bank + "</td>" + "<td style='" + border + "'>₱" + string.Format("{0:n}", Math.Round((bankDetails.total ?? 0), 2)) + "</td></tr>";
                            }
                        }
                    }
                    htmlTable += "<tr style='font-weight:700;font-size:14px;text-decoration:underline;'><td style='text-align:right;padding-top:10px;' colspan='2'>" +
                        "Total Amount : </td><td style='padding-left:8px;padding-top:10px;'> ₱" +
                        string.Format("{0:n}", Math.Round((totalAmount ?? 0), 2)) + "</td></tr>";

                    htmlTable += "</tbody></table></div>";


                    body = "<div style='margin-bottom:25px;font-size:16px;'>Good day treasury team! </div>" +
                        "<div style='margin-bottom:35px;font-size:16px;'>Here is the <label style='color:#af0505;'>For Releasing Checks "
                         + "Report for the Date of " +
                        (dtReleasingDate ?? DateTime.Now).ToString("MMM dd, yyyy") + " </label>. " +
                        "</div>";

                    body += htmlTable;
                }
                else
                {
                    return "No Check is For Releasing";
                }

                var otherRecipients = db.tblOtherRecipientEmails.Select(x => new
                {
                    email = x.recipientEmail
                }).ToList();

                var emailArr = "";

                foreach (var or in otherRecipients)
                {
                    emailArr = emailArr + "," + or.email;
                }

                var email = new MailMessage("tdipsnoreply@shakeys.biz", emailArr.Substring(1))
                {
                    Subject = "For Releasing Checks Report",
                    Body = body,
                    IsBodyHtml = true
                };

                if (isCheck && filePath != "")
                {
                    if (System.IO.File.Exists(filePath))
                    {
                        Attachment attachment = new Attachment(filePath);
                        email.Attachments.Add(attachment);
                    }
                }

                using (var smtp = new SmtpClient())
                {
                    var credential = new NetworkCredential
                    {
                        UserName = "tdipsnoreply@shakeys.biz",  // replace with valid value,
                        Password = "td1p$@dm1n"
                    };
                    smtp.Credentials = credential;
                    smtp.Host = "smtp-mail.outlook.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.Send(email);
                }

                SystemLog sl = new SystemLog
                {
                    userId = user,
                    type = "EOD VALIDATION",
                    description = "Sent an EOD Validation Mail.",
                    status = "COMPLETED"
                };
                SystemLogBusLogic.Logs.Add(sl);

                return "SUCCESS";
            }
            catch (Exception ex)
            {
                SystemLog sl = new SystemLog
                {
                    userId = user,
                    type = "EOD VALIDATION",
                    description = ex.Message,
                    status = "ERROR"
                };
                SystemLogBusLogic.Logs.Add(sl);

                return ex.Message;
            }
        }

        public string SendCanceledVoucherNoticeMail(List<Voucher> lstVouchers)
        {
            try
            {
                const string theadtd = "padding:0px 0px 6px 7px;border-bottom:1px solid #464242;";
                const string border = "border-bottom: 1px solid #464242;padding:8px;";
                string body = "";
                StringBuilder table = new StringBuilder("<table style='border-collapse:collapse;'><thead><tr>");
                //table.Append("<th>Entity</th>");
                //table.Append("<th>Bank</th>");
                table.Append("<th style='" + theadtd + "'>Company</th>");
                table.Append("<th style='" + theadtd + "'>Voucher</th>");
                table.Append("<th style='" + theadtd + "'>Amount</th>");
                table.Append("<th style='" + theadtd + "'>Supposed Releasing Date</th>");
                table.Append("</tr></thead><tbody style='text-align:center'>");

                body = "<div style='margin-bottom:25px;font-size:16px;'>Good day treasury team! </div>" +
                            "<div style='margin-bottom:35px;font-size:16px;'>Kindly see the list below" +
                            " for the vouchers that were supposed to be 'For Releasing' but now are 'Cancelled'" +
                            " in D365. Please be advised that TDIPS will not be releasing the following vouchers" +
                            " on the supposed date of releasing.</div>";

                foreach (var voucher in lstVouchers)
                {
                    table.Append("<tr>");
                    //table.Append("<td style='" + border + "'>" + voucher.entity + "</td>");
                    //table.Append("<td style='" + border + "'>" + voucher.bank + "</td>");
                    table.Append("<td style='" + border + "'>" + voucher.companyName + "</td>");
                    table.Append("<td style='" + border + "'>" + voucher.voucherId + "</td>");
                    table.Append("<td style='" + border + "'>" + string.Format("{0:n}", Math.Round((voucher.amount ?? 0), 2)) + "</td>");
                    table.Append("<td style='" + border + "'>" + (voucher.releasingDate ?? DateTime.Now).ToString("MM/dd/yyyy") + "</td>");
                    table.Append("</tr>");
                }

                table.Append("</tbody></table>");

                body += table.ToString();

                var users = UserRepository.GetUsersList();

                var emailArr = "";

                foreach (var u in users)
                {
                    if (!String.IsNullOrWhiteSpace(u.Email))
                    {
                        emailArr = emailArr + "," + u.Email;
                    }
                }

                var email = new MailMessage("tdipsnoreply@shakeys.biz", emailArr.Substring(1))
                {
                    Subject = "PAYMENTS CANCELLATION NOTICE",
                    Body = body,
                    IsBodyHtml = true
                };

                using (var smtp = new SmtpClient())
                {
                    var credential = new NetworkCredential
                    {
                        UserName = "tdipsnoreply@shakeys.biz",
                        Password = "td1p$@dm1n"
                    };
                    smtp.Credentials = credential;
                    smtp.Host = "smtp-mail.outlook.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.Send(email);
                }

                SystemLog sl = new SystemLog
                {
                    userId = "SYSTEM",
                    type = "PAYMENTS CANCELLATION NOTICE MAIL",
                    description = "Sent a PAYMENTS CANCELLATION Notice Mail.",
                    status = "COMPLETED"
                };
                SystemLogBusLogic.Logs.Add(sl);

                return "SUCCESS";
            }
            catch (Exception ex)
            {
                SystemLog sl = new SystemLog
                {
                    userId = "SYSTEM",
                    type = "CANCEL NOTICE MAIL",
                    description = ex.Message,
                    status = "FAILED"
                };
                SystemLogBusLogic.Logs.Add(sl);

                return "ERROR";
            }
        }

        public void SpecialAnnouncement(string[] attachments)
        {
            var vendors = db.tblVendors.Where(x => x.companyEmail != "" && x.companyEmail != null).ToList();

            tblBM = new tblBatchMail
            {
                logDate = DateTime.Now,
                status = "PROCESSING"
            };
            db.tblBatchMails.Add(tblBM);
            db.SaveChanges();

            var batchId = tblBM.batchMailId;

            foreach (var vendor in vendors)
            {
                tblML = new tblMailLog
                {
                    type = "EMAIL",
                    receiverName = vendor.companyName,
                    status = "SENDING",
                    contact = vendor.companyEmail,
                    description = "Special Announcement",
                    batchId = batchId
                };

                db.tblMailLogs.Add(tblML);
            }

            db.SaveChanges();


            #region FirstMail
            var body = new StringBuilder();

            body.Append("<div>");

            body.Append("<div style='margin-bottom:25px;font-weight:500'>Dear Business Partners, </div>");

            body.Append("<div style='color:#f30000;" +
                "margin-bottom:25px;font-weight:500'>ADVISORY!!!</div>");

            body.Append($"<div>In accordance with the government-imposed enhanced quarantine measures " +
                "in the entire Luzon and National Capital Region, Shakey's Treasury Department " +
                "will prepare payment settlements through:</div>");
            body.Append("<ol>");
            body.Append("<li>On-line payment. </li>");
            body.Append("<li>Over-the-counter payment. </li>");
            body.Append("<li>Direct check deposit. </li>");
            body.Append("</ol>");
            body.Append("<div style='margin-bottom:20px;margin-top:25px;'>With this regard, we are " +
                "<a style='font-weight:600' href='" +
                "https://forms.office.com/FormsPro/Pages/ResponsePage.aspx?id=x73fk5EXtkS6r1L2rYb2E38nfryOdOBJpVsMt89dnIlUOEFPRzNUUUhKOUpLQTNSTDZOUDhNU0JLWC4u'>" +
                "requesting you to fill-out the form in this link</a>.</div>");
            body.Append("<div>Please do note of the following changes in payment releasing: </div>");
            body.Append("<ol>");
            body.Append("<li>We will notify of your Payment via email.</li>");
            body.Append("<li>You must email to <span style='color:#f30000'>jggonzales@shakeys.biz</span> the scanned copy of the " +
                "Official Receipt/Collections Receipt.</li>");
            body.Append("<li>The 2307 or EWT issuance will be put on hold in order that all will " +
                "submit the Original Official/Collection Receipt until further notice, or the " +
                "community quarantine has been lifted. </li>");
            body.Append("</ol>");
            body.Append("<div style='margin-bottom:15px;'>This way we could avoid person to person contact during actual " +
                "check release thus eliminating possibility of viral transmission. If you have " +
                "any questions, please don’t hesitate to email at <span style='color:#f30000'>jggonzales@shakeys.biz</span>. </div>");
            body.Append("<div style='margin-bottom:30px;'>To ensure the safety of all concerned parties and stakeholders, your cooperation is highly appreciated. </div>");
            body.Append("<div style='margin-bottom:40px;'>Thank you. </div>");
            body.Append("<div style='margin-bottom:10px;font-weight:500'>Stay Safe,</div>");
            body.Append("<div style='margin-bottom:50px;font-weight:500'><i>Shakey's Treasury Department</i></div>");

            body.Append("</div>");

            #endregion

            #region SecondMail

            var body2 = new StringBuilder();

            body2.Append("<div>");
            body2.Append("<div style='color:#f30000;" +
                "margin-bottom:25px;font-weight:500'>ADVISORY!!!</div>");
            body2.Append("<div style='margin-bottom:20px;font-weight:500'>Good day!</div>");
            body2.Append("<div style='margin-bottom:25px;'>In connection with the temporary Work from Home Scheme due to Covid-19, " +
                "please send your Sales Invoices (SI) including the supporting documents " +
                "(Delivery Receipt & Purchase Order) to the following email addresses: </div>");

            body2.Append("<div style='font-weight:500;margin-bottom:10px;'>Scanned copies: </div>");
            body2.Append("<div style='margin-bottom:10px;margin-left:15px;'>");
            body2.Append("<div><span style='font-weight:500'>Warehouse:</span> " +
                "(Shakey’s – Anne) <span style='color:#f30000'>amsanpedro@shakeys.biz</span> " +
                "</div>");
            body2.Append("<div><span style='font-weight:500'>Warehouse:</span> " +
                "(Peri-Peri – Cams) <span style='color:#f30000'>crlagunay@shakeys.biz</span> " +
                "</div>");
            body2.Append("<div><span style='font-weight:500'>NAF:</span> " +
                "(Shiela) <span style='color:#f30000'>safigueroa@shakeys.biz</span> " +
                "</div>");
            body2.Append("</div>");
            body2.Append("<div><span style='font-weight:500'>Excel Detailed Report:</span>  " +
                "<i>(see attached template for reference)</i> </div>");
            body2.Append("<div><span style='font-weight:500'>Direct Delivery:</span> " +
                "(Shakey’s – Prin) <span style='color:#f30000'>tmtauyan@shakeys.biz</span> " +
                "</div>");
            body2.Append("<div style='margin-bottom:15px;'><span style='font-weight:500'>" +
                "Direct Delivery:</span> (Peri-Peri – Cams) <span style='color:#f30000'>" +
                "crlagunay@shakeys.biz</span>" +
                "</div>");
            body2.Append("<div style='font-weight:500;margin-bottom:25px;'>" +
                "Schedule: <i>Every Friday</i> </div>");
            body2.Append("<div style='margin-bottom:10px;'>For the actual documents (SI, DR & PO) countering, please wait for further notice.</div>");
            body2.Append("<div>We would like to reiterate our SOP on receiving of goods and countering of SI.</div>");
            body2.Append("<ul style='margin-bottom:30px;'>");
            body2.Append("<li>All DR’s should be properly signed and with PRINTED NAME.</li>");
            body2.Append("<li>Before sending and to AVOID DELAYS in processing, the items, " +
                "quantity, Unit of Measure and Unit Price should match with PO and SI. </li>");
            body2.Append("</ul>");
            body2.Append("<div style='margin-bottom:40px;'>Thank you. </div>");
            body2.Append("<div style='margin-bottom:10px;font-weight:500'>Stay Safe,</div>");
            body2.Append("<div style='margin-bottom:50px;font-weight:500'><i>Shakey's AP Department</i></div>");

            body2.Append("</div>");

            #endregion

            foreach (var vendor in vendors)
            {
                Message msg = new Message
                {
                    Body = body.ToString(),
                    Subject = "Shakey's Treasury Payment COVID-Advisory",
                    ReceiverEmail = vendor.companyEmail
                };

                var result = SendToEmail(msg);

                tblML = db.tblMailLogs.FirstOrDefault(x => x.contact == vendor.companyEmail && x.batchId == batchId);
                tblML.status = result;
                tblML.dateTimeSent = DateTime.Now;

                db.SaveChanges();
            }

            foreach (var vendor in vendors)
            {
                Message msg = new Message
                {
                    Body = body2.ToString(),
                    Subject = "Shakeys AP Advisory for Suppliers Sales Invoice Countering",
                    ReceiverEmail = vendor.companyEmail,
                    Attachment = attachments
                };

                var result = SendToEmail(msg);
            }

            tblBM = db.tblBatchMails.FirstOrDefault(y => y.batchMailId == batchId);
            tblBM.status = "COMPLETED";
            db.SaveChanges();
        }

        public void SendOnlinePaymentMail(GeneralOnlinePayment[] model)
        {
            tblBM = new tblBatchMail
            {
                logDate = DateTime.Now,
                status = "PROCESSING"
            };
            db.tblBatchMails.Add(tblBM);
            db.SaveChanges();

            var batchId = tblBM.batchMailId;

            foreach (var payment in model)
            {
                if (payment.vendor == null) continue;


                if (!String.IsNullOrWhiteSpace(payment.vendor.companyEmail))
                {
                    tblML = new tblMailLog
                    {
                        type = "EMAIL",
                        receiverName = payment.vendor.companyName,
                        status = "SENDING",
                        contact = payment.vendor.companyEmail,
                        description = "General Online Payment Notice",
                        batchId = batchId
                    };

                    db.tblMailLogs.Add(tblML);
                }

                if (!String.IsNullOrWhiteSpace(payment.vendor.contactNumber))
                {
                    tblML = new tblMailLog
                    {
                        type = "SMS",
                        receiverName = payment.vendor.companyName,
                        status = "SENDING",
                        contact = payment.vendor.contactNumber,
                        description = "General Online Payment Notice",
                        batchId = batchId
                    };

                    db.tblMailLogs.Add(tblML);
                }
            }

            db.SaveChanges();


            foreach (var payment in model)
            {
                if (payment.vendor == null || payment.BankDetails == null)
                {
                    continue;
                }

                StringBuilder body = new StringBuilder();

                var sms = new StringBuilder();

                body.Append($"<div style='font-weight:500;font-size:16px;margin-bottom:30px;margin-top:15px;'>" +
                    $"To: {payment.vendor.companyName}" +
                    $"</div>");

                sms.Append($"To: {payment.vendor.companyName} \n\n");

                body.Append($"<div style='margin-bottom:30px;font-size:15px;'>" +
                    $"We are pleased to inform you that your payment had been deposited " +
                    $"to your account. Please see the details below:<" +
                    $"/div>");

                sms.Append($"We are pleased to inform you that your payment had been deposited " +
                    $"to your account. Please see the details below: \n\n");

                body.Append("<div style='margin-bottom:30px;font-weight:600;color:black;font-size:15px;'>");
                body.Append($"<div style='margin-bottom:10px;'>Amount: " +
                    $"<span style='color:blue;'>Php {String.Format("{0:n}", Math.Round(payment.amount ?? 0, 2))}</span>" +
                    $"</div>");
                body.Append($"<div style='margin-bottom:10px;'>Bank Account: " +
                    $"<span style='color:blue;'>{payment.bankAccountNumber}</span>" +
                    $"</div>");
                body.Append($"<div>Bank: " +
                    $"<span style='color:blue;'>" +
                    $"{payment.BankDetails.bankCompany}" +
                    $"</span></div>");
                body.Append("</div>");


                sms.Append($"Amount: Php {String.Format("{0:n}", Math.Round(payment.amount ?? 0, 2))}\n");
                sms.Append($"Bank Account: {payment.bankAccountNumber}\n");
                sms.Append($"Bank: {payment.BankDetails.bankCompany}\n\n");


                body.Append("<div style='margin-bottom:50px;font-size:15px;'>");
                body.Append($"Kindly email your scanned " +
                    $"<span style='font-weight:500;color:#d60a18;'>Official Receipt (OR)</span>" +
                    $" or " +
                    $"<span style='font-weight:500;color:#d60a18;'>Collection Receipt (CR)</span>" +
                    $" to <span style='color:blue;text-decoration:underline;font-weight:500;'>" +
                    $"jggonzales@shakeys.biz" +
                    $"</span>.");
                body.Append("</div>");


                sms.Append("Kindly email your scanned Official Receipt (OR) or Collection Receipt (CR) to jggonzales@shakeys.biz.\n\n");

                body.Append("<div style='font-weight:600;font-size:16px;margin-bottom:40px;'>");
                body.Append("<div style='margin-bottom:10px;'>Kind Regards, </div>");
                body.Append("<div>Shakey’s Treasury </div>");
                body.Append("</div>");

                sms.Append("Kind Regards, \n");
                sms.Append("Shakey’s Treasury \n\n\n");

                body.Append("<div style='margin-bottom:50px;font-style:italic;font-size:15px;'>");

                body.Append($"CONFIDENTIALITY NOTICE: This is a system generated email – " +
                    $"please do not reply to it. The contents of this email/sms is confidential " +
                    $"and intended for the recipient specified in message only and may be legally " +
                    $"protected from disclosure. If you received this message by mistake, please " +
                    $"reply to this email jggonzales@shakeys.biz and follow with its deletion, so " +
                    $"that we can ensure such a mistake does not occur in the future. It is strictly " +
                    $"forbidden to share any part of this message with any third party, without a " +
                    $"written consent of the sender. ");

                sms.Append("CONFIDENTIALITY NOTICE: This is a system generated email – please do not reply to it. " +
                    "The contents of this email/sms is confidential and intended for the recipient specified in message " +
                    "only and may be legally protected from disclosure. If you received this message by mistake, please " +
                    "reply to this email jggonzales@shakeys.biz and follow with its deletion, so that we can ensure such a " +
                    "mistake does not occur in the future. It is strictly forbidden to share any part of this message with " +
                    "any third party, without a written consent of the sender. ");

                body.Append("</div>");

                var mailRecords = db.tblMailLogs.Where(x => x.receiverName == payment.vendor.companyName
                && x.batchId == batchId).ToList();

                if (!String.IsNullOrWhiteSpace(payment.vendor.companyEmail))
                {
                    Message msg = new Message
                    {
                        Body = body.ToString(),
                        Subject = "ONLINE PAYMENT NOTICE",
                        ReceiverEmail = payment.vendor.companyEmail
                    };
                    string emailResult = SendToEmail(msg);

                    tblML = mailRecords.FirstOrDefault(x => x.batchId == batchId && x.contact == payment.vendor.companyEmail);
                    tblML.dateTimeSent = DateTime.Now;
                    tblML.status = emailResult;
                }

                if (!String.IsNullOrWhiteSpace(payment.vendor.contactNumber))
                {
                    string smsResult = SendSMSAsync(payment.vendor.contactNumber, sms.ToString()).Result;
                    tblML = mailRecords.FirstOrDefault(x => x.batchId == batchId && x.contact == payment.vendor.contactNumber);
                    tblML.dateTimeSent = DateTime.Now;
                    tblML.status = smsResult;
                }

                db.SaveChanges();
            }

            var sendingMails = db.tblMailLogs.Where(x => x.status == "SENDING" && x.batchId == batchId).ToList();
            sendingMails.ForEach(x => x.status = "STOPPED");

            tblBM = db.tblBatchMails.FirstOrDefault(y => y.batchMailId == batchId);
            tblBM.status = "COMPLETED";
            db.SaveChanges();
        }

        public void SendOtherOnlinePaymentsEODMail(string user, bool isBySystem = false)
        {
            if (isBySystem)
            {
                var dateFrom = DateTime.Today;
                var dateTo = DateTime.Today.AddDays(1);

                if (db.tblEODValidationLogs.Where(x => x.logDate > dateFrom && x.logDate < dateTo && x.type == "OTHER ONLINE PAYMENT").Count() > 0)
                {
                    return;
                }
            }

            IEODRepository EODRepository = new EODRepository();
            EODRepository.Add(user, "OTHER ONLINE PAYMENT");

            var onlinePayments = OnlinePaymentRepository.GetOtherOnlinePayments(0, "BANK");
            var transactionTypes = onlinePayments.Select(x => x.transactionType).Distinct().ToArray();
            var nonOffsetBanks = onlinePayments
                .Where(x => x.accountType?.ToUpper() == "BANK")
                .Select(x => x.account).Distinct().ToList();
            var offsetBanks = onlinePayments
                .Where(x => x.offsetAccountType?.ToUpper() == "BANK")
                .Select(x => x.offsetAccount).Distinct().ToList();

            var banks = nonOffsetBanks.Union(offsetBanks).Where(x => x != null).ToArray();

            const string border = "border-bottom: 1px solid #464242;padding:8px;";
            string body = "";
            decimal? totalAmount = 0;

            string htmlTable = "";

            if (onlinePayments.Count() == 0)
            {
                body = "<div style='margin-bottom:25px;font-size:16px;'>Good day treasury team!</div>" +
                        "<div style='margin-bottom:50px;font-size:16px;'> There are no other online payments released for today's cut off <label style='color:#af0505;'>" +
                        DateTime.Now.ToString("MMM dd, yyyy hh:mm tt") + "</label>.</div>";
            }
            else
            {
                htmlTable = "<div style='text-transform:capitalize;font-size:17px;font-weight:700;margin-bottom:20px;'>" +
                        "Other Online Payments" + "</div>" +
                        "<table style='width:100%;margin-top:8px;margin-bottom:10px;width:100%;" +
                    "border-collapse:collapse;'><thead><tr style='font-size:15px;font-weight:500;'><td style='padding:0px 0px 6px 7px;border-bottom:1px solid #464242;'>Bank</td>";


                foreach (var transaction in transactionTypes)
                {
                    htmlTable += "<td style='padding:0px 0px 6px 7px;border-bottom:1px solid #464242;'>" + transaction + "</td>";
                }

                htmlTable += "<td style='padding:0px 0px 6px 7px;border-bottom:1px solid #464242;'>Sum</td></tr><tbody>";

                decimal overAllSum = Convert.ToDecimal(0);

                foreach (var bank in banks)
                {
                    htmlTable += "<tr><td style='" + border + "'>" + bank + "</td>";

                    decimal totalSum = Convert.ToDecimal(0);

                    foreach (var transactionType in transactionTypes)
                    {
                        var nonOffsetSum = onlinePayments.Where(x => x.accountType?.ToUpper() == "BANK" &&
                        x.account == bank && x.transactionType == transactionType).Select(x => new {
                            sum = (x.debit ?? 0) - (x.credit ?? 0)
                        }).AsEnumerable().Sum(x => x.sum);

                        var offsetSum = onlinePayments.Where(x => x.offsetAccountType?.ToUpper() == "BANK" &&
                        x.offsetAccount == bank && x.transactionType == transactionType).Select(x => new {
                            sum = (x.credit ?? 0) - (x.debit ?? 0)
                        }).AsEnumerable().Sum(x => x.sum);

                        var sum = nonOffsetSum + offsetSum;
                        totalSum += sum;

                        htmlTable += "<td style='" + border + "'>" + string.Format("{0:n}", Math.Round((sum), 2)) + "</td>";
                    }

                    overAllSum += totalSum;

                    htmlTable += "<td style='" + border + "'>" + string.Format("{0:n}", Math.Round((totalSum), 2)) + "</td>";
                }

                htmlTable += "<tr><td colspan='" + (transactionTypes.Length + 1) + "' style='" +
                    border + "text-align:right'><label style='margin-right:5px;font-weight:600'>" +
                    "Total: </label></td><td style='" + border + "font-weight:600'>" + string.Format("{0:n}", Math.Round((overAllSum), 2))
                    + "</td></tr>";

                htmlTable += "</tbody></table>";

                body = "<div style='margin-bottom:25px;font-size:16px;'>Good day treasury team! </div>" +
                        "<div style='margin-bottom:35px;font-size:16px;'>Here is the <label style='color:#af0505;'>End of Day "
                        + (isBySystem == true ? "System Generated " : "") + "Released Other Online Payments Summarized Report</label> as of " +
                        DateTime.Now.ToString("MMM dd, yyyy hh:mm tt") + ". " +
                        (isBySystem == true ? "Note that this is a system generated report." : "") + "</div>";

                body += htmlTable;
            }
            
            try
            {
                //List<User> users = db.AspNetUsers.Select(x => new User
                //{
                //    role = x.AspNetRoles.Select(y => y.Name).FirstOrDefault(),
                //    email = x.Email
                //}).Where(x => x.role != "TREASURY STAFF").ToList();

                var users = new List<User>();

                var emailArr = "";

                foreach (var u in users)
                {
                    if (!String.IsNullOrWhiteSpace(u.email))
                    {
                        emailArr = emailArr + "," + u.email;
                    }
                }

                var otherRecipients = db.tblOtherRecipientEmails.Select(x => new
                {
                    email = x.recipientEmail
                }).ToList();

                foreach (var or in otherRecipients)
                {
                    emailArr = emailArr + "," + or.email;
                }

                var email = new MailMessage("tdipsnoreply@shakeys.biz", emailArr.Substring(1))
                {
                    Subject = "End of Day Other Online Payments Report",
                    Body = body,
                    IsBodyHtml = true
                };

                using (var smtp = new SmtpClient())
                {
                    var credential = new NetworkCredential
                    {
                        UserName = "tdipsnoreply@shakeys.biz",  // replace with valid value,
                        Password = "td1p$@dm1n"
                    };
                    smtp.Credentials = credential;
                    smtp.Host = "smtp-mail.outlook.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.Send(email);
                }

                SystemLog sl = new SystemLog
                {
                    userId = user,
                    type = "EOD VALIDATION",
                    description = "Sent an EOD Online Payments Mail.",
                    status = "COMPLETED"
                };
                SystemLogBusLogic.Logs.Add(sl);
            }
            catch (Exception ex)
            {
                SystemLog sl = new SystemLog
                {
                    userId = user,
                    type = "EOD VALIDATION",
                    description = ex.Message,
                    status = "ERROR"
                };
                SystemLogBusLogic.Logs.Add(sl);
            }
        }
    }
}
