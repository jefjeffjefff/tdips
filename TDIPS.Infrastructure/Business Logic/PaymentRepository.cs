﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Core.Models.SS_Entities;
using TDIPS.Core.Models.SS_HELPERS;
using TDIPS.Infrastructure.TDIPSDBContextModels;

namespace TDIPS.Infrastructure.Business_Logic
{
    public class PaymentRepository : IPaymentRepository
    {
        TDIPSDbContext db = new TDIPSDbContext();
        tblExcelUploadedPayment tblEUP;
        tblD365IntegratedPayment tblDIP;

        public string Add(Payment p)
        {
            //Prevents from adding a duplicate record
            tblEUP = db.tblExcelUploadedPayments.FirstOrDefault(x => x.voucherId == p.voucherId);

            if (tblEUP != null)
            {
                tblEUP.vendorCode = p.vendorCode;
                tblEUP.bankName = p.bankName;
                tblEUP.voucherId = p.voucherId;
                tblEUP.amount = p.amount;
                tblEUP.methodOfPayment = p.methodOfPayment;
                tblEUP.chequeNumber = p.chequeNumber;
                tblEUP.chequeDate = p.chequeDate;
                tblEUP.logDate = DateTime.Now;

                
                if (p.dataAreaId != null || p.dataAreaId != "")
                {
                    p.dataAreaId = p.dataAreaId ?? "";
                    tblEUP.dataAreaId = p.dataAreaId.ToUpper();
                }

                db.SaveChanges();
                return "SUCCESS";
            }
            else
            {
                tblEUP = new tblExcelUploadedPayment
                {
                    vendorCode = p.vendorCode,
                    bankName = p.bankName,
                    voucherId = p.voucherId,
                    amount = p.amount,
                    methodOfPayment = p.methodOfPayment,
                    chequeNumber = p.chequeNumber,
                    chequeDate = p.chequeDate,
                    logDate = DateTime.Now
                };
                if (p.dataAreaId != null || p.dataAreaId != "")
                {
                    p.dataAreaId = p.dataAreaId ?? "";
                    tblEUP.dataAreaId = p.dataAreaId.ToUpper();
                }
                db.tblExcelUploadedPayments.Add(tblEUP);
                db.SaveChanges();
                return "SUCCESS";
            }
        }

        public string Add(D365IntegratedPayment p)
        {
            //Prevents from adding a duplicate record
            tblDIP = db.tblD365IntegratedPayment.FirstOrDefault(x => x.voucherId == p.voucherId);

            try
            {
                if (tblDIP != null)
                {
                    tblDIP.voucherId = p.voucherId;
                    tblDIP.accountId = p.accountId;
                    tblDIP.amountCur = Math.Abs(p.bankTransAmountCur ?? 0);
                    tblDIP.bankCurrencyAmount = p.bankTransAmountCur;
                    tblDIP.bankRecipientName = p.dimensionName;
                    tblDIP.recipientAccountNum = p.displayValue;
                    tblDIP.paymentMode = p.paymentMode;
                    tblDIP.chequeDate = p.transDate;
                    tblDIP.logDate = DateTime.Now;
                    tblDIP.transDate = p.transDate;
                    tblDIP.dataAreaId = p.dataAreaId.ToUpper();

                    tblVoucherStatu tblVS = db.tblVoucherStatus.OrderByDescending(x => x.logId)
                        .FirstOrDefault(x => x.voucherId == p.voucherId);
                    if (tblVS != null && (tblVS.status == "VOID" || tblVS.status == "VOIDED"))
                    {
                        tblVS = new tblVoucherStatu
                        {
                            batchId = 0,
                            voucherId = p.voucherId,
                            logDate = DateTime.Now,
                            status = "UNRELEASED",
                            userId = "SYSTEM"
                        };
                        db.tblVoucherStatus.Add(tblVS);
                    }
                }
                else
                {
                    tblDIP = new tblD365IntegratedPayment
                    {
                        voucherId = p.voucherId,
                        accountId = p.accountId,
                        amountCur = Math.Abs(p.bankTransAmountCur ?? 0),
                        bankCurrencyAmount = p.bankTransAmountCur,
                        bankRecipientName = p.dimensionName,
                        recipientAccountNum = p.displayValue,
                        paymentMode = p.paymentMode,
                        transDate = p.transDate,
                        dataAreaId = p.dataAreaId.ToUpper(),
                        chequeDate = p.transDate,
                        logDate = DateTime.Now
                    };

                    db.tblD365IntegratedPayment.Add(tblDIP);
                }

                db.SaveChanges();
                return "SUCCESS";
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public List<Payment> List(string[] status, DateTime? statusDate = null, bool filterCheckDate = false)
        {
            if (statusDate != null)
            {
                statusDate = (statusDate ?? DateTime.Now).AddDays(1);
            }

            if (status == null)
            {
                status = new string[12];
                status[0] = "UNRELEASED";
                status[1] = "FOR VERIFICATION";
                status[2] = "FOR APPROVAL";
                status[3] = "FOR RELEASING";
                status[4] = "RELEASED";
                status[5] = "UNCOLLECTED";
                status[6] = "ON HOLD";
                status[7] = "REVERSED";
                status[8] = "REQUESTED FOR HOLD";
                status[9] = "CANCELLED";
                status[10] = "CANCELED";
                status[11] = "VOID";
            }

            var payments = db.tblExcelUploadedPayments.Select(x => new Payment
            {
                vendorCode = db.tblVendors.FirstOrDefault(v => v.vendorCode.Contains(x.vendorCode) && v.isDeleted == false).vendorCode ?? "-",
                dataAreaId = x.dataAreaId == "" ? "Unknown-Entity" : x.dataAreaId.ToUpper(),
                paymentId = x.paymentId,
                bankName = x.bankName ?? "Unknown-Bank",
                voucherId = x.voucherId,
                amount = x.amount,
                methodOfPayment = x.methodOfPayment,
                chequeNumber = x.chequeNumber,
                chequeDate = x.chequeDate,
                companyName = (db.tblVendors.FirstOrDefault(v => v.vendorCode.Contains(x.vendorCode)).companyName) ?? "Company not found.",
                source = "EXCEL",
                status = db.tblVoucherStatus.Where(z => z.voucherId == x.voucherId).OrderByDescending(z => z.logId).FirstOrDefault().status ?? "UNRELEASED"
            }).ToList().Union(db.tblD365IntegratedPayment.Select((y => new Payment
            {
                vendorCode = y.recipientAccountNum,
                dataAreaId = y.dataAreaId == "" ? "Unknown-Entity" : y.dataAreaId.ToUpper(),
                paymentId = y.paymentId,
                bankName = y.accountId ?? "Unknown-Bank",
                voucherId = y.voucherId,
                amount = y.amountCur,
                methodOfPayment = y.paymentMode,
                chequeNumber = y.chequeNum,
                chequeDate = y.chequeDate,
                companyName = y.bankRecipientName,
                source = "D365",
                status = db.tblVoucherStatus.Where(z => z.voucherId == y.voucherId).OrderByDescending(z => z.logId).FirstOrDefault().status ?? "UNRELEASED"
            }))).Where(x => status.Contains(x.status) &&
            (filterCheckDate == true ? x.chequeDate < statusDate : x.voucherId != "")).ToList();

            return payments;
        }

        public string InsertCheckDetails(Payment p)
        {
            tblDIP = db.tblD365IntegratedPayment.FirstOrDefault(x => x.voucherId == p.voucherId);
            tblVoucherRequest tblVR;
            tblCollectedCheck tblCC;
            
            if (tblDIP == null)
            {
                return p.voucherId;
            }
            else
            {
                tblDIP.chequeNum = p.chequeNumber;
                tblDIP.chequeDate = p.chequeDate;
                tblDIP.logDate = DateTime.Now;

                tblVR = db.tblVoucherRequests.FirstOrDefault(x => x.voucherId == p.voucherId);
                if (tblVR != null)
                {
                    tblVR.checkNum = p.chequeNumber;
                    tblVR.checkDate = p.chequeDate;
                }

                tblCC = db.tblCollectedChecks.FirstOrDefault(x => x.voucherId == p.voucherId);
                if (tblCC != null)
                {
                    tblCC.checkNum = p.chequeNumber;
                    tblCC.checkDate = p.chequeDate;
                }

                db.SaveChanges();
                return "SUCCESS";
            }
        }

        public List<Payment> SSList(
            DataFilter filter,
            string vendorcode,
            string companyName,
            string bank,
            string entity,
            string voucherId,
            decimal? amount,
            string payment,
            string check,
            DateTime? date,
            string status
            )
        {
            var payments = db.tblExcelUploadedPayments.Select(x => new
            {
                vendorCode = db.tblVendors.FirstOrDefault(v => v.vendorCode.Contains(x.vendorCode) && v.isDeleted == false).vendorCode ?? "-",
                dataAreaId = x.dataAreaId == "" ? "Unknown-Entity" : x.dataAreaId.ToUpper(),
                paymentId = x.paymentId,
                bankName = x.bankName ?? "Unknown-Bank",
                voucherId = x.voucherId,
                amount = x.amount,
                methodOfPayment = x.methodOfPayment,
                chequeNumber = x.chequeNumber,
                chequeDate = x.chequeDate,
                companyName = (db.tblVendors.FirstOrDefault(v => v.vendorCode.Contains(x.vendorCode)).companyName) ?? "Company not found.",
                source = "EXCEL",
                status = db.tblVoucherStatus.Where(z => z.voucherId == x.voucherId).OrderByDescending(z => z.logId).FirstOrDefault().status ?? "UNRELEASED"
            }).ToList().Union(db.tblD365IntegratedPayment.Select((y => new
            {
                vendorCode = y.recipientAccountNum,
                dataAreaId = y.dataAreaId == "" ? "Unknown-Entity" : y.dataAreaId.ToUpper(),
                paymentId = y.paymentId,
                bankName = y.accountId ?? "Unknown-Bank",
                voucherId = y.voucherId,
                amount = y.amountCur,
                methodOfPayment = y.paymentMode,
                chequeNumber = y.chequeNum,
                chequeDate = y.chequeDate,
                companyName = y.bankRecipientName,
                source = "D365",
                status = db.tblVoucherStatus.Where(z => z.voucherId == y.voucherId).OrderByDescending(z => z.logId).FirstOrDefault().status ?? "UNRELEASED"
            }))).ToList();

            var date1 = DateTime.Today.AddDays(-168);

            var test = payments.Where(x => x.status.ToUpper() != "RELEASED" && x.companyName.Contains(companyName));

            filter.TotalRecordCount = payments.Count();

            if (!string.IsNullOrEmpty(vendorcode))
            {
                payments = payments.Where(p => p.vendorCode.Contains(vendorcode)).ToList();
            }

            if (!string.IsNullOrEmpty(voucherId))
            {
                payments = payments.Where(p => p.voucherId.Contains(voucherId)).ToList();
            }

            if (!string.IsNullOrEmpty(companyName))
            {
                payments = payments.Where(p => p.companyName.Contains(companyName)).ToList();
            }

            if (!string.IsNullOrEmpty(bank))
            {
                payments = payments.Where(p => p.bankName.Contains(bank)).ToList();
            }

            if (!string.IsNullOrEmpty(entity))
            {
                payments = payments.Where(p => p.dataAreaId.Contains(entity)).ToList();
            }

            if (!string.IsNullOrEmpty(vendorcode))
            {
                payments = payments.Where(p => p.vendorCode.Contains(vendorcode)).ToList();
            }

            if (amount > 0)
            {
                payments = payments.Where(p => p.amount == amount).ToList();
            }

            if (!string.IsNullOrEmpty(payment))
            {
                payments = payments.Where(p => p.methodOfPayment.Contains(payment)).ToList();
            }

            if (!string.IsNullOrEmpty(check))
            {
                payments = payments.Where(p => p.chequeNumber.Contains(check)).ToList();
            }

            if (!string.IsNullOrEmpty(status))
            {
                payments = payments.Where(p => p.status.ToUpper() == status.ToUpper()).ToList();
            }

            if (date != null)
            {
                payments = payments.Where(p => p.chequeDate == date).ToList();
            }

            filter.FilteredRecordCount = payments.Count();

            //sort
            payments = QueryHelper.Ordering(payments.AsQueryable(), filter.SortColumn, filter.SortDirection != "asc", false).ToList();

            //// data length
            payments = filter.Length > 0 ? payments.Skip(filter.Start).Take(filter.Length).ToList() : payments.ToList();

            return payments.Select(p => new Payment()
            {
                vendorCode = p.vendorCode,
                amount = p.amount,
                bankName = p.bankName,
                voucherId = p.voucherId,
                chequeNumber = p.chequeNumber,
                chequeDate = p.chequeDate,
                companyName = p.companyName,
                source = p.source,
                status = p.status,
                dataAreaId = p.dataAreaId,
                methodOfPayment = p.methodOfPayment
            }).ToList();
        }
    }
}
