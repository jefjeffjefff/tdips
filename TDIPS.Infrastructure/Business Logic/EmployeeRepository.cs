﻿using System;
using System.Collections.Generic;
using System.Linq;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.TDIPSDBContextModels;

namespace TDIPS.Infrastructure.Business_Logic
{
    public class EmployeeRepository : IEmployeeRepository
    {
        TDIPSDbContext db = new TDIPSDbContext();
        tblEmployee tblE;
        tblEmployeeOnlinePayment tblOP;

        public string Add(Employee e, string userID)
        {
            string returnMsg = "";
            SystemLog sl = new SystemLog();
            sl.userId = userID;
            
            try
            {
                if (e.id > 0)
                {
                    if (e.isEmployeeNumChanged == true)
                    {
                        var checkDuplicate = db.tblEmployees.FirstOrDefault(x => x.employee_no == e.employee_no);
                        if (checkDuplicate != null) return "EMPLOYEE ALREADY EXISTS";
                    }

                    sl.type = "UPDATE EMPLOYEE";
                    sl.description = "Updated a new employee";
                    
                    tblE = db.tblEmployees.FirstOrDefault(x => x.id == e.id);

                    if (tblE == null) return "EMPLOYEE DOES NOT EXIST";
                }
                else
                {
                    sl.type = "ADD EMPLOYEE";
                    sl.description = "Added a new employee";
                    tblE = new tblEmployee();

                    var checkDuplicate = db.tblEmployees.FirstOrDefault(x => x.employee_no == e.employee_no);
                    if (checkDuplicate != null) return "EMPLOYEE ALREADY EXISTS";
                }

                tblE.accountNo = e.accountNo;
                tblE.employee_no = e.employee_no;
                tblE.employeeName = e.employeeName;
                tblE.email = e.email;

                if (!(e.id > 0))
                {
                    db.tblEmployees.Add(tblE);
                }
                
                db.SaveChanges();
                sl.status = "COMPLETED";
                returnMsg =  "SUCCESS";
            }
            catch (Exception ex)
            {
                sl.status = "FAILED";
                returnMsg = ex.Message;
            }

            SystemLogBusLogic.Logs.Add(sl);
            return returnMsg;
        }

        public AddListResult Add(List<EmployeeExcelUpload> employee)
        {
            var addListResult = new AddListResult();

            try
            {
                foreach (var e in employee)
                {
                    tblE = db.tblEmployees.FirstOrDefault(x => x.employee_no == e.EmployeeID);

                    if (tblE == null)
                    {
                        tblE = new tblEmployee
                        {
                            employee_no = e.EmployeeID,
                            employeeName = e.Firstname + " " + e.Surname,
                            email = e.Email,
                            accountNo = e.AccountNo
                        };

                        db.tblEmployees.Add(tblE);
                        db.SaveChanges();

                        addListResult.Added++;
                    }
                    else
                    {
                        tblE.employee_no = e.EmployeeID;
                        tblE.employeeName = e.Firstname + " " + e.Surname;
                        tblE.email = e.Email;
                        tblE.accountNo = e.AccountNo;

                        db.SaveChanges();

                        addListResult.Updated++;
                    }
                }

                addListResult.Status = "SUCCESS";
                return addListResult;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string DeleteEmployee(int employeeId)
        {
            var employee = db.tblEmployees.FirstOrDefault(x => x.id == employeeId);

            db.tblEmployees.Remove(employee);

            db.SaveChanges();

            return "SUCCESS";
        }

        public List<EmployeeOnlinePayment> GetEmployeeWithOnlinePaymentsList(DateTime? from = null, DateTime? to = null)
        {
            //if (from == null) {
            //    from = DateTime.Today;
            //}

            if (to != null)
            {
                to = (to ?? DateTime.Today).AddDays(1);
            }

            var list = db.tblEmployeeOnlinePayments.LeftJoin(
                db.tblEmployees,
                tblOP => tblOP.employeeId,
                tblE => tblE.employee_no,
                (tblOP, tblE) => new EmployeeOnlinePayment
                {
                    Employee = new Employee {
                        employee_no = tblE.employee_no ?? "-",
                        accountNo = tblE.accountNo ?? "-",
                        employeeName = tblE.employeeName ?? "No Employee is Set",
                        email = tblE.email ?? "-"
                    },
                    employeeAccNum = tblE.accountNo ?? "-",
                    amount = tblOP.amount,
                    transDate = tblOP.transDate.ToString(),
                    paymentDate = tblOP.transDate,
                    logDate = tblOP.logDate,
                    strLogDate = tblOP.logDate.ToString(),
                    paymentId = tblOP.paymentId,
                    entity = tblOP.entity ?? "",
                    bank = tblOP.bank ?? ""
                }).Where(x => from == null ? x.paymentId > 0 : (x.logDate >= from && x.logDate < to)).ToList();

            return list;
        }

        public List<Employee> List()
        {
            return db.tblEmployees.Select(x => new Employee
            {
                id = x.id,
                employeeName = x.employeeName,
                employee_no = x.employee_no,
                accountNo = x.accountNo ?? "",
                email = x.email
            }).ToList();
        }

        public string OnlinePayment(EmployeeOnlinePayment op, bool isWithEmployee = true, bool willSave = true)
        {
            try
            {
                if (isWithEmployee) {
                    tblE = db.tblEmployees.FirstOrDefault(x => x.employee_no == op.employeeId);

                    if (tblE == null)
                    {
                        return "NOT FOUND";
                    }

                    if (string.IsNullOrWhiteSpace(tblE.email))
                    {
                        return "NO EMAIL";
                    }
                }
                
                tblOP = new tblEmployeeOnlinePayment
                {
                    employeeId = op.employeeId ?? "",
                    employeeAccNum = op.employeeAccNum ?? "",
                    amount = op.amount,
                    transDate = op.paymentDate,
                    logDate = DateTime.Now,
                    entity = op.entity,
                    bank = op.bank
                };

                if (willSave) {
                    db.tblEmployeeOnlinePayments.Add(tblOP);
                    db.SaveChanges();
                }
                return "SUCCESS";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
