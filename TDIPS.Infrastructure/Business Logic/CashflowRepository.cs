﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.TDIPSDBContextModels;

namespace TDIPS.Infrastructure.Business_Logic
{
    public class CashflowRepository : ICashflowRepository
    {
        TDIPSDbContext db = new TDIPSDbContext();

        public string AddDailyCashFlow(DailyCashFlow dailyCashFlow,
            List<DailyCashFlowTransactionEntry> dailyCashFlowTransactionEntry)
        {
            var existingDailyCashFlow = db.tblDailyCashFlows.FirstOrDefault(x => x.DailyCashDate == dailyCashFlow.DailyCashDate);

            if (existingDailyCashFlow != null) {
                db.tblDailyCashFlows.Remove(existingDailyCashFlow);

                var banks = db.tblDailyCashFlowBanks.Where(x => x.DailyCashFlowId == existingDailyCashFlow.Id).ToList();
                
                foreach (var bank in banks)
                {
                    var totals = db.tblDailyCashFlowBankTotals.Where(x => x.DailyCashFlowBankId == bank.Id);

                    db.tblDailyCashFlowBankTotals.RemoveRange(totals);

                    var transactions = db.tblDailyCashFlowBankTransactions.Where(x => x.DailyCashFlowBankId == bank.Id);

                    db.tblDailyCashFlowBankTransactions.RemoveRange(transactions);
                }

                db.tblDailyCashFlowBanks.RemoveRange(banks);
            }
            
            tblDailyCashFlow DailyCashFlow = new tblDailyCashFlow {
                DateOfUpload = DateTime.Now,
                UploadedBy = dailyCashFlow.UploadedBy,
                DailyCashDate = dailyCashFlow.DailyCashDate,
                DailyCashDateRange =dailyCashFlow.DailyCashDateRange
            };

            db.tblDailyCashFlows.Add(DailyCashFlow);

            db.SaveChanges();
            
            foreach (var entry in dailyCashFlowTransactionEntry)
            {
                var bank = new tblDailyCashFlowBank
                {
                    Bank = entry.Bank.Bank,
                    DailyCashFlowId = DailyCashFlow.Id,
                    BankClassification = entry.Bank.BankClassification
                };

                db.tblDailyCashFlowBanks.Add(bank);

                db.SaveChanges();

                foreach (var transaction in entry.Transactions)
                {
                    var bankTransaction = new tblDailyCashFlowBankTransaction
                    {
                        DailyCashFlowBankId = bank.Id,
                        TransactionName = transaction.TransactionName,
                        TransactionType = transaction.TransactionType,
                        Debit = transaction.Debit,
                        Credit = transaction.Credit
                    };

                    db.tblDailyCashFlowBankTransactions.Add(bankTransaction);
                }

                foreach (var total in entry.Totals)
                {
                    var bankTotal = new tblDailyCashFlowBankTotal
                    {
                        DailyCashFlowBankId = bank.Id,
                        TotalFrom = total.TotalFrom,
                        Type = total.Type,
                        Amount = total.Amount
                    };

                    db.tblDailyCashFlowBankTotals.Add(bankTotal);
                }
            }

            db.SaveChanges();

            return "SUCCESS";
        }

        public string AddProjectionCashflow(ProjectionCashflow projectionCashflow, List<ProjectionCashflowTransaction> projectionCashflowTransactions)
        {
            var existingProjectionCashflow = db.tblProjectionCashflows
                .FirstOrDefault(x => x.ProjectionDateRange == x.ProjectionDateRange &&
                x.ProjectionMonth == projectionCashflow.ProjectionMonth);

            if (existingProjectionCashflow != null) {
                db.tblProjectionCashflows.Remove(existingProjectionCashflow);
                var oldTransactions = db.tblProjectionCashflowTransactions
                    .Where(x => x.ProjectionCashflowId == existingProjectionCashflow.Id).ToList();

                db.tblProjectionCashflowTransactions.RemoveRange(oldTransactions);
                
                db.SaveChanges();
            }
            
            var tblProjectionCashflow = new tblProjectionCashflow
            {
                DateOfUpload = DateTime.Now,
                UploadedBy = projectionCashflow.UploadedBy,
                ProjectionMonth = projectionCashflow.ProjectionMonth,
                ProjectionDateRange = projectionCashflow.ProjectionDateRange
            };

            db.tblProjectionCashflows.Add(tblProjectionCashflow);

            db.SaveChanges();

            foreach (var transaction in projectionCashflowTransactions)
            {
                db.tblProjectionCashflowTransactions.Add(new tblProjectionCashflowTransaction()
                {
                    ProjectionCashflowId = tblProjectionCashflow.Id,
                    SegmentId = transaction.SegmentId,
                    Amount = transaction.Amount
                });
            }

            db.SaveChanges();

            return "SUCCESS";
        }

        public List<CashflowAccountMapping> GetCashflowAccountMappings()
        {
            var results = db.tblCashflowAccountMappings.Select(x => new CashflowAccountMapping
            {
                MainAccount = x.MainAccount,
                Segment = x.Segment,
                Subsegment = x.Subsegment,
                Activity = x.Activity,
                PurposeDimension = x.PurposeDimension
            }).ToList();

            return results;
        }

        public List<DailyCashFlow> GetDailyCashFlowsList()
        {
            var result = db.tblDailyCashFlows.Select(x => new DailyCashFlow
            {
                DateOfUpload = x.DateOfUpload,
                UploadedBy = x.UploadedBy,
                DailyCashDateRange = x.DailyCashDateRange,
                DailyCashDate = x.DailyCashDate
            }).ToList();

            return result;
        }

        public List<ProjectionCashflowSegment> GetProjectionCashflowSegments()
        {
            return db.tblProjectionCashflowSegments.Select(x => new ProjectionCashflowSegment
            {
                Id = x.Id,
                Segment = x.Segment,
                InflowOutflow = x.InflowOutflow,
                Activity = x.Activity
            }).ToList();
        }

        public List<ProjectionCashflow> GetProjectionCashFlowsList()
        {
            var result = db.tblProjectionCashflows.Select(x => new ProjectionCashflow
            {
                DateOfUpload = x.DateOfUpload,
                UploadedBy = x.UploadedBy,
                ProjectionDateRange = x.ProjectionDateRange,
                ProjectionMonth = x.ProjectionMonth,
                Id = x.Id
            }).ToList();

            return result;
        }
    }
}
