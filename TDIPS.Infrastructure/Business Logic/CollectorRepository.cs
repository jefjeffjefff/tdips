﻿using System;
using System.Collections.Generic;
using System.Linq;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.TDIPSDBContextModels;

namespace TDIPS.Infrastructure.Business_Logic
{
	public class CollectorRepository : ICollectorRepository
	{
		TDIPSDbContext db = new TDIPSDbContext();
		tblCollector tblC;

		public string AddOrUpdate(Collector[] c, int vendorID)
		{
			if (c != null)
			{
				var oldinfo = db.tblCollectors.Where(x => x.vendorID == vendorID).ToList();
				db.tblCollectors.RemoveRange(oldinfo);
                db.SaveChanges();

                foreach (var collector in c)
				{
                    if (!((collector.collectorName == null || collector.collectorName == "") &&
                        (collector.contactNumber == null || collector.contactNumber == "") &&
                        (collector.email == null || collector.email == "")))
                    {
                        tblC = new tblCollector();
                        tblC.vendorID = vendorID;
                        tblC.collectorName = collector.collectorName;
                        tblC.position = collector.position;
                        tblC.email = collector.email;
                        tblC.contactNumber = collector.contactNumber;
                        db.tblCollectors.Add(tblC);
                    }
				}

                db.SaveChanges();

                return "SUCCESS";
			}
			else
			{
				return "FAILED";
			}
		}

        public object List(int vendorID)
		{
			List<Collector> colletors = db.tblCollectors.Select(x => new Collector
			{
				vendorID = x.vendorID,
				collectorID = x.collectorID,
				collectorName = x.collectorName,
				position = x.position,
				email = x.email,
				contactNumber = x.contactNumber
			}
			).Where(x => x.vendorID == vendorID).ToList<Collector>();

			return colletors;
		}
	}
}
