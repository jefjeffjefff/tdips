﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.TDIPSDBContextModels;

namespace TDIPS.Infrastructure.Business_Logic
{
    public class MailComposerRepository : IMailComposerRepository
    {
        TDIPSDbContext db = new TDIPSDbContext();

        public string DeleteDraft(int draftId)
        {
            var draft = db.tblComposedMails.FirstOrDefault(x => x.Id == draftId);

            db.tblComposedMails.Remove(draft);

            db.SaveChanges();

            return "SUCCESS";
        }

        public List<Message> GetDrafts()
        {
            var result = db.tblComposedMails.Select(x => new Message
            {
                Subject = x.Subject,
                Id = x.Id,
                DateCreated = x.DateCreated
            }).ToList();

            return result;
        }

        public Message LoadDraft(int draftId)
        {
            var draft = db.tblComposedMails.FirstOrDefault(x => x.Id == draftId);

            if (draft == null) {
                return null;
            }
            else
            {
                var result = new Message
                {
                    DateCreated = draft.DateCreated,
                    Subject = draft.Subject ?? "",
                    Body = draft.Body ?? "",
                    Id = draft.Id
                };

                return result;
            }
        }

        public int SaveToDraft(Message draft)
        {
            if (draft.Id > 0)
            {
                var ComposedMail = db.tblComposedMails.FirstOrDefault(x => x.Id == draft.Id);

                if (ComposedMail != null)
                {
                    ComposedMail.Subject = draft.Subject;
                    ComposedMail.Body = draft.Body;
                    ComposedMail.DateCreated = DateTime.Now;

                    db.SaveChanges();

                    return draft.Id;
                }
                else
                {
                    ComposedMail = new tblComposedMail
                    {
                        Subject = draft.Subject,
                        Body = draft.Body,
                        DateCreated = DateTime.Now
                    };
                    db.tblComposedMails.Add(ComposedMail);

                    db.SaveChanges();

                    return ComposedMail.Id;
                }
            }
            else
            {
                var ComposedMail = new tblComposedMail
                {
                    Subject = draft.Subject,
                    Body = draft.Body,
                    DateCreated = DateTime.Now
                };
                db.tblComposedMails.Add(ComposedMail);

                db.SaveChanges();

                return ComposedMail.Id;
            }
        }

        public void SendMessage(Message message, int[] vendorId, bool _includeCollectors = false)
        {
            if (vendorId == null) {
                return;
            }

            var contacts = db.tblVendors.Where(x => vendorId.Contains(x.vendorID))
                .Select(x => new Mail {
                    receiverEmail = x.companyEmail,
                    receiverContactNumber = x.contactNumber,
                    receiverIdentification = x.companyName,
                    companyName = x.companyName
                }).ToList();
            
            if (_includeCollectors) {
                var collectors = db.tblCollectors.Where(x => vendorId.Contains(x.vendorID ?? 0))
                    .Select(x => new Mail
                    {
                        receiverEmail = x.email,
                        receiverContactNumber = x.contactNumber,
                        receiverIdentification = x.collectorName,
                        companyName = db.tblVendors.FirstOrDefault(y => y.vendorID == x.vendorID).companyName
                    }).ToList();

                contacts = contacts.Concat(collectors).ToList();
            }

            MatchCollection matches = Regex.Matches(message.Body, @"(\@\([^\)]+\))");

            foreach (var contact in contacts)
            {
                if (!String.IsNullOrWhiteSpace(contact.receiverEmail))
                {
                    var newMessage = new Message {
                        ReceiverEmail = contact.receiverEmail,
                        Subject = message.Subject,
                        Body = message.Body,
                        Attachment = message.Attachment
                    };
                    
                    foreach (var match in matches)
                    {
                        string column = GenerateColumn(match);

                        if (column != "")
                        {
                            var newField = contact[column];
                            newMessage.Body = newMessage.Body.Replace(match.ToString(), (string)newField); 
                        }
                    }
                    
                    SendToEmail(newMessage);
                }
            }
        }

        static string GenerateColumn(object regexResult)
        {
            var column = regexResult.ToString().Replace("@", "");
            column = column.Replace("(", "");
            column = column.Replace(")", "");

            if (column.ToUpper() == "RECEIVERNAME")
            {
                return "receiverIdentification";
            }
            else if (column.ToUpper() == "VENDORCODE")
            {
                return "vendorCode";
            }
            else if (column.ToUpper() == "COMPANYNAME")
            {
                return "companyName";
            }
            else
            {
                return "";
            }
        } 

        static string SendToEmail(Message msg)
        {
            try
            {
                if (msg.ReceiverEmail == "" || msg.ReceiverEmail == null)
                {
                    return "EMAIL IS EMPTY";
                }

                msg.ReceiverEmail = Regex.Replace(msg.ReceiverEmail, @"\s+", "");

                var message = new MailMessage();
                message.To.Add(new MailAddress(msg.ReceiverEmail));  // replace with valid value 
                message.From = new MailAddress("tdipsnoreply@shakeys.biz");  // replace with valid value
                message.Subject = msg.Subject;
                message.Body = msg.Body;
                message.IsBodyHtml = true;

                if (msg.Attachment != null)
                {
                    for (int i = 0; i < msg.Attachment.Length; i++)
                    {
                        if (System.IO.File.Exists(msg.Attachment[i]))
                        {
                            Attachment attachment = new Attachment(msg.Attachment[i]);
                            message.Attachments.Add(attachment);
                        }
                    }
                }

                using (var smtp = new SmtpClient())
                {
                    var credential = new NetworkCredential
                    {
                        UserName = "tdipsnoreply@shakeys.biz",  // replace with valid value,
                        Password = "td1p$@dm1n"
                    };
                    smtp.Credentials = credential;
                    smtp.Host = "smtp-mail.outlook.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.Send(message);
                }

                return "SUCCESS";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
