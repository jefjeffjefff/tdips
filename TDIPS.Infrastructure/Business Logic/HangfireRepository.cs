﻿using System;
using System.Collections.Generic;
using System.Linq;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.TDIPSDBContextModels;

namespace TDIPS.Infrastructure.Business_Logic
{
    public class HangfireRepository : IHangfireRepository
    {
        PSQLControl DbControl = new PSQLControl();

        TDIPSDbContext db = new TDIPSDbContext();
        public void Reset()
        {
            DbControl.ExecQuery("DELETE FROM hangfire.hash");
        }
    }
}
