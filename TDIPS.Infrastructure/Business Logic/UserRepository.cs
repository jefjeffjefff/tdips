﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.TDIPSDBContextModels;

namespace TDIPS.Infrastructure.Business_Logic
{
	public class UserRepository : IUserRepository
	{
		TDIPSDbContext db = new TDIPSDbContext();
		PSQLControl PSQLControl = new PSQLControl();

		public string IsUserOnLeave(string userId)
		{
			PSQLControl.AddParam("@userId", userId);

			PSQLControl.ExecQuery("Select \"IsUserOnLeave\" from \"AspNetUsers\" WHERE \"Id\" = @userId");

			var result = PSQLControl.DBDT.Rows[0][0];

			return result?.ToString().ToUpper();
		}

		public object List()
		{
			PSQLControl.ExecQuery("SELECT u.\"Id\", u.\"UserName\" as Username, u.\"Email\"," +
								"r.\"Name\" as UserType, u.\"IsUserOnLeave\" " +
								"from \"AspNetUsers\" u " +
								"LEFT JOIN " +
								"\"AspNetUserRoles\" ur " +
								"ON u.\"Id\" = ur.\"UserId\" " +
								"LEFT JOIN " +
								"\"AspNetRoles\" r " +
								"ON ur.\"RoleId\" = r.\"Id\"");

			var test = PSQLControl.DBDT.Rows;

			var users = (from DataRow row in PSQLControl.DBDT.Rows
				   select new
				   {
                       roles = row[3].ToString(),
					   username = row[1].ToString(),
					   status = row[4].ToString(),
					   email = row[2].ToString(),
				   }).ToList();

			return users;
		}

		public void SetUsersLeaveStatus(string userId, bool isOnLeave)
		{
            SystemLog sl = new SystemLog();
            sl.userId = userId;

            if (isOnLeave)
            {
                sl.type = "LEAVE USER";
                sl.description = "Set account on leave.";
            } else
            {
                sl.type = "ACTIVE USER";
                sl.description = "Set account active.";
            }
            

            try
            {
				PSQLControl.AddParam("@user", userId);
				PSQLControl.ExecQuery("UPDATE \"AspNetUsers\" SET \"IsUserOnLeave\" = '"+ isOnLeave +"'" + 
					"Where \"UserName\" = @user");

                sl.status = "COMPLETED";
            }
            catch (Exception)
            {
                sl.status = "FAILED";
                throw;
            }

            SystemLogBusLogic.Logs.Add(sl);
        }

		public string IsTreasuryManagerOnLeave()
		{
			PSQLControl.ExecQuery("SELECT u.\"Id\", u.\"UserName\" as Username, u.\"Email\"," + 
								"r.\"Name\" as UserType, u.\"IsUserOnLeave\" " +
								"from \"AspNetUsers\" u " +
								"LEFT JOIN " +
								"\"AspNetUserRoles\" ur " +
								"ON u.\"Id\" = ur.\"UserId\" " +
								"LEFT JOIN " +
								"\"AspNetRoles\" r " +
								"ON ur.\"RoleId\" = r.\"Id\" " +
								"WHERE r.\"Name\" = 'TREASURY MANAGER' AND " +
								"u.\"IsUserOnLeave\" = 'false'");

			return PSQLControl.RecordCount == 0 ? "TRUE" : "FALSE";
		}

		public string IsTreasuryLeadOnLeave()
		{
			PSQLControl.ExecQuery("SELECT u.\"Id\", u.\"UserName\" as Username, u.\"Email\"," +
								"r.\"Name\" as UserType, u.\"IsUserOnLeave\" " +
								"from \"AspNetUsers\" u " +
								"LEFT JOIN " +
								"\"AspNetUserRoles\" ur " +
								"ON u.\"Id\" = ur.\"UserId\" " +
								"LEFT JOIN " +
								"\"AspNetRoles\" r " +
								"ON ur.\"RoleId\" = r.\"Id\" " +
								"WHERE r.\"Name\" = 'TREASURY LEAD' AND " +
								"u.\"IsUserOnLeave\" = 'false'");

			return PSQLControl.RecordCount == 0 ? "TRUE" : "FALSE";
		}

        public bool IsEmailExisting(string email)
        {
			PSQLControl.ExecQuery($@"SELECT u.""Email"" FROM ""AspNetUsers"" u WHERE u.""Email"" = '{email}'");

			return PSQLControl.RecordCount > 0;
		}

        public List<ApplicationUser> GetUsersList()
        {
			PSQLControl.ExecQuery("SELECT u.\"Id\", u.\"UserName\" as Username, u.\"Email\"," +
								"r.\"Name\" as UserType, u.\"IsUserOnLeave\" " +
								"from \"AspNetUsers\" u " +
								"LEFT JOIN " +
								"\"AspNetUserRoles\" ur " +
								"ON u.\"Id\" = ur.\"UserId\" " +
								"LEFT JOIN " +
								"\"AspNetRoles\" r " +
								"ON ur.\"RoleId\" = r.\"Id\"");

			var test = PSQLControl.DBDT.Rows;

			var users = (from DataRow row in PSQLControl.DBDT.Rows
						 select new ApplicationUser
						 {
							 Role = row[3].ToString(),
							 Username = row[1].ToString(),
							 Email = row[2].ToString(),
						 }).ToList();

			return users;
		}
    }
}
