﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.TDIPSDBContextModels;


namespace TDIPS.Infrastructure.Business_Logic
{
	public class BankRepository : IBankRepository
	{
		TDIPSDbContext db = new TDIPSDbContext();
		tblBank tblB;

		public string AddOrUpdate(Bank[] b, int vendorID)
		{
			if (b != null)
			{
				var oldinfo = db.tblBanks.Where(x => x.vendorID == vendorID).ToList();
				db.tblBanks.RemoveRange(oldinfo);

				foreach (var bank in b)
				{
                    if (!string.IsNullOrWhiteSpace(bank.bankAccount))
                    {
                        bank.bankAccount = Regex.Replace(bank.bankAccount, @"\s+", "");
                    }
                    
                    tblB = new tblBank
                    {
                        vendorID = vendorID,
                        bankCompany = bank.bankCompany,
                        bankAccount = bank.bankAccount,
                        bankBranch = bank.bankBranch,
                        accountName = bank.accountName
                    };
                    db.tblBanks.Add(tblB);
					db.SaveChanges();
				}

				return "SUCCESS";
			} else
			{
				return "FAILED";
			}
		}

        public string AddOrUpdateByUpload(VendorBanksExcelUpload bank)
        {
            var vendor = db.tblVendors.FirstOrDefault(x => x.vendorCode.Contains(bank.VendorCode));

            if (String.IsNullOrWhiteSpace(bank.BankAccount)) return "EMPTY";

            if (vendor == null)
            {
                return bank.VendorCode;
            }
            else
            {
                var oldBank = db.tblBanks.FirstOrDefault(x => x.bankAccount == bank.BankAccount);

                if (oldBank == null)
                {
                    var newBank = new tblBank
                    {
                        vendorID = vendor.vendorID,
                        bankCompany = bank.BankCompany,
                        bankAccount = bank.BankAccount,
                        bankBranch = bank.BankBranch,
                        accountName = bank.AccountName
                    };

                    db.tblBanks.Add(newBank);
                    db.SaveChanges();

                    return "ADDED";
                }
                else
                {
                    oldBank.vendorID = vendor.vendorID;
                    oldBank.bankCompany = bank.BankCompany;
                    oldBank.bankBranch = bank.BankBranch;
                    oldBank.accountName = bank.AccountName;
                    db.SaveChanges();

                    return "UPDATED";
                }
            }
        }

        public List<Bank> GetBankListByVendorIds(List<int> vendorIdArr)
        {
            var banks = db.tblBanks.Where(x => vendorIdArr.Contains(x.vendorID ?? 0))
                .Select(x => new Bank
                {
                    bankID = x.bankID,
                    vendorID = x.vendorID,
                    bankCompany = x.bankCompany,
                   bankAccount = x.bankAccount,
                   bankBranch = x.bankBranch,
                   accountName = x.accountName
                }).ToList();

            return banks;
        }

        public object List(int vendorID)
		{
			List<Bank> banks = db.tblBanks.Select(x => new Bank
			{
				vendorID = x.vendorID,
				bankCompany = x.bankCompany,
				bankAccount = x.bankAccount,
				bankBranch = x.bankBranch,
                accountName = x.accountName
			}
			).Where(x => x.vendorID == vendorID).ToList<Bank>();

			return banks;
		}
	}
}
