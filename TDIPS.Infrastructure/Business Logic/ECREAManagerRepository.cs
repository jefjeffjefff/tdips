﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDIPS.Core.Interfaces;
using TDIPS.Core.Models;
using TDIPS.Infrastructure.TDIPSDBContextModels;

namespace TDIPS.Infrastructure.Business_Logic
{
    public class ECREAManagerRepository : IECREAManagerRepository
    {
        TDIPSDbContext db = new TDIPSDbContext();

        public List<ECREARating> GetLastECREARatings()
        {
            var ratings = db.tblECREARatings.Select(x => new ECREARating
            {
                id = x.id,
                CollectorName = x.CollectorName,
                Rate = x.Rate,
                DTRate = x.DTRate,
                VendorCode = x.VendorCode
            }).ToList();

            var latestRatingsDate = ratings.OrderByDescending(x =>
            x.DTRate).FirstOrDefault()?.DTRate?.Date;

            var latestRatings = ratings
                .Where(x => x.DTRate >= latestRatingsDate)
                .ToList();
            
            return latestRatings;
        }

        public bool IsKioskRatingActivated()
        {
            return db.tblSystemConfigurations.FirstOrDefault()?.isECREARateActive ?? false;
        }

        public void SetKioskRatingActivation(bool isActivate)
        {
            db.tblSystemConfigurations.FirstOrDefault().isECREARateActive = isActivate;
            db.SaveChanges();
        }
    }
}
