namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblSystemLog")]
    public partial class tblSystemLog
    {
        [Key]
        public int logId { get; set; }

        public DateTime? logDate { get; set; }

        [StringLength(100)]
        public string userId { get; set; }

        [StringLength(50)]
        public string type { get; set; }

        [StringLength(500)]
        public string description { get; set; }

        [StringLength(50)]
        public string status { get; set; }
    }
}
