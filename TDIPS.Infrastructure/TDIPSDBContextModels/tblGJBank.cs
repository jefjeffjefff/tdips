namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblGJBank")]
    public partial class tblGJBank
    {
        [Key]
        public int bankId { get; set; }

        [StringLength(200)]
        public string accountNo { get; set; }

        [StringLength(200)]
        public string accountName { get; set; }

        [StringLength(50)]
        public string accountType { get; set; }

        [StringLength(200)]
        public string store { get; set; }

        [StringLength(100)]
        public string department { get; set; }

        [StringLength(100)]
        public string businessUnit { get; set; }
    }
}
