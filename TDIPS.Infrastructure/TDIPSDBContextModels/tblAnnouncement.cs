namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblAnnouncement")]
    public partial class tblAnnouncement
    {
        [Key]
        public int announcementId { get; set; }

        [StringLength(500)]
        public string announcement { get; set; }

        public string test { get; set; }
    }
}
