namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblSystemConfiguration")]
    public partial class tblSystemConfiguration
    {
        public int id { get; set; }

        public bool? isECREARateActive { get; set; }
    }
}
