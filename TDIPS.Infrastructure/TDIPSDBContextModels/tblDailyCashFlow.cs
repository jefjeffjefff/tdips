namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblDailyCashFlow")]
    public partial class tblDailyCashFlow
    {
        public int Id { get; set; }

        public DateTime DateOfUpload { get; set; }

        [Required]
        [StringLength(50)]
        public string UploadedBy { get; set; }

        [Required]
        [StringLength(100)]
        public string DailyCashDateRange { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DailyCashDate { get; set; }
    }
}
