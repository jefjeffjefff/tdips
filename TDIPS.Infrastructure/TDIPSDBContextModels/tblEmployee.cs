namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblEmployee")]
    public partial class tblEmployee
    {
        public int id { get; set; }

        [Required]
        [StringLength(200)]
        public string employee_no { get; set; }

        [Required]
        [StringLength(200)]
        public string employeeName { get; set; }

        [Required]
        [StringLength(200)]
        public string email { get; set; }

        [StringLength(200)]
        public string accountNo { get; set; }
    }
}
