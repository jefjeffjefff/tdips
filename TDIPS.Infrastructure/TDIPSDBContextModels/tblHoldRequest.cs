namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblHoldRequest")]
    public partial class tblHoldRequest
    {
        [Key]
        public int logid { get; set; }

        [StringLength(50)]
        public string voucherId { get; set; }

        public DateTime? logdate { get; set; }

        [StringLength(100)]
        public string userId { get; set; }

        [StringLength(500)]
        public string note { get; set; }

        [StringLength(50)]
        public string status { get; set; }
    }
}
