namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblQueue")]
    public partial class tblQueue
    {
        [Key]
        public int queueID { get; set; }

        [StringLength(200)]
        public string vendorCode { get; set; }

        public int? queueNumber { get; set; }

        [StringLength(100)]
        public string voucherId { get; set; }

        [StringLength(50)]
        public string queueType { get; set; }

        [StringLength(100)]
        public string collectorName { get; set; }

        public DateTime? queueDateTime { get; set; }

        [StringLength(100)]
        public string collectorType { get; set; }

        [StringLength(100)]
        public string thirdPartyCompany { get; set; }

        [StringLength(50)]
        public string status { get; set; }
    }
}
