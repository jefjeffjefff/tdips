namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblOtherOnlinePayment")]
    public partial class tblOtherOnlinePayment
    {
        [Key]
        public int paymentId { get; set; }

        public int? batchId { get; set; }

        public DateTime? transDate { get; set; }

        public string description { get; set; }

        [StringLength(200)]
        public string accountType { get; set; }

        [StringLength(200)]
        public string account { get; set; }

        [StringLength(200)]
        public string stores { get; set; }

        [StringLength(200)]
        public string department { get; set; }

        [StringLength(200)]
        public string businessUnit { get; set; }

        [StringLength(200)]
        public string purpose { get; set; }

        [StringLength(200)]
        public string salesSegment { get; set; }

        [StringLength(200)]
        public string worker { get; set; }

        [StringLength(200)]
        public string costCenter { get; set; }

        [Column(TypeName = "money")]
        public decimal? debit { get; set; }

        [Column(TypeName = "money")]
        public decimal? credit { get; set; }

        [StringLength(200)]
        public string offsetAccountType { get; set; }

        [StringLength(200)]
        public string offsetAccount { get; set; }

        [StringLength(200)]
        public string oStores { get; set; }

        [StringLength(200)]
        public string oDepartment { get; set; }

        [StringLength(200)]
        public string oBusinessUnit { get; set; }

        [StringLength(200)]
        public string oPurpose { get; set; }

        [StringLength(200)]
        public string oSalesSegment { get; set; }

        [StringLength(200)]
        public string oWorker { get; set; }

        [StringLength(200)]
        public string oCostCenter { get; set; }
    }
}
