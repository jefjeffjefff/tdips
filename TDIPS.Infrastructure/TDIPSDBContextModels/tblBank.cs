namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblBank")]
    public partial class tblBank
    {
        [Key]
        public int bankID { get; set; }

        public int? vendorID { get; set; }

        [StringLength(100)]
        public string bankCompany { get; set; }

        [StringLength(100)]
        public string bankAccount { get; set; }

        [StringLength(150)]
        public string bankBranch { get; set; }

        [StringLength(500)]
        public string accountName { get; set; }
    }
}
