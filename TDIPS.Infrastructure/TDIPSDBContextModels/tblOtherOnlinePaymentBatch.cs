namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblOtherOnlinePaymentBatch")]
    public partial class tblOtherOnlinePaymentBatch
    {
        [Key]
        public int batchId { get; set; }

        public DateTime? uploadDate { get; set; }

        [StringLength(100)]
        public string uploadedBy { get; set; }

        [StringLength(100)]
        public string entity { get; set; }

        [StringLength(100)]
        public string transactionType { get; set; }

        public string description { get; set; }

        [StringLength(200)]
        public string generalJournalNum { get; set; }
    }
}
