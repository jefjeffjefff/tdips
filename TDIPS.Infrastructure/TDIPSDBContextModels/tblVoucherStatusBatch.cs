namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblVoucherStatusBatch")]
    public partial class tblVoucherStatusBatch
    {
        [Key]
        public int batchId { get; set; }

        public DateTime? logDate { get; set; }

        [StringLength(100)]
        public string userId { get; set; }

        [StringLength(512)]
        public string note { get; set; }

        [StringLength(50)]
        public string status { get; set; }

        [StringLength(50)]
        public string batchStatus { get; set; }

        public bool? isProcessed { get; set; }

        public bool? isOpened { get; set; }
    }
}
