namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblExcelUploadedPayment")]
    public partial class tblExcelUploadedPayment
    {
        [Key]
        public int paymentId { get; set; }

        [StringLength(100)]
        public string vendorCode { get; set; }

        [StringLength(150)]
        public string bankName { get; set; }

        [StringLength(50)]
        public string voucherId { get; set; }

        [Column(TypeName = "money")]
        public decimal? amount { get; set; }

        [StringLength(100)]
        public string methodOfPayment { get; set; }

        [StringLength(100)]
        public string chequeNumber { get; set; }

        [Column(TypeName = "date")]
        public DateTime? chequeDate { get; set; }

        [StringLength(100)]
        public string dataAreaId { get; set; }

        public DateTime? logDate { get; set; }
    }
}
