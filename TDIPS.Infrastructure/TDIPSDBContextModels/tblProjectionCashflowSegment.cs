namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblProjectionCashflowSegment")]
    public partial class tblProjectionCashflowSegment
    {
        public int Id { get; set; }

        [StringLength(100)]
        public string Segment { get; set; }

        [StringLength(100)]
        public string InflowOutflow { get; set; }

        [StringLength(100)]
        public string Activity { get; set; }
    }
}
