namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblMailLog")]
    public partial class tblMailLog
    {
        [Key]
        public int logId { get; set; }

        public int? batchId { get; set; }

        [Column(TypeName = "date")]
        public DateTime? logDate { get; set; }

        [StringLength(50)]
        public string type { get; set; }

        public DateTime? dateTimeSent { get; set; }

        [StringLength(350)]
        public string receiverName { get; set; }

        [StringLength(5000)]
        public string status { get; set; }

        [StringLength(500)]
        public string contact { get; set; }

        [StringLength(100)]
        public string description { get; set; }
    }
}
