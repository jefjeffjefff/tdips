namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblProjectionCashflowTransaction")]
    public partial class tblProjectionCashflowTransaction
    {
        public int Id { get; set; }

        public int ProjectionCashflowId { get; set; }

        public int SegmentId { get; set; }

        [Column(TypeName = "money")]
        public decimal Amount { get; set; }
    }
}
