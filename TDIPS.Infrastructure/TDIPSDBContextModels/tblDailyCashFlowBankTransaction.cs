namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblDailyCashFlowBankTransaction")]
    public partial class tblDailyCashFlowBankTransaction
    {
        public int Id { get; set; }

        public int DailyCashFlowBankId { get; set; }

        [Required]
        [StringLength(100)]
        public string TransactionName { get; set; }

        [Required]
        [StringLength(100)]
        public string TransactionType { get; set; }

        [Column(TypeName = "money")]
        public decimal Debit { get; set; }

        [Column(TypeName = "money")]
        public decimal Credit { get; set; }

        [StringLength(50)]
        public string InputType { get; set; }
    }
}
