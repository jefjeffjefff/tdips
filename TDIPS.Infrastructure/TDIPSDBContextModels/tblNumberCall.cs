namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblNumberCall")]
    public partial class tblNumberCall
    {
        public int id { get; set; }

        [StringLength(100)]
        public string type { get; set; }

        [StringLength(500)]
        public string companyName { get; set; }

        [StringLength(100)]
        public string queueType { get; set; }

        public int? queueNumber { get; set; }

        [StringLength(100)]
        public string status { get; set; }

        [StringLength(1000)]
        public string text { get; set; }

        [StringLength(500)]
        public string collectorName { get; set; }
    }
}
