namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblDailyCashFlowBank")]
    public partial class tblDailyCashFlowBank
    {
        public int Id { get; set; }

        public int DailyCashFlowId { get; set; }

        [Required]
        [StringLength(100)]
        public string Bank { get; set; }

        [StringLength(100)]
        public string BankClassification { get; set; }
    }
}
