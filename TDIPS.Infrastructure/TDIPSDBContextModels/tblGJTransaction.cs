namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblGJTransaction")]
    public partial class tblGJTransaction
    {
        public int Id { get; set; }

        [Column(TypeName = "date")]
        public DateTime? PostingDate { get; set; }

        public DateTime? UploadedDT { get; set; }

        [Column(TypeName = "money")]
        public decimal? Debit { get; set; }

        [Column(TypeName = "money")]
        public decimal? Credit { get; set; }

        [StringLength(100)]
        public string AccountNo { get; set; }
    }
}
