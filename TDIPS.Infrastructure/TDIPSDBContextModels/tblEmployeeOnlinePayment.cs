namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblEmployeeOnlinePayment")]
    public partial class tblEmployeeOnlinePayment
    {
        [Key]
        public int paymentId { get; set; }

        [Required]
        [StringLength(100)]
        public string employeeId { get; set; }

        [StringLength(100)]
        public string employeeAccNum { get; set; }

        [Column(TypeName = "money")]
        public decimal? amount { get; set; }

        [Column(TypeName = "date")]
        public DateTime? transDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? logDate { get; set; }

        [StringLength(100)]
        public string entity { get; set; }

        [StringLength(100)]
        public string bank { get; set; }
    }
}
