namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblReceipt")]
    public partial class tblReceipt
    {
        [Key]
        public int receiptId { get; set; }

        [StringLength(100)]
        public string voucherId { get; set; }

        [StringLength(100)]
        public string receiptNo { get; set; }

        [StringLength(50)]
        public string receiptType { get; set; }
    }
}
