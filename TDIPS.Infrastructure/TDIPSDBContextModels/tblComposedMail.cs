namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblComposedMail")]
    public partial class tblComposedMail
    {
        public int Id { get; set; }

        public string Body { get; set; }

        [StringLength(200)]
        public string Subject { get; set; }

        [StringLength(1000)]
        public string Attachments { get; set; }

        public DateTime? DateCreated { get; set; }
    }
}
