namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblCollector")]
    public partial class tblCollector
    {
        [Key]
        public int collectorID { get; set; }

        public int? vendorID { get; set; }

        [StringLength(100)]
        public string collectorName { get; set; }

        [StringLength(100)]
        public string position { get; set; }

        [StringLength(100)]
        public string email { get; set; }

        [StringLength(100)]
        public string contactNumber { get; set; }

        public bool? isDeleted { get; set; }
    }
}
