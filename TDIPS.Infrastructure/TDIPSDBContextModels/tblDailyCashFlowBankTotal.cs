namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblDailyCashFlowBankTotal")]
    public partial class tblDailyCashFlowBankTotal
    {
        public int Id { get; set; }

        public int? DailyCashFlowBankId { get; set; }

        [StringLength(100)]
        public string Type { get; set; }

        [Column(TypeName = "money")]
        public decimal? Amount { get; set; }

        [StringLength(100)]
        public string TotalFrom { get; set; }
    }
}
