namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblEODValidationLog")]
    public partial class tblEODValidationLog
    {
        [Key]
        public int logId { get; set; }

        public DateTime? logDate { get; set; }

        [StringLength(100)]
        public string userId { get; set; }

        [StringLength(100)]
        public string type { get; set; }
    }
}
