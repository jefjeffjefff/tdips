namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblGeneralJournalMapping")]
    public partial class tblGeneralJournalMapping
    {
        public int Id { get; set; }

        [StringLength(100)]
        public string MainAccount { get; set; }

        [StringLength(100)]
        public string Segment { get; set; }

        [StringLength(100)]
        public string Subsegment { get; set; }
    }
}
