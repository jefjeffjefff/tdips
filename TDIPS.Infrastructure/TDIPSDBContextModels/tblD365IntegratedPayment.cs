namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class tblD365IntegratedPayment
    {
        [Key]
        public int paymentId { get; set; }

        [StringLength(100)]
        public string voucherId { get; set; }

        [StringLength(100)]
        public string accountId { get; set; }

        [Column(TypeName = "money")]
        public decimal? amountCur { get; set; }

        [StringLength(50)]
        public string bankCurrency { get; set; }

        [Column(TypeName = "money")]
        public decimal? bankCurrencyAmount { get; set; }

        [StringLength(200)]
        public string bankRecipientName { get; set; }

        [StringLength(100)]
        public string chequeNum { get; set; }

        [StringLength(100)]
        public string recipientAccountNum { get; set; }

        [Column(TypeName = "date")]
        public DateTime? transDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? chequeDate { get; set; }

        [StringLength(100)]
        public string dataAreaId { get; set; }

        [StringLength(100)]
        public string paymentMode { get; set; }

        public DateTime? logDate { get; set; }
    }
}
