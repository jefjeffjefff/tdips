namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblBatchMail")]
    public partial class tblBatchMail
    {
        [Key]
        public int batchMailId { get; set; }

        public DateTime? logDate { get; set; }

        [StringLength(100)]
        public string status { get; set; }
    }
}
