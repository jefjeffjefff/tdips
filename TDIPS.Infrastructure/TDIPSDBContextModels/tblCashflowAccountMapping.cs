namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblCashflowAccountMapping")]
    public partial class tblCashflowAccountMapping
    {
        public int Id { get; set; }

        [StringLength(100)]
        public string MainAccount { get; set; }

        [StringLength(100)]
        public string Segment { get; set; }

        [StringLength(100)]
        public string Subsegment { get; set; }

        [StringLength(100)]
        public string Activity { get; set; }

        [StringLength(100)]
        public string PurposeDimension { get; set; }

        public bool? IsCredit { get; set; }

        [StringLength(100)]
        public string TransactionType { get; set; }
    }
}
