namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblGeneralOnlinePayment")]
    public partial class tblGeneralOnlinePayment
    {
        public int Id { get; set; }

        public DateTime? logDate { get; set; }

        [StringLength(500)]
        public string vendorCode { get; set; }

        [StringLength(400)]
        public string invoiceVouchers { get; set; }

        [StringLength(100)]
        public string entity { get; set; }

        [StringLength(100)]
        public string bank { get; set; }

        [Column(TypeName = "date")]
        public DateTime? transDate { get; set; }

        [Column(TypeName = "money")]
        public decimal? amount { get; set; }

        [StringLength(100)]
        public string modeOfPayment { get; set; }

        [StringLength(100)]
        public string bankAccountNumber { get; set; }
    }
}
