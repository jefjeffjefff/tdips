namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblUnreleasedGeneralJournal")]
    public partial class tblUnreleasedGeneralJournal
    {
        public int id { get; set; }

        public DateTime? postDate { get; set; }

        [StringLength(100)]
        public string postedBy { get; set; }

        [StringLength(200)]
        public string generalJournalNum { get; set; }

        public string description { get; set; }

        [StringLength(200)]
        public string entity { get; set; }

        [StringLength(100)]
        public string month { get; set; }

        [StringLength(100)]
        public string year { get; set; }
    }
}
