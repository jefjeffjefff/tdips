namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblVoucherRequest")]
    public partial class tblVoucherRequest
    {
        [Key]
        public int logId { get; set; }

        public int? batchId { get; set; }

        [StringLength(100)]
        public string checkNum { get; set; }

        [StringLength(200)]
        public string voucherId { get; set; }

        [Column(TypeName = "date")]
        public DateTime? checkDate { get; set; }

        [StringLength(500)]
        public string vendorCode { get; set; }

        [StringLength(500)]
        public string companyName { get; set; }

        [StringLength(50)]
        public string entity { get; set; }

        [StringLength(50)]
        public string bank { get; set; }

        [Column(TypeName = "money")]
        public decimal? amount { get; set; }

        [Column(TypeName = "date")]
        public DateTime? releasingDate { get; set; }

        [StringLength(50)]
        public string status { get; set; }

        public DateTime? logDate { get; set; }

        [StringLength(100)]
        public string userId { get; set; }

        [StringLength(50)]
        public string SEANCode { get; set; }
    }
}
