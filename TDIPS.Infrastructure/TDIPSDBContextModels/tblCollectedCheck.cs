namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblCollectedCheck")]
    public partial class tblCollectedCheck
    {
        [Key]
        public int logId { get; set; }

        [StringLength(500)]
        public string companyName { get; set; }

        [StringLength(500)]
        public string vendorCode { get; set; }

        [StringLength(50)]
        public string entity { get; set; }

        [StringLength(50)]
        public string bank { get; set; }

        [StringLength(200)]
        public string voucherId { get; set; }

        [StringLength(100)]
        public string checkNum { get; set; }

        [Column(TypeName = "date")]
        public DateTime? checkDate { get; set; }

        [Column(TypeName = "money")]
        public decimal? amount { get; set; }

        public DateTime? releasedDate { get; set; }

        [StringLength(100)]
        public string receiptNum { get; set; }

        [StringLength(50)]
        public string receiptType { get; set; }

        [StringLength(50)]
        public string collectorName { get; set; }

        [StringLength(50)]
        public string collectorType { get; set; }

        [StringLength(100)]
        public string thirdPartyCompany { get; set; }

        [StringLength(100)]
        public string checkType { get; set; }
    }
}
