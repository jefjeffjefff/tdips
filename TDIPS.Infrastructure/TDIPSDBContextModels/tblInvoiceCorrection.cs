namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblInvoiceCorrection")]
    public partial class tblInvoiceCorrection
    {
        public int Id { get; set; }

        [StringLength(200)]
        public string OldInvoiceNo { get; set; }

        [StringLength(200)]
        public string NewInvoiceNo { get; set; }

        [StringLength(100)]
        public string ModifiedBy { get; set; }
    }
}
