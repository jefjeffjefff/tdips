namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblVendor")]
    public partial class tblVendor
    {
        [Key]
        public int vendorID { get; set; }

        [StringLength(1000)]
        public string vendorCode { get; set; }

        [StringLength(1000)]
        public string companyName { get; set; }

        [StringLength(1000)]
        public string tinNumber { get; set; }

        [StringLength(1000)]
        public string companyEmail { get; set; }

        [StringLength(1000)]
        public string contactNumber { get; set; }

        [StringLength(1000)]
        public string companyAddress { get; set; }

        [StringLength(1000)]
        public string vendorType { get; set; }

        [StringLength(1000)]
        public string dataAreaId { get; set; }

        public bool? isDeleted { get; set; }

        [StringLength(200)]
        public string segment { get; set; }

        [StringLength(200)]
        public string subSegment { get; set; }

        [StringLength(100)]
        public string type { get; set; }

        [StringLength(100)]
        public string origDataAreaId { get; set; }
    }
}
