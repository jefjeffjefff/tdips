namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblVideo")]
    public partial class tblVideo
    {
        [Key]
        public int videoId { get; set; }

        public DateTime? dateUploaded { get; set; }

        [StringLength(100)]
        public string fileName { get; set; }

        [StringLength(100)]
        public string fileExtension { get; set; }
    }
}
