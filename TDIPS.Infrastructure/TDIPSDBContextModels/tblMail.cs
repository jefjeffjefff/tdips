namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblMail")]
    public partial class tblMail
    {
        public long id { get; set; }

        [StringLength(50)]
        public string companyName { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        [StringLength(100)]
        public string vendorCode { get; set; }

        [StringLength(100)]
        public string EmailFrom { get; set; }

        [Column(TypeName = "money")]
        public decimal? amount { get; set; }

        [StringLength(100)]
        public string receiverName { get; set; }

        [StringLength(100)]
        public string contactNumber { get; set; }

        [StringLength(50)]
        public string voucherId { get; set; }

        [Column(TypeName = "date")]
        public DateTime? chequeDate { get; set; }

        [StringLength(100)]
        public string chequeNumber { get; set; }

        [StringLength(50)]
        public string dataAreaid { get; set; }

        [StringLength(100)]
        public string bank { get; set; }
    }
}
