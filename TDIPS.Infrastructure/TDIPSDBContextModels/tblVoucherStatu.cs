namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class tblVoucherStatu
    {
        [Key]
        public int logId { get; set; }

        public int? batchId { get; set; }

        [StringLength(50)]
        public string voucherId { get; set; }

        public DateTime? logDate { get; set; }

        [StringLength(50)]
        public string status { get; set; }

        public DateTime? releasingDate { get; set; }

        [StringLength(50)]
        public string SEANCode { get; set; }

        [StringLength(100)]
        public string userId { get; set; }
    }
}
