namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblECREARating")]
    public partial class tblECREARating
    {
        public int id { get; set; }

        [StringLength(200)]
        public string CollectorName { get; set; }

        public bool? Rate { get; set; }

        public DateTime? DTRate { get; set; }

        [StringLength(300)]
        public string VendorCode { get; set; }
    }
}
