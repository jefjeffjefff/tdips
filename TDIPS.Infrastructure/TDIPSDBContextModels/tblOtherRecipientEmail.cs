namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblOtherRecipientEmail")]
    public partial class tblOtherRecipientEmail
    {
        public int Id { get; set; }

        [StringLength(200)]
        public string recipientName { get; set; }

        [StringLength(200)]
        public string recipientEmail { get; set; }
    }
}
