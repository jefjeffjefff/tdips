namespace TDIPS.Infrastructure.TDIPSDBContextModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tblProjectionCashflow")]
    public partial class tblProjectionCashflow
    {
        public int Id { get; set; }

        public DateTime? DateOfUpload { get; set; }

        [StringLength(100)]
        public string UploadedBy { get; set; }

        [StringLength(20)]
        public string ProjectionMonth { get; set; }

        [StringLength(20)]
        public string ProjectionDateRange { get; set; }
    }
}
