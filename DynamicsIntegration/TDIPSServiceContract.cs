﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DynamicsIntegration
{
    public class TDIPSServiceContract
    {
        public TDIPSServiceContract()
        {
        }

        public string DocumentNumber { get; set; }

        public string JournalNumber { get; set; }

        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }

        public string Purpose { get; set; }

        public string MainAccount { get; set; }

        public string DataAreaId { get; set; }

        public DataTable dtVoucher { get; set; }

        public DataTable dtGeneralJournal { get; set; }

        public string Description { get; set; }
    }
}
