﻿using System;

namespace DynamicsIntegration
{
    public class RootObject
    {
        public RootObject()
        {
        }
        public object CallContext { get; set; }

        public TDIPSServiceContract _dataContract { get; set; }
    }
}
