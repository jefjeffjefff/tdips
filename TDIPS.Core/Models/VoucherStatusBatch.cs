﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
	public class VoucherStatusBatch
	{
		public int batchId { get; set; }

		public Nullable<System.DateTime> logDate { get; set; }

		public string userId { get; set; }

		public string note { get; set; }

		public string status { get; set; }

		public Nullable<decimal> totalAmount { get; set; }

		public string batchStatus { get; set; }

		public Nullable<bool> isOpened { get; set; }

		public Nullable<bool> isProcessed { get; set; }
	}
}
