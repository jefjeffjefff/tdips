﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class NumberCall
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string CompanyName { get; set; }
        public string QueueType { get; set; }
        public int QueueNumber { get; set; }
        public string Status { get; set; }
        public string Text { get; set; }
        public string CollectorName { get; set; }
    }
}
