﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
	public class Mail
	{
        public object this[string propertyName]
        {
            get { return this.GetType().GetProperty(propertyName).GetValue(this, null); }
            set { this.GetType().GetProperty(propertyName).SetValue(this, value, null); }
        }

        public int vendorId { get; set; }
        public string companyName { get; set; }
        public string receiverIdentification { get; set; }
        public string receiverEmail { get; set; }
		public string receiverContactNumber { get; set; }
		public string receiverName { get; set; }
		public string SEANCODE { get; set; }
		public string vendorCode { get; set; }
		public string voucherId { get; set; }
		public Nullable<decimal> amount { get; set; }
        public DateTime releasingDate { get; set; }
        public Nullable<DateTime> chequeDate { get; set; }
        public string chequeNumber { get; set; }
        public string dateAreaId { get; set; }
        public string bank { get; set; }
    }
}
