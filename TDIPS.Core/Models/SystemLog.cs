﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class SystemLog
    {
        public int logId { get; set; }
        public Nullable<System.DateTime> logDate { get; set; }
        public string userId { get; set; }
        public string type { get; set; }
        public string description { get; set; }
        public string status { get; set; }
        public string strTime { get; set; }
    }
}
