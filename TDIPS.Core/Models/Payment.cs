﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TDIPS.Core.Models
{
	public class Payment
	{
		public int paymentId { get; set; }
		[MaxLength(100)]
		public string vendorCode { get; set; }
		[MaxLength(150)]
		public string bankName { get; set; }
		[Required]
		public string voucherId { get; set; }
		public Nullable<int> batchId { get; set; }
		public Nullable<decimal> amount { get; set; }
		[MaxLength(100)]
		public string methodOfPayment { get; set; }
		
		public string chequeNumber { get; set; }
		[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
		public Nullable<System.DateTime> chequeDate { get; set; }

		[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
		public Nullable<System.DateTime> releasingDate { get; set; }
		
		//Vendor Name
		public string companyName { get; set; }

		public string source { get; set; }

		public string status { get; set; }

		public object statusDetails { get; set; }

		public string dataAreaId { get; set; }

        public string holdStatus { get; set; }

        public object requestDetails { get; set; }
    }
}
