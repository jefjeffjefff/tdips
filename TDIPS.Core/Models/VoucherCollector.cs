﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class VoucherCollector
    {
        public string CollectorName { get; set; }
        public string CollectorType { get; set; }
        public string ThirdPartyCompany { get; set; }
    }
}
