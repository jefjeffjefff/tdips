﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
	public class Voucher
	{
		public int logId { get; set; }

		public Nullable<int> batchId { get; set; }

		public string voucherId { get; set; }

        public string vendorCode { get; set; }

        public string companyName { get; set; }

        public string entity { get; set; }

        public string bank { get; set; }

        public Nullable<decimal> amount { get; set; }

        public Nullable<System.DateTime> logDate { get; set; }

		public string status { get; set; }

        public string checkNum { get; set; }

        public Nullable<System.DateTime> checkDate { get; set; }
        
        public Nullable<System.DateTime> releasingDate { get; set; }

		public string SEANCode { get; set; }

        public string CheckType { get; set; }

        public string ReceiptNo { get; set; }

        public string ReceiptType { get; set; }
    }
}
