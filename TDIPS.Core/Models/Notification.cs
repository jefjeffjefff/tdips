﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
	public class Notification
	{
		public int ApprovalRequest { get; set; }
		public int VerificationRequest { get; set; }
		public int DeclinedRequest { get; set; }
		public int ReadyForReleasing { get; set; }
	}
}
