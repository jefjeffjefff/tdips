﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class Receipt
    {
        public string VOUCHERID { get; set; }
        public string RECEIPTNUMBER { get; set; }
        public string RECEIPTTYPE { get; set; }
        public string CHECKTYPE { get; set; }
    }
}
