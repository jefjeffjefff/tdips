﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class D365Vendor
    {
        public string dataAreaId { get; set; }
        public string accountNum { get; set; }
        public string name { get; set; }
        public string segment { get; set; }
        public string subsegment { get; set; }
        public string vendGroup { get; set; }
    }
}
