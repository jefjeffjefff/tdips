﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class GeneralJournalMap
    {
        public string MainAccount { get; set; }

        public string Segment { get; set; }

        public string Subsegment { get; set; }
    }
}
