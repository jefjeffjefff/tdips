﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TDIPS.Core.Models
{
    public class ApplicationUser
    {
        public string Email { get; set; }

        public string Username { get; set; }

        public string Role { get; set; }
    }
}
