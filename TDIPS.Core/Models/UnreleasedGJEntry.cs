﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class UnreleasedGJEntry
    {
        public string AccountType { get; set; }

        public string Account { get; set; }

        public string Department { get; set; }

        public string BusinessUnit { get; set; }

        public string Purpose { get; set; }

        public DateTime? Transdate { get; set; }

        public string strTransdate { get; set; }

        public string TXT { get; set; }

        public decimal Credit { get; set; }

        public string OffsetAccountType { get; set; }

        public string OffsetAccount { get; set; }

        public string PostingProfile { get; set; }

        public DateTime? DueDate { get; set; }

        public string Document { get; set; }

        public List<string> EntryDescription { get; set; }

        public string EntryStatus { get; set; }

        public string VoucherNum { get; set; }
    }
}
