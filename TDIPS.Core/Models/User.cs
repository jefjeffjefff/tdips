﻿
namespace TDIPS.Core.Models
{
	public class User
	{
		public string username { get; set; }
        public string email { get; set; }
        public string role { get; set; }
	}
}
