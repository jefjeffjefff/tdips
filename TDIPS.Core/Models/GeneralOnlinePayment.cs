﻿using System;

namespace TDIPS.Core.Models
{
    public class GeneralOnlinePayment
    {
        public int Id { get; set; }
        public Nullable<System.DateTime> logDate { get; set; }
        public string vendorCode { get; set; }
        public string invoiceVouchers { get; set; }
        public string entity { get; set; }
        public string bank { get; set; }
        public Nullable<System.DateTime> transDate { get; set; }
        public Nullable<decimal> amount { get; set; }
        public string modeOfPayment { get; set; }
        public string companyName { get; set; }
        public string bankAccountNumber { get; set; }
        public Vendor vendor { get; set; }
        public Bank BankDetails { get; set; }
    }
}
