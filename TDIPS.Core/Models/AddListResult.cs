﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class AddListResult
    {
        public int Total { get; set; }
        public int Added { get; set; }
        public int Updated { get; set; }
        public int Failed { get; set; }
        public string Status { get; set; }
    }
}
