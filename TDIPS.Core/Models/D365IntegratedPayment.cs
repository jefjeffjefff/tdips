﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TDIPS.Core.Models
{
	public class D365IntegratedPayment
	{
		public int id { get; set; }
		public string displayValue { get; set; } //vendor code
		public string dimensionName { get; set; }
		public Nullable<decimal> bankTransAmountCur { get; set; }
		public string paymentMode { get; set; }
		public string accountId { get; set; }
		public string chequeNum { get; set; }
		public string chequeStatus { get; set; }
		public Nullable<System.DateTime> transDate { get; set; }
		public string voucherId { get; set; }
		public string dataAreaId { get; set; }
        public string voucher { get; set; }
    }
}
