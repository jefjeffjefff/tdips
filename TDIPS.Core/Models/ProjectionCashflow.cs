﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class ProjectionCashflow
    {
        public int Id { get; set; }
        public Nullable<System.DateTime> DateOfUpload { get; set; }
        public string UploadedBy { get; set; }
        public string ProjectionMonth { get; set; }
        public string ProjectionDateRange { get; set; }
    }
}
