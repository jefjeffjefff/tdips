﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class InvoiceCorrection
    {
        public int Id { get; set; }
        public string OldInvoiceNo { get; set; }
        public string NewInvoiceNo { get; set; }
        public string ModifiedBy { get; set; }
    }
}
