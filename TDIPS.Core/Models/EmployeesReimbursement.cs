﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class EmployeesReimbursement
    {
        public int Id { get; set; }

        public string journalNum { get; set; }

        public string name { get; set; }

        public double lineNum { get; set; }

        public string voucher { get; set; }

        public string accountType { get; set; }

        public string account { get; set; }

        public string text { get; set; }

        public decimal amountcurdebit { get; set; }

        public decimal amountcurcredit { get; set; }

        public DateTime transdate { get; set; }

        public DateTime postedDateTime { get; set; }

        public string dataAreaId { get; set; }
    }
}
