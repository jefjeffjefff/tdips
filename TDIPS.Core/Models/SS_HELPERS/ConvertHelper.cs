﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models.SS_HELPERS
{
    public class ConvertHelper
    {
        /// <summary>
        /// To the integer.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>The integer value.</returns>
        public static int ToInteger(object value)
        {
            if (value == null)
            {
                return 0;
            }

            var val = value.ToString();
            int result = 0;
            int.TryParse(val, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out result);
            return result;
        }

        /// <summary>
        /// To the decimal.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>The decimal value</returns>
        public static decimal ToDecimal(object value)
        {
            var val = value.ToString();
            decimal result = 0;
            decimal.TryParse(val, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out result);
            return result;
        }

        /// <summary>
        /// To the date.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>The date time value.</returns>
        public static DateTime? ToDate(object value)
        {
            if (value == null)
            {
                return null;
            }

            var val = value.ToString();
            DateTime result;
            if (!DateTime.TryParse(val, out result))
            {
                return null;
            }

            return result;
        }

        /// <summary>
        /// To the data table.
        /// </summary>
        /// <typeparam name="T">Model type.</typeparam>
        /// <param name="data">The data.</param>
        /// <returns>The data in datable format.</returns>
        public static DataTable ToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();

            foreach (PropertyDescriptor prop in properties)
            {
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }

            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                }

                table.Rows.Add(row);
            }

            return table;
        }

        /// <summary>
        /// Converts the meters to feet.
        /// </summary>
        /// <param name="strMeters">The string meters.</param>
        public static Tuple<int, double> ConvertMetersToFeetAndInches(double convert)
        {
            double value = 0.3048;

            //// feet
            var inft = convert / value;
            var ft = (int)inft;

            //// inches
            double temp = (inft - Math.Truncate(inft)) / 0.08333;
            var inchesleft = Math.Round(temp);

            return new Tuple<int, double>(ft, inchesleft);
        }

        /// <summary>
        /// Gets the characters only.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns>Characters only.</returns>
        public static string GetCharactersOnly(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
                {
                    sb.Append(c);
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// Removes the special characters.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns>The string</returns>
        public static string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
                {
                    sb.Append(c);
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// Romans to number.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns>Number</returns>
        public static int RomanToNumber(string str)
        {
            var number = 0;

            foreach (KeyValuePair<string, int> entry in RomanMap)
            {
                if (entry.Key == str)
                {
                    number = entry.Value;
                    break;
                }
            }
            return number;
        }

        private static Dictionary<string, int> RomanMap = new Dictionary<string, int>()
        {
            {"I", 1},
            {"II", 2},
            {"III", 3},
            {"IV", 4},
            {"V", 5},
            {"VI", 6},
            {"VII", 7},
            {"VIII", 8},
            {"IX", 9},
            {"X", 10},
        };
    }
}
