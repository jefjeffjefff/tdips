﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TDIPS.Core.Models
{
	public class Collector
	{
		public int collectorID { get; set; }
		
		public Nullable<int> vendorID { get; set; }
		[MaxLength(100)]
		public string collectorName { get; set; }
		[MaxLength(100)]
		public string position { get; set; }
		[DataType(DataType.EmailAddress)]
		public string email { get; set; }
		[RegularExpression("^[0-9]*$")]
		[MaxLength(100)]
		public string contactNumber { get; set; }
	}
}
