﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class DailyCashFlow
    {
        public int Id { get; set; }
        public System.DateTime DateOfUpload { get; set; }
        public string UploadedBy { get; set; }
        public string DailyCashDateRange { get; set; }
        public Nullable<System.DateTime> DailyCashDate { get; set; }
    }
}
