﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class VendorExcelUpload
    {
        public string VendorCode { get; set; }
        public string Entity { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyEmail { get; set; }
        public string CompanyMobileNumber { get; set; }
        public string TinNumber { get; set; }
        public string C1_Name { get; set; }
        public string C1_Position { get; set; }
        public string C1_Email { get; set; }
        public string C1_Mobile { get; set; }
        public string C2_Name { get; set; }
        public string C2_Position { get; set; }
        public string C2_Email { get; set; }
        public string C2_Mobile { get; set; }
        public string C3_Name { get; set; }
        public string C3_Position { get; set; }
        public string C3_Email { get; set; }
        public string C3_Mobile { get; set; }
        public string BankName { get; set; }
        public string BankBranch { get; set; }
        public string AccountName { get; set; }
        public string BankAccountNo { get; set; }
        public string Segment { get; set; }
        public string Subsegment { get; set; }
    }
}
