﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class EmployeeOnlinePayment
    {
        public int paymentId { get; set; }
        public string employeeId { get; set; }
        public string employeeAccNum { get; set; }
        public Nullable<decimal> amount { get; set; }
        public Nullable<System.DateTime> paymentDate { get; set; }
        public string transDate { get; set; }
        public Employee Employee { get; set; }
        public DateTime? logDate { get; set; }
        public string strLogDate { get; set; }
        public string entity { get; set; }
        public string bank { get; set; }
    }
}
