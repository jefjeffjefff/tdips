﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class OtherOnlinePayment
    {
        public int paymentId { get; set; }
        public Nullable<System.DateTime> transDate { get; set; }
        public string description { get; set; }
        public string accountType { get; set; }
        public string account { get; set; }
        public string stores { get; set; }
        public string department { get; set; }
        public string businessUnit { get; set; }
        public string purpose { get; set; }
        public string salesSegment { get; set; }
        public string worker { get; set; }
        public string costCenter { get; set; }
        public Nullable<decimal> debit { get; set; }
        public Nullable<decimal> credit { get; set; }
        public string offsetAccountType { get; set; }
        public string offsetAccount { get; set; }
        public string oStores { get; set; }
        public string oDepartment { get; set; }
        public string oBusinessUnit { get; set; }
        public string oPurpose { get; set; }
        public string oSalesSegment { get; set; }
        public string oWorker { get; set; }
        public string oCostCenter { get; set; }

        public string transactionType { get; set; }
        public int batchId { get; set; }
        public DateTime? uploadDate { get; set; }

        public string entity { get; set; }

        public string genJournNum { get; set; }
    }
}
