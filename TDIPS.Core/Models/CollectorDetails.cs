﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class CollectorDetails
    {
        public string CollectorName { get; set; }
        public string CollectorType { get; set; }
        public string ThirdPartyCompany { get; set; }
        public string Status { get; set; }
        public string QueueType { get; set; }
    }
}
