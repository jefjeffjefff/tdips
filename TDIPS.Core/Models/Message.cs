﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class Message
    {
        public int Id { get; set; }
        public string ReceiverEmail { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
        public string[] Attachment { get; set; }
        public DateTime? DateCreated { get; set; }

    }
}
