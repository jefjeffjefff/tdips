﻿using System;
using System.Collections.Generic;

namespace TDIPS.Core.Models
{
    public class Aging
    {
        public string CheckNumber { get; set; }
        public string Payee { get; set; }
        public string DateReceived { get; set; }
        public int NoOfDays { get; set; }
        public int Term { get; set; }
        public Nullable<DateTime> DueDate { get; set; }
        public int? OverDue { get; set; }
        public Nullable<DateTime> CheckDate { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<decimal> Current { get; set; }
        public Nullable<decimal> Term1 { get; set; }
        public Nullable<decimal> Term2 { get; set; }
        public Nullable<decimal> Term3 { get; set; }
        public Nullable<decimal> Term4 { get; set; }
        public Nullable<decimal> Term5 { get; set; }
        public Nullable<decimal> Amount2 { get; set; }
        public string Entity { get; set; }
        public string Bank { get; set; }
    }
}
