﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class CollectedChecks
    {
        public int logId { get; set; }
        public string companyName { get; set; }
        public string vendorCode { get; set; }
        public string entity { get; set; }
        public string bank { get; set; }
        public string voucherId { get; set; }
        public string checkNum { get; set; }
        public DateTime? checkDate { get; set; }
        public string checkType { get; set; }
        public decimal? amount { get; set; }
        public DateTime? releasedDate { get; set; }
        public string receiptNum { get; set; }
        public string receiptType { get; set; }
        public string collectorType { get; set; }
        public string collectorName { get; set; }
        public string thirdPartyCompany { get; set; }
        public string segment { get; set; }
        public string subsegment { get; set; }
    }
}
