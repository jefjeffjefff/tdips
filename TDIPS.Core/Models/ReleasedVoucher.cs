﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
	public class ReleasedVoucher
	{
		public string VoucherId { get; set; }
		public string ReceiptNo { get; set; }
		public string ReceiptType { get; set; }
        public string CheckType { get; set; }
    }
}
