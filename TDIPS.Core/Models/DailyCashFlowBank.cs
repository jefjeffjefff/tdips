﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class DailyCashFlowBank
    {
        public int Id { get; set; }
        public int DailyCashFlowId { get; set; }
        public string Bank { get; set; }
        public Nullable<decimal> BeginningBalance { get; set; }
        public Nullable<decimal> EndingBalance { get; set; }
        public Nullable<decimal> CashBalance { get; set; }
        public Nullable<decimal> AvailableCashEndingBalance { get; set; }
        public string BankClassification { get; set; }
    }
}
