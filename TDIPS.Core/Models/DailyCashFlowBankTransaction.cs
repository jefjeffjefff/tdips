﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class DailyCashFlowBankTransaction
    {
        public int Id { get; set; }
        public int DailyCashFlowBankId { get; set; }
        public string TransactionName { get; set; }
        public string TransactionType { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public string InputType { get; set; }
    }
}
