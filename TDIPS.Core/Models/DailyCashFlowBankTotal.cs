﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class DailyCashFlowBankTotal
    {
        public int Id { get; set; }
        public Nullable<int> DailyCashFlowBankId { get; set; }
        public string Type { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public string TotalFrom { get; set; }
    }
}
