﻿namespace TDIPS.Core.Models
{
	public class ContactDetails
	{
		public string EmailAddress { get; set; }
		public string ContactNumber { get; set; }
        public string ReceiverIdentification { get; set; }
        public string ReceiverName { get; set; }
    }
}
