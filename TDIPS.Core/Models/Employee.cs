﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class Employee
    {
        public int id { get; set; }
        public string employee_no { get; set; }
        public string employeeName { get; set; }
        public string email { get; set; }
        public string accountNo { get; set; }

        public bool isEmployeeNumChanged { get; set; }
        
    }
}
