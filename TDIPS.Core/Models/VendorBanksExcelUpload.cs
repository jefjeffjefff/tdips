﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class VendorBanksExcelUpload
    {
        public string VendorCode { get; set; }

        public string BankCompany { get; set; }

        public string BankAccount { get; set; }

        public string BankBranch { get; set; }

        public string AccountName { get; set; }
    }
}
