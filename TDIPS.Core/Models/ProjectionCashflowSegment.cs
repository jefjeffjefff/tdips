﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class ProjectionCashflowSegment
    {
        public int Id { get; set; }
        public string Segment { get; set; }
        public string InflowOutflow { get; set; }
        public string Activity { get; set; }
    }
}
