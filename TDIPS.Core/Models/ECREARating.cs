﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class ECREARating
    {
        public int id { get; set; }
        public string CollectorName { get; set; }
        public Nullable<bool> Rate { get; set; }
        public Nullable<System.DateTime> DTRate { get; set; }
        public string VendorCode { get; set; }
    }
}
