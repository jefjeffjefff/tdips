﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class MailStatus
    {
        public int Sending { get; set; }
        public int Success { get; set; }
        public int Failed { get; set; }
    }
}
