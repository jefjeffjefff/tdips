﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class Transaction
    {
        public decimal DEBIT { get; set; }
        public decimal CREDIT { get; set; }
        public string DESCRIPTION { get; set; }
    }

}
