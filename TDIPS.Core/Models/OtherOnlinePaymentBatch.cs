﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class OtherOnlinePaymentBatch
    {
        public int batchId { get; set; }
        public Nullable<System.DateTime> uploadDate { get; set; }
        public string uploadedBy { get; set; }
        public string entity { get; set; }
        public string transactionType { get; set; }
        public string description { get; set; }
        public string generalJournalNum { get; set; }
    }
}
