﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class Invoice
    {
        //voucherNumber
        public string offSetTransVoucher { get; set; }
        public string invoiceId { get; set; }
        public string voucherId { get; set; }
        public string dataAreaId { get; set; }
    }
}
