﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class DailyCashFlowTransactionEntry
    {
        public DailyCashFlowBank Bank { get; set; }

        public List<DailyCashFlowBankTransaction> Transactions { get; set; }

        public List<DailyCashFlowBankTotal> Totals { get; set; }

        public int ExcelColumnIndex { get; set; }
    }
}
