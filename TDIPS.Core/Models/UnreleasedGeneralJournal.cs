﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class UnreleasedGeneralJournal
    {
        public int id { get; set; }
        public Nullable<System.DateTime> postDate { get; set; }
        public string postedBy { get; set; }
        public string generalJournalNum { get; set; }
        public string description { get; set; }
        public string entity { get; set; }
        public string month { get; set; }
        public string year { get; set; }
    }
}
