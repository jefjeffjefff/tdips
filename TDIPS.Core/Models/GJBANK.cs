﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class GJBank
    {
        public int BankId { get; set; }
        public string AccountNo { get; set; }
        public string AccountName { get; set; }
        public string AccountType { get; set; }
        public string Store { get; set; }
        public string Department { get; set; }
        public string BusinessUnit { get; set; }
    }
}
