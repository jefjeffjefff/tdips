﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class VoucherExcelExport
    {
        public string Entity { get; set; }
        public string VendorCode { get; set; }
        public string CompanyName { get; set; }
        public string BankName { get; set; }
        public string VoucherNumber { get; set; }
        public decimal? Amount { get; set; }
        public string CheckNumber { get; set; }
        public DateTime? CheckDate { get; set; }
    }
}
