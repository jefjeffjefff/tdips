﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class ProjectionCashflowTransaction
    {
        public int Id { get; set; }
        public int ProjectionCashflowId { get; set; }
        public int SegmentId { get; set; }
        public decimal Amount { get; set; }
    }
}
