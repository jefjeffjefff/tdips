﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class MailLog
    {
        public int LogId { get; set; }
        public string Type { get; set; }
        public Nullable<System.DateTime> DateTimeSent { get; set; }
        public string ReceiverName { get; set; }
        public string Status { get; set; }
        public string Contact { get; set; }
        public string Description { get; set; }
    }
}
