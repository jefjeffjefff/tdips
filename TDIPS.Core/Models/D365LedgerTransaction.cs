﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class D365LedgerTransaction
    {
        public int Id { get; set; }

        public string journalNumber { get; set; }

        public string voucher { get; set; }

        public DateTime transDate { get; set; }

        public string ledgerAccount { get; set; }

        public string accountName { get; set; }

        public string description { get; set; }

        public string currency { get; set; }

        public decimal amount { get; set; }

        public string postingType { get; set; }

        public string postingLayer { get; set; }

        public string recId { get; set; }

        public string isCredit { get; set; }

        public string transactionType { get; set; }

        public string journalCategory { get; set; }
    }
}
