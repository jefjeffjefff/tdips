﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Models
{
    public class BrainVoucher
    {
        public string Cashflow { get; set; }

        public DateTime? Transdate { get; set; }

        public string MainAccount { get; set; }

        public string Voucher { get; set; }

        public decimal? Amount { get; set; }

        public string Segment { get; set; }

        public string Subsegment { get; set; }

        public string Activity { get; set; }

        public string Source { get; set; }

        public string Entity { get; set; }
    }
}
