﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TDIPS.Core.Models
{
	public class Vendor
	{
		public int vendorID { get; set; }
		public string vendorCode { get; set; }
		public string companyName { get; set; }
		public string tinNumber { get; set; }
		public string companyEmail { get; set; }
		public string contactNumber { get; set; }
		public string companyAddress { get; set; }
        public string dataAreaId { get; set; }
        public string vendorType { get; set; }
        public string segment { get; set; }
        public string subSegment { get; set; }
        public string type { get; set; }

        public Nullable<bool> isDeleted { get; set; }
	}
}
