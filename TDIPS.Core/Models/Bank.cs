﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TDIPS.Core.Models
{
	public class Bank
	{
		public int bankID { get; set; }
		
		public Nullable<int> vendorID { get; set; }

		public string bankCompany { get; set; }

		public string bankAccount { get; set; }
		
		public string bankBranch { get; set; }

        public string accountName { get; set; }
    }
}
