﻿using System;
using System.Collections.Generic;
using System.Linq;
using TDIPS.Core.Models;
using TDIPS.Core.Models.SS_Entities;

namespace TDIPS.Core.Interfaces
{
	public interface IPaymentRepository
	{
		List<Payment> List(string[] status = null, DateTime? statusDate = null, bool filterCheckDate = false);
        List<Payment> SSList(DataFilter filter,
            string vendorcode,
            string companyName,
            string bank,
            string entity,
            string voucherId,
            decimal? amount,
            string payment,
            string check,
            DateTime? date,
            string status);
        
		string Add(Payment p);
		string Add(D365IntegratedPayment p);
		string InsertCheckDetails(Payment p);
	}
}
