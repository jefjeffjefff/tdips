﻿using System;
using System.Collections.Generic;
using System.Linq;
using TDIPS.Core.Models;

namespace TDIPS.Core.Interfaces
{
	public interface IMailRepository
	{
		List<Mail> GetContactFromCollectorsAndVendors(string[] voucherId = null);
        List<Mail> GetContactFromCollectorsAndVendorsWithNoCollection(string[] vendorCodes = null);

		void SendMail(List<Mail> e, string noticeType, List<Invoice> invoice = null);
        void VoucherNotification(string status, string[] usertype);
        void OnlinePaymentMail(List<EmployeeOnlinePayment> op);

        void SendOnlinePaymentMail(GeneralOnlinePayment[] model);
        
        string SendEODMail(List<CollectedChecks> collectedChecks, string user, bool isBySystem = false, string filePath = "");

        void SendOtherOnlinePaymentsEODMail(string user, bool isBySystem = false);

        string SendForReleasingMail(string user, DateTime? dtReleasingDate = null, string filePath = "");

        string SendCanceledVoucherNoticeMail(List<Voucher> lstVouchers);

        void SpecialAnnouncement(string[] attachments);

        object MailLogs(DateTime dateFrom, DateTime dateTo);
        MailStatus TrackMailStatus();
	}
}
