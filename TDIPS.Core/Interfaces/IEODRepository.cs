﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Interfaces
{
    public interface IEODRepository
    {
        string Add(string username, string type = "VOUCHER");
        object IsEODValidationSent(DateTime? date, string type = "VOUCHER");
    }
}
