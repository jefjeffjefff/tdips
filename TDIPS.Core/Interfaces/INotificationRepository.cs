﻿using System.Collections.Generic;
using TDIPS.Core.Models;

namespace TDIPS.Core.Interfaces
{
	public interface INotificationRepository
	{
		Notification Notifications();
		void OpenNotification(string[] area);
	}
}
