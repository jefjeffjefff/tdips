﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDIPS.Core.Models;

namespace TDIPS.Core.Interfaces
{
    public interface ICashflowRepository
    {
        string AddDailyCashFlow(DailyCashFlow dailyCashFlow, 
            List<DailyCashFlowTransactionEntry> dailyCashFlowTransactionEntry);

        string AddProjectionCashflow(ProjectionCashflow projectionCashflow, List<ProjectionCashflowTransaction> projectionCashflowTransactions);

        List<ProjectionCashflowSegment> GetProjectionCashflowSegments();

        List<ProjectionCashflow> GetProjectionCashFlowsList();

        List<DailyCashFlow> GetDailyCashFlowsList();

        List<CashflowAccountMapping> GetCashflowAccountMappings();
    }
}
