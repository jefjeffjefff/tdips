﻿using System.Collections.Generic;
using TDIPS.Core.Models;

namespace TDIPS.Core.Interfaces
{
	public interface IQueueRepository
	{
		object GetQueueList(int rowNumber = -1, string status = "");
		void DeleteFromQueue(int queueId = 0, string vendorCode = "");
        void AddQueue(CollectorDetails c, string vendorCode);
        string Dequeue(int queueId, string userID);
        string Queue(int queueId, string userID);
	}
}
