﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDIPS.Core.Models;

namespace TDIPS.Core.Interfaces
{
    public interface IGeneralJournalRepository
    {
        List<GJBank> GetBankList();
        string AddBank(GJBank gjbank);
        string AddTransaction(GJTransaction gjtransaction);
        List<GJTransaction> JoinGJTandGJB(List<GJTransaction> lstGjtransaction);

        string AddGeneralJournal(string entity, string description, string genJourNum, string user, string month, string year);

        List<UnreleasedGeneralJournal> GetPostedUnreleasedGeneralJournal();
        List<UnreleasedGJEntry> GetUnreleasedGJEntries(string entity, DateTime from, DateTime to, DateTime? asOf = null);
    }
}
