﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDIPS.Core.Models;

namespace TDIPS.Core.Interfaces
{
    public interface IMailComposerRepository
    {
        int SaveToDraft(Message draft);

        List<Message> GetDrafts();

        Message LoadDraft(int draftId);

        string DeleteDraft(int draftId);

        void SendMessage(Message message, int[] vendorId, bool _includeCollectors = false);
    }
}
