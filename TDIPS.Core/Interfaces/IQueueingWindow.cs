﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDIPS.Core.Models;

namespace TDIPS.Core.Interfaces
{
	public interface IQueueingWindow
	{
		int SaveVideo(string fileName, string fileExtension, string userID);
		string DeleteVideo(int videoId, string userID);
		string SaveMarqueeText(string text, string userID);
		string GetMarqueeText();
        object VideoList();
        int AddNumberCall(NumberCall n);
        object GetCall();
        void SetCallReady(int id);
	}
}
