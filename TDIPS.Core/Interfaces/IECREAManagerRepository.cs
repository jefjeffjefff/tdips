﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDIPS.Core.Models;

namespace TDIPS.Core.Interfaces
{
	public interface IECREAManagerRepository
	{
        void SetKioskRatingActivation(bool isActivate);
        bool IsKioskRatingActivated();
        List<ECREARating> GetLastECREARatings();
	}
}
