﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDIPS.Core.Models;

namespace TDIPS.Core.Interfaces
{
    public interface IInvoiceRepository
    {
        string AddUpdateInvoiceNumCorrection(InvoiceCorrection invoiceCorrection);

        string DeleteInvoiceCorrection(int invoiceId);

        List<InvoiceCorrection> GetInvoiceCorrectionList();
    }
}
