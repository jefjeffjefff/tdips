﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDIPS.Core.Models;

namespace TDIPS.Core.Interfaces
{
	public interface IUserRepository
	{
		object List();

		List<ApplicationUser> GetUsersList();

		string IsUserOnLeave(string userId);
		void SetUsersLeaveStatus(string userId, bool isOnLeave);
		string IsTreasuryManagerOnLeave();
		string IsTreasuryLeadOnLeave();
        bool IsEmailExisting(string email);
	}
}
