﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Interfaces
{
    public class VoucherStatus
    {
        public int Unreleased { get; set; }
        public int ForVerification { get; set; }
        public int ForApproval { get; set; }
        public int ForRelease { get; set; }
        public int Uncollected { get; set; }
        public int Released { get; set; }
    }
}
