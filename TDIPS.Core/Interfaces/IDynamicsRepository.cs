﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDIPS.Core.Models;

namespace TDIPS.Core.Interfaces
{
    public interface IDynamicsRepository
    {
        void UpdateDataToBrains();

        void UpdateCashflowsToBrains();

        void UpdateLedgerTransaction(CashflowAccountMapping map, IEnumerable<D365LedgerTransaction> transactions, string entity);

        void UpdateEmployeesReimbursements(IEnumerable<EmployeesReimbursement> reimbursements);

        void UpdateOnlinePayments(GeneralJournalMap map, IEnumerable<D365LedgerTransaction> transactions, string entity);

        List<GeneralJournalMap> GetGeneralJournalMaps();
    }
}
