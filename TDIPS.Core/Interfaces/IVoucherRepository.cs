﻿using System;
using System.Collections.Generic;
using System.Linq;
using TDIPS.Core.Models;
using TDIPS.Core.Models.SS_Entities;

namespace TDIPS.Core.Interfaces
{
	public interface IVoucherRepository
	{
        string AddVoucherStatus(string voucherId, string status, string userId = null, D365IntegratedPayment d = null, bool isAddNew = true);

		string AcceptVerificationRequest(int requestedBatchId, int batchId, string userId);

		string[] AcceptApprovalRequest(int requestedBatchId, int batchId, string userId); //For Release

		object ReleaseVoucher(ReleasedVoucher[] model, string userId);

		string DeclineRequest(int declinedBatchId, int batchId, string userId);

		string RequestForVerification(Voucher[] vouchers, DateTime releasingDate, int batchId, string userId);

		int DeclinedVerificationRequestNotification();

		void OpenDeclinedVerificationRequestNotification();

        string UpdateReceiptDetails(int receiptId, string receiptNumber, string receiptType, string userID);

		int SaveVoucherStatusBatch(string userId, string note, string status, string batchStatus = "");

        void UpdateUncollectedVouchers();

        string ForceUncollectedVouchers(string[] voucherIds);

		void UpdateVoucherStatusBatch(int batchId, string batchStatus = "");

        List<VoucherStatusBatch> GetVoucherStatusBatchList(string[] status = null, string[] batchStatus = null, bool includeCompleted = false);

		object GetVoucherList(int batchId = 0, string[] status = null, string vendorCode = "", bool includeHoldRequest = false);

        object GetCollectedVoucherList(DateTime dateFrom, DateTime dateTo);

        List<CollectedChecks> GetCreatedOnlinePayments(DateTime dateFrom, DateTime dateTo);

		object GetVoucherIDListFromBatch(int batchId = 0, string status = "");

        string PutVoucherOnHold(string voucherId, string userId);

        string RequestVoucherOnHold(string voucherId, string userId, string note);

        string UnholdVoucher(string voucherId, string userId);

        string ReverseVoucher(string voucherId, string userId);

        string AdverseVoucher(string voucherId);

        List<Payment> GetVoucherListWithHoldRequest(
            DataFilter filter,
            string vendorcode,
            string companyName,
            string bank,
            string entity,
            string voucherId,
            decimal? amount,
            string payment,
            string check,
            DateTime? date,
            string status,
            string[] statusArr = null
            );

        string SeePreviousStatusBeforeHold(string voucherId);

        string DeclineHoldRequest(string voucherId, string userId);

        string DeclineUnholdRequest(string voucherId, string userId);

        string RequestVoucherUnhold(string voucherId, string userId, string note);

        string ForceReleaseVoucher(string[] voucherId, DateTime releaseDate, string userId);

        object GetVoucherStatusLogs(string voucherId);

        List<Aging> GetAging(DateTime asOfDate);
        
        //added seperate table for voucher statuses to enhance performance and to avoid complicated joinings
        List<Voucher> GetVoucherListByBatchId(int batchId);

        List<Voucher> GetForReleasingVoucherList(string vendorId = "", DateTime? releasingDate = null);

        void UpdateVoucherStatusForRelease(int batchId, string[] arrVoucherIdAndSeanCode);

        void UpdateCollectedVouchers(ReleasedVoucher[] model, VoucherCollector c);

        List<CollectedChecks> GetCollectedChecks(DateTime dateFrom, DateTime dateTo, string[] checkType = null);

        string UpdateCollectedCheck(int logId, string receiptNumber, string receiptType, string checkType);

        string UpdateReceiptDetailsFromExcel(IQueryable<Receipt> receipt);

        void ForceReleaseVoucher(Voucher[] voucher, DateTime releaseDate, VoucherCollector c = null);

        void CancelVoucherStatus(string voucherId);

        CollectedChecks GetCollectedCheckDetails(string voucherId);

        DateTime? GetLatestReleasingDateFromBatch(int batchId);

        void CleanVouchers();

	}
}
