﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Interfaces
{
    public interface IBackupRepository
    {
        string Restore(string filepath);
    }
}
