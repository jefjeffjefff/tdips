﻿using System.Collections.Generic;
using TDIPS.Core.Models;

namespace TDIPS.Core.Interfaces
{
	public interface IBankRepository
	{
		string AddOrUpdate(Bank[] b, int vendorID);
		object List(int vendorID);

        string AddOrUpdateByUpload(VendorBanksExcelUpload bank);

        List<Bank> GetBankListByVendorIds(List<int> vendorIdArr);
	}
}
