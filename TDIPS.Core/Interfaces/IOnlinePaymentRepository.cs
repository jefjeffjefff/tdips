﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDIPS.Core.Models;

namespace TDIPS.Core.Interfaces
{
    public interface IOnlinePaymentRepository
    {
        List<GeneralOnlinePayment> GetGeneralOnlinePaymentList(DateTime? from = null, DateTime? to = null, bool isByTransdate = true);
        void SaveGeneralOnlinePayment(GeneralOnlinePayment[] model);
        string SaveOtherOnlinePayments(OtherOnlinePaymentBatch batch, OtherOnlinePayment[] paymentsArr);
        List<OtherOnlinePaymentBatch> GetOtherOnlinePaymentBatches(DateTime dateFrom, DateTime dateTo);
        List<OtherOnlinePayment> GetOtherOnlinePayments(int batchId = 0, string accountType = "", DateTime ? dateFrom = null, DateTime? dateTo = null);
    }
}
