﻿using System;
using System.Collections.Generic;
using TDIPS.Core.Models;


namespace TDIPS.Core.Interfaces
{
    public interface IEmployeeRepository
    {
        string Add(Employee e, string userID);
        string DeleteEmployee(int employeeId);
        string OnlinePayment(EmployeeOnlinePayment op, bool isWithEmployee = true, bool willSave = true);
        List<EmployeeOnlinePayment> GetEmployeeWithOnlinePaymentsList(DateTime? from = null, DateTime? to = null);
        List<Employee> List();
        AddListResult Add(List<EmployeeExcelUpload> employee);
    }
}
