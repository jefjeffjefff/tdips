﻿using System.Collections.Generic;
using System.Linq;
using TDIPS.Core.Models;
using TDIPS.Core.Models.SS_Entities;

namespace TDIPS.Core.Interfaces
{
	public interface IVendorRepository
	{
		List<Vendor> List(DataFilter filter, Vendor v);
        List<Vendor> GetVendorListByVendorCodes(string[] vendorCodes);

        List<Vendor> VendorToEmailList();
		int AddOrUpdate(Vendor v, string userID);
        string[] AddOrUpdate(IEnumerable<D365Vendor> vendor);
        string AddOrUpdate(IList<VendorExcelUpload> vendor);
		object Delete(int vendorid, string userID);
        string MergeVendor(int refVendorId, int[] vendorIds, string mergedVendorCode, string mergedEntity, string userID);
        string CancelMerge(string mergedVendorCode, string userID, string companyName = "");
        object GetVendorListWithVoucherForRelease();
	}
}
