﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDIPS.Core.Interfaces
{
    public interface IDashboardRepository
    {
        VoucherStatus VoucherStatus();
        object ReleasedVouchers();
        object GetBatchMailDates();
        object GetProcessedMails(DateTime date);
        object GetQueueProgressAsToday();
    }
}
