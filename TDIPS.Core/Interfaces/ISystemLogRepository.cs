﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDIPS.Core.Models;

namespace TDIPS.Core.Interfaces
{
    public interface ISystemLogRepository
    {
        void Add(SystemLog sl);
        List<SystemLog> List(Nullable<DateTime> dateFrom = null, Nullable<DateTime> dateTo = null, string type = "");
    }
}
