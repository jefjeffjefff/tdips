﻿using System.Collections.Generic;
using TDIPS.Core.Models;


namespace TDIPS.Core.Interfaces
{
	public interface ICollectorRepository
	{
		string AddOrUpdate(Collector[] c, int vendorID);
		object List(int vendorID);
	}
}
